/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.repository;

import org.junit.Test;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class RepositorySettingsTest {

    @Test
    public void shouldNotifyLastUsedCarpoolChange_DifferentValue() {
        RepositorySettings settings = new RepositorySettings();
        PropertyChangeListener listener = mock(PropertyChangeListener.class);
        settings.addPropertyChangeListener(listener);

        settings.setLastUsedCarpool("Carpool Name");
        verify(listener).propertyChange(any(PropertyChangeEvent.class));
    }

    @Test
    public void shouldNotNotifyLastUsedCarpoolChange_SameValue() {
        RepositorySettings settings = new RepositorySettings();
        settings.setLastUsedCarpool("Carpool Name");

        PropertyChangeListener listener = mock(PropertyChangeListener.class);
        settings.addPropertyChangeListener(listener);

        settings.setLastUsedCarpool("Carpool Name");
        verify(listener, never()).propertyChange(any(PropertyChangeEvent.class));
    }

    @Test
    public void shouldNotifyArchiveCarpoolDataAfterMonths_DifferentValue() {
        RepositorySettings settings = new RepositorySettings();
        PropertyChangeListener listener = mock(PropertyChangeListener.class);
        settings.addPropertyChangeListener(listener);

        settings.setArchiveCarpoolDataAfterMonths(1);
        verify(listener).propertyChange(any(PropertyChangeEvent.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotNotifyArchiveCarpoolDataAfterMonths_InvalidValue() {
        RepositorySettings settings = new RepositorySettings();
        settings.setArchiveCarpoolDataAfterMonths(0);
    }

    @Test
    public void shouldNotNotifyArchiveCarpoolDataAfterMonths_SameValue() {
        RepositorySettings settings = new RepositorySettings();
        PropertyChangeListener listener = mock(PropertyChangeListener.class);
        settings.addPropertyChangeListener(listener);

        settings.setArchiveCarpoolDataAfterMonths(2);
        verify(listener, never()).propertyChange(any(PropertyChangeEvent.class));
    }
}