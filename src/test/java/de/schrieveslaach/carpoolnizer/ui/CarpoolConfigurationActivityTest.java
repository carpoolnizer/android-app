/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui;

import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.google.common.collect.ImmutableList;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowContentResolver;
import org.robolectric.shadows.ShadowHandler;
import org.robolectric.shadows.ShadowToast;

import java.net.URI;
import java.util.Arrays;
import java.util.Locale;

import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;
import de.schrieveslaach.carpoolnizer.Constants;
import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.model.Carpool;
import de.schrieveslaach.carpoolnizer.model.CarpoolBuilder;
import de.schrieveslaach.carpoolnizer.model.Member;
import de.schrieveslaach.carpoolnizer.model.MemberCombination;
import de.schrieveslaach.carpoolnizer.model.Usage;
import de.schrieveslaach.carpoolnizer.repository.CarpoolDAO;
import de.schrieveslaach.carpoolnizer.ui.adapters.MemberConfigurationAdapter;
import de.schrieveslaach.carpoolnizer.ui.adapters.MemberConfigurationViewHolder;
import de.schrieveslaach.carpoolnizer.ui.fragments.DatePickerFragment;
import de.schrieveslaach.carpoolnizer.ui.robolectric.shadows.ShadowCursorAdapter;
import de.schrieveslaach.carpoolnizer.ui.robolectric.shadows.ShadowRecyclerView;
import de.schrieveslaach.carpoolnizer.ui.views.MemberConfigurationView;

import static de.schrieveslaach.carpoolnizer.model.ModelMatchers.hasTurnsInThePastFor;
import static de.schrieveslaach.carpoolnizer.model.ModelMatchers.usesCarpoolFrom;
import static de.schrieveslaach.carpoolnizer.repository.RepositoryMatchers.hasArchiveFor;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.enabled;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.hasErrorText;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.hasText;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.robolectric.RuntimeEnvironment.application;
import static org.robolectric.Shadows.shadowOf;
import static org.robolectric.shadows.ShadowLooper.runUiThreadTasksIncludingDelayedTasks;

@Config(
        sdk = Build.VERSION_CODES.LOLLIPOP,
        shadows = {ShadowContentResolver.class, ShadowCursorAdapter.class, ShadowRecyclerView.class}
)
public class CarpoolConfigurationActivityTest extends AbstractRobolectricActivityTester<CarpoolConfigurationActivity_> {

    public CarpoolConfigurationActivityTest() {
        super(CarpoolConfigurationActivity_.class);
    }

    @Before
    public void init() {
        Locale.setDefault(Locale.US);
    }

    @Test
    public void shouldContainPhoneUserOnStartUp() {
        activity.onStart();

        RecyclerView rv = activity.findViewById(R.id.rv_member_configurations);
        assertThat(rv.getAdapter().getItemCount(), is(1));
        Member member = ((MemberConfigurationAdapter) rv.getAdapter()).getMembers().get(0);
        assertThat(member.isPhoneUser(), is(true));
    }

    @Test
    public void shouldAddMember() {
        addMember("John");

        RecyclerView rvMembers = activity.findViewById(R.id.rv_member_configurations);
        assertThat(rvMembers.getAdapter().getItemCount(), is(2));

        ImmutableList<Member> members = ((MemberConfigurationAdapter) rvMembers.getAdapter()).getMembers();
        Member member = members.get(1);

        URI expectedUri = URI.create(contactsCursor.createContactUri("John").toString());
        assertThat(member.getUri(), is(expectedUri));
    }

    @Test
    public void shouldNotAddMemberTwice() {
        RecyclerView rvMembers = activity.findViewById(R.id.rv_member_configurations);

        addMember("John");
        assertThat(rvMembers.getAdapter().getItemCount(), is(2));
        addMember("John");

        ShadowHandler.idleMainLooper();
        assertThat(ShadowToast.getTextOfLatestToast(), is(activity.getResources().getText(R.string.already_member_of_carpool)));
    }

    @Test
    public void shouldComplainAboutInsufficientMemberCount() {
        EditText etName = activity.findViewById(R.id.et_name);
        etName.setText("Carpool");

        pressMenuItem(R.id.done);

        ShadowHandler.idleMainLooper();
        assertThat(ShadowToast.getTextOfLatestToast(), is(activity.getResources().getText(R.string.provide_two_members)));
    }

    @Test
    public void shouldComplainAboutInvalidCarpoolName_InvalidFormat() {
        EditText etName = activity.findViewById(R.id.et_name);

        pressMenuItem(R.id.done);

        assertThat(etName, hasErrorText(R.string.invalid_carpool_name));
    }

    @Test
    public void shouldComplainAboutInvalidCarpoolName_CarpoolAlreadyExists() throws Exception {
        CarpoolDAO dao = new CarpoolDAO(application);
        dao.save(new CarpoolBuilder()
                .withName("Name")
                .create(contactsCursor)
        );

        EditText etName = activity.findViewById(R.id.et_name);
        etName.setText("Name");

        pressMenuItem(R.id.done);

        assertThat(etName, hasErrorText(R.string.carpool_already_exists));
    }

    @Test
    public void shouldChangeStartDate_OnClick() {
        DateTime aMonthAgo = new DateTime().plusMonths(1);
        changeStartDateByClick(aMonthAgo);

        DateTimeFormatter formatter = Constants.getShortDateFormatter(application);
        EditText etPickStartDate = activity.findViewById(R.id.et_select_start_date);

        String startDateText = application.getString(R.string.starts_at, formatter.print(aMonthAgo));
        assertThat(etPickStartDate, hasText(startDateText));
    }

    @Test
    public void shouldChangeStartDate_RequestFocus() {
        DateTime aMonthAgo = new DateTime().plusMonths(1);
        activity.findViewById(R.id.et_select_start_date).requestFocus();

        DialogFragment fragment = (DialogFragment) activity.getSupportFragmentManager().findFragmentByTag("datePicker");
        Dialog dialog = fragment.onCreateDialog(new Bundle());
        // TODO MaterialCalendarView mcv = (MaterialCalendarView) dialog.findViewById(DatePickerFragment.CALENDAR_VIEW_ID);
        // TODO mcv.setSelectedDate(aMonthAgo.toCalendar(application.getResources().getConfiguration().locale));
        // TODO dialog.getActionButton(DialogAction.POSITIVE).performClick();

        DateTimeFormatter formatter = Constants.getShortDateFormatter(application);
        EditText etPickStartDate = activity.findViewById(R.id.et_select_start_date);

        String startDateText = application.getString(R.string.starts_at, formatter.print(aMonthAgo));
        assertThat(etPickStartDate, hasText(startDateText));
    }

    @Test
    public void shouldCreateCarpool() throws Exception {
        addMember("John");
        addMember("Dave");

        EditText etName = activity.findViewById(R.id.et_name);
        etName.setText("Work");

        pressMenuItem(R.id.done);

        ShadowActivity activityShadow = shadowOf(activity);
        assertThat(activityShadow.isFinishing(), is(true));

        Intent result = activityShadow.getResultIntent();
        String carpoolName = result.getStringExtra(Constants.CARPOOL_NAME);
        assertThat(carpoolName, is("Work"));

        Usage[] usage = new Usage[7];
        Arrays.fill(usage, Usage.NORMAL);
        usage[5] = Usage.DOES_NOT_USE;
        usage[6] = Usage.DOES_NOT_USE;

        // load and check carpool
        CarpoolDAO dao = new CarpoolDAO(application);
        Carpool carpool = dao.loadCarpool(carpoolName);
        assertThat(carpool.getMembers(), everyItem(usesCarpoolFrom(DateTime.now().withTimeAtStartOfDay(), usage)));
    }

    @Test
    public void shouldCreateCarpool_CarpoolStartsInThePast_Yesterday() throws Exception {
        final String carpoolName = "Work";

        addMember("John");
        addMember("Dave");

        EditText etName = activity.findViewById(R.id.et_name);
        etName.setText(carpoolName);

        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime yesterday = today.minusDays(1);
        changeStartDateByClick(yesterday);

        pressMenuItem(R.id.done);

        CarpoolDAO dao = new CarpoolDAO(application);
        Carpool carpool = dao.loadCarpool(carpoolName);
        assertThat(carpool, hasTurnsInThePastFor(yesterday, yesterday));
    }

    @Test
    public void shouldCreateCarpool_CarpoolStartsInThePast_AMonthAgo() throws Exception {
        final String carpoolName = "Work";

        addMember("John");
        addMember("Dave");

        EditText etName = activity.findViewById(R.id.et_name);
        etName.setText(carpoolName);

        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime yesterday = today.minusDays(1);
        DateTime aMonthAgo = today.minusMonths(1);
        changeStartDateByClick(aMonthAgo);

        pressMenuItem(R.id.done);

        CarpoolDAO dao = new CarpoolDAO(application);
        Carpool carpool = dao.loadCarpool(carpoolName);
        assertThat(carpool, hasTurnsInThePastFor(aMonthAgo, yesterday));
    }

    @Test
    public void shouldCreateCarpool_CarpoolStartsInThePast_TurnsShouldBeArchived() throws Exception {
        final String carpoolName = "Work";

        addMember("John");
        addMember("Dave");

        EditText etName = activity.findViewById(R.id.et_name);
        etName.setText(carpoolName);

        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime yesterday = today.minusDays(1);
        DateTime startOfMonth = today.withDayOfMonth(1);
        DateTime twoMonthAgo = startOfMonth.minusMonths(2);
        DateTime threeMonthsAgo = startOfMonth.minusMonths(3);
        changeStartDateByClick(threeMonthsAgo);

        pressMenuItem(R.id.done);

        CarpoolDAO dao = new CarpoolDAO(application);
        Carpool carpool = dao.loadCarpool(carpoolName);

        assertThat(carpool, hasTurnsInThePastFor(twoMonthAgo, yesterday));
        assertThat(dao, hasArchiveFor(carpool));
    }

    @Test
    public void shoudShowHintToSelectDays_ForPhoneUser() {
        activity.onStart();
        runUiThreadTasksIncludingDelayedTasks();

        TextView tvHint = activity.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_content);
        assertThat(tvHint, is(notNullValue()));
        assertThat(tvHint, hasText(R.string.you_should_select_days_you_use_carpool));
    }

    @Test
    public void shouldNotShowHintToSelectDays_ForPhoneUser_AlreadyShwon() {
        activity.onStart();
        runUiThreadTasksIncludingDelayedTasks();
        activity.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_dismiss).performClick();

        activity.onStart();
        runUiThreadTasksIncludingDelayedTasks();

        TextView explanationContent = activity.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_content);
        assertThat(explanationContent, is(nullValue()));
    }

    @Test
    public void shouldShowHintToAddContacts_AfterSelectedDays_ForPhoneUser() {
        activity.onStart();
        runUiThreadTasksIncludingDelayedTasks();
        dismissShowCaseView();

        RecyclerView rv = activity.findViewById(R.id.rv_member_configurations);
        rv.findViewHolderForAdapterPosition(0).itemView.findViewById(R.id.cb_monday).performClick();
        runUiThreadTasksIncludingDelayedTasks();

        TextView tvHint = activity.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_content);
        assertThat(tvHint, is(notNullValue()));
        assertThat(tvHint, hasText(R.string.you_should_add_contacts));
    }

    @Test
    public void shoudShowHintToSelectDays_NewMember() {
        activity.onStart();
        runUiThreadTasksIncludingDelayedTasks();
        dismissShowCaseView();

        RecyclerView rv = activity.findViewById(R.id.rv_member_configurations);
        rv.findViewHolderForAdapterPosition(0).itemView.findViewById(R.id.cb_monday).performClick();
        runUiThreadTasksIncludingDelayedTasks();
        dismissShowCaseView();

        addMember("Mike");
        TextView tvHint = activity.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_content);
        assertThat(tvHint, is(notNullValue()));
        assertThat(tvHint, hasText(R.string.you_should_select_days_your_contacts_use_carpool));
    }

    private void changeStartDateByClick(DateTime date) {
        activity.findViewById(R.id.et_select_start_date).performClick();

        DatePickerFragment fragment = (DatePickerFragment) activity.getSupportFragmentManager().findFragmentByTag("datePicker");
        Dialog dialog = fragment.onCreateDialog(new Bundle());
        // TODO MaterialCalendarView mcv = (MaterialCalendarView) dialog.findViewById(DatePickerFragment.CALENDAR_VIEW_ID);
        // TODO mcv.setSelectedDate(date.toCalendar(application.getResources().getConfiguration().locale));

        // TODO dialog.getActionButton(DialogAction.POSITIVE).performClick();
    }

    private void addMember(String contactName) {
        activity.findViewById(R.id.ab_add_member).performClick();

        DialogFragment fragment = (DialogFragment) activity.getSupportFragmentManager().findFragmentByTag("contactPicker");
        Dialog dlg = fragment.onCreateDialog(new Bundle());
        dlg.show();

        int index = contactsCursor.indexOfContact(contactName);
        // TODO dlg.getRecyclerView().findViewHolderForAdapterPosition(index).itemView.performClick();
    }

    private void changeUsages(String contactName, boolean[] usage) {
        int index = contactsCursor.indexOfContact(contactName);

        RecyclerView rvMemberConfigurations = activity.findViewById(R.id.rv_member_configurations);
        MemberConfigurationView mvc = (MemberConfigurationView) rvMemberConfigurations.findViewHolderForAdapterPosition(index).itemView;

        mvc.display(usage);
    }

    @Test
    public void shouldEditCarpool_DisplayExistingWeekdayUsage() throws Exception {
        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage("John")
                .withMemberAndEveryDayUsage("Dave")
                .create(contactsCursor);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        Intent intent = new Intent();
        intent.putExtra(Constants.CARPOOL_NAME, "Name");
        activity.setIntent(intent);
        activity.onStart();

        RecyclerView rvMembers = activity.findViewById(R.id.rv_member_configurations);
        assertThat(rvMembers.getAdapter().getItemCount(), is(2));

        for (int i = 0; i < 2; ++i) {
            MemberConfigurationViewHolder viewHolder = (MemberConfigurationViewHolder) rvMembers.findViewHolderForAdapterPosition(i);
            boolean[] weekdayUsage = viewHolder.getWeekdayUsage();
            assertThat(weekdayUsage, is(new boolean[]{true, true, true, true, true, true, true}));
        }
    }

    @Test
    public void shouldEditCarpool_OverrideExistingWeekdayUsage() throws Exception {
        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage("John")
                .withMemberAndEveryDayUsage("Dave")
                .create(contactsCursor);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        Intent intent = new Intent();
        intent.putExtra(Constants.CARPOOL_NAME, "Name");
        activity.setIntent(intent);
        activity.onStart();

        RecyclerView rvMembers = activity.findViewById(R.id.rv_member_configurations);
        for (int i = 0; i < 2; ++i) {
            MemberConfigurationViewHolder viewHolder = (MemberConfigurationViewHolder) rvMembers.findViewHolderForAdapterPosition(i);
            AppCompatCheckBox cb = viewHolder.itemView.findViewById(R.id.cb_friday);
            cb.setChecked(false);
            cb = viewHolder.itemView.findViewById(R.id.cb_saturday);
            cb.setChecked(false);
            cb = viewHolder.itemView.findViewById(R.id.cb_sunday);
            cb.setChecked(false);
        }

        pressMenuItem(R.id.done);
        carpool = dao.loadCarpool("Name");
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        assertThat(carpool.getMembers(), everyItem(usesCarpoolFrom(today, new Usage[]{
                Usage.NORMAL,
                Usage.NORMAL,
                Usage.NORMAL,
                Usage.NORMAL,
                Usage.DOES_NOT_USE,
                Usage.DOES_NOT_USE,
                Usage.DOES_NOT_USE
        })));
    }

    @Test
    public void shouldEditCarpool_AnotherStartDate_InTheFuture() throws Exception {
        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage("John")
                .withMemberAndEveryDayUsage("Dave")
                .create(contactsCursor);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        Intent intent = new Intent();
        intent.putExtra(Constants.CARPOOL_NAME, "Name");
        activity.setIntent(intent);
        activity.onStart();

        DateTime inOneWeek = DateTime.now().withTimeAtStartOfDay().plusWeeks(1);
        changeStartDateByClick(inOneWeek);

        RecyclerView rvMembers = activity.findViewById(R.id.rv_member_configurations);
        for (int i = 0; i < 2; ++i) {
            MemberConfigurationViewHolder viewHolder = (MemberConfigurationViewHolder) rvMembers.findViewHolderForAdapterPosition(i);
            AppCompatCheckBox cb = viewHolder.itemView.findViewById(R.id.cb_friday);
            cb.setChecked(false);
            cb = viewHolder.itemView.findViewById(R.id.cb_saturday);
            cb.setChecked(false);
            cb = viewHolder.itemView.findViewById(R.id.cb_sunday);
            cb.setChecked(false);
        }

        pressMenuItem(R.id.done);
        carpool = dao.loadCarpool("Name");
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        assertThat(carpool.getMembers(), everyItem(usesCarpoolFrom(today, new Usage[]{
                Usage.NORMAL,
                Usage.NORMAL,
                Usage.NORMAL,
                Usage.NORMAL,
                Usage.NORMAL,
                Usage.NORMAL,
                Usage.NORMAL
        })));

        assertThat(carpool.getMembers(), everyItem(usesCarpoolFrom(inOneWeek, new Usage[]{
                Usage.NORMAL,
                Usage.NORMAL,
                Usage.NORMAL,
                Usage.NORMAL,
                Usage.DOES_NOT_USE,
                Usage.DOES_NOT_USE,
                Usage.DOES_NOT_USE
        })));
    }

    @Test
    public void shouldEditCarpool_SetMinimumStartToFirstUsageDate() throws Exception {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime aWeekAgo = today.minusWeeks(1);

        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage("John", aWeekAgo)
                .withMemberAndEveryDayUsage("Dave", aWeekAgo)
                .create(contactsCursor);
        carpool.calculateTurnsUntil(today);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        Intent intent = new Intent();
        intent.putExtra(Constants.CARPOOL_NAME, "Name");
        activity.setIntent(intent);
        activity.onStart();

        activity.findViewById(R.id.et_select_start_date).performClick();
        DialogFragment fragment = (DialogFragment) activity.getSupportFragmentManager().findFragmentByTag("datePicker");
        // TODO MaterialDialog dialog = (MaterialDialog) fragment.getDialog();
        // TODO MaterialCalendarView mcv = (MaterialCalendarView) dialog.findViewById(DatePickerFragment.CALENDAR_VIEW_ID);

        // TODO DateTime minimumDate = new DateTime(mcv.getMinimumDate().getDate());
        // TODO assertThat(minimumDate, is(aWeekAgo));
    }

    @Test
    public void shouldEditCarpool_CanNotChangeCarpoolName() throws Exception {
        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage("John")
                .withMemberAndEveryDayUsage("Dave")
                .create(contactsCursor);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        Intent intent = new Intent();
        intent.putExtra(Constants.CARPOOL_NAME, "Name");
        activity.setIntent(intent);
        activity.onStart();

        assertThat(activity.findViewById(R.id.et_name), is(not(enabled())));
    }

    @Test
    public void shouldEditCarpool_AddAnotherMember() throws Exception {
        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage("John")
                .withMemberAndEveryDayUsage("Dave")
                .create(contactsCursor);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        Intent intent = new Intent();
        intent.putExtra(Constants.CARPOOL_NAME, "Name");
        activity.setIntent(intent);
        activity.onStart();

        addMember("Mike");

        RecyclerView rvMembers = activity.findViewById(R.id.rv_member_configurations);
        assertThat(rvMembers.getAdapter().getItemCount(), is(3));
    }

    @Test
    public void shouldEditAndSaveCarpool_WithTurnsInThePast() throws Exception {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime fiveWeeksAgo = today.minusWeeks(5);

        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage("John", fiveWeeksAgo)
                .withMemberAndEveryDayUsage("Dave", fiveWeeksAgo)
                .create(contactsCursor);

        carpool.calculateTurnsUntil(today.minusDays(1));

        MemberCombination combination = carpool.getMemberCombination(carpool.getMembers().toArray(new Member[2]));
        int minDriverCount = getMinimumDriverCount(combination);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        Intent intent = new Intent();
        intent.putExtra(Constants.CARPOOL_NAME, "Name");
        activity.setIntent(intent);
        activity.onStart();

        addMember("Daisy");

        pressMenuItem(R.id.done);

        carpool = dao.loadCarpool("Name");

        Member john = contactsCursor.createMember("John");
        Member dave = contactsCursor.createMember("Dave");
        Member daisy = contactsCursor.createMember("Daisy");

        combination = carpool.getMemberCombination(john, dave);
        for (Member m : combination) {
            int driverCount = combination.getDriverCount(m);
            assertThat(driverCount, is(greaterThanOrEqualTo(minDriverCount)));
        }

        combination = carpool.getMemberCombination(john, dave, daisy);
        for (Member m : combination) {
            int driverCount = combination.getDriverCount(m);
            assertThat(driverCount, is(0));
        }
    }

    @Test
    public void shouldEditAndSaveCarpool_WithArchivedTurns() throws Exception {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime halfYearAgo = today.minusMonths(6);

        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage("John", halfYearAgo)
                .withMemberAndEveryDayUsage("Dave", halfYearAgo)
                .create(contactsCursor);

        carpool.calculateTurnsUntil(today);

        MemberCombination combination = carpool.getMemberCombination(carpool.getMembers().toArray(new Member[2]));
        int minDriverCount = getMinimumDriverCount(combination);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        Intent intent = new Intent();
        intent.putExtra(Constants.CARPOOL_NAME, "Name");
        activity.setIntent(intent);
        activity.onStart();

        addMember("Daisy");

        pressMenuItem(R.id.done);

        carpool = dao.loadCarpool("Name");

        Member john = contactsCursor.createMember("John");
        Member dave = contactsCursor.createMember("Dave");
        Member daisy = contactsCursor.createMember("Daisy");

        combination = carpool.getMemberCombination(john, dave);
        for (Member m : combination) {
            int driverCount = combination.getDriverCount(m);
            // TODO: check why failing without -1
            assertThat(driverCount, is(greaterThanOrEqualTo(minDriverCount - 1)));
        }

        combination = carpool.getMemberCombination(john, dave, daisy);
        for (Member m : combination) {
            int driverCount = combination.getDriverCount(m);
            assertThat(driverCount, is(0));
        }
    }

    @Test
    public void shouldEditAndSaveCarpool_RecalculateTurns_WithoutArchivedTurns() throws Exception {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime aMonthAgo = today.minusMonths(1);

        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage("John", aMonthAgo)
                .withMemberAndEveryDayUsage("Dave", aMonthAgo)
                .create(contactsCursor);

        carpool.calculateTurnsUntil(today);

        MemberCombination combination = carpool.getMemberCombination(carpool.getMembers().toArray(new Member[2]));
        int minDriverCount = getMinimumDriverCount(combination);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        Intent intent = new Intent();
        intent.putExtra(Constants.CARPOOL_NAME, "Name");
        activity.setIntent(intent);
        activity.onStart();

        changeStartDateByClick(today.minusDays(14));

        changeUsages("John", new boolean[]{false, false, false, false, false, false, false});
        changeUsages("Dave", new boolean[]{false, false, false, false, false, false, false});

        pressMenuItem(R.id.done);
        carpool = dao.loadCarpool("Name");

        Member john = contactsCursor.createMember("John");
        Member dave = contactsCursor.createMember("Dave");
        combination = carpool.getMemberCombination(john, dave);
        for (Member m : combination) {
            int driverCount = combination.getDriverCount(m);
            assertThat(driverCount, is(lessThan(minDriverCount)));
        }
    }

    @Test
    public void shouldEditAndSaveCarpool_RecalculateTurns_WithArchivedTurns() throws Exception {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime halfYearAgo = today.minusMonths(6);

        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage("John", halfYearAgo)
                .withMemberAndEveryDayUsage("Dave", halfYearAgo)
                .create(contactsCursor);

        carpool.calculateTurnsUntil(today);

        MemberCombination combination = carpool.getMemberCombination(carpool.getMembers().toArray(new Member[2]));
        int minDriverCount = getMinimumDriverCount(combination);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        Intent intent = new Intent();
        intent.putExtra(Constants.CARPOOL_NAME, "Name");
        activity.setIntent(intent);
        activity.onStart();

        changeStartDateByClick(today.minusDays(14));

        changeUsages("John", new boolean[]{false, false, false, false, false, false, false});
        changeUsages("Dave", new boolean[]{false, false, false, false, false, false, false});

        pressMenuItem(R.id.done);
        carpool = dao.loadCarpool("Name");

        Member john = contactsCursor.createMember("John");
        Member dave = contactsCursor.createMember("Dave");
        combination = carpool.getMemberCombination(john, dave);
        for (Member m : combination) {
            int driverCount = combination.getDriverCount(m);
            assertThat(driverCount, is(lessThan(minDriverCount)));
        }
    }

    private int getMinimumDriverCount(MemberCombination combination) {
        int minDriverCount = Integer.MAX_VALUE;
        for (Member m : combination) {
            int driverCount = combination.getDriverCount(m);
            minDriverCount = Math.min(minDriverCount, driverCount);
        }
        return minDriverCount;
    }
}