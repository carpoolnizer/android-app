/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.fragments;

import android.content.DialogInterface;
import android.os.Build;

import org.junit.Before;
import org.junit.Test;
import org.robolectric.annotation.Config;

import androidx.appcompat.app.AlertDialog;

import static de.schrieveslaach.carpoolnizer.ui.fragments.ConfirmFragment.OnConfirmListener;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
public class ConfirmFragmentTest extends BaseDialogFragmentTest<ConfirmFragment, AlertDialog> {

    private OnConfirmListener confirmListener;

    private ConfirmFragment fragment;

    public ConfirmFragmentTest() {
        super(ConfirmFragment.class);
    }

    @Before
    public void createFragment() throws Exception {
        confirmListener = mock(OnConfirmListener.class);
        fragment = openFragment(new FragmentInitializer<ConfirmFragment>() {
            @Override
            public void init(ConfirmFragment fragment) {
                fragment.setOnConfirmListener(confirmListener);
                fragment.setConfirmationMessage(android.R.string.cancel);
                fragment.setConfirmationTitle(android.R.string.cancel);
            }

            @Override
            public void beforeStart(ConfirmFragment fragment) {

            }
        });
    }

    @Test
    public void shouldNotifyAboutConfirmation() {
        AlertDialog dialog = openDialog(fragment);
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).performClick();
        verify(confirmListener).onFinish();
    }

    @Test
    public void shouldNotNotifyAboutConfirmation() {
        AlertDialog dialog = openDialog(fragment);
        dialog.getButton(DialogInterface.BUTTON_NEGATIVE).performClick();
        verify(confirmListener, never()).onFinish();
    }
}