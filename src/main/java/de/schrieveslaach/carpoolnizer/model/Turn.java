/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.joda.time.DateTime;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides information for the usage of a {@link Carpool} on a specific day.
 */
public class Turn implements Comparable<Turn>, Serializable {

    private Carpool carpool;

    @JsonProperty("driver")
    private final Member driver;

    /**
     * The date of the {@code Turn}.
     */
    @JsonSerialize(using = DateSerializer.class)
    @JsonProperty("date")
    private final DateTime date;

    @JsonProperty("combination")
    private final MemberCombination combination;

    /**
     * Creates an empty {@code Turn} (no one uses the carpool on that {@link DateTime date}).
     *
     * @param date
     */
    Turn(DateTime date) {
        this(date, null);
    }

    /**
     * Creates a new {@code Turn} with the given {@link Member driver}. The {@link Member driver}
     * has no passengers.
     *
     * @param date
     * @param driver
     */
    Turn(DateTime date, Member driver) {
        this(date, driver, null);
    }

    /**
     * Creates a new {@code Turn} with the given {@link Member driver}. The given {@link MemberCombination combination}
     * contains the passengers.
     *
     * @param date
     * @param driver
     * @param combination
     */
    @JsonCreator
    Turn(
            @JsonDeserialize(using = DateDeserializer.class) @JsonProperty("date") DateTime date,
            @JsonProperty("driver") Member driver,
            @JsonProperty("combination") MemberCombination combination) {
        if (date == null) {
            throw new IllegalArgumentException("date can not be null");
        }

        if (combination != null && driver == null) {
            throw new IllegalArgumentException("driver can be null if a combination is provided.");
        }

        if (combination != null && !combination.contains(driver)) {
            throw new IllegalArgumentException("driver must be contained in the combination");
        }

        this.driver = driver;
        this.date = date;
        this.combination = combination;
    }

    /**
     * Returns the driver of the turn.
     *
     * @return
     */
    public Member getDriver() {
        return driver;
    }

    /**
     * Returns the {@link DateTime date} when the turn happened.
     *
     * @return
     */
    public DateTime getDate() {
        return date;
    }

    /**
     * Returns the {@link MemberCombination combination}.
     *
     * @return
     */
    @JsonIgnore
    MemberCombination getCombination() {
        return combination;
    }

    /**
     * Sets the carpool which owns the {@code Turn}.
     *
     * @param carpool
     */
    void setCarpool(Carpool carpool) {
        this.carpool = carpool;
    }

    /**
     * Returns the carpool which owns the {@code Turn}.
     *
     * @return
     */
    @JsonIgnore
    public Carpool getCarpool() {
        return carpool;
    }

    /**
     * Checks if the {@code Turn} is canceled.
     *
     * @return
     * @see Carpool#isCanceled(Turn)
     */
    @JsonIgnore
    public boolean isCanceled() {
        return carpool.isCanceled(this);
    }

    void revert() {
        if (combination == null) {
            throw new IllegalStateException("This method can only be called when at least one passenger is defined.");
        }

        combination.revert(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(date)
                .append(driver)
                .append(combination)
                .build();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        if (obj.getClass() != getClass()) {
            return false;
        }

        Turn rhs = (Turn) obj;
        return new EqualsBuilder()
                .append(date, rhs.date)
                .append(driver, rhs.driver)
                .append(combination, rhs.combination)
                .isEquals();
    }

    @Override
    public int compareTo(Turn turn) {
        return date.compareTo(turn.date);
    }

    /**
     * Returns all members of the {@code Turn}.
     *
     * @return
     */
    @JsonIgnore
    public List<Member> getMembers() {
        if (combination != null) {
            return Lists.newArrayList(combination);
        }

        List<Member> members = new ArrayList<>();
        if (driver != null) {
            members.add(driver);
        }
        return members;
    }

    /**
     * Returns the remaining members who do not use the {@link #getCarpool() carpool} that
     * {@link #getDate() day}.
     *
     * @return
     */
    @JsonIgnore
    public List<Member> getRemainingMembers() {
        return Lists.newArrayList(Sets.difference(carpool.getMembers(), Sets.newHashSet(getMembers())));
    }

    /**
     * Returns all passengers of the {@code Turn}.
     *
     * @return
     */
    @JsonIgnore
    public List<Member> getPassengers() {
        List<Member> passengers = new ArrayList<>();

        if (combination != null) {
            for (Member m : combination) {
                if (m.equals(driver)) {
                    continue;
                }
                passengers.add(m);
            }
        }

        return passengers;
    }

    /**
     * Returns the driver count for the given member.
     *
     * @param member
     * @return
     */
    public int getDriverCount(Member member) {
        if (combination == null) {
            throw new IllegalStateException("This method can only be called when at least one passenger is defined.");
        }
        return combination.getDriverCount(member);
    }

    /**
     * Checks if the given {@link Member} is part of the current {@code Turn}.
     *
     * @param member
     * @return
     */
    public boolean contains(Member member) {
        if (combination != null) {
            return combination.contains(member);
        }
        if (driver != null) {
            return driver.equals(member);
        }
        return false;
    }

    /**
     * Helper class to deserialize {@link Turn#date}.
     */
    private static class DateDeserializer extends JsonDeserializer<DateTime> {

        @Override
        public DateTime deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
            return Constants.DATE_TIME_FORMATTER.parseDateTime(p.getText());
        }
    }

    /**
     * Helper class to serialize {@link Turn#date}.
     */
    private static class DateSerializer extends JsonSerializer<DateTime> {

        @Override
        public void serialize(DateTime value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            gen.writeObject(Constants.DATE_TIME_FORMATTER.print(value));
        }
    }
}
