/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.test;

import androidx.annotation.IdRes;
import androidx.test.espresso.NoMatchingViewException;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.ViewAssertion;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import org.joda.time.DateTime;

import java.util.regex.Pattern;

import javax.annotation.Nullable;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.model.Member;
import de.schrieveslaach.carpoolnizer.ui.TurnDetailsActivity_;
import de.schrieveslaach.carpoolnizer.ui.views.MemberReadOnlyView;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.action.ViewActions.swipeRight;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.Visibility.GONE;
import static androidx.test.espresso.matcher.ViewMatchers.Visibility.VISIBLE;
import static androidx.test.espresso.matcher.ViewMatchers.hasSibling;
import static androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static androidx.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isEnabled;
import static androidx.test.espresso.matcher.ViewMatchers.withChild;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static de.schrieveslaach.carpoolnizer.test.ViewMatchers.withContactName;
import static de.schrieveslaach.carpoolnizer.Constants.getFullDateFormatter;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class TurnDetailsPage extends AbstractPage<TurnDetailsActivity_> {

    /**
     * Last selected driver
     */
    private Member currentDriver;

    public TurnDetailsPage() {
        super(TurnDetailsActivity_.class);
    }

    @When("^I change usage of (.*?) to (.*?)$")
    public void I_change_usage_of_member_to_exceptional_usage(final String contactName, final String exceptionalUsage) throws Throwable {
        I_clicked_on_contact(contactName);

        String exceptionalUsageText;
        if ("does not use".equals(exceptionalUsage)) {
            exceptionalUsageText = "Does not use carpool";
        } else if ("cannot drive".equals(exceptionalUsage)) {
            exceptionalUsageText = "Can not drive";
        } else if ("has to drive".equals(exceptionalUsage)) {
            exceptionalUsageText = "Has to drive";
        } else if ("normal".equals(exceptionalUsage)) {
            exceptionalUsageText = "Use carpool normally";
        } else {
            throw new IllegalArgumentException(exceptionalUsage);
        }

        onView(allOf(
                withText(exceptionalUsageText),
                isDisplayed()
        )).perform(click());
    }

    @When("^I change exceptional usage of the current driver to cannot drive$")
    public void I_change_usage_of_the_current_driver_to_cannot_drive() {
        MemberReadOnlyView mrovDriver = (MemberReadOnlyView) getCurrentActivity().findViewById(R.id.mv_driver);
        currentDriver = mrovDriver.getMember();

        onView(allOf(
                withId(R.id.mv_driver),
                isDisplayed()
        )).perform(click());

        String exceptionalUsageText = "Can not drive";
        onView(allOf(
                withText(exceptionalUsageText),
                isDisplayed()
        )).perform(click());
    }

    @When("^I clicked on the contact image of the contact (.+?)$")
    public void I_clicked_on_the_contact_image_of_the_contact(final String contactName) throws Exception {
        startContactInformationMonitor();

        onView(allOf(
                withId(R.id.iv_contact_image),
                hasSibling(allOf(
                        isAssignableFrom(ViewGroup.class),
                        withChild(withText(contactName))
                )),
                isDisplayed()
        )).perform(click());
    }

    @When("^I return to the main page$")
    public void I_return_to_the_main_page() {
        onView(allOf(
                isAssignableFrom(ImageButton.class),
                withParent(withClassName(endsWith("Toolbar")))
        )).perform(click());
    }

    @When("^I swipe to the (right|left) (\\d+) times$")
    public void I_swipe_to_the_direction_x_times(final String direction, final int times) {
        ViewAction swipeAction;
        if ("right".equals(direction)) {
            swipeAction = swipeRight();
        } else if ("left".equals(direction)) {
            swipeAction = swipeLeft();
        } else {
            throw new IllegalArgumentException(direction);
        }

        for (int i = 0; i < times; ++i) {
            onView(withId(R.id.vp_turns)).perform(swipeAction);
        }
    }

    @When("^I swipe to the left to next turn with driver$")
    public void I_swipe_to_the_left_to_next_turn_with_driver() {
        View view;
        do {
            onView(withId(R.id.vp_turns)).perform(swipeLeft());
            view = getCurrentActivity().getCurrentFragment().getView().findViewById(R.id.mv_driver);
        } while (view.getVisibility() == View.GONE);
    }

    @When("^I clicked on contact (.+)$")
    public void I_clicked_on_contact(String contactName) {
        onView(allOf(
                withId(R.id.tv_member_name),
                withText(contactName),
                isDisplayed()
        )).perform(click());
    }

    @Then("^I should see an explanation about '(.+)'$")
    public void I_should_see_an_explanation_about(String usage) {
        int explanationStringId;

        if ("Does not use".equals(usage)) {
            explanationStringId = R.string.explanation_does_not_use;
        } else if ("Can not drive".equals(usage)) {
            explanationStringId = R.string.explanation_can_not_drive;
        } else if ("Has to drive".equals(usage)) {
            explanationStringId = R.string.explanation_has_to_drive;
        } else if ("Uses carpool normally".equals(usage)) {
            explanationStringId = R.string.explanation_normal_usage;
        } else {
            throw new IllegalArgumentException(usage);
        }

        onView(
                withId(uk.co.deanwild.materialshowcaseview.R.id.tv_content)
        ).check(matches(withText(explanationStringId)));
        onView(
                withId(uk.co.deanwild.materialshowcaseview.R.id.tv_dismiss)
        ).perform(click());
    }

    @Then("^I should see an explanation about turn details$")
    public void I_should_see_an_explanation_about_turn_details() {
        onView(
                withId(uk.co.deanwild.materialshowcaseview.R.id.tv_content)
        ).check(matches(withText(R.string.explanation_turn_details)));
        onView(
                withId(uk.co.deanwild.materialshowcaseview.R.id.tv_content)
        ).perform(click());
    }

    @Then("^I should see a big pillow in the turn details$")
    public void i_should_see_a_big_pillow_in_the_turn_details() throws Throwable {
        onView(allOf(
                withId(R.id.iv_canceled_turn),
                isDisplayed()
        )).check(matches(withEffectiveVisibility(VISIBLE)));
    }

    @Then("^I should see no driver, no passengers and no remaining members$")
    public void I_should_see_no_driver_no_passengers_and_no_remaining_members() throws Throwable {
        onView(allOf(
                withId(R.id.tv_driver),
                ViewMatchers.<View>isFirst()
        )).check(matches(withEffectiveVisibility(GONE)));
        onView(allOf(
                withId(R.id.tv_passengers),
                ViewMatchers.<View>isFirst()
        )).check(matches(withEffectiveVisibility(GONE)));
        onView(allOf(
                withId(R.id.tv_remaining_members),
                ViewMatchers.<View>isFirst()
        )).check(matches(withEffectiveVisibility(GONE)));
    }

    @Then("^I should see contact as driver which was the current driver$")
    public void I_should_see_contact_not_as_driver_which_was_the_current_driver() {
        onView(allOf(
                withId(R.id.mv_driver),
                isDisplayed()
        )).check(hasDriver(currentDriver));
    }

    @Then("^I should see turn in (-?\\d+) days$")
    public void I_should_see_turn_in_x_days(final int days) {
        DateTime date = DateTime.now().withTimeAtStartOfDay().plusDays(days);
        String dateString = getFullDateFormatter(getInstrumentation().getContext()).print(date);

        onView(allOf(
                withText(dateString),
                isDisplayed()
        )).check(matches(withEffectiveVisibility(VISIBLE)));
    }

    @Then("^I should see contact (.+?) as driver$")
    public void I_should_see_contact_as_driver(final String driverName) {
        if (driverName.equals("-")) {
            onView(allOf(
                    withId(R.id.mv_driver),
                    withParent(withParent(isDisplayed()))
            )).check(matches(withEffectiveVisibility(GONE)));
            return;
        }

        onView(allOf(
                withId(R.id.tv_member_name),
                withContactName(driverName),
                isDescendantOfA(withId(R.id.mv_driver)),
                isDisplayed()
        )).check(matches(withEffectiveVisibility(VISIBLE)));
    }

    @Then("^I should see passengers\\s?(.+?)$")
    public void I_should_see_passengers(final String passengerNames) {
        checkThatMembersAreShowUpInLinearLayout(passengerNames, R.id.ll_passengers);
    }

    @Then("^I should see remaining members\\s?(.+?)$")
    public void I_should_see_remaining_members(final String remainingMemberNames) {
        checkThatMembersAreShowUpInLinearLayout(remainingMemberNames, R.id.ll_remaining_members);
    }

    @Then("^I should not be able to change usage of (.+?) to (.+?)$")
    public void i_should_not_be_able_to_change_usage_of_member_to_has_to_drive(final String member, final String exceptionalUsage) throws Throwable {
        onView(allOf(
                withId(R.id.tv_member_name),
                withText(member),
                isDisplayed()
        )).perform(click());

        String exceptionalUsageText;
        if ("does not use".equals(exceptionalUsage)) {
            exceptionalUsageText = "Does not use carpool";
        } else if ("cannot drive".equals(exceptionalUsage)) {
            exceptionalUsageText = "Can not drive";
        } else if ("has to drive".equals(exceptionalUsage)) {
            exceptionalUsageText = "Has to drive";
        } else if ("normal".equals(exceptionalUsage)) {
            exceptionalUsageText = "Use carpool normally";
        } else {
            throw new IllegalArgumentException(exceptionalUsage);
        }

        onView(allOf(
                withText(exceptionalUsageText),
                isDisplayed()
        )).check(matches(not(isEnabled())));
    }

    private void checkThatMembersAreShowUpInLinearLayout(final String memberNames, @IdRes final int linearLayoutId) {
        if (memberNames.trim().equals("")) {
            return;
        }

        if (memberNames.trim().equals("") || Pattern.compile("\\d+").matcher(memberNames).matches()) {
            int memberCount = 0;
            if (!memberNames.trim().equals("")) {
                memberCount = Integer.parseInt(memberNames);
            }

            LinearLayout ll = (LinearLayout) getCurrentActivity().findViewById(linearLayoutId);
            assertThat(ll.getChildCount(), is(memberCount));
            return;
        }

        String[] members = memberNames.trim().split(",\\s*");
        for (String member : members) {

            onView(allOf(
                    withId(R.id.tv_member_name),
                    withContactName(member),
                    isDescendantOfA(withId(linearLayoutId)),
                    isDisplayed()
            )).check(matches(withEffectiveVisibility(VISIBLE)));
        }
    }

    private ViewAssertion hasDriver(final Member member) {
        return new ViewAssertion() {

            @Override
            public void check(@Nullable View view, @Nullable NoMatchingViewException e) {
                MemberReadOnlyView mrov = (MemberReadOnlyView) view;
                Member currentMember = mrov.getMember();
                if (!currentMember.equals(member)) {
                    throw new AssertionError("current member does not match given member");
                }
            }
        };
    }
}
