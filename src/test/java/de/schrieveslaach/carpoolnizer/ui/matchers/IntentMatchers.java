/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.matchers;

import android.content.Intent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.util.Objects;

public class IntentMatchers {

    private IntentMatchers() {

    }

    public static Matcher<Intent> hasAction(final String action) {
        return new TypeSafeMatcher<Intent>() {
            @Override
            protected boolean matchesSafely(Intent item) {
                return Objects.equals(item.getAction(), action);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("has action ").appendValue(action);
            }

            @Override
            protected void describeMismatchSafely(Intent item, Description mismatchDescription) {
                mismatchDescription.appendText("has action ").appendValue(item.getAction());
            }
        };
    }

    public static Matcher<Intent> hasStringExtra(final String key, final String value) {
        return new TypeSafeMatcher<Intent>() {
            @Override
            protected boolean matchesSafely(Intent item) {
                return item.hasExtra(key) && Objects.equals(item.getStringExtra(key), value);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("has string extra with key ")
                        .appendValue(key)
                        .appendText(" and value ")
                        .appendValue(value);
            }

            @Override
            protected void describeMismatchSafely(Intent item, Description mismatchDescription) {
                if (item.hasExtra(key)) {
                    mismatchDescription.appendText("has string extra with key ")
                            .appendValue(key)
                            .appendText(" and value ")
                            .appendValue(item.getStringExtra(key));
                } else {
                    mismatchDescription.appendText("has no string extra with key ")
                            .appendValue(key);
                }
            }
        };
    }

}
