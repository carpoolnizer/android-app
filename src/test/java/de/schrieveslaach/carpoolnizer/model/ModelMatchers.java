/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.model;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.sameInstance;


/**
 * {@link Matcher} for all model classes.
 */
public final class ModelMatchers {

    public static Matcher<Member> usesCarpoolFrom(final DateTime date, final Usage[] weekdayUsage) {
        return new TypeSafeMatcher<Member>() {
            @Override
            public boolean matchesSafely(Member m) {
                return Arrays.equals(getUsage(m), weekdayUsage);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("member should use carpool from ")
                        .appendValue(date)
                        .appendText(" on (Mon, Tue, ...): ");

                for (Usage u : weekdayUsage) {
                    description.appendValue(u).appendText(", ");
                }
            }

            @Override
            public void describeMismatchSafely(Member m, Description mismatchDescription) {
                mismatchDescription.appendText("member ")
                        .appendValue(m)
                        .appendText(" uses carpool on: ");

                for (Usage u : getUsage(m)) {
                    mismatchDescription.appendValue(u).appendText(", ");
                }
            }

            private Usage[] getUsage(Member m) {
                Usage[] usage = new Usage[7];

                DateTime d = date;
                for (int i = 0; i < 7; ++i) {
                    usage[d.dayOfWeek().get() - 1] = m.getUsageAt(d);
                    d = d.plusDays(1);
                }

                return usage;
            }
        };
    }

    public static Matcher<Turn> withDriverCount(final Member driver, final int driverCount) {
        return new TypeSafeMatcher<Turn>() {
            @Override
            protected boolean matchesSafely(Turn item) {
                return item.getDriverCount(driver) == driverCount;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("driver count for ")
                        .appendValue(driver)
                        .appendText(" == ")
                        .appendValue(driverCount);
            }

            @Override
            protected void describeMismatchSafely(Turn item, Description mismatchDescription) {
                mismatchDescription.appendValue(item.getDriverCount(driver));
            }
        };
    }

    public static Matcher<Turn> hasCarpoolReference(final Carpool carpool) {
        return new TypeSafeMatcher<Turn>() {
            @Override
            protected boolean matchesSafely(Turn turn) {
                return sameInstance(turn.getCarpool()).matches(carpool);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("has reference to ").appendValue(carpool);
            }

            @Override
            protected void describeMismatchSafely(Turn turn, Description mismatchDescription) {
                mismatchDescription.appendText("has reference to ").appendValue(turn.getCarpool());
            }
        };
    }

    public static Matcher<MemberCombination> hasDriverCount(final Member driver, final int driverCount) {
        return new TypeSafeMatcher<MemberCombination>() {
            @Override
            public boolean matchesSafely(MemberCombination mc) {
                return mc.getDriverCount(driver) == driverCount;
            }

            @Override
            public void describeTo(Description description) {
                description.appendValue(driver)
                        .appendText(" should drive ")
                        .appendValue(driverCount);
            }

            @Override
            public void describeMismatchSafely(MemberCombination mc, Description mismatchDescription) {
                mismatchDescription.appendText(" drove ")
                        .appendValue(mc.getDriverCount(driver));
            }
        };
    }

    public static Matcher<MemberCombination> hasMembers(final Member... members) {
        return new TypeSafeMatcher<MemberCombination>() {

            @Override
            public boolean matchesSafely(MemberCombination mc) {
                return mc.containsAll(members);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(" should contain ");
                for (Member m : members) {
                    description.appendValue(m);
                    description.appendText(", ");
                }
            }

            @Override
            public void describeMismatchSafely(MemberCombination mc, Description mismatchDescription) {
                mismatchDescription.appendText(" should contain ");
                for (Member m : mc) {
                    mismatchDescription.appendValue(m);
                    mismatchDescription.appendText(", ");
                }
            }
        };
    }

    public static Matcher<Carpool> containsCombination(final Member... members) {
        return new TypeSafeMatcher<Carpool>() {

            @Override
            public boolean matchesSafely(Carpool carpool) {
                MemberCombination mc = carpool.getMemberCombination(members);
                return mc != null && mc.size() == members.length;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(" should contain combination with ");
                for (Member m : members) {
                    description.appendValue(m);
                    description.appendText(", ");
                }
            }

            @Override
            public void describeMismatchSafely(Carpool carpool, Description mismatchDescription) {
                MemberCombination mc = carpool.getMemberCombination(members);

                if (mc == null) {
                    mismatchDescription.appendValue(null);
                } else {
                    for (Member m : mc) {
                        mismatchDescription.appendValue(m);
                        mismatchDescription.appendText(", ");
                    }
                }
            }
        };
    }

    public static Matcher<List<Turn>> hasDateTimes(final DateTime... dateTimes) {
        return new TypeSafeMatcher<List<Turn>>() {
            @Override
            public boolean matchesSafely(List<Turn> turns) {
                if (turns.size() != dateTimes.length) {
                    return false;
                }

                for (int i = 0; i < dateTimes.length; ++i) {
                    Turn turn = turns.get(i);
                    DateTime dateTime = dateTimes[i];

                    if (!Objects.equal(dateTime, turn.getDate())) {
                        return false;
                    }
                }

                return true;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("should have dates: ");
                for (DateTime dateTime : dateTimes) {
                    description.appendValue(dateTime);
                    description.appendText(", ");
                }
            }

            @Override
            public void describeMismatchSafely(List<Turn> turns, Description mismatchDescription) {
                if (turns.isEmpty()) {
                    mismatchDescription.appendText("no dates");
                    return;
                }

                for (Turn turn : turns) {
                    mismatchDescription.appendValue(turn.getDate());
                    mismatchDescription.appendText(", ");
                }
            }
        };
    }

    public static Matcher<CarpoolArchive> containsMembers(final Member... members) {
        return new TypeSafeMatcher<CarpoolArchive>() {
            @Override
            protected boolean matchesSafely(CarpoolArchive archive) {
                Set<Member> memberSet = new HashSet<Member>();
                for (MemberCombination c : archive.getCombinations().values()) {
                    for (Member m : c) {
                        memberSet.add(m);
                    }
                }

                ImmutableList.Builder<Member> builder = ImmutableList.<Member>builder();
                builder.addAll(memberSet);
                return containsInAnyOrder(members).matches(builder.build());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(" contains the members: ");
                for (Member m : members) {
                    description.appendValue(m);
                }
            }
        };
    }

    public static Matcher<List<Turn>> hasDrivers(Member driver, Member... drivers) {
        final List<Member> allDrivers = new ArrayList<Member>();
        allDrivers.add(driver);
        allDrivers.addAll(Lists.newArrayList(drivers));

        return new TypeSafeMatcher<List<Turn>>() {
            @Override
            public boolean matchesSafely(List<Turn> turns) {
                if (turns.size() != allDrivers.size()) {
                    return false;
                }

                for (int i = 0; i < allDrivers.size(); ++i) {
                    Turn turn = turns.get(i);
                    Member driver = allDrivers.get(i);

                    if (!Objects.equal(driver, turn.getDriver())) {
                        return false;
                    }
                }

                return true;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("should have drivers: ");
                for (Member driver : allDrivers) {
                    if (driver == null) {
                        description.appendText("no driver");
                    } else {
                        description.appendValue(driver);
                    }
                    description.appendText(", ");
                }
            }

            @Override
            public void describeMismatchSafely(List<Turn> turns, Description mismatchDescription) {
                if (turns.isEmpty()) {
                    mismatchDescription.appendText("no drivers");
                    return;
                }

                for (Turn turn : turns) {
                    Member driver = turn.getDriver();
                    if (driver == null) {
                        mismatchDescription.appendText("no driver");
                    } else {
                        mismatchDescription.appendValue(driver);
                    }

                    mismatchDescription.appendText(", ");
                }
            }
        };
    }

    public static Matcher<Carpool> hasTurnsInThePastFor(final DateTime start, final DateTime end) {
        return new TypeSafeMatcher<Carpool>() {
            @Override
            protected boolean matchesSafely(Carpool carpool) {
                Iterator<Turn> it = carpool.getTurnsInThePast().iterator();
                if (!it.hasNext()) {
                    return false;
                }

                Turn turn = it.next();
                if (!turn.getDate().equals(start)) {
                    return false;
                }


                for (DateTime d = start.plusDays(1); d.isBefore(end) || d.isEqual(end); d = d.plusDays(1)) {
                    if (!it.hasNext()) {
                        return false;
                    }

                    turn = it.next();
                    if (!turn.getDate().equals(d)) {
                        return false;
                    }
                }
                return !it.hasNext();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("carpool should contain turns from ")
                        .appendValue(start)
                        .appendText(" until ")
                        .appendValue(end);
            }

            @Override
            protected void describeMismatchSafely(Carpool carpool, Description mismatchDescription) {
                Interval i = new Interval(start, end.withTimeAtStartOfDay().plusDays(1));
                mismatchDescription.appendText("carpool has turns which not match: ");
                for (Turn turn : carpool.getTurnsInThePast()) {
                    if (i.contains(turn.getDate())) {
                        continue;
                    }

                    mismatchDescription.appendValue(turn.getDate()).appendText(", ");
                }
            }
        };
    }
}
