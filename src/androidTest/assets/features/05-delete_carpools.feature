Feature: Deleting carpools
  This feature allows to delete carpools in case the user does not use the 
  carpool anymore.
  
  Scenario Outline: Delete currently opened carpool by menu button.
    Given I started the app
    And I opened the carpool <carpool>
    When I click on the delete carpool menu button
    Then I should see a confirmation dialog with message <confirmation_message>
    When I confirm with OK
    Then the carpool <carpool> should not be available anymore

  Examples:
    | carpool       | confirmation_message                              |
    | Mike and John | Do you really want to delete the current carpool? |

