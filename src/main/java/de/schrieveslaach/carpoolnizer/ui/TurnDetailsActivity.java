/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui;

import android.content.Intent;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.common.collect.Lists;
import com.rey.material.widget.TabPageIndicator;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

import de.schrieveslaach.carpoolnizer.Constants;
import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.model.Carpool;
import de.schrieveslaach.carpoolnizer.model.Member;
import de.schrieveslaach.carpoolnizer.model.Turn;
import de.schrieveslaach.carpoolnizer.model.Usage;
import de.schrieveslaach.carpoolnizer.repository.CarpoolDAO;
import de.schrieveslaach.carpoolnizer.repository.CarpoolDAOException;
import de.schrieveslaach.carpoolnizer.ui.fragments.TurnDetailsFragment;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.shape.CircleShape;

import static de.schrieveslaach.carpoolnizer.Constants.getFullDateFormatter;
import static de.schrieveslaach.carpoolnizer.Constants.getPredictionDate;

/**
 * Displays details of a specific {@link de.schrieveslaach.carpoolnizer.model.Turn turn}.
 */
@EActivity(R.layout.activity_turn_details)
public class TurnDetailsActivity extends AppCompatActivity {

    @ViewById(R.id.vp_turns)
    ViewPager vpTurns;

    @ViewById(R.id.tpi_days)
    TabPageIndicator tpiDays;

    private TurnPagerAdapter paTurns;

    private final CarpoolDAO dao = new CarpoolDAO(this);

    private Carpool carpool;

    /**
     * The {@link Turn#getDate() date of a turn} in the {@link TurnDetailsActivity#carpool} which
     * is not a archived {@link Turn turn}.
     */
    private DateTime dateTimeOfFirstNotArchivedTurn;

    @AfterViews
    protected void afterViews() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initializeViewPager();
    }

    @Override
    protected void onStart() {
        super.onStart();

        loadCarpool();

        DateTime turnDate = getFullDateFormatter(this).parseDateTime(getIntent().getStringExtra(Constants.TURN_DATE));
        update(turnDate);
        updateActivityState();

        showTurnDetailsExplanation();
    }

    /**
     * Shows a {@link MaterialShowcaseView show case view} explaining the turn details view.
     */
    private void showTurnDetailsExplanation() {
        CircleShape circleShape = new CircleShape(1);
        circleShape.setAdjustToTarget(false);

        new MaterialShowcaseView.Builder(this)
                .setTarget(vpTurns)
                .setShape(circleShape)
                .setDismissOnTouch(true)
                .setContentText(R.string.explanation_turn_details)
                .setDismissText(R.string.got_it)
                .setDelay(500)
                .singleUse(Constants.Notifications.EXPLAIN_TURN_DETAILS.getId())
                .show();
    }

    /**
     * Initializes {@link TurnDetailsActivity#vpTurns}
     */
    private void initializeViewPager() {
        paTurns = new TurnPagerAdapter(getSupportFragmentManager());
        vpTurns.setAdapter(paTurns);
        vpTurns.addOnPageChangeListener(new OnTurnPageChangeListener());
        tpiDays.setViewPager(vpTurns);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Loads the {@link TurnDetailsActivity#carpool}
     */
    private void loadCarpool() {
        String carpoolName = getIntent().getStringExtra(Constants.CARPOOL_NAME);
        DateTime turnDate = getFullDateFormatter(this).parseDateTime(getIntent().getStringExtra(Constants.TURN_DATE));

        try {
            carpool = dao.loadCarpool(carpoolName);

            dateTimeOfFirstNotArchivedTurn = carpool.getFirstUsedDate();
            while (turnDate.isBefore(carpool.getFirstUsedDate()) && dao.hasArchiveFor(carpool)) {
                dao.loadArchive(carpool);
            }
        } catch (CarpoolDAOException e) {
            Log.e(TurnDetailsActivity.class.getSimpleName(), "could not load carpool", e);
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            return;
        }
    }

    /**
     * Defines the exceptional usage for the member and update the UI.
     *
     * @param member
     * @param usage
     */
    private void defineExceptionalUsage(Member member, Usage usage) {
        DateTime turnDate = paTurns.getCurrentTurn().getDate();
        carpool.defineExceptionalUsage(member, turnDate, usage);

        try {
            dao.save(carpool);
        } catch (CarpoolDAOException e) {
            Log.e(TurnDetailsActivity.class.getSimpleName(), "could not save carpool changes", e);
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            return;
        }

        loadCarpool();
        update(turnDate);
    }

    /**
     * Updates the state of {@link TurnDetailsActivity#paTurns}.
     *
     * @param turnDate
     */
    private void update(DateTime turnDate) {
        List<Turn> turns = Lists.newArrayList(carpool.getTurnsInThePast());
        turns.addAll(carpool.predictTurnsUntil(getPredictionDate()));
        paTurns.setTurns(turns, turnDate);
    }

    /**
     * Sets {@link TurnDetailsActivity#setResult(int, android.content.Intent) result} of the activity.
     */
    private void updateActivityState() {
        DateTime turnDate = paTurns.getCurrentTurn().getDate();
        DateTimeFormatter formatter = getFullDateFormatter(this);

        Intent data = new Intent();
        data.putExtra(Constants.CARPOOL_NAME, carpool.getName());
        data.putExtra(Constants.TURN_DATE, formatter.print(turnDate));
        setResult(RESULT_OK, data);

        setTitle(carpool.getName());
        getSupportActionBar().setSubtitle(R.string.app_name);
    }

    /**
     * Returns the currently displayed {@link TurnDetailsFragment fragment}.
     *
     * @return
     */
    public TurnDetailsFragment getCurrentFragment() {
        return paTurns.getCurrentFragment();
    }

    /**
     * Adapter for displaying the {@link Turn turns} in the {@link TurnDetailsActivity#paTurns view pager}.
     */
    private class TurnPagerAdapter extends FragmentPagerAdapter {

        private final List<Turn> turns = new ArrayList<>();

        private TurnPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return turns.size();
        }

        /**
         * Sets the list of {@link Turn turns}
         *
         * @param turns
         * @param turnDate
         */
        private void setTurns(List<Turn> turns, DateTime turnDate) {
            this.turns.clear();
            this.turns.addAll(turns);
            notifyDataSetChanged();

            int index = 0;
            for (; index < turns.size(); ++index) {
                if (turns.get(index).getDate().equals(turnDate)) {
                    break;
                }
            }
            vpTurns.setCurrentItem(index);

            // Update all from the first index because they might need to be updated.
            // e.g. after rotation of the phoney
            for (index = 0; index < turns.size(); ++index) {
                update(index);
            }
        }

        @Override
        public Fragment getItem(int i) {
            TurnDetailsFragment fragment = new TurnDetailsFragment();
            fragment.setOnPickUsageListener(new PickUsageOfMemberListener());
            update(fragment, i);
            return fragment;
        }

        /**
         * Updates the data in the {@link TurnDetailsFragment fragment} at the given index.
         *
         * @param index
         */
        private void update(int index) {
            TurnDetailsFragment fragment = (TurnDetailsFragment) getSupportFragmentManager().findFragmentByTag(getFragmentTag(index));
            update(fragment, index);
        }

        /**
         * Updates the data in the {@link TurnDetailsFragment fragment} at the given index.
         *
         * @param fragment
         * @param index
         */
        private void update(TurnDetailsFragment fragment, int index) {
            if (fragment == null) {
                return;
            }

            Turn turn = turns.get(index);
            boolean editable = !turn.getDate().isBefore(dateTimeOfFirstNotArchivedTurn);
            fragment.setEditable(editable);
            fragment.setTurn(turn);
        }

        private String getFragmentTag(int index) {
            return "android:switcher:" + vpTurns.getId() + ":" + index;
        }

        private Turn getCurrentTurn() {
            return turns.get(vpTurns.getCurrentItem());
        }

        /**
         * Returns the {@link TurnDetailsFragment current active fragment}.
         *
         * @return
         */
        private TurnDetailsFragment getCurrentFragment() {
            return (TurnDetailsFragment) getSupportFragmentManager().findFragmentByTag(getFragmentTag(vpTurns.getCurrentItem()));
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Turn turn = turns.get(position);
            return getFullDateFormatter(getBaseContext()).print(turn.getDate());
        }
    }

    /**
     * {@link TurnDetailsFragment.OnPickUsageListener Listener} will handle the change of exceptional
     * usage
     */
    private class PickUsageOfMemberListener implements TurnDetailsFragment.OnPickUsageListener {

        @Override
        public void onFinish(Member member, Usage usage) {
            defineExceptionalUsage(member, usage);
        }
    }

    /**
     * {@link ViewPager.OnPageChangeListener Listener} which will update title and return value
     */
    private class OnTurnPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            // do nothing -> forced implementation and not needed
        }

        @Override
        public void onPageSelected(int position) {
            updateActivityState();
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            // do nothing -> forced implementation and not needed
        }
    }
}
