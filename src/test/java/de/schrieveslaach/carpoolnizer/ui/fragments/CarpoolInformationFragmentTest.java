/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.fragments;


import android.content.DialogInterface;
import android.os.Build;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import androidx.appcompat.app.AlertDialog;
import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.model.Carpool;
import de.schrieveslaach.carpoolnizer.model.CarpoolBuilder;

import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.hasText;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertThat;

@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
public class CarpoolInformationFragmentTest extends BaseDialogFragmentTest<CarpoolInformationFragment, AlertDialog> {

    private CarpoolInformationFragment fragment;

    private Carpool carpool;

    public CarpoolInformationFragmentTest() {
        super(CarpoolInformationFragment.class);
    }

    @Before
    public void createFragment() throws Exception {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime tenDaysAgo = today.minusDays(10);

        carpool = new CarpoolBuilder()
                .withName("Carpool")
                .withMemberAndEveryDayUsage("Mike", tenDaysAgo)
                .withMemberAndEveryDayUsage(null, tenDaysAgo)
                .withCalculationDate(today)
                .create();

        fragment = openFragment(new FragmentInitializer<CarpoolInformationFragment>() {
            @Override
            public void init(CarpoolInformationFragment fragment) {
                fragment.setCarpool(carpool);
            }

            @Override
            public void beforeStart(CarpoolInformationFragment fragment) {

            }
        });
    }

    @Test
    public void shouldNotShowMoneySavings_NoDataEntered() {
        AlertDialog dialog = openDialog(fragment);

        TextView tvMoneyPutAside = (TextView) dialog.findViewById(R.id.tv_money_put_aside_value);
        assertThat(tvMoneyPutAside, hasText("?"));
    }

    @Test
    public void shouldShowMoneySaving_CarpoolWithTenTurns() throws Exception {
        AlertDialog dialog = openDialog(fragment);

        enterValues(dialog, 100, 7.50, 1.50);

        TextView tvMoneyPutAside = (TextView) dialog.findViewById(R.id.tv_money_put_aside_value);
        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(RuntimeEnvironment.application.getApplicationContext().getResources().getConfiguration().locale);
        double savedMoney = currencyFormat.parse(tvMoneyPutAside.getText().toString()).doubleValue();
        assertThat(savedMoney, is(greaterThanOrEqualTo(0.0)));
        assertThat(savedMoney, is(lessThanOrEqualTo(112.50)));
    }

    @Test
    public void shouldRestorePreviousValues() throws Exception {
        AlertDialog dialog = openDialog(fragment);

        enterValues(dialog, 77, 3.33, 1.11);
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).performClick();

        fragment = openFragment(new FragmentInitializer<CarpoolInformationFragment>() {
            @Override
            public void init(CarpoolInformationFragment fragment) {
                fragment.setCarpool(carpool);
            }

            @Override
            public void beforeStart(CarpoolInformationFragment fragment) {

            }
        });
        dialog = openDialog(fragment);

        DecimalFormat numberFormat = (DecimalFormat) NumberFormat.getNumberInstance(RuntimeEnvironment.application.getApplicationContext().getResources().getConfiguration().locale);
        TextView tvDistance = dialog.findViewById(R.id.et_distance);
        assertThat(tvDistance, hasText(numberFormat.format(77)));

        TextView tvGasolinePrice = dialog.findViewById(R.id.et_gasoline_price);
        assertThat(tvGasolinePrice, hasText(numberFormat.format(1.11)));

        TextView tvGasolineConsumption = dialog.findViewById(R.id.et_gasoline_consumption);
        assertThat(tvGasolineConsumption, hasText(numberFormat.format(3.33)));
    }

    @Test
    public void shouldShowHowOftenMemberUsedCarpool_WithoutEnteringValues() {
        AlertDialog dialog = openDialog(fragment);

        TextView tvUsageCount = dialog.findViewById(R.id.tv_usage_count);
        assertThat(tvUsageCount, anyOf(
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 0, 0)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 1, 0)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 1, 1)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 2, 1)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 3, 1)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 3, 2)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 4, 2)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 4, 3)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 5, 2)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 5, 3)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 6, 3)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 6, 4)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 7, 3)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 7, 4)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 8, 4)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 8, 5)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 9, 4)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 9, 5)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 10, 5)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 10, 6)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 11, 6)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 11, 7))
        ));
    }

    @Test
    public void shouldShowHowOftenMemberUsedCarpool_AfterEnteringValuesl() {
        AlertDialog dialog = openDialog(fragment);
        enterValues(dialog, 100, 7.50, 1.50);

        TextView tvUsageCount = dialog.findViewById(R.id.tv_usage_count);
        assertThat(tvUsageCount, anyOf(
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 0, 0)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 1, 0)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 1, 1)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 2, 1)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 3, 1)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 3, 2)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 4, 2)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 4, 3)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 5, 2)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 5, 3)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 6, 3)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 6, 4)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 7, 3)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 7, 4)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 8, 4)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 8, 5)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 9, 4)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 9, 5)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 10, 5)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 10, 6)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 11, 6)),
                hasText(RuntimeEnvironment.application.getApplicationContext().getString(R.string.usage_count, 11, 7))
        ));
    }

    private void enterValues(AlertDialog dialog, int distance, double consumption, double price) {
        DecimalFormat numberFormat = (DecimalFormat) NumberFormat.getNumberInstance(RuntimeEnvironment.application.getApplicationContext().getResources().getConfiguration().locale);

        TextView tvDistance = dialog.findViewById(R.id.et_distance);
        tvDistance.setText(numberFormat.format(distance));

        TextView tvGasolinePrice = dialog.findViewById(R.id.et_gasoline_price);
        tvGasolinePrice.setText(numberFormat.format(price));

        TextView tvGasolineConsumption = dialog.findViewById(R.id.et_gasoline_consumption);
        tvGasolineConsumption.setText(numberFormat.format(consumption));
    }
}