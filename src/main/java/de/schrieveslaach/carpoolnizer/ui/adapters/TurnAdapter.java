/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;

import com.google.common.collect.Lists;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.model.Turn;
import de.schrieveslaach.carpoolnizer.ui.views.MemberReadOnlyView;

/**
 * {@link RecyclerView.Adapter Adapter} for {@link de.schrieveslaach.carpoolnizer.model.Turn turns}.
 */
public class TurnAdapter extends RecyclerView.Adapter<TurnViewHolder> implements SectionIndexer {

    private final Context context;

    /**
     * Holds the {@link Turn archived turns}
     */
    private final SortedSet<Turn> archivedTurns = new TreeSet<>();

    /**
     * Holds the {@link Turn turns} in the past.
     */
    private final List<Turn> turnsInThePast = new ArrayList<>();

    /**
     * Holds the {@link Turn turns} for display
     */
    private final List<Turn> turns = new ArrayList<>();

    /**
     * If {@code true} the {@link TurnAdapter#turnsInThePast first turn in the past} will show
     * the load archived turns button.
     */
    private boolean showLoadArchivedTurnsButton;

    /**
     * Listener for loading archived turns
     */
    private OnLoadArchivedTurnsListener onLoadArchivedTurnsListener;

    /**
     * Listener for handling a click on a {@link Turn}
     */
    private OnTurnClickListener onTurnClickListener;

    /**
     * Listener for handling click on contact image
     */
    private MemberReadOnlyView.OnContactInformationListener onContactInformationListener;

    /**
     * Creates a new instance of {@code TurnAdapter}
     *
     * @param context
     */
    public TurnAdapter(Context context) {
        this.context = context;
    }

    /**
     * Returns the index of the first {@link Turn} which is predicted by the carpool.
     *
     * @return
     */
    public int getFirstPredictedTurnIndex() {
        synchronized (turns) {
            return archivedTurns.size() + turnsInThePast.size();
        }
    }

    /**
     * Sets the {@link Turn turns} which will be displayed by a {@link RecyclerView}.
     *
     * @param turnsInThePast
     * @param turns
     */
    public void setTurns(List<Turn> turnsInThePast, List<Turn> turns) {
        synchronized (this.turns) {
            this.turnsInThePast.clear();
            this.turnsInThePast.addAll(turnsInThePast);
            this.turns.clear();
            this.turns.addAll(turns);
            notifyDataSetChanged();
        }
    }

    /**
     * Adds the {@link Turn archived turns} which will be displayed by a {@link RecyclerView}.
     *
     * @param archivedTurns
     */
    public void addArchivedTurns(List<Turn> archivedTurns) {
        synchronized (this.turns) {
            this.archivedTurns.addAll(archivedTurns);
            notifyDataSetChanged();
        }
    }

    /**
     * Sets if the first turn in the past should display the load archived button.
     *
     * @param showLoadArchivedTurnsButton
     */
    public void setShowLoadArchivedTurnsButton(boolean showLoadArchivedTurnsButton) {
        if (showLoadArchivedTurnsButton && turnsInThePast.isEmpty()) {
            throw new IllegalStateException("No turns in the past.");
        }

        this.showLoadArchivedTurnsButton = showLoadArchivedTurnsButton;
        notifyDataSetChanged();
    }

    @Override
    public TurnViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RelativeLayout turnView = (RelativeLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.view_turn, parent, false);
        return new TurnViewHolder(turnView, onTurnClickListener, onContactInformationListener);
    }

    @Override
    public void onBindViewHolder(TurnViewHolder holder, int position) {
        synchronized (turns) {

            Turn turn = getTurn(position);
            holder.display(turn);

            boolean showLoadButton = position == 0 && showLoadArchivedTurnsButton;
            if (showLoadButton) {
                holder.displayLoadArchivedTurnsButton(new OnLoadArchivedTurnsClickListener());
            } else {
                holder.hideLoadArchivedTurnsButton();
            }
        }
    }

    private Turn getTurn(int position) {
        Turn turn;
        if (position < archivedTurns.size()) {
            turn = Lists.newArrayList(archivedTurns).get(position);
        } else if (position < turnsInThePast.size() + archivedTurns.size()) {
            turn = turnsInThePast.get(position - archivedTurns.size());
        } else {
            turn = turns.get(position - turnsInThePast.size() - archivedTurns.size());
        }
        return turn;
    }

    @Override
    public int getItemCount() {
        synchronized (turns) {
            return archivedTurns.size() + turnsInThePast.size() + turns.size();
        }
    }

    /**
     * Sets the {@link OnTurnClickListener} which will be invoked when the user clicked on a
     * {@link Turn}.
     *
     * @param onTurnClickListener
     */
    public void setOnTurnClickListener(OnTurnClickListener onTurnClickListener) {
        this.onTurnClickListener = onTurnClickListener;
    }

    /**
     * Sets the {@link OnLoadArchivedTurnsListener} which will be invoke when the user wants to load
     * the archived turns.
     *
     * @param onLoadArchivedTurnsListener
     */
    public void setOnLoadArchivedTurnsListener(OnLoadArchivedTurnsListener onLoadArchivedTurnsListener) {
        this.onLoadArchivedTurnsListener = onLoadArchivedTurnsListener;
    }

    /**
     * Sets the {@link de.schrieveslaach.carpoolnizer.ui.views.MemberReadOnlyView.OnContactInformationListener}
     * which will be invoked when the user wants to see contact information.
     *
     * @param onContactInformationListener
     */
    public void setOnContactInformationListener(MemberReadOnlyView.OnContactInformationListener onContactInformationListener) {
        this.onContactInformationListener = onContactInformationListener;
    }

    /**
     * @param dateTime
     * @return
     */
    public int getIndexOfTurn(DateTime dateTime) {
        int index = getIndexOfTurn(archivedTurns, dateTime);
        if (index < 0) {
            index = getIndexOfTurn(turnsInThePast, dateTime);
            if (index < 0) {
                index = getIndexOfTurn(turns, dateTime);
                if (index < 0) {
                    return -1;
                }
                index += turnsInThePast.size();
            }
            index += archivedTurns.size();
        }
        return index;
    }

    private int getIndexOfTurn(Iterable<Turn> turns, DateTime dateTime) {
        int i = 0;
        for (Turn turn : turns) {
            if (turn.getDate().equals(dateTime)) {
                return i;
            }
            ++i;
        }
        return -1;
    }

    private String getSectionName(int position) {
        DateTime date = getTurn(position).getDate();
        return context.getResources().getString(R.string.calendar_week) + " " + String.valueOf(date.getWeekOfWeekyear());
    }

    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>();

        for (int d = 0; d < getItemCount(); d += 7) {
            sections.add(getSectionName(d));
        }

        if (!sections.isEmpty()) {
            // check if last turn was covered
            // +7 might skip the last week
            String section = getSectionName(getItemCount() - 1);
            String lastSection = sections.get(sections.size() - 1);
            if (!section.equals(lastSection)) {
                sections.add(section);
            }
        }

        return sections.toArray();
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        DateTime date = getTurn(0).getDate();
        DateTime firstWeekDay = date.withDayOfWeek(1);
        DateTime firstDayOfSection = firstWeekDay.plusDays(7 * sectionIndex);
        return (int) new Duration(date, firstDayOfSection).getStandardDays();
    }

    @Override
    public int getSectionForPosition(int position) {
        DateTime firstDate = getTurn(0).getDate().withDayOfWeek(1);
        DateTime date = getTurn(Math.min(position, getItemCount() - 1)).getDate().withDayOfWeek(1);
        int i = (int) (new Duration(firstDate, date).getStandardDays() / 7);
        return Math.min(i, getSections().length - 1);
    }

    /**
     * Observer for handling click events on a {@link Turn}
     */
    public interface OnTurnClickListener {

        /**
         * Will be invoked when the user clicked on a {@link Turn}.
         *
         * @param turn
         */
        void onClick(Turn turn);

        /**
         * Will be invoked when the user performed a long click on a {@link Turn}.
         *
         * @param turn
         */
        void onLongClick(Turn turn);
    }

    /**
     * Observer for notifying that the user wants to load the archived turns.
     */
    public interface OnLoadArchivedTurnsListener {

        /**
         * Will be invoked when the user wants to load the archived turns.
         */
        void onLoadArchivedTurns();

    }

    /**
     * Observer for checking if the user wants to load archived turns.
     */
    private class OnLoadArchivedTurnsClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            onLoadArchivedTurnsListener.onLoadArchivedTurns();
        }
    }
}
