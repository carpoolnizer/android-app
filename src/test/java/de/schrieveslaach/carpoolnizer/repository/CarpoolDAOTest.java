/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.repository;

import android.content.Context;

import com.google.common.collect.ImmutableList;
import com.google.common.io.Files;

import net.javacrumbs.jsonunit.core.Option;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

import de.schrieveslaach.carpoolnizer.model.Carpool;
import de.schrieveslaach.carpoolnizer.model.CarpoolBuilder;
import de.schrieveslaach.carpoolnizer.model.Member;
import de.schrieveslaach.carpoolnizer.model.Turn;
import de.schrieveslaach.carpoolnizer.model.Usage;
import de.schrieveslaach.carpoolnizer.model.settings.UsageNotification;
import lombok.SneakyThrows;

import static de.schrieveslaach.carpoolnizer.model.Constants.DATE_TIME_FORMATTER;
import static de.schrieveslaach.carpoolnizer.model.ModelMatchers.hasCarpoolReference;
import static de.schrieveslaach.carpoolnizer.model.ModelMatchers.hasDateTimes;
import static de.schrieveslaach.carpoolnizer.model.ModelMatchers.hasDrivers;
import static de.schrieveslaach.carpoolnizer.model.ModelMatchers.hasTurnsInThePastFor;
import static de.schrieveslaach.carpoolnizer.model.ModelMatchers.usesCarpoolFrom;
import static de.schrieveslaach.carpoolnizer.repository.RepositoryMatchers.hasArchiveFor;
import static net.javacrumbs.jsonunit.JsonMatchers.jsonEquals;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CarpoolDAOTest {

    private static final DateTime DATE = new DateTime(2014, 9, 8, 0, 0, 0);

    private CarpoolDAO dao;

    @Rule
    public final TemporaryFolder temporaryFolder = new TemporaryFolder();

    private File getCarpoolArchiveExampleFile() {
        return new File("./src/test/resources/de/schrieveslaach/carpoolnizer/repository/Carpool.archive.json");
    }

    public static File getCarpoolExampleFile() {
        return new File("./src/test/resources/de/schrieveslaach/carpoolnizer/repository/Carpool.json");
    }

    private File getExampleRepositorySettingsFile() {
        return new File("./src/test/resources/de/schrieveslaach/carpoolnizer/repository/Repository.settings.json");
    }

    @SneakyThrows(URISyntaxException.class)
    private File getExampleUsageNotifcationFile() {
        return new File(CarpoolDAOTest.class.getResource("UsageNotification.json").toURI());
    }

    @Before
    public void init() {
        Context context = mock(Context.class);
        when(context.getFilesDir()).thenReturn(temporaryFolder.getRoot());
        when(context.fileList()).thenAnswer(new Answer<String[]>() {
            @Override
            public String[] answer(InvocationOnMock invocation) {
                File[] files = temporaryFolder.getRoot().listFiles();
                String[] filenames = new String[files.length];
                for (int i = 0; i < files.length; ++i) {
                    filenames[i] = files[i].toString();
                }
                return filenames;
            }
        });
        when(context.getFileStreamPath(anyString())).thenAnswer(new Answer<File>() {
            @Override
            public File answer(InvocationOnMock invocation) {
                String filename = (String) invocation.getArguments()[0];
                return new File(temporaryFolder.getRoot(), filename);
            }
        });
        dao = new CarpoolDAO(context);
    }

    @Test
    public void shouldSaveCarpool() throws Exception {
        File file = getCarpoolExampleFile();
        String expectedJson = Files.toString(file, Charset.defaultCharset());

        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndWeekdayUsage(null, DATE)
                .withMemberAndWeekdayUsage("tom", DATE)
                .withMemberAndWeekdayUsage("john", DATE.minusDays(1))
                .create();

        carpool.defineExceptionalUsage(
                new Member(URI.create("http://tempuri.org#john")),
                DATE.minusDays(1),
                Usage.HAS_TO_DRIVE
        );
        carpool.calculateTurnsUntil(carpool.getLastUsedDate().plusDays(2));
        dao.save(carpool);

        List<String> names = dao.listCarpoolNames();
        assertThat(names, hasSize(1));

        File carpoolFile = dao.getCarpoolFile(carpool);
        assertThat("file should exist", carpoolFile.isFile(), is(true));
        String json = Files.toString(carpoolFile, Charset.defaultCharset());

        assertThat(json, jsonEquals(expectedJson).when(Option.IGNORING_EXTRA_FIELDS));
    }

    @Test
    public void shouldListCarpoolNames_None() {
        List<String> names = dao.listCarpoolNames();
        assertThat(names, is(empty()));
    }

    @Test
    public void shouldListCarpoolNames() throws Exception {
        File carpoolFile1 = new File(temporaryFolder.getRoot(), "Name1.carpool.json");
        carpoolFile1.createNewFile();
        File carpoolFile2 = new File(temporaryFolder.getRoot(), "Name2.carpool.json");
        carpoolFile2.createNewFile();

        File otherFile = new File(temporaryFolder.getRoot(), "other.txt");
        otherFile.createNewFile();

        List<String> names = dao.listCarpoolNames();
        assertThat(names, hasSize(2));
        assertThat(names, contains("Name1", "Name2"));
    }

    @Test
    public void shouldLoadCarpool() throws Exception {
        File file = getCarpoolExampleFile();
        File carpoolFile = new File(temporaryFolder.getRoot(), "Name.carpool.json");
        Files.copy(file, carpoolFile);

        Carpool carpool = dao.loadCarpool("Name");
        ImmutableList<Turn> turns = carpool.getTurnsInThePast();
        Member john = new Member(URI.create("http://tempuri.org#john"));

        assertThat(turns, hasDateTimes(DATE.minusDays(1), DATE));
        assertThat(turns, hasDrivers(john, john));

        assertThat(carpool.getMembers(), everyItem(usesCarpoolFrom(
                DATE,
                new Usage[]{
                        Usage.NORMAL,
                        Usage.NORMAL,
                        Usage.NORMAL,
                        Usage.NORMAL,
                        Usage.NORMAL,
                        Usage.DOES_NOT_USE,
                        Usage.DOES_NOT_USE
                }
        )));
    }

    @Test
    public void shouldLoadCarpool_TurnsInThePastHaveBackreference() throws Exception {
        File file = getCarpoolExampleFile();
        File carpoolFile = new File(temporaryFolder.getRoot(), "Name.carpool.json");
        Files.copy(file, carpoolFile);

        Carpool carpool = dao.loadCarpool("Name");

        ImmutableList<Turn> turns = carpool.getTurnsInThePast();
        assertThat(turns, everyItem(hasCarpoolReference(carpool)));
    }

    @Test
    public void shouldNotLoadCarpool_NotExisting() throws Exception {
        Carpool carpool = dao.loadCarpool("Name");
        assertThat(carpool, is(nullValue()));
    }

    @Test
    public void shouldNotGetLastUsedCarpoolName_NoRepositoryData() throws Exception {
        assertThat(dao.getLastUsedCarpoolName(), is(nullValue()));
    }

    @Test
    public void shouldStoreLastUsedCarpoolName_AfterLoadingCarpool() throws Exception {
        File file = getCarpoolExampleFile();
        File carpoolFile = new File(temporaryFolder.getRoot(), "Name.carpool.json");
        Files.copy(file, carpoolFile);

        dao.loadCarpool("Name");
        assertThat(dao.getLastUsedCarpoolName(), is("Name"));
    }

    @Test
    public void shouldNotLoadLastUsedCarpool_NoRepositoryData() throws Exception {
        assertThat(dao.loadLastUsedCarpool(), is(nullValue()));
    }

    @Test
    public void shouldNotLoadLastUsedCarpool_CarpoolDoesNotExistAnymore() throws Exception {
        File file = getExampleRepositorySettingsFile();
        File settingsFile = new File(temporaryFolder.getRoot(), "Repository.settings.json");
        Files.copy(file, settingsFile);

        assertThat(dao.loadLastUsedCarpool(), is(nullValue()));
    }

    @Test
    public void shouldLoadLastUsedCarpool() throws Exception {
        File file = getExampleRepositorySettingsFile();
        File settingsFile = new File(temporaryFolder.getRoot(), "Repository.settings.json");
        Files.copy(file, settingsFile);
        file = getCarpoolExampleFile();
        File carpoolFile = new File(temporaryFolder.getRoot(), "Name.carpool.json");
        Files.copy(file, carpoolFile);

        assertThat(dao.loadLastUsedCarpool(), is(notNullValue()));
    }

    @Test
    public void shouldArchiveCarpoolData() throws Exception {
        DateTime lastDayOfMonth = DateTime.now().withTimeAtStartOfDay()
                .plusMonths(1)
                .withDayOfMonth(1)
                .minusDays(1);
        DateTime threeMonthsAgo = lastDayOfMonth.withDayOfMonth(1).minusMonths(2);

        DateTime archiveStart = new DateTime(2015, 1, 1, 0, 0);
        DateTime archiveEnd = new DateTime(2015, 2, 1, 0, 0);

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndWeekdayUsage("john", archiveStart)
                .withMemberAndWeekdayUsage("daisy", archiveStart)
                .withMemberAndWeekdayUsage("mike", archiveStart)
                .withCalculationDate(lastDayOfMonth)
                .create();

        dao.save(carpool);
        assertThat(carpool, hasTurnsInThePastFor(threeMonthsAgo, lastDayOfMonth));

        File archiveFile = new File(temporaryFolder.getRoot(), String.format(
                "My Carpool.carpool.archive.%s-%s.json",
                DATE_TIME_FORMATTER.print(archiveStart),
                DATE_TIME_FORMATTER.print(archiveEnd))
        );
        assertThat("Check that the archive file exists", archiveFile.isFile(), is(true));

        final String expectedJson = Files.toString(getCarpoolArchiveExampleFile(), Charset.defaultCharset());
        String json = Files.toString(archiveFile, Charset.defaultCharset());
        assertThat(json, jsonEquals(expectedJson));
    }

    @Test
    public void shouldArchiveCarpoolData_ArchivedTurnsHaveCarpoolReference() throws Exception {
        DateTime lastDayOfMonth = DateTime.now().withTimeAtStartOfDay()
                .plusMonths(1)
                .withDayOfMonth(1)
                .minusDays(1);
        DateTime threeMonthsAgo = lastDayOfMonth.withDayOfMonth(1).minusMonths(2);
        ;
        DateTime fourMonthsAgo = threeMonthsAgo.minusMonths(1);

        DateTime archiveStart = new DateTime(2015, 1, 1, 0, 0);
        DateTime archiveEnd = new DateTime(2015, 2, 1, 0, 0);

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndWeekdayUsage("john", archiveStart)
                .withMemberAndWeekdayUsage("daisy", archiveStart)
                .withMemberAndWeekdayUsage("mike", archiveStart)
                .withCalculationDate(lastDayOfMonth)
                .create();

        dao.save(carpool);
        dao.loadArchive(carpool);

        ImmutableList<Turn> turns = carpool.getTurnsInThePast();
        assertThat(turns, everyItem(hasCarpoolReference(carpool)));
    }

    @Test
    public void shouldArchiveCarpoolData_IntoMultipleChunks() throws Exception {
        DateTime lastDayOfMonth = DateTime.now().withTimeAtStartOfDay()
                .plusMonths(1)
                .withDayOfMonth(1)
                .minusDays(1);

        DateTime halfAYearAgo = lastDayOfMonth.withDayOfMonth(1).minusMonths(6);
        DateTime archiveDate = halfAYearAgo.plusMonths(4);

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndWeekdayUsage("john", halfAYearAgo)
                .withMemberAndWeekdayUsage("daisy", halfAYearAgo)
                .withMemberAndWeekdayUsage("mike", halfAYearAgo)
                .withCalculationDate(lastDayOfMonth)
                .create();

        dao.save(carpool);

        assertThat(carpool, hasTurnsInThePastFor(archiveDate, lastDayOfMonth));
        Map<Interval, File> archiveFiles = dao.getArchiveFilesFor(carpool);
        assertThat(archiveFiles, hasKey(new Interval(halfAYearAgo, halfAYearAgo.plusMonths(1))));
        assertThat(archiveFiles, hasKey(new Interval(halfAYearAgo.plusMonths(1), halfAYearAgo.plusMonths(2))));
        assertThat(archiveFiles, hasKey(new Interval(halfAYearAgo.plusMonths(2), halfAYearAgo.plusMonths(3))));
        assertThat(archiveFiles, hasKey(new Interval(halfAYearAgo.plusMonths(3), archiveDate)));
    }

    @Test
    public void shouldNotArchiveCarpoolData_UntilEnoughDataIsAvailable_ContinuousCalculation() throws Exception {
        DateTime lastDayOfMonth = DateTime.now().withTimeAtStartOfDay()
                .plusMonths(1)
                .withDayOfMonth(1)
                .minusDays(1);

        DateTime aMonthsAgo = lastDayOfMonth.withDayOfMonth(1);
        DateTime twoMonthsAgo = lastDayOfMonth.minusMonths(1).withDayOfMonth(1);
        DateTime threeMonthsAgo = lastDayOfMonth.minusMonths(2).withDayOfMonth(1);
        DateTime fourMonthsAgo = lastDayOfMonth.minusMonths(3).withDayOfMonth(1);

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndWeekdayUsage("john", fourMonthsAgo)
                .withMemberAndWeekdayUsage("daisy", fourMonthsAgo)
                .withMemberAndWeekdayUsage("mike", fourMonthsAgo)
                .create();

        carpool.calculateTurnsUntil(threeMonthsAgo);
        dao.save(carpool);
        assertThat("first calculation without archiving", carpool, hasTurnsInThePastFor(fourMonthsAgo, threeMonthsAgo));

        carpool.calculateTurnsUntil(twoMonthsAgo);
        dao.save(carpool);
        assertThat("second calculation without archiving", carpool, hasTurnsInThePastFor(fourMonthsAgo, twoMonthsAgo));

        carpool.calculateTurnsUntil(aMonthsAgo);
        dao.save(carpool);
        assertThat("first archiving", carpool, hasTurnsInThePastFor(threeMonthsAgo, aMonthsAgo));

        carpool.calculateTurnsUntil(lastDayOfMonth);
        dao.save(carpool);
        assertThat("third calculation without archiving", carpool, hasTurnsInThePastFor(threeMonthsAgo, lastDayOfMonth));
    }

    @Test
    public void shouldHaveCarpoolArchive() throws Exception {
        DateTime lastDayOfMonth = DateTime.now().withTimeAtStartOfDay()
                .plusMonths(1)
                .withDayOfMonth(1)
                .minusDays(1);

        DateTime fourMonthsAgo = lastDayOfMonth.minusMonths(3).withDayOfMonth(1);

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndWeekdayUsage("john", fourMonthsAgo)
                .withMemberAndWeekdayUsage("daisy", fourMonthsAgo)
                .withMemberAndWeekdayUsage("mike", fourMonthsAgo)
                .withCalculationDate(lastDayOfMonth)
                .create();
        dao.save(carpool);

        assertThat(dao, hasArchiveFor(carpool));
    }

    @Test
    public void shouldNotHaveCarpoolArchive_NullValue() throws Exception {
        DateTime lastDayOfMonth = DateTime.now().withTimeAtStartOfDay()
                .plusMonths(1)
                .withDayOfMonth(1)
                .minusDays(1);

        DateTime fourMonthsAgo = lastDayOfMonth.minusMonths(3).withDayOfMonth(1);

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndWeekdayUsage("john", fourMonthsAgo)
                .withMemberAndWeekdayUsage("daisy", fourMonthsAgo)
                .withMemberAndWeekdayUsage("mike", fourMonthsAgo)
                .withCalculationDate(lastDayOfMonth)
                .create();
        dao.save(carpool);

        assertThat(dao, not(hasArchiveFor(null)));
    }

    @Test
    public void shouldNotHaveCarpoolArchive_DifferentName() throws Exception {
        DateTime lastDayOfMonth = DateTime.now().withTimeAtStartOfDay()
                .plusMonths(1)
                .withDayOfMonth(1)
                .minusDays(1);

        DateTime fourMonthsAgo = lastDayOfMonth.minusMonths(3).withDayOfMonth(1);

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndWeekdayUsage("john", fourMonthsAgo)
                .withMemberAndWeekdayUsage("daisy", fourMonthsAgo)
                .withMemberAndWeekdayUsage("mike", fourMonthsAgo)
                .withCalculationDate(lastDayOfMonth)
                .create();
        dao.save(carpool);

        carpool = new CarpoolBuilder()
                .withName("Another Carpool")
                .create();
        dao.save(carpool);

        assertThat(dao, not(hasArchiveFor(carpool)));
    }

    @Test
    public void shouldLoadCarpoolArchive() throws Exception {
        DateTime lastDayOfMonth = DateTime.now().withTimeAtStartOfDay()
                .plusMonths(1)
                .withDayOfMonth(1)
                .minusDays(1);

        DateTime threeMonthsAgo = lastDayOfMonth.minusMonths(2).withDayOfMonth(1);
        DateTime fourMonthsAgo = lastDayOfMonth.minusMonths(3).withDayOfMonth(1);

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndWeekdayUsage("john", fourMonthsAgo)
                .withMemberAndWeekdayUsage("daisy", fourMonthsAgo)
                .withMemberAndWeekdayUsage("mike", fourMonthsAgo)
                .withCalculationDate(lastDayOfMonth)
                .create();
        dao.save(carpool);
        assertThat(carpool, hasTurnsInThePastFor(threeMonthsAgo, lastDayOfMonth));

        Interval archivePeriod = dao.loadArchive(carpool);
        assertThat(carpool, hasTurnsInThePastFor(fourMonthsAgo, lastDayOfMonth));
        assertThat(archivePeriod.getStart(), is(fourMonthsAgo));
        assertThat(archivePeriod.getEnd(), is(threeMonthsAgo));
    }

    @Test
    public void shouldDeleteCarpool_WithoutArchive() throws Exception {
        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndWeekdayUsage(null, DATE)
                .withMemberAndWeekdayUsage("tom", DATE)
                .withMemberAndWeekdayUsage("john", DATE.minusDays(1))
                .withCalculationDate(DATE.plusDays(3))
                .create();

        dao.save(carpool);
        File carpoolFile = dao.getCarpoolFile(carpool);
        assertThat("file should exist", carpoolFile.isFile(), is(true));

        dao.delete(carpool);
        assertThat("file should not exist", carpoolFile.isFile(), is(false));
    }

    @Test
    public void shouldDeleteCarpool_WithArchive() throws Exception {
        DateTime lastDayOfMonth = DateTime.now().withTimeAtStartOfDay()
                .plusMonths(1)
                .withDayOfMonth(1)
                .minusDays(1);

        DateTime fourMonthsAgo = lastDayOfMonth.minusMonths(3).withDayOfMonth(1);

        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndWeekdayUsage("john", fourMonthsAgo)
                .withMemberAndWeekdayUsage("daisy", fourMonthsAgo)
                .withMemberAndWeekdayUsage("mike", fourMonthsAgo)
                .withCalculationDate(lastDayOfMonth)
                .create();

        dao.save(carpool);
        Map<Interval, File> archiveFilesFor = dao.getArchiveFilesFor(carpool);

        dao.delete(carpool);

        File carpoolFile = dao.getCarpoolFile(carpool);
        assertThat("file should not exist", carpoolFile.isFile(), is(false));
        for (File f : archiveFilesFor.values()) {
            assertThat("file should not exist", f.isFile(), is(false));
        }
    }

    @Test
    public void shouldSaveUsageNotifcationForCarpool() throws Exception {
        String expectedJson = Files.toString(getExampleUsageNotifcationFile(), Charset.defaultCharset());

        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndWeekdayUsage(null, DATE)
                .withMemberAndWeekdayUsage("tom", DATE)
                .withMemberAndWeekdayUsage("john", DATE.minusDays(1))
                .create();

        UsageNotification notification = new UsageNotification(7, 0);

        dao.saveUsageNotifcation(carpool, notification);

        File usageNotificationFile = dao.getUsageNotificationFile(carpool);
        String json = Files.toString(usageNotificationFile, Charset.defaultCharset());

        assertThat(json, jsonEquals(expectedJson));
    }

    @Test
    public void shouldLoadUsageNotification() throws Exception {
        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndWeekdayUsage(null, DATE)
                .withMemberAndWeekdayUsage("tom", DATE)
                .withMemberAndWeekdayUsage("john", DATE.minusDays(1))
                .create();

        Files.copy(getExampleUsageNotifcationFile(), dao.getUsageNotificationFile(carpool));

        UsageNotification notification = dao.loadUsageNotification(carpool);
        assertThat(notification.getPeriod().getHours(), is(7));
        assertThat(notification.getPeriod().getMinutes(), is(0));
    }

    @Test
    public void shouldNotLoadUsageNotification_CarpoolHasNoNotification() throws Exception {
        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndWeekdayUsage(null, DATE)
                .withMemberAndWeekdayUsage("tom", DATE)
                .withMemberAndWeekdayUsage("john", DATE.minusDays(1))
                .create();

        UsageNotification notification = dao.loadUsageNotification(carpool);
        assertThat(notification, is(nullValue()));
    }
}