/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import java.net.URI;

import de.schrieveslaach.carpoolnizer.model.Member;
import de.schrieveslaach.carpoolnizer.repository.ContactDAO_;

/**
 * Provides basic operations for displaying information of {@link Member members}.
 */
@TargetApi(18)
public abstract class AbstractMemberView extends RelativeLayout {

    private Member member;

    /**
     * Creates a new instance of {@code AbstractMemberView}.
     *
     * @param context
     */
    public AbstractMemberView(Context context) {
        super(context);
    }

    /**
     * Creates a new instance of {@code AbstractMemberView}.
     *
     * @param context
     * @param attrs
     */
    public AbstractMemberView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Creates a new instance of {@code AbstractMemberView}.
     *
     * @param context
     * @param attrs
     * @param defStyle
     */
    public AbstractMemberView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Returns the currently displayed {@link Member}.
     *
     * @return
     */
    public Member getMember() {
        return member;
    }

    /**
     * Returns the contact picture of the current {@link AbstractMemberView#getMember() member}.
     *
     * @return
     */
    public Bitmap getContactPicture() {
        if (member == null) {
            throw new IllegalStateException("display a member at first");
        }

        return ContactDAO_.getInstance_(getContext()).getContactPicture(member);
    }

    /**
     * Returns the display of the current {@link AbstractMemberView#getMember() member}.
     *
     * @return
     */
    public String getDisplayName() {
        if (member == null) {
            throw new IllegalStateException("display a member at first");
        }

        return ContactDAO_.getInstance_(getContext()).getDisplayName(member);
    }

    /**
     * Loads the data from the given {@link android.database.Cursor} and displays the member.
     * The cursor must have loaded data for {@link android.provider.ContactsContract.Contacts#_ID}
     * and {@link android.provider.ContactsContract.Contacts#DISPLAY_NAME}.
     *
     * @param cursor
     */
    public void display(Cursor cursor) {
        if (cursor == null) {
            throw new IllegalArgumentException("cursor must not be null!");
        }

        Uri uri = Uri.withAppendedPath(
                ContactsContract.Contacts.CONTENT_URI,
                String.valueOf(cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts._ID)))
        );

        display(new Member(URI.create(uri.toString())));
    }

    /**
     * Displays the given {@link Member member}.
     *
     * @param member
     */
    public void display(Member member) {
        this.member = member;
    }
}
