/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.fragments;

import android.annotation.TargetApi;
import android.os.Bundle;
import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import de.schrieveslaach.carpoolnizer.BuildConfig;
import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.model.Carpool;
import de.schrieveslaach.carpoolnizer.model.Member;
import de.schrieveslaach.carpoolnizer.model.Turn;
import de.schrieveslaach.carpoolnizer.model.Usage;
import de.schrieveslaach.carpoolnizer.ui.ShowContactInformationListener;
import de.schrieveslaach.carpoolnizer.ui.views.MemberReadOnlyView;
import de.schrieveslaach.carpoolnizer.ui.views.resolvers.UnknownContactDrawableResolver;

import static java.lang.Math.min;

/**
 * {@link android.app.Fragment} to display details of a {@link de.schrieveslaach.carpoolnizer.model.Turn turn}.
 */
public class TurnDetailsFragment extends Fragment {

    private boolean editable = false;

    /**
     * ID of the {@link TextView} displaying how often a member was driver of a carpool.
     */
    @IdRes
    public static final int DRIVER_COUNT_ID = View.generateViewId();

    /**
     * ID of the {@link TextView} displaying the exceptional usage of a member
     */
    @IdRes
    public static final int EXCEPTIONAL_USAGE_ID = View.generateViewId();

    private TextView tvDriver;

    private MemberReadOnlyView mrovDriver;

    private TextView tvPassengers;

    private LinearLayout llPassengers;

    private TextView tvRemainingMembers;

    private LinearLayout llRemainingMembers;

    private ImageView ivCanceledTurn;

    private TextView tvCanceledTurn;

    /**
     * Currently displayed {@link Turn turn}.
     */
    private Turn turn;

    private OnPickUsageListener onPickUsageListener;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_turn_details, container, false);

        tvDriver = (TextView) view.findViewById(R.id.tv_driver);
        mrovDriver = (MemberReadOnlyView) view.findViewById(R.id.mv_driver);
        mrovDriver.setNoMemberDrawableResolver(new UnknownContactDrawableResolver());
        tvPassengers = (TextView) view.findViewById(R.id.tv_passengers);
        llPassengers = (LinearLayout) view.findViewById(R.id.ll_passengers);
        tvRemainingMembers = (TextView) view.findViewById(R.id.tv_remaining_members);
        llRemainingMembers = (LinearLayout) view.findViewById(R.id.ll_remaining_members);
        tvCanceledTurn = (TextView) view.findViewById(R.id.tv_canceled_turn);
        ivCanceledTurn = (ImageView) view.findViewById(R.id.iv_canceled_turn);
        ivCanceledTurn.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                displayCanceledStatus();
            }
        });

        update();

        return view;
    }

    /**
     * Sets if the {@code TurnDetailsFragment} is editable, e.g. selecting exceptional usage.
     *
     * @param editable
     */
    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    /**
     * Sets the {@link Turn turn} which will be displayed.
     *
     * @param turn
     */
    public void setTurn(Turn turn) {
        this.turn = turn;
        update();
    }

    /**
     * Sets the {@link OnPickUsageListener listener} which will be invoked when the user selected
     * exceptional usage for a specific member.
     *
     * @param onPickUsageListener
     */
    public void setOnPickUsageListener(OnPickUsageListener onPickUsageListener) {
        this.onPickUsageListener = onPickUsageListener;
    }

    /**
     * Updates the UI with the data which should be displayed.
     */
    private void update() {
        if (turn == null || tvDriver == null) {
            return;
        }

        Member driver = turn.getDriver();
        int driverVisible = driver != null ? View.VISIBLE : View.GONE;
        tvDriver.setVisibility(driverVisible);
        mrovDriver.setVisibility(driverVisible);
        mrovDriver.display(driver);
        mrovDriver.setOnClickListener(new DefineExceptionalUsageListener());
        displayAdditionalData(mrovDriver);

        initMemberGroup(
                turn.getPassengers(),
                tvPassengers,
                llPassengers
        );

        initMemberGroup(
                turn.getRemainingMembers(),
                tvRemainingMembers,
                llRemainingMembers
        );

        displayCanceledStatus();
    }

    /**
     * Display exceptional usage and driver count for the {@link MemberReadOnlyView#getMember() member}.
     *
     * @param mrov
     */
    private void displayAdditionalData(MemberReadOnlyView mrov) {
        mrov.removeAllViews();

        if (editable) {
            mrov.setBackground(getResources().getDrawable(R.drawable.clickable_background));
        } else {
            mrov.setBackground(null);
        }
        mrov.setClickable(editable);

        if (mrov.getMember() != null && !mrov.getMember().isPhoneUser()) {
            mrov.setOnContactInformationListener(new ShowContactInformationListener(getActivity()));
        } else {
            mrov.setOnContactInformationListener(null);
        }

        displayExceptionalUsage(mrov);
        displayDriverCount(mrov);
    }

    /**
     * Displays the driver count for the given {@link MemberReadOnlyView}.
     *
     * @param mrov
     */
    @TargetApi(18)
    private void displayDriverCount(MemberReadOnlyView mrov) {
        if (turn.getPassengers().isEmpty() || !turn.contains(mrov.getMember())) {
            return;
        }

        int driverCount = turn.getDriverCount(mrov.getMember());
        int padding = (int) getResources().getDimension(R.dimen.text_view_padding);

        TextView tvDrove = new TextView(this.getActivity());
        tvDrove.setText(getResources().getQuantityString(R.plurals.times_driven, driverCount, driverCount));
        tvDrove.setTextAppearance(this.getActivity(), android.R.style.TextAppearance_Small);
        tvDrove.setId(DRIVER_COUNT_ID);
        tvDrove.setPadding(padding, padding, padding, padding);

        mrov.addView(tvDrove);
    }

    /**
     * Displays the
     *
     * @param mrov
     */
    @TargetApi(BuildConfig.VERSION_CODE)
    private void displayExceptionalUsage(MemberReadOnlyView mrov) {
        Member member = mrov.getMember();
        if (member == null || !member.hasExceptionalUsage(turn.getDate())) {
            return;
        }

        int padding = (int) getResources().getDimension(R.dimen.text_view_padding);

        LinearLayout ll = new LinearLayout(getActivity());
        ll.setOrientation(LinearLayout.HORIZONTAL);

        ImageView iv = new ImageView(getActivity());
        iv.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_warning));
        ll.addView(iv);

        TextView tvExceptionalUsage = new TextView(getActivity());
        tvExceptionalUsage.setId(EXCEPTIONAL_USAGE_ID);
        tvExceptionalUsage.setTextAppearance(getActivity(), android.R.style.TextAppearance_DeviceDefault_Medium);
        tvExceptionalUsage.setPadding(padding, padding, padding, padding);
        tvExceptionalUsage.setTextColor(getResources().getColor(R.color.infoTextLight));

        Usage usage = member.getUsageAt(turn.getDate());
        switch (usage) {
            case DOES_NOT_USE:
                tvExceptionalUsage.setText(R.string.does_not_use_usage);
                break;
            case HAS_TO_DRIVE:
                tvExceptionalUsage.setText(R.string.has_to_drive_usage);
                break;
            case CAN_NOT_DRIVE:
                tvExceptionalUsage.setText(R.string.can_not_drive_usage);
                break;
            default:
                tvExceptionalUsage.setText(R.string.normal_usage);
                break;
        }

        ll.addView(tvExceptionalUsage);

        mrov.addView(ll);
    }

    /**
     * Displays if the {@link #turn} canceled.
     */
    private void displayCanceledStatus() {
        if (turn == null) {
            return;
        }

        int visibility = turn.isCanceled() ? View.VISIBLE : View.GONE;
        tvCanceledTurn.setVisibility(visibility);
        ivCanceledTurn.setVisibility(visibility);

        if (turn.isCanceled()) {
            tvRemainingMembers.setVisibility(View.GONE);
            llRemainingMembers.setVisibility(View.GONE);
        }

        final View decorView = getActivity().getWindow().getDecorView();
        int size = min(decorView.getWidth(), decorView.getHeight());

        if (size > 0) {
            ivCanceledTurn.setMinimumHeight(size);
            ivCanceledTurn.setMinimumWidth(size);
        }
    }

    /**
     * Initializes the given {@link LinearLayout} to display the given list of {@link Member members}.
     *
     * @param members
     * @param textView
     * @param llMemberGroup
     */
    private void initMemberGroup(List<Member> members, TextView textView, LinearLayout llMemberGroup) {
        llMemberGroup.removeAllViews();

        int visible = members.isEmpty() ? View.GONE : View.VISIBLE;
        textView.setVisibility(visible);

        for (Member passenger : members) {
            MemberReadOnlyView mrov = new MemberReadOnlyView(getActivity());
            mrov.setClickable(true);
            mrov.display(passenger);
            mrov.setOnClickListener(new DefineExceptionalUsageListener());
            llMemberGroup.addView(mrov);

            displayAdditionalData(mrov);
        }
    }

    /**
     * Listener to define {@link Usage exceptional usage} for a {@link Member member}.
     */
    private class DefineExceptionalUsageListener implements View.OnClickListener, ExceptionalUsagePickerFragment.OnPickUsageListener {

        private Member member;

        @Override
        public void onClick(View v) {
            if (!editable) {
                return;
            }

            member = ((MemberReadOnlyView) v).getMember();

            ExceptionalUsagePickerFragment fragment = new ExceptionalUsagePickerFragment();
            Carpool carpool = turn.getCarpool();
            for (Usage usage : carpool.getInvalidUsage(turn.getDate())) {
                fragment.disable(usage);
            }

            fragment.setOnPickUsageListener(this);
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .add(fragment, "pick.usage")
                    .commit();
        }

        @Override
        public void onFinish(Usage usage) {
            onPickUsageListener.onFinish(member, usage);
        }
    }

    /**
     * Listener which will be called when the user picked {@link Usage usage} for a {@link Member specific member}.
     */
    public interface OnPickUsageListener {

        /**
         * Will be raised when the user picked {@link Usage usage} for a {@link Member specific member}.
         *
         * @param member
         * @param usage
         */
        void onFinish(Member member, Usage usage);

    }
}
