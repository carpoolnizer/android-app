/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui;

import android.app.Activity;
import androidx.annotation.IdRes;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import org.junit.After;
import org.junit.Before;
import org.robolectric.android.controller.ActivityController;

import java.net.URI;

import de.schrieveslaach.carpoolnizer.ui.robolectric.fakes.ContactsCursor;
import lombok.SneakyThrows;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.robolectric.Robolectric.buildActivity;

public abstract class AbstractRobolectricActivityTester<A extends Activity> extends RobolectricTest {

    private final ActivityController<A> controller;

    protected A activity;

    protected ContactsCursor contactsCursor;

    protected AbstractRobolectricActivityTester(Class<A> activityClass) {
        controller = buildActivity(activityClass);
    }

    @Before
    public void initActivity() {
        activity = controller.create().visible().get();
    }

    @Before
    public void addMembersToContentResolver() {
        contactsCursor = new ContactsCursor()
                .withContact("John")
                .withContact("Dave")
                .withContact("Mike")
                .withContact("Daisy")
                .withContact(URI.create("http://tempuri.org#john"))
                .withContact(URI.create("http://tempuri.org#tom"))
                .fake();
    }

    @After
    @SneakyThrows
    public void cleanUp() {
        controller.stop()
                .destroy();
    }

    /**
     * Simulates pressing a {@link MenuItem}.
     *
     * @param menuId
     */
    public boolean pressMenuItem(@IdRes final int menuId) {
        MenuItem item = mock(MenuItem.class);
        when(item.getItemId()).thenReturn(menuId);
        return activity.onOptionsItemSelected(item);
    }

    public void dismissShowCaseView() {
        getShowCaseView()
                .findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_dismiss)
                .performClick();

        MaterialShowcaseView showCaseView = getShowCaseView();
        if (showCaseView != null) {
            showCaseView.removeFromWindow();
        }
    }

    public MaterialShowcaseView getShowCaseView() {
        return getShowCaseView((ViewGroup) activity.getWindow().getDecorView());
    }

    private MaterialShowcaseView getShowCaseView(ViewGroup vg) {
        if (vg instanceof MaterialShowcaseView) {
            return (MaterialShowcaseView) vg;
        }

        for (int i = 0; i < vg.getChildCount(); ++i) {
            View c = vg.getChildAt(i);
            if (c instanceof ViewGroup) {
                MaterialShowcaseView showCaseView = getShowCaseView((ViewGroup) c);
                if (showCaseView != null) {
                    return showCaseView;
                }
            }
        }
        return null;
    }
}
