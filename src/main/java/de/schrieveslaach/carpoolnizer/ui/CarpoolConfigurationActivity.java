/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.FocusChange;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

import java.net.URI;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.schrieveslaach.carpoolnizer.Constants;
import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.model.Carpool;
import de.schrieveslaach.carpoolnizer.model.Member;
import de.schrieveslaach.carpoolnizer.repository.CarpoolDAO;
import de.schrieveslaach.carpoolnizer.repository.CarpoolDAOException;
import de.schrieveslaach.carpoolnizer.ui.adapters.MemberConfigurationAdapter;
import de.schrieveslaach.carpoolnizer.ui.fragments.ContactPickerFragment;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.shape.RectangleShape;
import uk.co.deanwild.materialshowcaseview.shape.Shape;

/**
 * {@link Activity} to create or modify {@link de.schrieveslaach.carpoolnizer.model.Carpool carpools}.
 */
@EActivity(R.layout.activity_carpool_configuration)
@OptionsMenu(R.menu.carpool_configuration)
@RuntimePermissions
public class CarpoolConfigurationActivity extends AppCompatActivity {

    /**
     * Data access object to save {@link Carpool carpools}.
     */
    private final CarpoolDAO dao = new CarpoolDAO(this);

    private MemberConfigurationAdapter memberConfigurationsAdapter;

    @ViewById(R.id.et_name)
    EditText etName;

    @ViewById(R.id.et_select_start_date)
    EditText etPickStartDate;

    @ViewById(R.id.rv_member_configurations)
    RecyclerView rvMemberConfigurations;

    @ViewById(R.id.ab_add_member)
    FloatingActionButton abAddMember;

    private DateTime startDate = new DateTime().withTimeAtStartOfDay();
    private DateTime minimumDate;

    private boolean isEditingCarpool = false;

    @AfterViews
    protected void afterViews() {
        updateStartDateButtonText();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            initMemberConfigurationsView();
        }
    }

    /**
     * Initializes {@link CarpoolConfigurationActivity#rvMemberConfigurations} containing all {@link de.schrieveslaach.carpoolnizer.model.Member members}.
     */
    @NeedsPermission(Manifest.permission.READ_CONTACTS)
    void initMemberConfigurationsView() {
        rvMemberConfigurations.setLayoutManager(new LinearLayoutManager(this));
        memberConfigurationsAdapter = new MemberConfigurationAdapter();
        memberConfigurationsAdapter.setConfigurationChangedListener(new MemberConfigurationAdapter.ConfigurationChangedListener() {
            @Override
            public void changedUsage(Member m) {
                explainAddContacts(m);
            }
        });
        memberConfigurationsAdapter.add(new Member(null));
        rvMemberConfigurations.setAdapter(memberConfigurationsAdapter);

        explainUsageOfPhoneUser();
    }

    @OnShowRationale(Manifest.permission.READ_CONTACTS)
    void showRationaleForReadContact(final PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.request_contacts_read_permission_dialog_title)
                .setMessage(R.string.request_contacts_read_permission)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        request.proceed();
                    }
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.READ_CONTACTS)
    void showDeniedForContacts() {
        finish();
    }

    private void updateStartDateButtonText() {
        DateTimeFormatter formatter = Constants.getShortDateFormatter(this);
        String text;
        synchronized (etPickStartDate) {
            text = getString(R.string.starts_at, formatter.print(startDate));
        }
        etPickStartDate.setText(text);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            CarpoolConfigurationActivityPermissionsDispatcher.initMemberConfigurationsViewWithPermissionCheck(this);
        }

        Intent intent = getIntent();
        if (intent.hasExtra(Constants.CARPOOL_NAME)) {
            isEditingCarpool = true;
            initializeForEditing();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            CarpoolConfigurationActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
        }
    }

    /**
     * Opens a {@link de.schrieveslaach.carpoolnizer.ui.fragments.ContactPickerFragment} to pick a contact for the {@link Carpool}.
     */
    @Click(R.id.ab_add_member)
    protected void pickMemberFromContacts() {
        ContactPickerFragment cpf = new ContactPickerFragment();
        cpf.setOnPickContactListener(new PickedContactListener());
        getSupportFragmentManager()
                .beginTransaction()
                .add(cpf, "contactPicker")
                .commit();
    }

    /**
     * Lets the user pick a start date for the carpool.
     */
    @Click(R.id.et_select_start_date)
    protected void pickStartDate() {
        Locale locale = getResources().getConfiguration().getLocales().get(0);

        DateTime date = DateTime.now().withTimeAtStartOfDay();
        Calendar now = date.toCalendar(locale);
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                new PickedStartDateListener(),
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );

        if (isEditingCarpool && minimumDate != null) {
            dpd.setMinDate(minimumDate.toCalendar(locale));
        }

        dpd.show(getSupportFragmentManager(), "datePicker");
    }

    @FocusChange(R.id.et_select_start_date)
    protected void focusOnPickStartDate(View view, boolean hasFocus) {
        if (hasFocus) {
            etPickStartDate.performClick();
        }
    }

    /**
     * Loads the {@link Carpool carpool} with the given name and sets up the ui.
     */
    private void initializeForEditing() {
        Carpool carpool = loadCarpool();

        // set the name
        etName.setText(carpool.getName());
        etName.setEnabled(false);

        // remember minimum date
        minimumDate = carpool.getFirstUsedDate();

        // set members
        memberConfigurationsAdapter.clear();
        for (Member member : carpool.getMembers()) {
            boolean[] weekdayUsage = member.getWeekdayUsage(startDate);
            memberConfigurationsAdapter.add(member, weekdayUsage);
        }
    }

    private Carpool loadCarpool() {
        String carpoolName = getIntent().getStringExtra(Constants.CARPOOL_NAME);

        Carpool carpool = null;
        try {
            carpool = dao.loadCarpool(carpoolName);
        } catch (CarpoolDAOException e) {
            Log.d(CarpoolConfigurationActivity.class.getName(), e.getMessage(), e);
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
        return carpool;
    }

    /**
     * Validates if all {@link CarpoolConfigurationActivity#validateState() data has been set correctly} and
     * if so the data will be saved and the activity will be {@link android.app.Activity#finish() finished}.
     */
    @OptionsItem(R.id.done)
    protected void validateAndClose() {
        if (!validateState()) {
            return;
        }

        Carpool carpool;
        if (isEditingCarpool) {
            carpool = loadCarpool();
            carpool.revertTurnsFrom(startDate);
        } else {
            carpool = new Carpool(etName.getText().toString());
        }

        for (Member member : memberConfigurationsAdapter.getMembers()) {
            Member actualMember = carpool.getActualMember(member);
            if (actualMember == null) {
                carpool.add(member);
                actualMember = member;
            }

            boolean[] weekdayUsage = memberConfigurationsAdapter.getWeekdayUsageOf(actualMember);
            actualMember.addUsageAt(startDate, weekdayUsage);
        }

        try {
            DateTime yesterday = DateTime.now().withTimeAtStartOfDay().minusDays(1);
            if (startDate.isBefore(yesterday) || startDate.isEqual(yesterday)) {
                carpool.calculateTurnsUntil(yesterday);
            }
            dao.save(carpool);
        } catch (CarpoolDAOException e) {
            Log.d(CarpoolConfigurationActivity.class.getName(), e.getMessage(), e);
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            return;
        }

        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.putExtra(Constants.CARPOOL_NAME, carpool.getName());
        setResult(RESULT_OK, resultIntent);

        finish();
    }

    /**
     * Validates if all data has been entered correctly.
     *
     * @return
     */
    private boolean validateState() {
        String carpoolName = etName.getText().toString().trim();

        // When editing a carpool the carpool will be overwritten
        // Name can not be changed
        if (!isEditingCarpool) {
            if (!Carpool.isValidCarpoolName(carpoolName)) {
                etName.setError(getResources().getString(R.string.invalid_carpool_name));
                return false;
            } else {
                etName.setError(null);
            }

            List<String> carpoolNames = dao.listCarpoolNames();
            if (carpoolNames.contains(carpoolName)) {
                etName.setError(getResources().getString(R.string.carpool_already_exists));
                return false;
            } else {
                etName.setError(null);
            }
        }

        if (memberConfigurationsAdapter.getItemCount() < 2) {
            Toast.makeText(this, R.string.provide_two_members, Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    /**
     * Adds the member with the given {@link Uri} to the configuration.
     *
     * @param contactUri
     */
    private void addMember(Uri contactUri) {
        Member member = new Member(URI.create(contactUri.toString()));
        if (!memberConfigurationsAdapter.add(member)) {
            Toast.makeText(this, R.string.already_member_of_carpool, Toast.LENGTH_LONG).show();
            return;
        }

        final int index = memberConfigurationsAdapter.getItemCount() - 1;
        explainUsageOfPhoneContact(index);
    }

    @UiThread(delay = 1000)
    void explainUsageOfPhoneUser() {
        View itemView = rvMemberConfigurations.findViewHolderForAdapterPosition(0).itemView;
        Shape shape = new RectangleShape(itemView.getWidth(), itemView.getHeight());

        new MaterialShowcaseView.Builder(CarpoolConfigurationActivity.this)
                .setTarget(itemView)
                .setShape(shape)
                .setDismissOnTouch(true)
                .setContentText(R.string.you_should_select_days_you_use_carpool)
                .setDismissText(R.string.got_it)
                .singleUse(Constants.Notifications.SELECT_DAYS_OF_PHONE_USER.getId())
                .show();

        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @UiThread
    void explainUsageOfPhoneContact(int contactIndex) {
        RecyclerView.ViewHolder viewHolder = rvMemberConfigurations.findViewHolderForAdapterPosition(contactIndex);
        if (viewHolder == null) {
            return;
        }

        Shape shape = new RectangleShape(
                viewHolder.itemView.getWidth(),
                viewHolder.itemView.getHeight());

        new MaterialShowcaseView.Builder(CarpoolConfigurationActivity.this)
                .setTarget(viewHolder.itemView)
                .setShape(shape)
                .setDismissOnTouch(true)
                .setContentText(R.string.you_should_select_days_your_contacts_use_carpool)
                .setDismissText(R.string.got_it)
                .singleUse(Constants.Notifications.SELECT_DAYS_OF_CONTACT.getId())
                .show();
    }

    @UiThread
    void explainAddContacts(Member m) {
        new MaterialShowcaseView.Builder(CarpoolConfigurationActivity.this)
                .setTarget(abAddMember)
                .setDismissOnTouch(true)
                .setContentText(R.string.you_should_add_contacts)
                .setDismissText(R.string.got_it)
                .singleUse(Constants.Notifications.ADD_CONTACTS.getId())
                .show();
    }

    /**
     * Helper class which handles picking of a start date.
     */
    private class PickedStartDateListener implements DatePickerDialog.OnDateSetListener {

        @Override
        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
            DateTime date = new DateTime(year, monthOfYear + 1, dayOfMonth, 0, 0);
            synchronized (etPickStartDate) {
                startDate = date;
            }
            updateStartDateButtonText();
        }
    }

    private class PickedContactListener implements ContactPickerFragment.OnPickContactListener {

        @Override
        public void onFinish(Uri uri) {
            addMember(uri);
        }
    }
}
