/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.views;


import android.os.Build;

import org.junit.Before;
import org.junit.Test;
import org.robolectric.annotation.Config;

import java.util.Arrays;

import androidx.appcompat.widget.AppCompatCheckBox;
import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.ui.RobolectricTest;
import de.schrieveslaach.carpoolnizer.ui.views.MemberConfigurationView.OnConfigurationChangedListener;

import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.checkBoxIsChecked;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.robolectric.RuntimeEnvironment.application;


@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
public class MemberConfigurationViewTest extends RobolectricTest {

    private MemberConfigurationView mcv;

    @Before
    public void init() {
        mcv = new MemberConfigurationView(application);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailToDisplayUsage_InvalidArraySize() {
        mcv.display(new boolean[]{});
    }

    @Test
    public void shouldDisplayUsage_Weekend() {
        boolean[] usage = new boolean[]{
                false,
                false,
                false,
                false,
                false,
                true,
                true
        };

        mcv.display(usage);

        assertThat(mcv, not(checkBoxIsChecked(R.id.cb_monday)));
        assertThat(mcv, not(checkBoxIsChecked(R.id.cb_tuesday)));
        assertThat(mcv, not(checkBoxIsChecked(R.id.cb_wednesday)));
        assertThat(mcv, not(checkBoxIsChecked(R.id.cb_thursday)));
        assertThat(mcv, not(checkBoxIsChecked(R.id.cb_friday)));
        assertThat(mcv, checkBoxIsChecked(R.id.cb_saturday));
        assertThat(mcv, checkBoxIsChecked(R.id.cb_sunday));
    }

    @Test
    public void shouldGetWeekdayUsage_Default() {
        assertThatViewHasWeekdayUsage(
                true,
                true,
                true,
                true,
                true,
                false,
                false
        );
    }

    @Test
    public void shouldChangeConfiguration_Monday() {
        OnConfigurationChangedListener listener = mock(MemberConfigurationView.OnConfigurationChangedListener.class);
        mcv.setOnConfigurationChangedListener(listener);

        AppCompatCheckBox cb = mcv.findViewById(R.id.cb_monday);
        cb.setChecked(false);

        verify(listener).onConfigurationChanged(mcv);

        assertThatViewHasWeekdayUsage(
                false,
                true,
                true,
                true,
                true,
                false,
                false
        );
    }

    @Test
    public void shouldChangeConfiguration_Tuesday() {
        OnConfigurationChangedListener listener = mock(MemberConfigurationView.OnConfigurationChangedListener.class);
        mcv.setOnConfigurationChangedListener(listener);

        AppCompatCheckBox cb = mcv.findViewById(R.id.cb_tuesday);
        cb.setChecked(false);

        verify(listener).onConfigurationChanged(mcv);

        assertThatViewHasWeekdayUsage(
                true,
                false,
                true,
                true,
                true,
                false,
                false
        );
    }

    @Test
    public void shouldChangeConfiguration_Wednesday() {
        OnConfigurationChangedListener listener = mock(MemberConfigurationView.OnConfigurationChangedListener.class);
        mcv.setOnConfigurationChangedListener(listener);

        AppCompatCheckBox cb = mcv.findViewById(R.id.cb_wednesday);
        cb.setChecked(false);

        verify(listener).onConfigurationChanged(mcv);

        assertThatViewHasWeekdayUsage(
                true,
                true,
                false,
                true,
                true,
                false,
                false
        );
    }

    @Test
    public void shouldChangeConfiguration_Thursday() {
        OnConfigurationChangedListener listener = mock(MemberConfigurationView.OnConfigurationChangedListener.class);
        mcv.setOnConfigurationChangedListener(listener);

        AppCompatCheckBox cb = mcv.findViewById(R.id.cb_thursday);
        cb.setChecked(false);

        verify(listener).onConfigurationChanged(mcv);

        assertThatViewHasWeekdayUsage(
                true,
                true,
                true,
                false,
                true,
                false,
                false
        );
    }

    @Test
    public void shouldChangeConfiguration_Friday() {
        OnConfigurationChangedListener listener = mock(MemberConfigurationView.OnConfigurationChangedListener.class);
        mcv.setOnConfigurationChangedListener(listener);

        AppCompatCheckBox cb = mcv.findViewById(R.id.cb_friday);
        cb.setChecked(false);

        verify(listener).onConfigurationChanged(mcv);

        assertThatViewHasWeekdayUsage(
                true,
                true,
                true,
                true,
                false,
                false,
                false
        );
    }

    @Test
    public void shouldChangeConfiguration_Saturday() {
        OnConfigurationChangedListener listener = mock(MemberConfigurationView.OnConfigurationChangedListener.class);
        mcv.setOnConfigurationChangedListener(listener);

        AppCompatCheckBox cb = mcv.findViewById(R.id.cb_saturday);
        cb.setChecked(true);

        verify(listener).onConfigurationChanged(mcv);

        assertThatViewHasWeekdayUsage(
                true,
                true,
                true,
                true,
                true,
                true,
                false
        );
    }

    @Test
    public void shouldChangeConfiguration_Sunday() {
        OnConfigurationChangedListener listener = mock(MemberConfigurationView.OnConfigurationChangedListener.class);
        mcv.setOnConfigurationChangedListener(listener);

        AppCompatCheckBox cb = mcv.findViewById(R.id.cb_sunday);
        cb.setChecked(true);

        verify(listener).onConfigurationChanged(mcv);

        assertThatViewHasWeekdayUsage(
                true,
                true,
                true,
                true,
                true,
                false,
                true
        );
    }

    private void assertThatViewHasWeekdayUsage(boolean... weekdayUsage) {
        assertThat(Arrays.asList(mcv.getWeekdayUsage()), contains(weekdayUsage));
    }
}
