/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.repository.resolver;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.repository.CursorDataCache;

/**
 * {@ölink DataResolver} for contact pictures.
 */
public class ContactPictureResolver implements CursorDataCache.DataResolver<Bitmap> {

    @Override
    public Bitmap loadFrom(Cursor cursor, int columnIndex) {
        byte[] data = cursor.getBlob(columnIndex);
        return BitmapFactory.decodeByteArray(data, 0, data.length);
    }

    @Override
    public Bitmap getAlternativeData(Context context) {
        return BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_contact_picture);
    }
}