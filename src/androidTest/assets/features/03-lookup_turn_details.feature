Feature: Lookup details for archived turns
  This feature allows to lookup details of archived turns which are older than a number of months
  
  Scenario Outline: Scroll to the first turn, display the archived turns and lookup some details
    Given I started the app
    And I opened the carpool <carpool>
    And I scrolled to the first turn
    When I click on the load archived turns button
    Then I should see the turns older than <months> months beginning from first day of month
    When I scrolled to the first turn
    And I clicked on first archived turn older than <months> months with contact <driver> as driver
    Then I should see contact <driver> as driver
    And I should see passengers <passengers>
    And I should see remaining members <remaining_members>
    

  Examples:
    | carpool             | months | driver | passengers        | remaining_members |
    | A long term carpool | 2      | Me     | Daisy, Mike, John |                   |
    | A long term carpool | 2      | Daisy  | Me, Mike, John    |                   |
    | A long term carpool | 2      | Mike   | Daisy, Me, John   |                   |
    | A long term carpool | 2      | John   | Daisy, Mike, Me   |                   |
    | A long term carpool | 2      | -      |                   | Daisy, Mike, John |        
