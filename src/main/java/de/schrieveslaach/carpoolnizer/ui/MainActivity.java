/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialog;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.widget.ToolbarWidgetWrapper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormatter;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import de.schrieveslaach.carpoolnizer.Alarms;
import de.schrieveslaach.carpoolnizer.Constants;
import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.model.Carpool;
import de.schrieveslaach.carpoolnizer.model.Turn;
import de.schrieveslaach.carpoolnizer.model.settings.UsageNotification;
import de.schrieveslaach.carpoolnizer.repository.CarpoolDAO;
import de.schrieveslaach.carpoolnizer.repository.CarpoolDAOException;
import de.schrieveslaach.carpoolnizer.ui.adapters.TurnAdapter;
import de.schrieveslaach.carpoolnizer.ui.adapters.TurnViewHolder;
import de.schrieveslaach.carpoolnizer.ui.fragments.CarpoolInformationFragment;
import de.schrieveslaach.carpoolnizer.ui.fragments.ConfirmFragment;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.shape.CircleShape;
import uk.co.deanwild.materialshowcaseview.shape.Shape;

import static de.schrieveslaach.carpoolnizer.Constants.getFullDateFormatter;
import static de.schrieveslaach.carpoolnizer.Constants.getPredictionDate;
import static de.schrieveslaach.carpoolnizer.Constants.getShortTimeDateFormatter;
import static org.apache.commons.lang3.exception.ExceptionUtils.getRootCauseMessage;

/**
 * Main {@link Activity} of the app.
 */
@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    /**
     * Saves and load carpools from the persistent memory.
     */
    private CarpoolDAO dao = new CarpoolDAO(this);

    /**
     * {@link LinearLayoutManager} for the {@link MainActivity#rvTurns}.
     */
    private LinearLayoutManager llmTurns;

    @ViewById(R.id.rv_turns)
    RecyclerView rvTurns;

    @ViewById(R.id.ab_carpool_information)
    FloatingActionButton abCarpoolInformation;

    @ViewById(R.id.nav_view)
    NavigationView navigationView;

    @ViewById(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;

    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    private TurnAdapter turnAdapter;

    private Carpool displayedCarpool;

    private final BiMap<Integer, String> menuIdAndCarpoolNames = HashBiMap.create();

    /**
     * {@link DateTime} when the user started the activity. Required to update the carpool after a
     * certain time
     *
     * @see #onStart()
     */
    private DateTime lastStartUp;

    /**
     * @see TurnContextActionModeCallback
     */
    private ActionMode turnActionMode;

    /**
     * Pending user notification. Will be executed in {@link #onCreateOptionsMenu(Menu)}.
     */
    private Runnable pendingUserNotification;

    @AfterViews
    protected void afterViews() {
        setSupportActionBar(toolbar);

        // init recyclerview for turns
        llmTurns = new LinearLayoutManager(this);
        rvTurns.setLayoutManager(llmTurns);

        turnAdapter = new TurnAdapter(this);
        turnAdapter.setOnTurnClickListener(new ShowDetailsOfTurnListener());
        turnAdapter.setOnLoadArchivedTurnsListener(new LoadArchivedTurnsListener());
        turnAdapter.setOnContactInformationListener(new ShowContactInformationListener(this));

        rvTurns.setAdapter(turnAdapter);

        initializeNavigation();
    }

    /**
     * Initializes the slider menu/navigation bar for e.g. selecting {@link Carpool carpools}.
     */
    private void initializeNavigation() {
        drawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                null,
                R.string.app_name,
                R.string.app_name
        );

        drawerLayout.setDrawerListener(drawerToggle);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        navigationView.setNavigationItemSelectedListener(new MainNavigationListener());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);

        // Run pending notifications
        if (pendingUserNotification != null) {
            new Handler().postDelayed(pendingUserNotification, 500);
            pendingUserNotification = null;
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.mi_create_carpool:
                createCarpool();
                return true;
            case R.id.mi_today:
                scrollToTurnOfToday();
                return true;
            case R.id.mi_delete_carpool:
                deleteDisplayedCarpool();
                return true;
            case R.id.mi_edit_carpool:
                editDisplayedCarpool();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();

        DateTime now = DateTime.now();
        try {
            Intent intent = getIntent();
            if (intent.hasExtra(Constants.CARPOOL_NAME)) {
                loadAndDisplayCarpoolNames();
                loadAndDisplayCarpool(intent.getStringExtra(Constants.CARPOOL_NAME), null);
                return;
            }

            if (displayedCarpool != null && lastStartUp != null && 15 > new Duration(lastStartUp, now).getStandardMinutes()) {
                return;
            }

            loadAndDisplayAfterStart();
        } finally {
            lastStartUp = now;
        }
    }

    /**
     * Find the menu entry of {@link R.id#mi_create_carpool}
     *
     * @return
     */
    private View findCreateCarpoolMenu() {
        View createCarpoolMenu = findViewById(R.id.mi_create_carpool);
        if (createCarpoolMenu != null) {
            return createCarpoolMenu;
        }

        return findOverflowMenuButton((ViewGroup) getWindow().getDecorView());
    }

    private View findOverflowMenuButton(ViewGroup vg) {
        for (int i = 0; i < vg.getChildCount(); ++i) {
            View child = vg.getChildAt(i);

            if (child instanceof ViewGroup) {
                View view = findOverflowMenuButton((ViewGroup) child);
                if (view != null) {
                    return view;
                }
            } else {
                if (child.getClass().getSimpleName().endsWith("OverflowMenuButton")) {
                    return child;
                }
            }
        }
        return null;
    }

    /**
     * Creates a {@link #pendingUserNotification} with a {@link MaterialShowcaseView} which shows that no
     * carpool exists.
     */
    private void createCreateCarpoolNotification() {
        pendingUserNotification = () -> {
            View createCarpoolMenu = findCreateCarpoolMenu();

            if (createCarpoolMenu == null) {
                return;
            }

            new MaterialShowcaseView.Builder(MainActivity.this)
                    .setTarget(createCarpoolMenu)
                    .setDismissOnTouch(true)
                    .setContentText(R.string.you_should_create_a_carpool)
                    .setDismissText(R.string.got_it)
                    .singleUse(Constants.Notifications.CREATE_CARPOOL.getId())
                    .show();
        };
    }

    /**
     * Shows a {@link MaterialShowcaseView} to the user explaining the turn overview
     */
    @UiThread(delay = 1000)
    protected void explainTurnOverview() {
        RecyclerView.ViewHolder viewHolder = rvTurns.findViewHolderForAdapterPosition(2);
        if (viewHolder == null) {
            return;
        }

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        Shape shape = new CircleShape((int) (Math.min(metrics.widthPixels, metrics.heightPixels) * 0.3));

        new MaterialShowcaseView.Builder(MainActivity.this)
                .setTarget(viewHolder.itemView)
                .setShape(shape)
                .setDismissOnTouch(true)
                .setContentText(R.string.explanation_turn_overview)
                .setDismissText(R.string.got_it)
                .singleUse(Constants.Notifications.EXPLAIN_TURN_OVERVIEW.getId())
                .show();
    }

    /**
     * Shows a {@link MaterialShowcaseView} to the user explaining how to switch among carpools
     */
    private void explainSwitchAmongCarpools() {
        if (dao.listCarpoolNames().size() < 2) {
            return;
        }

        new MaterialShowcaseView.Builder(this)
                .setTarget(getNavigationIconView())
                .setDismissOnTouch(true)
                .setContentText(R.string.explanation_to_switch_among_carpools)
                .setDismissText(R.string.got_it)
                .singleUse(Constants.Notifications.EXPLAIN_SWITCH_AMONG_CARPOOLS.getId())
                .show();
    }

    /**
     * Returns the {@link View} containing the application navigation icon view.
     *
     * @return
     */
    private View getNavigationIconView() {
        try {
            ActionBar actionBar = getSupportActionBar();
            Field toolbarField = actionBar.getClass().getDeclaredField("mDecorToolbar");
            toolbarField.setAccessible(true);
            ToolbarWidgetWrapper tww = (ToolbarWidgetWrapper) toolbarField.get(actionBar);

            toolbarField = tww.getClass().getDeclaredField("mToolbar");
            toolbarField.setAccessible(true);
            androidx.appcompat.widget.Toolbar tb = (androidx.appcompat.widget.Toolbar) toolbarField.get(tww);

            return tb.getChildAt(1);
        } catch (Exception ex) {
            Log.e(getClass().getSimpleName(), ex.toString());
            throw new IllegalStateException("This should never happen: getting navigation icon view", ex);
        }
    }

    private void createCarpool() {
        Intent intent = new Intent(this, CarpoolConfigurationActivity_.class);
        startActivityForResult(intent, Constants.CONFIGURE_CARPOOL_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == Constants.CONFIGURE_CARPOOL_REQUEST) {
            String carpoolName = data.getStringExtra(Constants.CARPOOL_NAME);
            loadAndDisplayCarpoolNames();
            loadAndDisplayCarpool(carpoolName, null);
            explainTurnOverview();
            explainSwitchAmongCarpools();
        } else if (requestCode == Constants.DISPLAY_TURN_DETAILS) {
            String carpoolName = data.getStringExtra(Constants.CARPOOL_NAME);
            DateTime turnDate = getFullDateFormatter(this).parseDateTime(data.getStringExtra(Constants.TURN_DATE));
            loadAndDisplayCarpool(carpoolName, turnDate);
        }
    }

    private void loadAndDisplayAfterStart() {
        clearData();

        List<String> carpoolNames = loadAndDisplayCarpoolNames();

        if (carpoolNames.isEmpty()) {
            createCreateCarpoolNotification();
        } else {
            Carpool carpool = loadCarpoolAndCalculateTurns(null);
            if (carpool != null) {
                display(carpool, null);
            } else {
                loadAndDisplayCarpool(carpoolNames.get(0), null);
            }
        }
    }

    /**
     * Clears all data
     */
    private void clearData() {
        turnAdapter.setTurns(
                Arrays.<Turn>asList(),
                Arrays.<Turn>asList()
        );

        Menu menu = navigationView.getMenu();
        menu.removeGroup(R.id.nav_carpools_group);
        if (menu.size() >= 2) {
            menu.getItem(1).getSubMenu().removeGroup(R.id.nav_carpools_group);
        }
        menuIdAndCarpoolNames.clear();

        setTitle(R.string.app_name);
        getSupportActionBar().setSubtitle(null);

        displayedCarpool = null;
    }

    /**
     * Loads all available carpool names from the {@link MainActivity#dao DAO}.
     */
    private List<String> loadAndDisplayCarpoolNames() {
        List<String> carpoolNames = dao.listCarpoolNames();

        Menu menu = navigationView.getMenu();
        SubMenu subMenu;
        if (menu.size() >= 2) {
            subMenu = menu.getItem(1).getSubMenu();
        } else {
            subMenu = menu.addSubMenu(R.string.carpools);
        }

        subMenu.removeGroup(R.id.nav_carpools_group);
        menuIdAndCarpoolNames.clear();

        subMenu.setGroupCheckable(R.id.nav_carpools_group, true, true);
        for (int i = 0; i < carpoolNames.size(); ++i) {
            int menuId = View.generateViewId();
            String carpoolName = carpoolNames.get(i);

            subMenu.add(R.id.nav_carpools_group, menuId, i, carpoolName);
            menuIdAndCarpoolNames.put(menuId, carpoolName);
        }

        return carpoolNames;
    }

    /**
     * Loads the {@link Carpool} with the given name and {@link MainActivity#display(Carpool, DateTime) displays} it.
     *
     * @param carpoolName
     * @param turnDate    The {@link DateTime} of the {@link Turn turn} to be displayed.
     */
    private void loadAndDisplayCarpool(String carpoolName, DateTime turnDate) {
        Carpool carpool = loadCarpoolAndCalculateTurns(carpoolName);
        if (carpool != null) {
            display(carpool, turnDate);
        }
    }

    private Carpool loadCarpoolAndCalculateTurns(String carpoolName) {
        Carpool carpool;
        try {
            if (carpoolName != null) {
                carpool = dao.loadCarpool(carpoolName);
            } else {
                carpool = dao.loadLastUsedCarpool();
            }

            if (carpool == null) {
                return null;
            }

            DateTime yesterday = DateTime.now().withTimeAtStartOfDay().minusDays(1);
            List<Turn> turns = carpool.calculateTurnsUntil(yesterday);

            if (!turns.isEmpty()) {
                dao.save(carpool);
            }
        } catch (CarpoolDAOException e) {
            Log.e(MainActivity.class.getName(), "Could not load carpool: " + carpoolName, e);
            showFailingLoadingOfCarpool(e);
            return null;
        }

        return carpool;
    }

    private void showFailingLoadingOfCarpool(CarpoolDAOException ex) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.could_not_load_carpool)
                .setPositiveButton(android.R.string.ok, null)
                .setMessage(getRootCauseMessage(ex))
                .show();
    }

    /**
     * Displays the {@link Carpool} in the current {@code MainActivity}.
     *
     * @param carpool
     * @param turnDate The {@link DateTime} of the {@link Turn turn} to be displayed.
     */
    void display(Carpool carpool, DateTime turnDate) {
        setTitle(carpool.getName());
        getSupportActionBar().setSubtitle(R.string.app_name);

        ImmutableList<Turn> turnsInThePast = carpool.getTurnsInThePast();
        List<Turn> turns = carpool.predictTurnsUntil(getPredictionDate());
        turnAdapter.setTurns(turnsInThePast, turns);
        turnAdapter.setShowLoadArchivedTurnsButton(dao.hasArchiveFor(carpool));

        int turnIndex = turnDate != null ? turnAdapter.getIndexOfTurn(turnDate) : turnAdapter.getFirstPredictedTurnIndex();
        llmTurns.scrollToPositionWithOffset(turnIndex, 0);

        displayedCarpool = carpool;

        if (!menuIdAndCarpoolNames.isEmpty()) {
            navigationView.getMenu().findItem(menuIdAndCarpoolNames.inverse().get(carpool.getName())).setChecked(true);
        }

        try {
            showUsageNotification(dao.loadUsageNotification(carpool));
        } catch (CarpoolDAOException e) {
            showCarpoolDOAException(e, "Could not load carpool notification");
        }
    }

    /**
     * Displays information of the {@link #displayedCarpool currently displayed carpool}.
     */
    @Click(R.id.ab_carpool_information)
    protected void showCarpoolInformation() {
        if (displayedCarpool == null) {
            return;
        }

        CarpoolInformationFragment cif = new CarpoolInformationFragment();
        cif.setCarpool(displayedCarpool);
        getSupportFragmentManager()
                .beginTransaction()
                .add(cif, "carpoolInformation")
                .commit();
    }

    /**
     * Scrolls to the {@link Turn} of today
     */
    private void scrollToTurnOfToday() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        int index = turnAdapter.getIndexOfTurn(today);
        rvTurns.scrollToPosition(index);
    }

    /**
     * Starts the edit the displayed carpool
     */
    private void editDisplayedCarpool() {
        if (displayedCarpool == null) {
            return;
        }

        Intent intent = new Intent(this, CarpoolConfigurationActivity_.class);
        intent.putExtra(Constants.CARPOOL_NAME, displayedCarpool.getName());
        startActivityForResult(intent, Constants.CONFIGURE_CARPOOL_REQUEST);
    }

    /**
     * Deletes the {@link MainActivity#displayedCarpool currently displayed carpool}
     */
    private void deleteDisplayedCarpool() {
        if (displayedCarpool == null) {
            return;
        }

        ConfirmFragment cf = new ConfirmFragment();
        cf.setConfirmationTitle(R.string.delete);
        cf.setConfirmationMessage(R.string.delete_confirmation);
        cf.setOnConfirmListener(new DeleteConfirmationListener());
        getSupportFragmentManager()
                .beginTransaction()
                .add(cf, "deleteConfirmation")
                .commit();
    }

    /**
     * {@link Carpool#cancel(DateTime) Cancels} or {@link Carpool#resume(DateTime) resumes} the given {@link Turn turn}.
     *
     * @param turn
     */
    private void toggleCancelStatus(Turn turn) {
        if (turn.isCanceled()) {
            displayedCarpool.resume(turn.getDate());
        } else {
            displayedCarpool.cancel(turn.getDate());
        }

        try {
            dao.save(displayedCarpool);
            display(displayedCarpool, turn.getDate());
        } catch (CarpoolDAOException e) {
            showCarpoolDOAException(e, "could not save carpool");
        }
    }

    /**
     * Gets the currently displayed carpool.
     *
     * @return
     */
    Carpool getDisplayedCarpool() {
        return displayedCarpool;
    }

    /**
     * Checks if the {@link ActionMode} is activated.
     *
     * @return
     */
    public boolean isActionModeActivated() {
        return turnActionMode != null;
    }

    private void showUsageNotification(UsageNotification notification) {
        MenuItem menuItem = navigationView.getMenu().findItem(R.id.nav_mi_notification_settings);
        if (notification != null) {
            DateTimeFormatter formatter = getShortTimeDateFormatter(this);
            menuItem.setTitle(getResources().getString(R.string.notifaction_at, formatter.print(
                    DateTime.now()
                            .withHourOfDay(notification.getPeriod().getHours())
                            .withMinuteOfHour(notification.getPeriod().getMinutes())
            )));
        } else {
            menuItem.setTitle(getResources().getString(R.string.no_notification));
        }
    }

    private void saveUsageNotification(int hour, int minute) {
        UsageNotification notification = new UsageNotification(hour, minute);
        try {
            dao.saveUsageNotifcation(displayedCarpool, notification);
            new Alarms().setAlarm(this, displayedCarpool, notification);
            showUsageNotification(notification);
        } catch (CarpoolDAOException e) {
            showCarpoolDOAException(e, "Could not save notification");
        }
    }

    /**
     * {@link NavigationView.OnNavigationItemSelectedListener Listener} for e.g. changing the carpools
     * or setting a notification.
     */
    private class MainNavigationListener implements NavigationView.OnNavigationItemSelectedListener {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            if (item.getGroupId() == R.id.nav_carpools_group) {
                handleCarpoolNameMenuItem(item);
                return true;
            }

            if (item.getItemId() == R.id.nav_mi_notification_settings) {
                setNotificationForSelectedCarpool();
                return true;
            }

            return false;
        }

        private void handleCarpoolNameMenuItem(@NonNull MenuItem item) {
            for (int menuId : menuIdAndCarpoolNames.keySet()) {
                navigationView.getMenu().findItem(menuId).setChecked(false);
            }

            item.setChecked(true);
            String carpoolName = menuIdAndCarpoolNames.get(item.getItemId());
            loadAndDisplayCarpool(carpoolName, null);
            drawerLayout.closeDrawers();
        }


        private void setNotificationForSelectedCarpool() {
            UsageNotification notification;
            try {
                notification = dao.loadUsageNotification(displayedCarpool);
            } catch (CarpoolDAOException e) {
                Log.e(MainActivity.class.getSimpleName(), "could not load notifiaction", e);
                return;
            }

            TimePickerDialog dialog = TimePickerDialog.newInstance(
                    (view, hourOfDay, minute, second) -> {
                        view.dismiss();
                        saveUsageNotification(hourOfDay, minute);
                    },
                    notification != null ? notification.getPeriod().getHours() : 7,
                    notification != null ? notification.getPeriod().getMinutes() : 0,
                    0,
                    true
            );

            dialog.show(getSupportFragmentManager(), "TimePickerDialog");
        }
    }

    /**
     * {@link TurnAdapter.OnTurnClickListener Listener} for showing details of a {@link Turn turn}.
     */
    private class ShowDetailsOfTurnListener implements TurnAdapter.OnTurnClickListener {

        @Override
        public void onClick(Turn turn) {
            if (isActionModeActivated()) {
                turnActionMode.finish();
            }

            Intent intent = new Intent(MainActivity.this, TurnDetailsActivity_.class);
            intent.putExtra(Constants.TURN_DATE, getFullDateFormatter(MainActivity.this).print(turn.getDate()));
            intent.putExtra(Constants.CARPOOL_NAME, displayedCarpool.getName());
            startActivityForResult(intent, Constants.DISPLAY_TURN_DETAILS);
        }

        @Override
        public void onLongClick(Turn turn) {
            if (isActionModeActivated()) {
                turnActionMode.finish();
            }

            turnActionMode = startSupportActionMode(new TurnContextActionModeCallback(turn));
        }
    }

    /**
     * {@link TurnAdapter.OnLoadArchivedTurnsListener Listener} for loading archived turns
     */
    private class LoadArchivedTurnsListener implements TurnAdapter.OnLoadArchivedTurnsListener {

        @Override
        public void onLoadArchivedTurns() {
            try {
                RecyclerView.ViewHolder vh = rvTurns.findViewHolderForAdapterPosition(0);
                View buLoadArchivedTurns = vh.itemView.findViewById(R.id.bu_load_archived_turns);

                Interval archiveInterval = dao.loadArchive(displayedCarpool);
                ImmutableList<Turn> archivedTurns = displayedCarpool.getTurnsInThePast(archiveInterval);

                turnAdapter.addArchivedTurns(archivedTurns);
                turnAdapter.setShowLoadArchivedTurnsButton(dao.hasArchiveFor(displayedCarpool));

                llmTurns.scrollToPositionWithOffset(turnAdapter.getIndexOfTurn(archiveInterval.getEnd()), buLoadArchivedTurns.getHeight());
            } catch (CarpoolDAOException e) {
                showCarpoolDOAException(e, "Could not load archived data of " + displayedCarpool.getName());
            }
        }
    }

    private void showCarpoolDOAException(CarpoolDAOException e, String message) {
        Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        Log.e(MainActivity.class.getName(), message, e);
    }

    /**
     * {@link ConfirmFragment.OnConfirmListener} for deleting carpools
     */
    private class DeleteConfirmationListener implements ConfirmFragment.OnConfirmListener {

        @Override
        public void onFinish() {
            dao.delete(displayedCarpool);
            displayedCarpool = null;
            loadAndDisplayAfterStart();
        }
    }

    /**
     * {@link ActionMode.Callback} to handle context events on turns.
     */
    private class TurnContextActionModeCallback implements ActionMode.Callback {

        private final Turn turn;

        private TurnViewHolder turnViewHolder;

        private TurnContextActionModeCallback(Turn turn) {
            this.turn = turn;
            this.turnViewHolder = (TurnViewHolder) rvTurns.findViewHolderForAdapterPosition(turnAdapter.getIndexOfTurn(turn.getDate()));
            turnViewHolder.setSelected(true);
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.main_context, menu);
            DateTimeFormatter formatter = Constants.getFullDateFormatter(MainActivity.this);
            mode.setTitle(formatter.print(turn.getDate()));
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            if (item.getItemId() == R.id.mi_cancel_turn) {
                toggleCancelStatus(turn);
                mode.finish();
                return true;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            turnViewHolder.setSelected(false);
            turnActionMode = null;
        }
    }
}
