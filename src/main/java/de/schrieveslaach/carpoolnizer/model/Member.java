/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.Lists;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.util.Arrays;
import java.util.Map;
import java.util.NavigableMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Represents the a member of a carpool.
 * <p/>
 * Created by marc on 18.08.14.
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@mem-id")
public class Member implements Serializable {

    private static final int WEEKDAYS = 7;

    /**
     * The uri of the contact in the address book of the user.
     */
    @JsonProperty("uri")
    private final URI uri;

    /**
     * Models on which days in the week the {@code Member} uses a carpool. The {@link DateTime}
     * marks the start of a using period.
     */
    @JsonDeserialize(using = UsageDeserializer.class)
    @JsonSerialize(using = UsageSerializer.class)
    @JsonProperty("carpoolUsage")
    private final NavigableMap<DateTime, boolean[]> carpoolUsage;

    @JsonProperty("exceptionalUsage")
    private final SortedSet<ExceptionalUsage> exceptionalUsage;

    /**
     * Creates a new instance of {@code Member}.
     *
     * @param uri
     */
    @JsonCreator
    public Member(@JsonProperty("uri") URI uri) {
        this.uri = uri;
        this.carpoolUsage = new TreeMap<>();
        this.exceptionalUsage = new TreeSet<>();
    }

    /**
     * Gets the {@link URI} of the contact in the address book of the user.
     *
     * @return
     */
    @JsonIgnore
    public URI getUri() {
        return uri;
    }

    /**
     * Returns if the current {@code Member} is the user of the phone.
     *
     * @return
     */
    @JsonIgnore
    public boolean isPhoneUser() {
        return getUri() == null;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(uri)
                .build();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Member)) {
            return false;
        }

        Member other = (Member) o;
        return new EqualsBuilder()
                .append(uri, other.uri)
                .isEquals();
    }

    @Override
    public String toString() {
        return uri != null ? uri.toString() : "phone user";
    }

    /**
     * Defines the usage in a week for the current {@code Member}. The given {@link DateTime} is the
     * start of the given usage.
     *
     * @param date
     * @param weekdayUsage An array of length seven. First element determines the usage on Monday, second
     *                     on Tuesday, etc.
     */
    public void addUsageAt(DateTime date, boolean[] weekdayUsage) {
        if (date == null) {
            throw new IllegalArgumentException("date must not be null!");
        }

        if (weekdayUsage == null || weekdayUsage.length != WEEKDAYS) {
            throw new IllegalArgumentException("Provide an array of length 7!");
        }

        carpoolUsage.put(date.withTimeAtStartOfDay(), weekdayUsage);
    }

    /**
     * Returns the weekday usage which is defined by
     * {@link Member#addUsageAt(org.joda.time.DateTime, boolean[])}.
     *
     * @param date
     * @return
     */
    public boolean[] getWeekdayUsage(DateTime date) {
        if (date == null) {
            throw new IllegalArgumentException("date must not be null!");
        }

        Map.Entry<DateTime, boolean[]> usageEntry = carpoolUsage.floorEntry(date);
        if (usageEntry == null) {
            return new boolean[]{false, false, false, false, false, false, false};
        }

        return Arrays.copyOf(usageEntry.getValue(), WEEKDAYS);
    }

    /**
     * Returns how the {@code Member} wants to {@link Usage use} a {@link Carpool} an the given
     * {@link DateTime}.
     *
     * @param date
     * @return
     */
    public Usage getUsageAt(DateTime date) {
        Usage usage = getExceptionalUsage(date);
        if (usage != null) {
            return usage;
        }

        Map.Entry<DateTime, boolean[]> usageEntry = carpoolUsage.floorEntry(date);

        if (usageEntry == null) {
            return Usage.DOES_NOT_USE;
        }

        boolean uses = usageEntry.getValue()[date.dayOfWeek().get() - 1];
        return uses ? Usage.NORMAL : Usage.DOES_NOT_USE;
    }

    /**
     * Looks up the {@link ExceptionalUsage usage} which
     * {@link ExceptionalUsage#matches(DateTime) matches} the given {@link DateTime}.
     *
     * @param date
     * @return
     */
    private Usage getExceptionalUsage(DateTime date) {
        for (ExceptionalUsage eu : exceptionalUsage) {
            if (eu.matches(date)) {
                return eu.getUsage();
            }
        }
        return null;
    }

    /**
     * Returns the first {@link DateTime date} which defines the usage of the carpool.
     *
     * @return
     */
    @JsonIgnore
    DateTime getFirstUsageDateTime() {
        if (carpoolUsage.isEmpty()) {
            return null;
        }

        return carpoolUsage.firstEntry().getKey();
    }

    /**
     * Defines {@link Usage exceptional usage} at the given {@code date}.
     *
     * @param date
     * @param usage
     */
    public void addExceptionalUsage(DateTime date, Usage usage) {
        DateTime start = date.withTimeAtStartOfDay();
        addExceptionalUsage(new Interval(start, start), usage);
    }

    /**
     * Defines {@link Usage exceptional usage} at the given {@link Interval}.
     *
     * @param interval {@code [start, end[} of time period.
     * @param usage
     */
    void addExceptionalUsage(Interval interval, Usage usage) {
        DateTime start = interval.getStart().withTimeAtStartOfDay();
        DateTime end = interval.getEnd().withTimeAtStartOfDay().plusDays(1);
        interval = new Interval(start, end);

        if (hasExactMatchingExceptionalUsage(interval)) {
            replaceExceptionalUsage(interval, usage);
        } else if (hasNoOverlappingExceptionalUsage(interval)) {
            exceptionalUsage.add(new ExceptionalUsage(interval, usage));
        } else {
            throw new IllegalArgumentException("For the given interval exists already an exceptional usage rule which overlaps with given interval.");
        }
    }

    /**
     * Looks up if the {@code Member} has any {@link ExceptionalUsage} matching exactly the given
     * {@link Interval}.
     *
     * @param interval
     * @return
     */
    private boolean hasExactMatchingExceptionalUsage(Interval interval) {
        for (ExceptionalUsage eu : exceptionalUsage) {
            if (eu.matches(interval)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Looks up if the {@code Member} has any {@link ExceptionalUsage} overlapping the given
     * {@link Interval}.
     *
     * @param interval
     * @return
     */
    private boolean hasNoOverlappingExceptionalUsage(Interval interval) {
        for (ExceptionalUsage eu : exceptionalUsage) {
            if (eu.overlaps(interval)) {
                return false;
            }
        }
        return true;
    }

    private void replaceExceptionalUsage(Interval interval, Usage usage) {
        for (ExceptionalUsage eu : exceptionalUsage) {
            if (eu.matches(interval)) {
                eu.setUsage(usage);
            }
        }
    }

    /**
     * Checks if the {@code Member} has any exceptional usage for the given date.
     *
     * @param date
     * @return
     */
    public boolean hasExceptionalUsage(DateTime date) {
        return getExceptionalUsage(date) != null;
    }

    /**
     * Archives all data which is older than the given {@link DateTime dateTime}. All data which
     * overlaps the {@link DateTime dateTime} will be split.
     *
     * @param dateTime
     * @return A {@code Member} which contains the archived data.
     */
    Member archiveDataOlderThan(DateTime dateTime) {
        Member archivedMember = new Member(uri != null ? URI.create(uri.toString()) : null);

        archiveCarpoolUsageInto(archivedMember, dateTime);
        archiveExceptionalUsageInto(archivedMember, dateTime);

        return archivedMember;
    }

    private void archiveCarpoolUsageInto(final Member archivedMember, final DateTime dateTime) {
        boolean[] usage = null;
        for (DateTime d : Lists.newArrayList(carpoolUsage.navigableKeySet())) {
            if (!d.isBefore(dateTime)) {
                break;
            }

            usage = carpoolUsage.remove(d);
            archivedMember.addUsageAt(d, usage);
        }

        if (usage != null) {
            // current member now use from this day the carpool
            addUsageAt(dateTime, usage);

            // archived member does not use the carpool anymore
            archivedMember.addUsageAt(
                    dateTime,
                    new boolean[]{false, false, false, false, false, false, false}
            );
        }
    }

    private void archiveExceptionalUsageInto(final Member archivedMember, final DateTime dateTime) {
        for (ExceptionalUsage eu : Lists.newArrayList(exceptionalUsage)) {
            if (!eu.getInterval().getStart().isBefore(dateTime)) {
                break;
            }

            exceptionalUsage.remove(eu);

            // check if the exceptional usage needs to be splitted
            if (eu.matches(dateTime)) {
                DateTime start = eu.getInterval().getStart();
                DateTime end = eu.getInterval().getEnd().minusDays(1);

                archivedMember.addExceptionalUsage(new Interval(start, dateTime.minusDays(1)), eu.getUsage());
                addExceptionalUsage(new Interval(dateTime, end), eu.getUsage());
            } else {
                archivedMember.exceptionalUsage.add(eu);
            }
        }
    }

    /**
     * Helper class to deserialize {@link Member#carpoolUsage}.
     */
    private static class UsageDeserializer extends JsonDeserializer<Map<DateTime, boolean[]>> {

        @Override
        public Map<DateTime, boolean[]> deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
            Map<DateTime, boolean[]> usage = new TreeMap<>();

            final TreeNode treeNode = p.readValueAsTree();
            for (int i = 0; i < treeNode.size(); ++i) {
                TreeNode child = treeNode.get(i);

                String field = child.fieldNames().next();
                DateTime dateTime = Constants.DATE_TIME_FORMATTER.parseDateTime(field);

                boolean[] u = new boolean[WEEKDAYS];
                TreeNode array = child.path(field);
                for (int j = 0; j < WEEKDAYS; ++j) {
                    u[j] = Boolean.parseBoolean(String.valueOf(array.get(j)));
                }

                usage.put(dateTime, u);
            }

            return usage;
        }
    }

    /**
     * Helper class to serialize {@link Member#carpoolUsage}.
     */
    private static class UsageSerializer extends JsonSerializer<Map<DateTime, boolean[]>> {


        @Override
        public void serialize(Map<DateTime, boolean[]> value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            gen.writeStartArray();

            for (Map.Entry<DateTime, boolean[]> e : value.entrySet()) {
                gen.writeStartObject();

                gen.writeFieldName(Constants.DATE_TIME_FORMATTER.print(e.getKey()));
                serializers.defaultSerializeValue(e.getValue(), gen);

                gen.writeEndObject();
            }

            gen.writeEndArray();
        }
    }
}
