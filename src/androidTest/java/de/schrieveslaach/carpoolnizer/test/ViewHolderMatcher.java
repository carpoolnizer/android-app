/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.test;

import androidx.recyclerview.widget.RecyclerView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.util.Arrays;
import java.util.List;

/**
 * {@link Matcher} for {@link RecyclerView.ViewHolder view holders}. It also returns the position index.
 */
public class ViewHolderMatcher extends TypeSafeMatcher<RecyclerView.ViewHolder> {

    private final List<Matcher<RecyclerView.ViewHolder>> matchers;

    private int viewHolderPosition;

    public ViewHolderMatcher(Matcher<RecyclerView.ViewHolder>... matchers) {
        this(Arrays.asList(matchers));
    }

    public ViewHolderMatcher(List<Matcher<RecyclerView.ViewHolder>> matchers) {
        this.matchers = matchers;
    }

    public int getViewHolderPosition() {
        return viewHolderPosition;
    }

    @Override
    public boolean matchesSafely(RecyclerView.ViewHolder viewHolder) {
        for (Matcher<RecyclerView.ViewHolder> m : matchers) {
            if (!m.matches(viewHolder)) {
                return false;
            }
        }
        viewHolderPosition = viewHolder.getPosition();
        return true;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("all of: ");
        for (Matcher<RecyclerView.ViewHolder> m : matchers) {
            description.appendDescriptionOf(m).appendText(", ");
        }
    }
}
