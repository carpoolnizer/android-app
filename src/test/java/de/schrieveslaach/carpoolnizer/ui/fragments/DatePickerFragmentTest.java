/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.fragments;

import android.app.Dialog;
import android.os.Build;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.robolectric.annotation.Config;

import static de.schrieveslaach.carpoolnizer.ui.fragments.DatePickerFragment.OnPickDateListener;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
public class DatePickerFragmentTest extends BaseDialogFragmentTest<DatePickerFragment, Dialog> {

    private DatePickerFragment fragment;

    public DatePickerFragmentTest() {
        super(DatePickerFragment.class);
    }

    @Before
    public void createFragment() throws Exception {
        fragment = openFragment();
    }

    @Test
    public void shouldSetSelectedDate() throws Exception {
        final DateTime date = DateTime.now().withTimeAtStartOfDay().minusMonths(4);
        fragment = openFragment(new FragmentInitializer<DatePickerFragment>() {
            @Override
            public void init(DatePickerFragment fragment) {
                fragment.setDate(date);
            }

            @Override
            public void beforeStart(DatePickerFragment fragment) {

            }
        });

        OnPickDateListener dateListener = mock(OnPickDateListener.class);
        fragment.setOnPickDateListener(dateListener);

        Dialog dialog = openDialog(fragment);
        dialog.findViewById(com.wdullaer.materialdatetimepicker.R.id.mdtp_ok).performClick();

        verify(dateListener).onFinish(date);
    }

    @Test
    public void shouldPickDate() {
        OnPickDateListener dateListener = mock(OnPickDateListener.class);
        fragment.setOnPickDateListener(dateListener);

        Dialog dialog = openDialog(fragment);
        dialog.findViewById(com.wdullaer.materialdatetimepicker.R.id.mdtp_ok).performClick();

        DateTime today = DateTime.now().withTimeAtStartOfDay();
        verify(dateListener).onFinish(today);
    }

    @Test
    public void shouldSetMinimumDate() {
        OnPickDateListener dateListener = mock(OnPickDateListener.class);
        fragment.setOnPickDateListener(dateListener);

        DateTime today = DateTime.now().withTimeAtStartOfDay();

        fragment.setMinimumDate(today);
        fragment.setDate(today.minusDays(10));

        Dialog dialog = openDialog(fragment);
        dialog.findViewById(com.wdullaer.materialdatetimepicker.R.id.mdtp_ok).performClick();
        verify(dateListener).onFinish(today);
    }
}