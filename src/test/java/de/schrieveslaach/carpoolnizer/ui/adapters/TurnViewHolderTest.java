/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.adapters;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.Arrays;

import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.model.Member;
import de.schrieveslaach.carpoolnizer.model.Turn;
import de.schrieveslaach.carpoolnizer.ui.robolectric.fakes.ContactsCursor;
import de.schrieveslaach.carpoolnizer.ui.views.MemberReadOnlyView;

import static de.schrieveslaach.carpoolnizer.ui.matchers.ImageViewMatchers.showsDrawable;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.clickable;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.gone;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.hasOnClickListeners;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.hasText;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.visible;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.robolectric.RuntimeEnvironment.application;

@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
@RunWith(RobolectricTestRunner.class)
public class TurnViewHolderTest {

    private TurnViewHolder holder;

    private TurnAdapter.OnTurnClickListener onTurnClickListener;

    private MemberReadOnlyView.OnContactInformationListener onContactInformationListener;

    private ContactsCursor contactsCursor;

    private Turn mockTurn(DateTime date) {
        return mockTurn(date, null);
    }

    private Turn mockTurn(DateTime date, Member driver, Member... passengers) {
        Turn turn = mock(Turn.class);
        when(turn.getDate()).thenReturn(date);
        when(turn.getDriver()).thenReturn(driver);
        when(turn.getPassengers()).thenReturn(Arrays.asList(passengers));
        return turn;
    }

    @Before
    public void init() {
        RelativeLayout turnView = (RelativeLayout) LayoutInflater.from(application.getBaseContext()).inflate(R.layout.view_turn, null, false);
        onTurnClickListener = mock(TurnAdapter.OnTurnClickListener.class);
        onContactInformationListener = mock(MemberReadOnlyView.OnContactInformationListener.class);
        holder = new TurnViewHolder(turnView, onTurnClickListener, onContactInformationListener);
    }

    @Before
    public void fakeContactsCursor() {
        contactsCursor = new ContactsCursor()
                .withContact("Mike")
                .withContact("John")
                .withContact("Daisy")
                .fake();
    }

    @Test
    public void shouldDisplayCalendarWeek() {
        DateTime monday = new DateTime(2015, 3, 16, 0, 0);
        Turn turn = mockTurn(monday);
        holder.display(turn);

        assertThat(holder.itemView.findViewById(R.id.rl_calendar_week), is(visible()));
        assertThat(holder.itemView.findViewById(R.id.first_divider), is(visible()));

        assertThat((TextView) holder.itemView.findViewById(R.id.tv_year), hasText("2015"));
        assertThat((TextView) holder.itemView.findViewById(R.id.tv_week), hasText("12"));
    }

    @Test
    public void shouldNotDisplayCalendarWeek() {
        DateTime sunday = new DateTime(2015, 3, 15, 0, 0);
        Turn turn = mockTurn(sunday);
        holder.display(turn);

        assertThat(holder.itemView.findViewById(R.id.rl_calendar_week), is(gone()));
        assertThat(holder.itemView.findViewById(R.id.first_divider), is(gone()));
    }

    @Test
    public void shouldDisplayNumberOfPassengers_TwoPassengers() {
        DateTime sunday = new DateTime(2015, 3, 15, 0, 0);

        Member mike = contactsCursor.createMember("Mike");
        Member john = contactsCursor.createMember("John");
        Member daisy = contactsCursor.createMember("Daisy");

        Turn turn = mockTurn(sunday, mike, john, daisy);
        holder.display(turn);

        assertThat(holder.itemView.findViewById(R.id.tv_passengers), is(visible()));

        TextView tvNumberOfPassengers = (TextView) holder.itemView.findViewById(R.id.tv_number_of_passengers);
        assertThat(tvNumberOfPassengers, is(visible()));
        assertThat(tvNumberOfPassengers, hasText("2"));
    }

    @Test
    public void shouldNotDisplayNumberOfPassengers_NoUsages() {
        DateTime sunday = new DateTime(2015, 3, 15, 0, 0);
        Turn turn = mockTurn(sunday);
        holder.display(turn);

        assertThat(holder.itemView.findViewById(R.id.tv_passengers), is(gone()));
        assertThat(holder.itemView.findViewById(R.id.tv_number_of_passengers), is(gone()));
    }

    @Test
    @Ignore
    public void shouldChangeBackground_Today() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Turn turn = mockTurn(today);
        holder.display(turn);

        Drawable currentBackground = holder.itemView.findViewById(R.id.member_read_only_view).getBackground();
        Drawable expectedBackground = application.getResources().getDrawable(R.drawable.clickable_turn_today_background);
        assertThat(currentBackground, is(expectedBackground));
    }

    @Test
    @Ignore
    public void shouldChangeBackground_InThePast() {
        DateTime yesterday = DateTime.now().withTimeAtStartOfDay().minusDays(1);
        Turn turn = mockTurn(yesterday);
        holder.display(turn);

        Drawable currentBackground = holder.itemView.findViewById(R.id.member_read_only_view).getBackground();
        Drawable expectedBackground = application.getResources().getDrawable(R.drawable.clickable_turn_in_the_past_background);
        assertThat(currentBackground, is(expectedBackground));
    }

    @Test
    @Ignore
    public void shouldChangeBackground_Tomorrow() {
        DateTime tomorrow = DateTime.now().withTimeAtStartOfDay().plusDays(1);
        Turn turn = mockTurn(tomorrow);
        holder.display(turn);

        Drawable currentBackground = holder.itemView.findViewById(R.id.member_read_only_view).getBackground();
        Drawable expectedBackground = application.getResources().getDrawable(R.drawable.clickable_background);
        assertThat(currentBackground, is(expectedBackground));
    }

    @Test
    public void shouldDisplayLoadArchivedTurnsButton() {
        DateTime tomorrow = DateTime.now().withTimeAtStartOfDay().plusDays(1);
        Turn turn = mockTurn(tomorrow);
        holder.display(turn);
        holder.displayLoadArchivedTurnsButton(mock(View.OnClickListener.class));

        Button buLoadArchivedTurns = (Button) holder.itemView.findViewById(R.id.bu_load_archived_turns);
        assertThat(buLoadArchivedTurns, is(visible()));
        assertThat(buLoadArchivedTurns, hasOnClickListeners());
    }

    @Test
    public void shouldHideDisplayLoadArchivedTurnsButton() {
        DateTime tomorrow = DateTime.now().withTimeAtStartOfDay().plusDays(1);
        Turn turn = mockTurn(tomorrow);
        holder.display(turn);
        holder.displayLoadArchivedTurnsButton(mock(View.OnClickListener.class));
        holder.hideLoadArchivedTurnsButton();

        Button buLoadArchivedTurns = (Button) holder.itemView.findViewById(R.id.bu_load_archived_turns);
        assertThat(buLoadArchivedTurns, is(gone()));
        assertThat(buLoadArchivedTurns, not(hasOnClickListeners()));
    }

    @Test
    public void shouldSetOnContactInformationListener_MemberIsNotPhoneUser() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Member mike = contactsCursor.createMember("Mike");
        Turn turn = mockTurn(today, mike);
        holder.display(turn);

        View ivContactImage = holder.itemView.findViewById(R.id.iv_contact_image);
        assertThat(ivContactImage, is(clickable()));
    }

    @Test
    public void shouldNotSetOnContactInformationListener_MemberIsPhoneUser() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Turn turn = mockTurn(today, new Member(null));
        holder.display(turn);

        View ivContactImage = holder.itemView.findViewById(R.id.iv_contact_image);
        assertThat(ivContactImage, is(not(clickable())));
    }

    @Test
    public void shouldNotSetOnContactInformationListener_NoDriver() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Turn turn = mockTurn(today);
        holder.display(turn);

        View ivContactImage = holder.itemView.findViewById(R.id.iv_contact_image);
        assertThat(ivContactImage, is(not(clickable())));
    }

    @Test
    public void shouldShowPillow_TurnIsCanceled() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Turn turn = mockTurn(today);
        when(turn.isCanceled()).thenReturn(true);
        holder.display(turn);

        ImageView ivContactImage = (ImageView) holder.itemView.findViewById(R.id.iv_contact_image);
        assertThat(ivContactImage, showsDrawable(R.drawable.ic_action_pillow2));
    }

    @Test
    public void shouldShowCoffee_TurnIsNotCanceled() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Turn turn = mockTurn(today);
        when(turn.isCanceled()).thenReturn(false);
        holder.display(turn);

        ImageView ivContactImage = (ImageView) holder.itemView.findViewById(R.id.iv_contact_image);
        assertThat(ivContactImage, showsDrawable(R.drawable.ic_action_coffee));
    }
}