/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.CompoundButton;

import androidx.appcompat.widget.AppCompatCheckBox;
import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.model.Member;

/**
 * UI element to configure a {@link de.schrieveslaach.carpoolnizer.model.Member} in the
 * {@link de.schrieveslaach.carpoolnizer.ui.CarpoolConfigurationActivity}.
 */
public class MemberConfigurationView extends AbstractMemberView {

    private AppCompatCheckBox cbMonday, cbTuesday, cbWednesday, cbThursday, cbFriday, cbSaturday, cbSunday;

    private MemberReadOnlyView mrovMember;

    private OnConfigurationChangedListener onConfigurationChangedListener;

    /**
     * Creates a new instance of {@code MemberConfigurationView}
     *
     * @param context
     */
    public MemberConfigurationView(Context context) {
        super(context);
        inflateLayout(context);
    }

    /**
     * Creates a new instance of {@code MemberConfigurationView}
     *
     * @param context
     * @param attrs
     */
    public MemberConfigurationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Creates a new instance of {@code MemberConfigurationView}
     *
     * @param context
     * @param attrs
     * @param defStyle
     */
    public MemberConfigurationView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    private void inflateLayout(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.view_member_configuration, this);

        cbMonday = findViewById(R.id.cb_monday);
        cbTuesday = findViewById(R.id.cb_tuesday);
        cbWednesday = findViewById(R.id.cb_wednesday);
        cbThursday = findViewById(R.id.cb_thursday);
        cbFriday = findViewById(R.id.cb_friday);
        cbSaturday = findViewById(R.id.cb_saturday);
        cbSunday = findViewById(R.id.cb_sunday);

        mrovMember = (MemberReadOnlyView) findViewById(R.id.mrov_member);

        setOnCheckedListener(cbMonday);
        setOnCheckedListener(cbTuesday);
        setOnCheckedListener(cbWednesday);
        setOnCheckedListener(cbThursday);
        setOnCheckedListener(cbFriday);
        setOnCheckedListener(cbSaturday);
        setOnCheckedListener(cbSunday);
    }

    private void setOnCheckedListener(AppCompatCheckBox cb) {
        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (onConfigurationChangedListener == null) {
                    return;
                }

                onConfigurationChangedListener.onConfigurationChanged(MemberConfigurationView.this);
            }
        });
    }

    /**
     * Displays the given {@link Member} in the current {@code MemberConfigurationView}.
     *
     * @param member
     */
    @Override
    public void display(Member member) {
        super.display(member);
        mrovMember.display(member);
    }

    /**
     * Displays the weekday usage for the currently displayed {@link MemberConfigurationView#getMember()}.
     *
     * @param weekdayUsage
     */
    public void display(boolean[] weekdayUsage) {
        if (weekdayUsage.length != 7) {
            throw new IllegalArgumentException("provide an array of length 7!");
        }

        cbMonday.setChecked(weekdayUsage[0]);
        cbTuesday.setChecked(weekdayUsage[1]);
        cbWednesday.setChecked(weekdayUsage[2]);
        cbThursday.setChecked(weekdayUsage[3]);
        cbFriday.setChecked(weekdayUsage[4]);
        cbSaturday.setChecked(weekdayUsage[5]);
        cbSunday.setChecked(weekdayUsage[6]);
    }

    /**
     * Returns the configuration weekday usage.
     *
     * @return
     */
    public boolean[] getWeekdayUsage() {
        boolean[] weekdayUsage = new boolean[7];

        weekdayUsage[0] = cbMonday.isChecked();
        weekdayUsage[1] = cbTuesday.isChecked();
        weekdayUsage[2] = cbWednesday.isChecked();
        weekdayUsage[3] = cbThursday.isChecked();
        weekdayUsage[4] = cbFriday.isChecked();
        weekdayUsage[5] = cbSaturday.isChecked();
        weekdayUsage[6] = cbSunday.isChecked();

        return weekdayUsage;
    }

    /**
     * Sets the {@link OnConfigurationChangedListener listener} which will be invoked when the
     * configuration has been changed.
     *
     * @param listener
     */
    public void setOnConfigurationChangedListener(OnConfigurationChangedListener listener) {
        this.onConfigurationChangedListener = listener;
    }

    /**
     * Event handler for invoking changed when the configuration changed.
     */
    public interface OnConfigurationChangedListener {

        /**
         * Will be raised when the configuration has been changed.
         *
         * @param mcv
         */
        void onConfigurationChanged(MemberConfigurationView mcv);

    }
}
