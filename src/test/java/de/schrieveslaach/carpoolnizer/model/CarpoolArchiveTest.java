/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.model;

import com.google.common.collect.ArrayListMultimap;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.Test;

import java.util.Arrays;
import java.util.TreeSet;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class CarpoolArchiveTest {

    @Test
    public void shouldReturnInterval() {
        Turn turn = new Turn(DateTime.now().withTimeAtStartOfDay());
        CarpoolArchive archive = new CarpoolArchive(
                ArrayListMultimap.<Integer, MemberCombination>create(),
                new TreeSet<Turn>(Arrays.asList(turn))
        );

        Interval interval = archive.getInterval();
        assertThat(interval.getStart(), is(DateTime.now().withTimeAtStartOfDay()));
        assertThat(interval.getEnd(), is(DateTime.now().withTimeAtStartOfDay().plusDays(1)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateArchive_NullCombinations() {
        Turn turn = new Turn(DateTime.now().withTimeAtStartOfDay());
        new CarpoolArchive(
                null,
                new TreeSet<Turn>(Arrays.asList(turn))
        );
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateArchive_NullTurns() {
        new CarpoolArchive(
                ArrayListMultimap.<Integer, MemberCombination>create(),
                null
        );
    }
}