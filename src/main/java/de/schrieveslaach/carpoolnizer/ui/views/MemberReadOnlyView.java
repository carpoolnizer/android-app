/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.model.Member;

/**
 *
 */
public class MemberReadOnlyView extends AbstractMemberView {

    private NoMemberDrawableResolver noMemberDrawableResolver;

    private ContactImageView ivContactImage;

    private TextView tvMemberName;

    private LinearLayout llSubItems;

    private OnContactInformationListener onContactInformationListener;

    /**
     * Creates a new instance of {@code AbstractMemberView}.
     *
     * @param context
     */
    public MemberReadOnlyView(Context context) {
        super(context);
        inflateLayout(context);
    }

    /**
     * Creates a new instance of {@code AbstractMemberView}.
     *
     * @param context
     * @param attrs
     */
    public MemberReadOnlyView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflateLayout(context);
    }

    /**
     * Creates a new instance of {@code AbstractMemberView}.
     *
     * @param context
     * @param attrs
     * @param defStyle
     */
    public MemberReadOnlyView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        inflateLayout(context);
    }

    /**
     * Sets the {@link MemberReadOnlyView.NoMemberDrawableResolver resolver} for displaying alternative member.
     *
     * @param noMemberDrawableResolver
     */
    public void setNoMemberDrawableResolver(NoMemberDrawableResolver noMemberDrawableResolver) {
        this.noMemberDrawableResolver = noMemberDrawableResolver;
    }

    /**
     * Sets the {@link OnContactInformationListener listener} which will be invoke when the user
     * wants to see contact information.
     *
     * @param onContactInformationListener
     */
    public void setOnContactInformationListener(OnContactInformationListener onContactInformationListener) {
        this.onContactInformationListener = onContactInformationListener;
        // redisplay
        display(getMember());
    }

    private void inflateLayout(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.view_member_read_only, this);

        ivContactImage = (ContactImageView) findViewById(R.id.iv_contact_image);
        ivContactImage.setOnClickListener(new OnContactImageListener());
        ivContactImage.setClickable(false);
        tvMemberName = (TextView) findViewById(R.id.tv_member_name);
        llSubItems = (LinearLayout) findViewById(R.id.ll_mrov_sub_items);
    }

    @Override
    public void display(Member member) {
        super.display(member);

        if (member != null) {
            tvMemberName.setText(getDisplayName());
            ivContactImage.setClickable(onContactInformationListener != null);
            ivContactImage.setImageBitmap(getContactPicture());
        } else {
            ivContactImage.setClickable(false);
            ivContactImage.setImageDrawable(noMemberDrawableResolver.resolveBitmap(getContext()));
            tvMemberName.setText("-");
        }
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        if (llSubItems == null) {
            super.addView(child, index, params);
        } else {
            llSubItems.addView(child, index, params);
        }
    }

    @Override
    public void removeAllViews() {
        if (llSubItems == null) {
            super.removeAllViews();
        } else {
            llSubItems.removeAllViews();
        }
    }

    /**
     * Listener which will be invoked when the user wants contact information of a {@link Member}.
     */
    public interface OnContactInformationListener {

        /**
         * Will be invoked when the user wants see the contact information.
         *
         * @param member
         */
        void onContactInformation(Member member);

    }

    /**
     * Handles clicks on the {@link MemberReadOnlyView#ivContactImage contact image}.
     */
    private class OnContactImageListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            Member member = getMember();
            if (member == null || onContactInformationListener == null) {
                return;
            }
            onContactInformationListener.onContactInformation(member);
        }
    }

    /**
     * Resolver for displaying an image when no {@link #display(Member) member is displayed}.
     */
    public interface NoMemberDrawableResolver {

        /**
         * Resolves a bitmap for displaying an image for the {@link #display(Member) member}.
         *
         * @return
         */
        Drawable resolveBitmap(Context context);

    }
}
