Feature: Creating carpools
  This feature allows to create carpools with members of the contact list. 
  The UI will also notify the user when he or she entered invalid data. 

  Scenario Outline: Create a carpool with given members
    Given I started the app
    And I want to create a new carpool
    When I enter the carpool name <carpool>
    And I add <contacts> as a members
    And I am done with defining the carpool configuration
    Then I should see turns for the next <days> days with <contacts>
    And I should see <carpool> in the title

  Examples:
    | carpool                 | contacts          | days |
    | Mike and John           | Mike, John        | 7    |
    | Daisy and John          | Daisy, John       | 7    |
    | Daisy and Mike and John | Daisy, Mike, John | 7    |
    | Work John               | John              | 7    |
    | Work Daisy              | Daisy             | 7    |

  Scenario Outline: Create a carpool starting in the past
    Given I started the app
    And I want to create a new carpool
    When I enter the carpool name <carpool>
    And I add <contacts> as a members
    And I change the start date to <month> ago
    And I am done with defining the carpool configuration
    Then I should see turns for the previous <month> months with <contacts>
    And I should see <carpool> in the title

  Examples:
    | carpool             | contacts              | month |
    | Work Mike           | Mike                  | 1     |
    | A long term carpool | Daisy, Mike, John     | 5     |
    
  Scenario Outline: Can not create carpool because I added no members
    Given I started the app
    And I want to create a new carpool
    When I enter the carpool name <carpool>
    And I am done with defining the carpool configuration
    Then I should see "<message>" as notification
    
  Examples:
    | carpool | message                      |
    | Work    | Select at least two members! |
    
  Scenario Outline: Can not create carpool because I entered no name
    Given I started the app
    And I want to create a new carpool
    When I enter the carpool name <carpool>
    And I am done with defining the carpool configuration
    Then I should see "<message>" as error in the carpool name field
    
  Examples:
    | carpool | message                           |
    |         | Enter a valid name for a carpool. |
    
    
  Scenario Outline: Can not create carpool because I entered an existing name
    Given I started the app
    And I want to create a new carpool
    When I enter the carpool name <carpool>
    And I am done with defining the carpool configuration
    Then I should see "<message>" as error in the carpool name field
    
  Examples:
    | carpool   | message                                       |
    | Work Mike | A carpool with the given name already exists. |

