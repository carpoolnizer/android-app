/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.robolectric.shadows;

import android.content.Context;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import com.google.android.material.navigation.NavigationView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Implementation;
import org.robolectric.annotation.Implements;
import org.robolectric.annotation.RealObject;
import org.robolectric.fakes.RoboMenuItem;
import org.robolectric.shadows.ShadowViewGroup;

import java.util.HashMap;
import java.util.Map;

import de.schrieveslaach.carpoolnizer.R;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Implements(NavigationView.class)
public class ShadowNavigationView extends ShadowViewGroup {

    private final BiMap<Integer, String> menuIdsAndMenuTitles = HashBiMap.create();

    private final Map<Integer, MenuItem> menuItems = new HashMap<>();

    private Menu menu = mock(Menu.class);

    @RealObject
    NavigationView realObject;

    private NavigationView.OnNavigationItemSelectedListener listener;

    public ShadowNavigationView() {
        final SubMenu carpoolSubMenu = mock(SubMenu.class);
        when(carpoolSubMenu.add(anyInt(), anyInt(), anyInt(), anyString())).thenAnswer(new Answer<MenuItem>() {
            @Override
            public MenuItem answer(InvocationOnMock invocation) {
                Integer groupId = invocation.getArgument(0);
                Integer menuId = invocation.getArgument(1);
                String menuTitle = invocation.getArgument(3);

                menuIdsAndMenuTitles.put(menuId, menuTitle);

                RoboMenuItem menuItem = new RoboMenuItem(menuId);
                menuItem.setGroupId(groupId);
                menuItems.put(menuId, menuItem);
                return menuItem;
            }
        });
        when(carpoolSubMenu.size()).thenAnswer(new Answer<Integer>() {
            @Override
            public Integer answer(InvocationOnMock invocation) throws Throwable {
                return menuIdsAndMenuTitles.size();
            }
        });
        when(carpoolSubMenu.getItem(anyInt())).thenAnswer(new Answer<MenuItem>() {
            @Override
            public MenuItem answer(InvocationOnMock invocation) throws Throwable {
                int index = invocation.getArgument(0);
                String title = menuIdsAndMenuTitles.values().toArray(new String[menuIdsAndMenuTitles.size()])[index];
                MenuItem item = mock(MenuItem.class);
                when(item.getTitle()).thenReturn(title);
                return item;
            }
        });
        doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                menuIdsAndMenuTitles.clear();
                return null;
            }
        }).when(carpoolSubMenu).removeGroup(R.id.nav_carpools_group);

        when(menu.addSubMenu(anyInt())).thenAnswer(new Answer<SubMenu>() {
            @Override
            public SubMenu answer(InvocationOnMock invocation) {
                return carpoolSubMenu;
            }
        });

        when(menu.findItem(anyInt())).thenAnswer(new Answer<MenuItem>() {
            @Override
            public MenuItem answer(InvocationOnMock invocation) {
                int menuId = invocation.getArgument(0);
                if (menuItems.containsKey(menuId)) {
                    return menuItems.get(menuId);
                }

                return getMenuItem(menuId);
            }
        });

        when(menu.size()).thenAnswer(new Answer<Integer>() {
            @Override
            public Integer answer(InvocationOnMock invocation) throws Throwable {
                return menuItems.size();
            }
        });

        when(menu.getItem(1)).thenAnswer(new Answer<MenuItem>() {
            @Override
            public MenuItem answer(InvocationOnMock invocation) throws Throwable {
                MenuItem item = mock(MenuItem.class);
                when(item.getSubMenu()).thenReturn(carpoolSubMenu);
                return item;
            }
        });

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                menuIdsAndMenuTitles.clear();
                menuItems.clear();
                return null;
            }
        }).when(menu).removeGroup(anyInt());
    }

    @NonNull
    private MenuItem getMenuItem(int menuId) {
        Context context = RuntimeEnvironment.application.getApplicationContext();
        RoboMenuItem menuItem = new RoboMenuItem(context);
        menuItem.setItemId(menuId);

        if (menuId == R.id.nav_mi_notification_settings) {
            menuItem.setTitle(context.getString(R.string.no_notification));
            menuItems.put(menuId, menuItem);
        }

        return menuItem;
    }

    @Implementation
    public void setNavigationItemSelectedListener(NavigationView.OnNavigationItemSelectedListener listener) {
        this.listener = listener;
    }

    @Implementation
    public Menu getMenu() {
        return menu;
    }

    public void selectMenu(String menuName) {
        Integer menuId = menuIdsAndMenuTitles.inverse().get(menuName);
        MenuItem menuItem = menuItems.get(menuId);
        listener.onNavigationItemSelected(menuItem);
    }

    public void selectMenu(@IdRes int menuId) {
        listener.onNavigationItemSelected(getMenuItem(menuId));
    }
}
