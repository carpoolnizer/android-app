#!/bin/bash
set -e

ANDROID_DEVICE_HOST_NAME="192.168.58.37:5555"

$ANDROID_HOME/platform-tools/adb connect $ANDROID_DEVICE_HOST_NAME

# Build and install app
gradle assembleDebug
$ANDROID_HOME/platform-tools/adb -s $ANDROID_DEVICE_HOST_NAME uninstall de.schrieveslaach.carpoolnizer
$ANDROID_HOME/platform-tools/adb -s $ANDROID_DEVICE_HOST_NAME install build/outputs/apk/App-debug.apk

# Unlock and wake up
$ANDROID_HOME/platform-tools/adb -s $ANDROID_DEVICE_HOST_NAME shell input keyevent 82
$ANDROID_HOME/platform-tools/adb -s $ANDROID_DEVICE_HOST_NAME shell input keyevent 26

# Run UI tests
gradle connectedCheck
