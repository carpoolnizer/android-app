/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.model;

/**
 * Define how a {@link Member#getUsageAt(org.joda.time.DateTime) member} wants to use the carpool.
 */
public enum Usage {

    /**
     * The member uses the carpool as usual.
     */
    NORMAL,

    /**
     * The member has to drive. Maybe he or she needs the car
     * at lunch break to settle something...
     */
    HAS_TO_DRIVE,
    
    /**
     * The member does not use the carpool that day.
     */
    DOES_NOT_USE,

    /**
     * The member wants to use the carpool but can't drive.
     * Maybe her or his car is in the car repair shop.
     */
    CAN_NOT_DRIVE
}
