/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.robolectric.shadows;

import androidx.recyclerview.widget.RecyclerView;
import android.widget.LinearLayout;

import com.google.android.material.internal.NavigationMenuView;

import org.robolectric.annotation.Implementation;
import org.robolectric.annotation.Implements;
import org.robolectric.annotation.RealObject;
import org.robolectric.shadows.ShadowView;

@Implements(RecyclerView.class)
public class ShadowRecyclerView extends ShadowView {

    @RealObject
    RecyclerView recyclerView;

    private RecyclerView.Adapter adapter;

    private RecyclerView.ViewHolder[] viewHolders;

    @Implementation
    public void setAdapter(RecyclerView.Adapter adapter) {
        this.adapter = adapter;
        adapter.registerAdapterDataObserver(new DataObserver());
    }

    @Implementation
    public RecyclerView.Adapter getAdapter() {
        return adapter;
    }

    @Implementation
    public RecyclerView.ViewHolder findViewHolderForAdapterPosition(int position) {
        if (viewHolders == null) {
            initViewHolder();
        }

        return viewHolders[position];
    }

    private class DataObserver extends RecyclerView.AdapterDataObserver {

        @Override
        public void onChanged() {
            initViewHolder();
        }
    }

    private void initViewHolder() {
        if (recyclerView instanceof NavigationMenuView) {
            return;
        }

        viewHolders = new RecyclerView.ViewHolder[5];
        for (int i = 0; i < Math.min(adapter.getItemCount(), 5); ++i) {
            viewHolders[i] = adapter.onCreateViewHolder(new LinearLayout(recyclerView.getContext()), i);
            adapter.onBindViewHolder(viewHolders[i], i);
        }
    }
}
