/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.repository;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * Contains settings which are specific for the {@link CarpoolDAO}.
 */
class RepositorySettings {

    @JsonIgnore
    private final PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    @JsonProperty("lastUsedCarpool")
    private String lastUsedCarpool;

    @JsonProperty(value = "archiveCarpoolDataAfterMonths", defaultValue = "2")
    private int archiveCarpoolDataAfterMonths = 2;

    /**
     * Adds a {@link java.beans.PropertyChangeListener} to be notified about changes in the properties.
     *
     * @param changeListener
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(java.beans.PropertyChangeListener)
     */
    public void addPropertyChangeListener(PropertyChangeListener changeListener) {
        changeSupport.addPropertyChangeListener(changeListener);
    }

    /**
     * Returns the name of the carpool which has been loaded the last time.
     *
     * @return
     */
    public String getLastUsedCarpool() {
        return lastUsedCarpool;
    }

    /**
     * Sets the name of the carpool which has been loaded the last time.
     *
     * @param lastUsedCarpool
     */
    public void setLastUsedCarpool(String lastUsedCarpool) {
        String oldValue = this.lastUsedCarpool;
        this.lastUsedCarpool = lastUsedCarpool;
        firePropertyChange("lastUsedCarpool", oldValue, lastUsedCarpool);
    }

    /**
     * Sets the number of months when the repository starts archiving data.
     *
     * @param months
     */
    public void setArchiveCarpoolDataAfterMonths(int months) {
        if (months < 1) {
            throw new IllegalArgumentException("Months must be greate zero");
        }
        int oldValue = this.archiveCarpoolDataAfterMonths;
        this.archiveCarpoolDataAfterMonths = months;
        firePropertyChange("archiveCarpoolDataAfterMonths", oldValue, months);
    }

    /**
     * The number of months when the repository starts archiving data.
     *
     * @return
     */
    public int getArchiveCarpoolDataAfterMonths() {
        return archiveCarpoolDataAfterMonths;
    }

    private <T> void firePropertyChange(String propertyName, T oldValue, T newValue) {
        if (Objects.equal(newValue, oldValue)) {
            return;
        }
        changeSupport.firePropertyChange(new PropertyChangeEvent(this, propertyName, oldValue, newValue));
    }
}
