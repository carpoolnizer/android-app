/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.test;

import androidx.annotation.IdRes;
import com.google.android.material.navigation.NavigationView;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.ViewAssertion;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.internal.MDButton;
import com.afollestad.materialdialogs.internal.MDRootLayout;
import com.google.common.collect.Lists;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Objects;

import cucumber.api.CucumberOptions;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.Constants;
import de.schrieveslaach.carpoolnizer.ui.MainActivity_;
import de.schrieveslaach.carpoolnizer.ui.views.MemberReadOnlyView;
import lombok.SneakyThrows;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.longClick;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.contrib.RecyclerViewActions.scrollToHolder;
import static androidx.test.espresso.contrib.RecyclerViewActions.scrollToPosition;
import static androidx.test.espresso.matcher.ViewMatchers.Visibility.VISIBLE;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.hasSibling;
import static androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static androidx.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static androidx.test.espresso.matcher.ViewMatchers.withChild;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static de.schrieveslaach.carpoolnizer.test.ViewMatchers.hasTitle;
import static de.schrieveslaach.carpoolnizer.test.ViewMatchers.isCanceledTurn;
import static de.schrieveslaach.carpoolnizer.test.ViewMatchers.isEmptyRecyclerView;
import static de.schrieveslaach.carpoolnizer.test.ViewMatchers.isNotCanceledTurn;
import static de.schrieveslaach.carpoolnizer.test.ViewMatchers.withContactName;
import static de.schrieveslaach.carpoolnizer.test.ViewMatchers.withDate;
import static de.schrieveslaach.carpoolnizer.test.ViewMatchers.withDriver;
import static de.schrieveslaach.carpoolnizer.test.ViewMatchers.withPassengerCount;
import static de.schrieveslaach.carpoolnizer.test.ViewMatchers.withinInterval;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

@CucumberOptions(features = "features")
public class MainPage extends AbstractPage<MainActivity_> {

    private final TurnViewHandler turnViewHandler = new TurnViewHandler();

    public MainPage() {
        super(MainActivity_.class);
    }

    @Given("^I started the app$")
    public void I_started_the_app() {
        getActivity();
    }

    @Given("^I want to create a new carpool$")
    public void I_want_to_create_a_new_carpool() {
        pressMenuItem(R.id.mi_create_carpool, getCurrentActivity().getResources().getString(R.string.create));
    }

    @Given("^I want to edit the current carpool$")
    public void I_want_to_edit_the_current_carpool() {
        pressMenuItem(R.id.mi_edit_carpool, getCurrentActivity().getResources().getString(R.string.edit));
    }

    /**
     * Presses the menu item with the given {@code menuButtonId}.
     *
     * @param menuButtonId
     * @param menuText     If is not {@code null}, then it will check if an overflow menu button is there
     *                     and if so, it will press the menu item with the given text.
     */
    private void pressMenuItem(@IdRes final int menuButtonId, final String menuText) {
        ViewGroup group = (ViewGroup) getCurrentActivity().getWindow().getDecorView();

        if (menuText != null && !isMenuItemVisible(menuButtonId, group)) {
            View overflowButton = getOverflowButton(group);

            if (overflowButton != null) {
                onView(isAssignableFrom(overflowButton.getClass())).perform(click());
                onView(withText(menuText)).perform(click());
                return;
            }
        }

        onView(withId(menuButtonId)).perform(click());
    }

    private View getOverflowButton(ViewGroup group) {
        for (int i = 0; i < group.getChildCount(); ++i) {
            View child = group.getChildAt(i);
            if ("OverflowMenuButton".equals(child.getClass().getSimpleName())) {
                return child;
            }

            if (child instanceof ViewGroup) {
                View overflowButton = getOverflowButton((ViewGroup) child);
                if (overflowButton != null) {
                    return overflowButton;
                }
            }
        }
        return null;
    }

    private boolean isMenuItemVisible(@IdRes final int menuButtonId, final ViewGroup group) {
        for (int i = 0; i < group.getChildCount(); ++i) {
            View child = group.getChildAt(i);
            if (child.getId() == menuButtonId) {
                return true;
            }

            if (child instanceof ViewGroup) {
                boolean isVisible = isMenuItemVisible(menuButtonId, (ViewGroup) child);
                if (isVisible) {
                    return true;
                }
            }
        }
        return false;
    }

    @Given("^I opened the carpool (.+)$")
    @SneakyThrows(InterruptedException.class)
    public void I_opened_the_carpool(final String carpoolName) {
        if (carpoolName.equals(getCurrentActivity().getTitle())) {
            return;
        }

        // click on home button to select carpool
        onView(allOf(
                isAssignableFrom(ImageButton.class),
                withParent(withClassName(endsWith("Toolbar")))
        )).perform(click());

        Thread.sleep(750);

        onView(allOf(
                withText(carpoolName)
        )).perform(click());

        assertThat(getCurrentActivity(), hasTitle(carpoolName));
    }

    @Given("^I clicked on first turn with contact (.+) as driver$")
    public void I_clicked_on_first_turn_with_contact_as_driver(final String contactName) {
        Interval interval = new Interval(DateTime.now().withTimeAtStartOfDay(), DateTime.now().withTimeAtStartOfDay().plusDays(7));

        turnViewHandler.clickOnTurn(
                withDriver(contactName),
                withinInterval(interval)
        );
    }

    @Given("^I clicked on first archived turn older than (\\d+) months with contact (.+?) as driver$")
    public void I_clicked_on_first_archived_turn_x_months_ago_with_contact_as_driver(final int months, final String contactName) {
        DateTime startDate = DateTime.now().withTimeAtStartOfDay().withDayOfMonth(1).minusMonths(months + 1);
        Interval interval = new Interval(startDate, startDate.plusMonths(1));

        turnViewHandler.clickOnTurn(
                withDriver(contactName),
                withinInterval(interval)
        );
    }

    @Given("^I clicked on first turn with contact (.+) as driver and (\\d+) passengers$")
    public void I_clicked_on_first_turn_with_contact_as_driver(final String contactName, final int passengers) {
        Interval interval = new Interval(DateTime.now().withTimeAtStartOfDay(), DateTime.now().withTimeAtStartOfDay().plusDays(7));

        turnViewHandler.clickOnTurn(
                withDriver(contactName),
                withinInterval(interval),
                withPassengerCount(passengers)
        );
    }

    @Given("^I scrolled to the (first|last) turn$")
    public void I_scroll_to_the_first_or_last_turn(final String firstOrLast) {
        int index;

        if ("first".equals(firstOrLast)) {
            index = 0;
        } else if ("last".equals(firstOrLast)) {
            RecyclerView rv = (RecyclerView) getCurrentActivity().findViewById(R.id.rv_turns);
            index = rv.getAdapter().getItemCount() - 1;
        } else {
            throw new IllegalArgumentException(firstOrLast);
        }

        onView(withId(R.id.rv_turns)).perform(scrollToPosition(index));
    }

    @Given("^I clicked on turn of today$")
    public void I_clicked_on_turn_of_today() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        turnViewHandler.clickOnTurnOf(today);
    }

    @When("^I click on the delete carpool menu button$")
    public void I_click_on_the_delete_carpool_menu_button() {
        pressMenuItem(R.id.mi_delete_carpool, "Delete");
    }

    @When("^I click on the today menu button$")
    public void I_click_on_the_today_menu_button() {
        pressMenuItem(R.id.mi_today, null);
    }

    @When("^I click on the load archived turns button$")
    public void I_click_on_the_load_archived_turns_button() {
        onView(allOf(
                withId(R.id.bu_load_archived_turns),
                withEffectiveVisibility(VISIBLE)
        )).perform(click());
    }

    @When("^I click on the last turn again$")
    public void I_click_on_the_last_turn_again() {
        turnViewHandler.clickOnLastTurnAgain();
    }

    @When("^I confirm with OK$")
    public void I_confirm_with_OK() {
        onView(allOf(
                isAssignableFrom(MDButton.class),
                withEffectiveVisibility(VISIBLE),
                withText("OK")
        )).perform(click());
    }

    @When("^I clicked on the contact image of the first turn with contact (.+?) as driver$")
    public void I_clicked_on_the_contact_image_of_the_first_turn_with_contact__as_driver(final String contactName) throws Exception {
        startContactInformationMonitor();

        Interval interval = new Interval(DateTime.now().withTimeAtStartOfDay(), DateTime.now().withTimeAtStartOfDay().plusDays(7));
        turnViewHandler.clickOnContactImageOfTurn(
                withDriver(contactName),
                withinInterval(interval)
        );
    }

    @When("^I clicked long on turn of today$")
    public void I_clicked_long_on_turn_of_today() throws Throwable {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        turnViewHandler.clickLongOnTurnOf(today);
    }

    @When("^I clicked on the pillow$")
    public void I_clicked_on_the_pillow() throws Throwable {
        pressMenuItem(R.id.mi_cancel_turn, null);
    }

    @When("^I open carpool information details$")
    public void i_open_carpool_information_details() throws Throwable {
        onView(
                withId(R.id.ab_carpool_information)
        ).perform(click());
    }

    @When("^I enter (\\d+) as gasoline consumption$")
    public void i_enter_as_gasoline_consumption(int gasolineConsumption) throws Throwable {
        onView(
                withId(R.id.et_gasoline_consumption)
        ).perform(typeText(Integer.toString(gasolineConsumption)));
    }

    @When("^I enter (\\d+\\.\\d+) as gasoline price$")
    public void i_enter_as_gasoline_price(float gasolinePrice) throws Throwable {
        DecimalFormat format = new DecimalFormat("#.##");
        onView(
                withId(R.id.et_gasoline_price)
        ).perform(typeText(format.format(gasolinePrice)));
    }

    @When("^I enter (\\d+) in km$")
    public void i_enter_in_km(int km) throws Throwable {
        onView(
                withId(R.id.et_distance)
        ).perform(typeText(Integer.toString(km)));
    }

    @When("I want to create my first new carpool$")
    public void i_want_to_create_my_first_new_carpool() {
        onView(withId(R.id.rv_turns))
                .check(matches(isEmptyRecyclerView()));
        I_want_to_create_a_new_carpool();
    }

    @When("^I a have no carpools available$")
    public void i_a_have_no_carpools_available() throws Throwable {
        onView(withId(R.id.rv_turns))
                .check(matches(isEmptyRecyclerView()));
    }

    @Then("^I should see an explanation to switch among carpools$")
    public void I_should_see_an_explanation_to_switch_among_carpools() throws Exception {
        Thread.sleep(1000);

        onView(
                withId(uk.co.deanwild.materialshowcaseview.R.id.tv_content)
        ).check(matches(withText(R.string.explanation_to_switch_among_carpools)));
        onView(
                withId(uk.co.deanwild.materialshowcaseview.R.id.tv_content)
        ).perform(click());
    }

    @Then("^I should see an explanation regarding overview$")
    public void i_should_see_an_explanation_regarding_overview() throws Exception {
        Thread.sleep(1000);

        onView(
                withId(uk.co.deanwild.materialshowcaseview.R.id.tv_content)
        ).check(matches(withText(R.string.explanation_turn_overview)));
        onView(
                withId(uk.co.deanwild.materialshowcaseview.R.id.tv_content)
        ).perform(click());
    }

    @Then("^I should see a hint to create a carpool$")
    public void i_should_see_a_hint_to_create_a_carpool() throws Throwable {
        onView(
                withId(uk.co.deanwild.materialshowcaseview.R.id.tv_content)
        ).check(matches(withText(R.string.you_should_create_a_carpool)));
        onView(
                withId(uk.co.deanwild.materialshowcaseview.R.id.tv_dismiss)
        ).perform(click());
    }

    @Then("^the turn of today is( not)? canceled$")
    public void turn_of_today_is_canceled(String not) throws Throwable {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        turnViewHandler.scrollTo(withDate(today));

        if (not != null) {
            turnViewHandler.checkTurn(isNotCanceledTurn());
        } else {
            turnViewHandler.checkTurn(isCanceledTurn());
        }
    }

    @Then("^I should see the turns older than (\\d+) months beginning from first day of month$")
    public void I_should_see_the_turns_older_than_x_months_beginning_from_first_day_of_month(final int months) {
        DateTime archiveDate = DateTime.now().withTimeAtStartOfDay().withDayOfMonth(1).minusMonths(months).minusDays(1);
        onView(withId(R.id.rv_turns))
                .perform(scrollToHolder(allOf(
                        withDate(archiveDate),
                        ViewMatchers.<RecyclerView.ViewHolder>isFirst()
                )));
    }

    @Then("^I should see (.+) in the title")
    public void I_should_see_carpool_name_in_the_title(final String carpool) {
        assertThat(getCurrentActivity(), hasTitle(carpool));
    }

    @Then("^I should see a confirmation dialog with message (.+?)$")
    public void I_should_see_a_confirmation_dialog_with_message(final String message) {
        onView(allOf(
                isAssignableFrom(TextView.class),
                isDescendantOfA(isAssignableFrom(MDRootLayout.class)),
                withText(message)
        )).check(matches(withEffectiveVisibility(VISIBLE)));
    }

    @Then("^the carpool (.+?) should not be available anymore$")
    public void the_carpool_x_should_not_be_available_anymore(final String carpoolName) {
        onView(allOf(
                isAssignableFrom(ImageButton.class),
                withParent(withClassName(endsWith("Toolbar")))
        )).perform(click());

        onView(withId(R.id.nav_view))
                .check(matches(not(hasCarpoolName(carpoolName))));
    }

    @Then("^I should see turns for the next (\\d+) days with (.+)$")
    public void I_should_see_turns_for_the_next_days_with_contacts(final int days, final String contacts) {
        DateTime startDate = DateTime.now().withTimeAtStartOfDay();
        DateTime endDate = startDate.plusDays(days);
        Interval interval = new Interval(startDate, endDate);
        turnViewHandler.checkTurnsWithinInterval(contacts, interval, matches(withEffectiveVisibility(VISIBLE)));
    }

    @Then("^I should see turns for the previous (\\d+) months with (.+)$")
    public void I_should_see_turns_for_the_previous_months_with_contacts(final int months, final String contacts) {
        DateTime endDate = DateTime.now().withTimeAtStartOfDay();
        DateTime startDate = endDate.minusMonths(months);
        Interval interval = new Interval(startDate, endDate);
        turnViewHandler.checkTurnsWithinInterval(contacts, interval, matches(withEffectiveVisibility(VISIBLE)));
    }

    @Then("^I should see contact (.+) as driver for the turn previously clicked$")
    public void I_should_see_contact_as_driver_for_the_turn_previously_clicked(final String contactName) {
        turnViewHandler.checkTurn(matches(
                hasDescendant(withContactName(contactName))
        ));
    }

    @Then("^I should see the turn of today$")
    public void I_should_see_the_turn_of_today() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        String todayString = Constants.getFullDateFormatter(getCurrentActivity()).print(today);

        onView(allOf(
                isAssignableFrom(TextView.class),
                withText(todayString)
        )).check(
                matches(withEffectiveVisibility(VISIBLE))
        );
    }

    @Then("^I should see the contact information of (.+?)$")
    public void I_should_see_the_contact_information_of_contact(final String contactName) {
        assertThatContactInformationMonitorHasBeenHit();
    }

    @Then("^I should see (\\d+)-(\\d+) for the last month$")
    public void i_should_see_money_put_aside_for_the_last_month(int minSavedMoney, int maxSavedMoney) throws Throwable {
        onView(
                withId(R.id.tv_money_put_aside_value)
        ).check(matches(allOf(
                hasMinSavedMoney(minSavedMoney),
                hasMaxSavedMoney(maxSavedMoney)
        )));
    }

    private static Matcher<View> hasMinSavedMoney(final double minSavedMoney) {
        return new TypeSafeMatcher<View>() {
            @Override
            @SneakyThrows
            public boolean matchesSafely(View textView) {
                if (!(textView instanceof TextView)) {
                    return false;
                }

                TextView tvMoney = (TextView) textView;

                NumberFormat format = NumberFormat.getCurrencyInstance(textView.getResources().getConfiguration().locale);
                Number parse = format.parse(tvMoney.getText().toString());

                return minSavedMoney <= parse.doubleValue();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("has min saved money ").appendValue(minSavedMoney);
            }
        };
    }

    private static Matcher<View> hasMaxSavedMoney(final double maxSavedMoney) {
        return new TypeSafeMatcher<View>() {
            @Override
            @SneakyThrows
            public boolean matchesSafely(View textView) {
                if (!(textView instanceof TextView)) {
                    return false;
                }

                TextView tvMoney = (TextView) textView;

                NumberFormat format = NumberFormat.getCurrencyInstance(textView.getResources().getConfiguration().locale);
                Number parse = format.parse(tvMoney.getText().toString());

                return maxSavedMoney >= parse.doubleValue();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("has max saved money ").appendValue(maxSavedMoney);
            }
        };
    }

    /**
     * Helper class to scroll to turns and perform actions on them
     */
    private class TurnViewHandler {

        private ViewHolderMatcher matcher;

        public void clickOnContactImageOfTurn(Matcher<RecyclerView.ViewHolder>... matchers) {
            scrollTo(matchers);

            RecyclerView rvTurns = (RecyclerView) getCurrentActivity().findViewById(R.id.rv_turns);
            RecyclerView.ViewHolder viewHolder = rvTurns.findViewHolderForAdapterPosition(matcher.getViewHolderPosition());
            MemberReadOnlyView mrov = (MemberReadOnlyView) viewHolder.itemView.findViewById(R.id.member_read_only_view);
            String memberName = ((TextView) mrov.findViewById(R.id.tv_member_name)).getText().toString();

            onView(allOf(
                    withId(R.id.iv_contact_image),
                    hasSibling(allOf(
                            isAssignableFrom(ViewGroup.class),
                            withChild(withText(memberName))
                    )),
                    ViewMatchers.<View>isFirst()
            )).perform(click());
        }

        public void clickOnTurn(Matcher<RecyclerView.ViewHolder>... matchers) {
            scrollTo(matchers);
            perform(click());
        }

        public void clickLongOnTurn(Matcher<RecyclerView.ViewHolder>... matchers) {
            scrollTo(matchers);
            perform(longClick());
        }

        public void clickOnTurnOf(DateTime date) {
            date = date.withTimeAtStartOfDay();
            Interval interval = new Interval(date, date.plusDays(1));
            clickOnTurn(withinInterval(interval));
        }

        public void clickLongOnTurnOf(DateTime date) {
            date = date.withTimeAtStartOfDay();
            Interval interval = new Interval(date, date.plusDays(1));
            clickLongOnTurn(withinInterval(interval));
        }

        public void clickOnLastTurnAgain() {
            perform(click());
        }

        public void checkTurn(ViewAssertion assertion) {
            onView(withId(R.id.rv_turns)).perform(
                    scrollToPosition(matcher.getViewHolderPosition())
            ).check(assertion);
        }

        public void checkTurn(Matcher<RecyclerView.ViewHolder> matches) {
            RecyclerView rvTurns = (RecyclerView) getActivity().findViewById(R.id.rv_turns);
            RecyclerView.ViewHolder turnViewHolder = rvTurns.findViewHolderForAdapterPosition(this.matcher.getViewHolderPosition());
            assertThat(turnViewHolder, matches);
        }

        @SneakyThrows(InterruptedException.class)
        public void scrollTo(Matcher<RecyclerView.ViewHolder>... matchers) {
            List<Matcher<RecyclerView.ViewHolder>> allMatchers = Lists.newArrayList(matchers);
            allMatchers.add(ViewMatchers.<RecyclerView.ViewHolder>isFirst());
            matcher = new ViewHolderMatcher(allMatchers);

            Thread.sleep(1000);

            onView(withId(R.id.rv_turns)).perform(scrollToHolder(matcher));
        }

        @SneakyThrows(InterruptedException.class)
        public void checkTurnsWithinInterval(final String contacts, final Interval interval, final ViewAssertion assertion) {
            String[] contactNames = contacts.split(",\\s*");
            for (String contact : contactNames) {
                Thread.sleep(750);

                onView(withId(R.id.rv_turns))
                        .perform(scrollToHolder(allOf(
                                withDriver(contact),
                                withinInterval(interval),
                                ViewMatchers.<RecyclerView.ViewHolder>isFirst()
                        )))
                        .check(assertion);
            }
        }

        private void perform(ViewAction action) {
            onView(withId(R.id.rv_turns)).perform(
                    scrollToPosition(matcher.getViewHolderPosition())
            );

            onView(withId(R.id.rv_turns)).perform(
                    actionOnItemAtPosition(matcher.getViewHolderPosition(), action)
            );
        }
    }

    private static Matcher<View> hasCarpoolName(final String carpoolName) {
        return new TypeSafeMatcher<View>() {

            @Override
            public void describeTo(Description description) {
                description.appendText("with carpool name: ").appendValue(carpoolName);
            }

            @Override
            public boolean matchesSafely(View view) {
                if (!(view instanceof NavigationView)) {
                    return false;
                }

                NavigationView nv = (NavigationView) view;
                MenuItem menuItem = nv.getMenu().getItem(1);
                SubMenu subMenu = menuItem.getSubMenu();
                for (int i = 0; i < subMenu.size(); ++i) {
                    MenuItem item = subMenu.getItem(i);
                    if (Objects.equals(item.getTitle().toString(), carpoolName)) {
                        return true;
                    }
                }

                return false;
            }
        };
    }
}


