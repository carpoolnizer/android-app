/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.fragments;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.annotation.IdRes;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.common.collect.Sets;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.robolectric.annotation.Config;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.model.Carpool;
import de.schrieveslaach.carpoolnizer.model.CarpoolBuilder;
import de.schrieveslaach.carpoolnizer.model.Member;
import de.schrieveslaach.carpoolnizer.model.Turn;
import de.schrieveslaach.carpoolnizer.model.Usage;
import de.schrieveslaach.carpoolnizer.ui.robolectric.fakes.ContactsCursor;
import de.schrieveslaach.carpoolnizer.ui.robolectric.shadows.ShadowRecyclerView;
import de.schrieveslaach.carpoolnizer.ui.views.MemberReadOnlyView;
import lombok.SneakyThrows;

import static de.schrieveslaach.carpoolnizer.ui.matchers.ActivityMatchers.startedActivity;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.clickable;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.displaysMember;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.exactlyOneChild;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.gone;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.hasText;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.visible;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.withDriverCount;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.robolectric.RuntimeEnvironment.application;

@Config(
        sdk = Build.VERSION_CODES.LOLLIPOP,
        shadows = ShadowRecyclerView.class
)
public class TurnDetailsFragmentTest extends BaseFragmentTest<TurnDetailsFragment> {

    private Member mike, john, daisy;

    private DateTime today;

    private ContactsCursor contactsCursor;

    private TurnDetailsFragment fragment;

    public TurnDetailsFragmentTest() {
        super(TurnDetailsFragment.class);
    }

    @Before
    public void init() throws Exception {
        today = DateTime.now().withTimeAtStartOfDay();
        fragment = openFragment();
    }

    @Before
    public void addMembersToContentResolver() {
        contactsCursor = new ContactsCursor()
                .withContact("John")
                .withContact("Mike")
                .withContact("Daisy")
                .fake();

        mike = contactsCursor.createMember("Mike");
        john = contactsCursor.createMember("John");
        daisy = contactsCursor.createMember("Daisy");
    }

    private Turn mockTurn(DateTime date) {
        return mockTurn(date, null);
    }

    private Turn mockTurn(DateTime date, Member driver, Member... passengers) {
        Turn turn = mock(Turn.class);

        Carpool carpool = mock(Carpool.class);
        when(carpool.getInvalidUsage(any(DateTime.class))).thenReturn(new HashSet<Usage>());
        when(turn.getCarpool()).thenReturn(carpool);

        when(turn.getDate()).thenReturn(date);
        when(turn.getDriver()).thenReturn(driver);
        when(turn.getPassengers()).thenReturn(Arrays.asList(passengers));
        if (driver != null) {
            List<Member> members = new ArrayList<Member>();
            members.add(driver);
            members.addAll(Arrays.asList(passengers));
            when(turn.getMembers()).thenReturn(members);
        }
        return turn;
    }

    @Test
    public void shouldDisplayNoneEditableTurn_UserCanNotDefineExceptionalUsage() throws Exception {
        Turn turn = mockTurn(today, mike, john);
        when(turn.getRemainingMembers()).thenReturn(Arrays.asList(daisy));
        fragment.setEditable(false);
        fragment.setTurn(turn);

        assertThat(fragment, hasDriver(allOf(
                not(clickable()),
                not(hasClickableBackground())
        )));
        assertThat(fragment, hasPassengers(allOf(
                not(clickable()),
                not(hasClickableBackground())
        )));
        assertThat(fragment, hasRemainingMembers(allOf(
                not(clickable()),
                not(hasClickableBackground())
        )));
    }

    @Test
    public void shouldDisplayTurn_DriverWithPassengersAndNoRemainingMembers() throws Exception {
        Turn turn = mockTurn(today, mike, john, daisy);
        fragment.setTurn(turn);

        assertThat(fragment, hasDriver(mike));
        assertThat(fragment, hasPassengers(john, daisy));
        assertThat(fragment, hasNoRemainingMembers());
    }

    @Test
    public void shouldDisplayTurnWithDriverCounts() throws Exception {
        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage("Mike")
                .withMemberAndEveryDayUsage("John")
                .withMemberAndEveryDayUsage("Daisy")
                .withCalculationDate(today)
                .create(contactsCursor);

        List<Turn> turns = carpool.calculateTurnsUntil(today.plusDays(2));

        fragment.setTurn(turns.get(0));

        assertThat(fragment, hasDriver(withDriverCount(mike, 1)));
        assertThat(fragment, hasPassengers(withDriverCount(john, 1), withDriverCount(daisy, 1)));
        assertThat(fragment, hasNoRemainingMembers());
    }

    @Test
    public void shouldDisplayTurn_DriverWithoutPassengersAndRemainingMembers() throws Exception {
        Turn turn = mockTurn(today, john);
        when(turn.getRemainingMembers()).thenReturn(Arrays.asList(mike, daisy));
        fragment.setTurn(turn);

        assertThat(fragment, hasDriver(john));
        assertThat(fragment, hasNoPassengers());
        assertThat(fragment, hasRemainingMembers(mike, daisy));
    }

    @Test
    @Ignore
    public void shouldDisplayTurn_UserCanDefineExceptionalUsage() throws Exception {
        Turn turn = mockTurn(today, mike, john);
        when(turn.getRemainingMembers()).thenReturn(Arrays.asList(daisy));
        fragment.setEditable(true);
        fragment.setTurn(turn);

        assertThat(fragment, hasDriver(allOf(
                        clickable(),
                        hasClickableBackground())
        ));
        assertThat(fragment, hasPassengers(allOf(
                        clickable(),
                        hasClickableBackground())
        ));
        assertThat(fragment, hasRemainingMembers(allOf(
                        clickable(),
                        hasClickableBackground())
        ));
    }

    @Test
    public void shouldDisplayTurn_WithoutDriverAndRemainingMembers() throws Exception {
        Turn turn = mockTurn(today);
        when(turn.getRemainingMembers()).thenReturn(Arrays.asList(john, mike, daisy));
        fragment.setTurn(turn);

        assertThat(fragment, hasNoDriver());
        assertThat(fragment, hasNoPassengers());
        assertThat(fragment, hasRemainingMembers(john, mike, daisy));
    }

    @SneakyThrows
    private void selectExceptionalUsage(Member m, Usage usage) {
        findMemberReadOnlyView(m, (ViewGroup) fragment.getView()).performClick();

        DialogFragment dialogFragment = (DialogFragment) fragment.getActivity().getSupportFragmentManager().findFragmentByTag("pick.usage");
        AlertDialog dlg = (AlertDialog) dialogFragment.getDialog();

        Field mAlertField = dlg.getClass().getDeclaredField("mAlert");
        mAlertField.setAccessible(true);
        Object alertController = mAlertField.get(dlg);
        Field mViewField = alertController.getClass().getDeclaredField("mView");
        mViewField.setAccessible(true);

        RecyclerView rv = (RecyclerView) mViewField.get(alertController);
        View itemView = rv.findViewHolderForAdapterPosition(usage.ordinal()).itemView;
        itemView.findViewById(R.id.tv_usage).performClick();
    }

    @Test
    public void shouldDefineExceptionalRule_Normal() throws Exception {
        TurnDetailsFragment.OnPickUsageListener listener = mock(TurnDetailsFragment.OnPickUsageListener.class);
        fragment.setOnPickUsageListener(listener);

        Turn turn = mockTurn(today, mike, john);
        when(turn.getRemainingMembers()).thenReturn(Arrays.asList(daisy));
        fragment.setTurn(turn);
        fragment.setEditable(true);

        selectExceptionalUsage(mike, Usage.NORMAL);

        verify(listener).onFinish(mike, Usage.NORMAL);
    }

    @Test
    public void shouldDefineExceptionalRule_CanNotDrive() throws Exception {
        TurnDetailsFragment.OnPickUsageListener listener = mock(TurnDetailsFragment.OnPickUsageListener.class);
        fragment.setOnPickUsageListener(listener);

        Turn turn = mockTurn(today, mike, john);
        when(turn.getRemainingMembers()).thenReturn(Arrays.asList(daisy));
        fragment.setEditable(true);
        fragment.setTurn(turn);

        selectExceptionalUsage(john, Usage.CAN_NOT_DRIVE);

        verify(listener).onFinish(john, Usage.CAN_NOT_DRIVE);
    }

    @Test
    public void shouldDefineExceptionalRule_HasToDrive() throws Exception {
        TurnDetailsFragment.OnPickUsageListener listener = mock(TurnDetailsFragment.OnPickUsageListener.class);
        fragment.setOnPickUsageListener(listener);

        Turn turn = mockTurn(today, mike, john);
        when(turn.getRemainingMembers()).thenReturn(Arrays.asList(daisy));
        fragment.setEditable(true);
        fragment.setTurn(turn);

        selectExceptionalUsage(daisy, Usage.HAS_TO_DRIVE);

        verify(listener).onFinish(daisy, Usage.HAS_TO_DRIVE);
    }

    @Test
    public void shouldDefineExceptionalRule_DoesNotUse() throws Exception {
        TurnDetailsFragment.OnPickUsageListener listener = mock(TurnDetailsFragment.OnPickUsageListener.class);
        fragment.setOnPickUsageListener(listener);

        Turn turn = mockTurn(today, mike, john);
        when(turn.getRemainingMembers()).thenReturn(Arrays.asList(daisy));
        fragment.setEditable(true);
        fragment.setTurn(turn);

        selectExceptionalUsage(mike, Usage.DOES_NOT_USE);

        verify(listener).onFinish(mike, Usage.DOES_NOT_USE);
    }

    @Test
    public void shouldShowExceptionalUsage_OpenedTurnWithExceptionalUsage_Normal() throws Exception {
        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage("Mike")
                .withMemberAndEveryDayUsage("John")
                .withExceptionalUsage("Mike", today, Usage.NORMAL)
                .withCalculationDate(today);

        Member mike = builder.getMember("Mike", contactsCursor);
        Member john = builder.getMember("John", contactsCursor);

        Carpool carpool = builder.create(contactsCursor);
        fragment.setTurn(carpool.getTurnsInThePast().get(0));

        assertThat(fragment, showsExceptionalUsageFor(mike, Usage.NORMAL));
        assertThat(fragment, showsNoExceptionalUsageFor(john));
    }

    @Test
    public void shouldShowExceptionalUsage_OpenedTurnWithExceptionalUsage_CanNotDriver() throws Exception {
        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage("Mike")
                .withMemberAndEveryDayUsage("John")
                .withExceptionalUsage("Mike", today, Usage.CAN_NOT_DRIVE)
                .withCalculationDate(today);

        Member mike = builder.getMember("Mike", contactsCursor);
        Member john = builder.getMember("John", contactsCursor);

        Carpool carpool = builder.create(contactsCursor);
        fragment.setTurn(carpool.getTurnsInThePast().get(0));

        assertThat(fragment, showsExceptionalUsageFor(mike, Usage.CAN_NOT_DRIVE));
        assertThat(fragment, showsNoExceptionalUsageFor(john));
    }

    @Test
    public void shouldShowExceptionalUsage_OpenedTurnWithExceptionalUsage_DoesNotUse() throws Exception {
        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage("Mike")
                .withMemberAndEveryDayUsage("John")
                .withExceptionalUsage("Mike", today, Usage.DOES_NOT_USE)
                .withCalculationDate(today);

        Member mike = builder.getMember("Mike", contactsCursor);
        Member john = builder.getMember("John", contactsCursor);

        Carpool carpool = builder.create(contactsCursor);
        Turn turn = carpool.getTurnsInThePast().get(0);
        fragment.setTurn(turn);

        assertThat(fragment, showsExceptionalUsageFor(mike, Usage.DOES_NOT_USE));
        assertThat(fragment, showsNoExceptionalUsageFor(john));
    }

    @Test
    public void shouldShowExceptionalUsage_OpenedTurnWithExceptionalUsage_HasToDrive() throws Exception {
        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage("Mike")
                .withMemberAndEveryDayUsage("John")
                .withExceptionalUsage("Mike", today, Usage.HAS_TO_DRIVE)
                .withCalculationDate(today);

        Member mike = builder.getMember("Mike", contactsCursor);
        Member john = builder.getMember("John", contactsCursor);

        Carpool carpool = builder.create(contactsCursor);
        fragment.setTurn(carpool.getTurnsInThePast().get(0));

        assertThat(fragment, showsExceptionalUsageFor(mike, Usage.HAS_TO_DRIVE));
        assertThat(fragment, showsNoExceptionalUsageFor(john));
    }

    @Test
    public void shoudShowCanceledCarpoolView_TurnIsCanceled() {
        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage("Mike")
                .withMemberAndEveryDayUsage("John")
                .withMember("Daisy")
                .create(contactsCursor);

        DateTime today = DateTime.now().withTimeAtStartOfDay();
        carpool.cancel(today);
        Turn turn = carpool.calculateTurnsUntil(today).get(0);

        fragment.setTurn(turn);

        assertThat(fragment, showsCanceledTurn());
        assertThat(fragment, isDriverViewGone());
        assertThat(fragment, hasNoDriver());
        assertThat(fragment, hasNoPassengers());
        assertThat(fragment.getView().findViewById(R.id.tv_remaining_members), is(gone()));
        assertThat(fragment.getView().findViewById(R.id.ll_remaining_members), is(gone()));
    }

    @Test
    public void shouldHideCanceledCarpoolView_TurnIsNotCanceled() {
        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage("Mike")
                .create(contactsCursor);

        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Turn turn = carpool.calculateTurnsUntil(today).get(0);

        fragment.setTurn(turn);

        assertThat(fragment, not(showsCanceledTurn()));
        assertThat(fragment, not(isDriverViewGone()));
        assertThat(fragment, hasDriver(mike));
    }

    @Test
    public void shouldOpenContactInformation_Driver() throws Exception {
        Turn turn = mockTurn(today, john);
        fragment.setEditable(false);
        fragment.setTurn(turn);

        fragment.getView().findViewById(R.id.mv_driver).findViewById(R.id.iv_contact_image).performClick();

        Intent expectedIntent = new Intent(Intent.ACTION_VIEW);
        expectedIntent.setData(contactsCursor.createContactUri("John"));

        assertThat(fragment.getActivity(), startedActivity(expectedIntent));
    }

    @Test
    public void shouldOpenContactInformation_Passenger() throws Exception {
        Turn turn = mockTurn(today, mike, john);
        fragment.setEditable(false);
        fragment.setTurn(turn);

        fragment.getView().findViewById(R.id.ll_passengers).findViewById(R.id.iv_contact_image).performClick();

        Intent expectedIntent = new Intent(Intent.ACTION_VIEW);
        expectedIntent.setData(contactsCursor.createContactUri("John"));

        assertThat(fragment.getActivity(), startedActivity(expectedIntent));
    }

    @Test
    public void shouldOpenContactInformation_RemainingMembers() throws Exception {
        Turn turn = mockTurn(today, mike);
        when(turn.getRemainingMembers()).thenReturn(Arrays.asList(john));
        fragment.setEditable(false);
        fragment.setTurn(turn);

        fragment.getView().findViewById(R.id.ll_remaining_members).findViewById(R.id.iv_contact_image).performClick();

        Intent expectedIntent = new Intent(Intent.ACTION_VIEW);
        expectedIntent.setData(contactsCursor.createContactUri("John"));

        assertThat(fragment.getActivity(), startedActivity(expectedIntent));
    }

    @Test
    public void shouldNotBeAbleToPickHasToDrive_OneMemberAlreadyHasToDriver() {
        TurnDetailsFragment.OnPickUsageListener listener = mock(TurnDetailsFragment.OnPickUsageListener.class);
        fragment.setOnPickUsageListener(listener);

        Turn turn = mockTurn(today, mike, john);
        when(turn.getCarpool().getInvalidUsage(today)).thenReturn(Sets.newHashSet(Usage.HAS_TO_DRIVE));
        fragment.setEditable(true);
        fragment.setTurn(turn);

        selectExceptionalUsage(john, Usage.HAS_TO_DRIVE);

        verify(listener, never()).onFinish(john, Usage.HAS_TO_DRIVE);
    }

    @Test
    public void shouldNotBeAbleToPickCanNotDrive_ForAllContacts() {
        TurnDetailsFragment.OnPickUsageListener listener = mock(TurnDetailsFragment.OnPickUsageListener.class);
        fragment.setOnPickUsageListener(listener);

        Turn turn = mockTurn(today, mike, john);
        when(turn.getCarpool().getInvalidUsage(today)).thenReturn(Sets.newHashSet(Usage.CAN_NOT_DRIVE));
        fragment.setEditable(true);
        fragment.setTurn(turn);

        selectExceptionalUsage(john, Usage.CAN_NOT_DRIVE);

        verify(listener, never()).onFinish(john, Usage.CAN_NOT_DRIVE);
    }

    @Test
    public void shouldNotBeAbleToPickCanNotDrive_AllAreRemainingMembers() {
        TurnDetailsFragment.OnPickUsageListener listener = mock(TurnDetailsFragment.OnPickUsageListener.class);
        fragment.setOnPickUsageListener(listener);

        Turn turn = mockTurn(today);
        when(turn.getCarpool().getInvalidUsage(today)).thenReturn(Sets.newHashSet(Usage.CAN_NOT_DRIVE));
        when(turn.getRemainingMembers()).thenReturn(Arrays.asList(john, mike));
        fragment.setEditable(true);
        fragment.setTurn(turn);

        selectExceptionalUsage(john, Usage.CAN_NOT_DRIVE);

        verify(listener, never()).onFinish(john, Usage.CAN_NOT_DRIVE);
    }

    public static Matcher<TurnDetailsFragment> isDriverViewGone() {
        return new TypeSafeMatcher<TurnDetailsFragment>() {

            @Override
            protected boolean matchesSafely(TurnDetailsFragment fragment) {
                return gone().matches(fragment.getView().findViewById(R.id.tv_driver)) &&
                        gone().matches(fragment.getView().findViewById(R.id.mv_driver));
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("driver text view is gone and driver view is gone");
            }

            @Override
            protected void describeMismatchSafely(TurnDetailsFragment fragment, Description mismatchDescription) {
                if (gone().matches(fragment.getView().findViewById(R.id.tv_driver))) {
                    mismatchDescription.appendText("driver text view is gone");
                } else {
                    mismatchDescription.appendText("driver text view is not gone");
                }
                mismatchDescription.appendText(" and ");
                if (gone().matches(fragment.getView().findViewById(R.id.mv_driver))) {
                    mismatchDescription.appendText("driver is gone");
                } else {
                    mismatchDescription.appendText("driver is not gone");
                }
            }
        };
    }

    public static Matcher<TurnDetailsFragment> showsCanceledTurn() {
        return new TypeSafeMatcher<TurnDetailsFragment>() {
            @Override
            protected boolean matchesSafely(TurnDetailsFragment fragment) {
                return is(visible()).matches(fragment.getView().findViewById(R.id.tv_canceled_turn))
                        && is(visible()).matches(fragment.getView().findViewById(R.id.iv_canceled_turn));
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("shows that turn is canceled");
            }

            @Override
            protected void describeMismatchSafely(TurnDetailsFragment fragment, Description mismatchDescription) {
                View view = fragment.getView().findViewById(R.id.iv_canceled_turn);
                mismatchDescription.appendText("image view ");
                is(visible()).describeMismatch(view, mismatchDescription);
                view = fragment.getView().findViewById(R.id.tv_canceled_turn);
                mismatchDescription.appendText("and text view ");
                is(visible()).describeMismatch(view, mismatchDescription);
            }
        };
    }

    public static Matcher<TurnDetailsFragment> hasDriver(final Member driver) {
        return hasDriver(withDriverCount(driver, null));
    }

    public static Matcher<TurnDetailsFragment> hasDriver(final Matcher<MemberReadOnlyView> matcher) {
        return new TypeSafeMatcher<TurnDetailsFragment>() {
            @Override
            public boolean matchesSafely(TurnDetailsFragment fragment) {
                MemberReadOnlyView mrovDriver = (MemberReadOnlyView) fragment.getView().findViewById(R.id.mv_driver);
                return matcher.matches(mrovDriver);
            }

            @Override
            public void describeTo(Description description) {
                description
                        .appendText("has driver with")
                        .appendDescriptionOf(matcher);
            }

            @Override
            protected void describeMismatchSafely(TurnDetailsFragment fragment, Description mismatchDescription) {
                MemberReadOnlyView mrovDriver = (MemberReadOnlyView) fragment.getView().findViewById(R.id.mv_driver);
                matcher.describeMismatch(mrovDriver, mismatchDescription);
            }
        };
    }

    private static Matcher<MemberReadOnlyView> hasClickableBackground() {
        return new TypeSafeMatcher<MemberReadOnlyView>() {
            @Override
            protected boolean matchesSafely(MemberReadOnlyView item) {
                Drawable clickableBackground = application.getResources().getDrawable(R.drawable.clickable_background);
                return new EqualsBuilder()
                        .append(clickableBackground, item.getBackground())
                        .build();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(" should have clickable background");
            }

            @Override
            protected void describeMismatchSafely(MemberReadOnlyView item, Description mismatchDescription) {
                mismatchDescription.appendText("background: ").appendValue(item.getBackground());
            }
        };
    }

    public static Matcher<TurnDetailsFragment> hasNoDriver() {
        return new TypeSafeMatcher<TurnDetailsFragment>() {
            @Override
            public boolean matchesSafely(TurnDetailsFragment fragment) {
                MemberReadOnlyView mrovDriver = (MemberReadOnlyView) fragment.getView().findViewById(R.id.mv_driver);
                return gone().matches(mrovDriver) && displaysMember(null).matches(mrovDriver);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("no driver is displayed");
            }

            @Override
            protected void describeMismatchSafely(TurnDetailsFragment fragment, Description mismatchDescription) {
                MemberReadOnlyView mrovDriver = (MemberReadOnlyView) fragment.getView().findViewById(R.id.mv_driver);
                mismatchDescription.appendText("driver view is ");
                gone().describeMismatch(mrovDriver, mismatchDescription);
                mismatchDescription.appendText(" and ");
                displaysMember(null).describeMismatch(mrovDriver, mismatchDescription);
            }
        };
    }

    public static Matcher<TurnDetailsFragment> hasPassengers(final Matcher<MemberReadOnlyView>... matchers) {
        return VisibleMemberGroupMatcher.createWithMatchers(
                R.id.tv_passengers,
                R.id.ll_passengers,
                Arrays.asList(matchers)
        );
    }

    public static Matcher<TurnDetailsFragment> hasPassengers(final Member... passengers) {
        return VisibleMemberGroupMatcher.createWithMembers(
                R.id.tv_passengers,
                R.id.ll_passengers,
                Arrays.asList(passengers)
        );
    }

    public static Matcher<TurnDetailsFragment> hasNoPassengers() {
        return new GoneMemberGroupMatcher(
                R.id.tv_passengers,
                R.id.ll_passengers
        );
    }

    public static Matcher<TurnDetailsFragment> hasRemainingMembers(final Matcher<MemberReadOnlyView>... matchers) {
        return VisibleMemberGroupMatcher.createWithMatchers(
                R.id.tv_remaining_members,
                R.id.ll_remaining_members,
                Arrays.asList(matchers)
        );
    }

    public static Matcher<TurnDetailsFragment> hasRemainingMembers(final Member... passengers) {
        return VisibleMemberGroupMatcher.createWithMembers(
                R.id.tv_remaining_members,
                R.id.ll_remaining_members,
                Arrays.asList(passengers)
        );
    }

    public static Matcher<TurnDetailsFragment> hasNoRemainingMembers() {
        return new GoneMemberGroupMatcher(
                R.id.tv_remaining_members,
                R.id.ll_remaining_members
        );
    }

    public static Matcher<TurnDetailsFragment> showsExceptionalUsageFor(final Member member, Usage usage) {
        return new ExceptionalUsageMatcher(member, usage);
    }

    public static Matcher<TurnDetailsFragment> showsNoExceptionalUsageFor(final Member member) {
        return new ExceptionalUsageMatcher(member, null);
    }

    private static class VisibleMemberGroupMatcher extends TypeSafeMatcher<TurnDetailsFragment> {

        private List<Matcher<MemberReadOnlyView>> matchers;

        @IdRes
        private int linearLayoutId;

        @IdRes
        private int textViewId;

        private VisibleMemberGroupMatcher() {
        }

        private static VisibleMemberGroupMatcher createWithMembers(@IdRes int textViewId, @IdRes int linearLayoutId, List<Member> members) {
            VisibleMemberGroupMatcher vmgm = new VisibleMemberGroupMatcher();
            vmgm.linearLayoutId = linearLayoutId;
            vmgm.textViewId = textViewId;
            vmgm.matchers = new ArrayList<Matcher<MemberReadOnlyView>>();
            for (Member m : members) {
                vmgm.matchers.add(withDriverCount(m, null));
            }
            return vmgm;
        }

        private static VisibleMemberGroupMatcher createWithMatchers(@IdRes int textViewId, @IdRes int linearLayoutId, List<Matcher<MemberReadOnlyView>> matchers) {
            VisibleMemberGroupMatcher vmgm = new VisibleMemberGroupMatcher();
            vmgm.linearLayoutId = linearLayoutId;
            vmgm.textViewId = textViewId;
            vmgm.matchers = matchers;
            return vmgm;
        }

        @Override
        public boolean matchesSafely(TurnDetailsFragment fragment) {
            if (!visible().matches(fragment.getView().findViewById(textViewId))) {
                return false;
            }

            LinearLayout ll = (LinearLayout) fragment.getView().findViewById(linearLayoutId);

            if (matchers.size() != ll.getChildCount()) {
                return false;
            }

            for (Matcher<MemberReadOnlyView> m : matchers) {
                if (!exactlyOneChild(m).matches(ll)) {
                    return false;
                }
            }
            return true;
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("members should match:");
            description.appendDescriptionOf(allOf(matchers.toArray(new Matcher[matchers.size()])));
        }

        @Override
        protected void describeMismatchSafely(TurnDetailsFragment fragment, Description mismatchDescription) {
            LinearLayout ll = (LinearLayout) fragment.getView().findViewById(linearLayoutId);

            for (Matcher<MemberReadOnlyView> m : matchers) {
                exactlyOneChild(m).describeMismatch(ll, mismatchDescription);
                mismatchDescription.appendText(" and ");
            }
        }
    }

    private static class GoneMemberGroupMatcher extends TypeSafeMatcher<TurnDetailsFragment> {

        @IdRes
        private final int linearLayoutId;

        @IdRes
        private final int textViewId;

        private GoneMemberGroupMatcher(@IdRes int textViewId, @IdRes int linearLayoutId) {
            this.linearLayoutId = linearLayoutId;
            this.textViewId = textViewId;
        }

        @Override
        public boolean matchesSafely(TurnDetailsFragment fragment) {
            if (!gone().matches(fragment.getView().findViewById(textViewId))) {
                return false;
            }

            LinearLayout ll = (LinearLayout) fragment.getView().findViewById(linearLayoutId);
            return ll.getChildCount() == 0;
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("group of text view and linear layout are gone");
        }

        @Override
        protected void describeMismatchSafely(TurnDetailsFragment fragment, Description mismatchDescription) {
            if (gone().matches(fragment.getView().findViewById(textViewId))) {
                mismatchDescription.appendText("text view is gone");
            } else {
                mismatchDescription.appendText("text view is not gone");
            }

            LinearLayout ll = (LinearLayout) fragment.getView().findViewById(linearLayoutId);
            mismatchDescription.appendText(" and linear has ").appendValue(ll.getChildCount()).appendText(" childs");
        }
    }

    private static class ExceptionalUsageMatcher extends TypeSafeMatcher<TurnDetailsFragment> {

        private final Member member;

        private final Usage usage;

        private ExceptionalUsageMatcher(Member member, Usage usage) {
            this.member = member;
            this.usage = usage;
        }

        @Override
        protected boolean matchesSafely(TurnDetailsFragment fragment) {
            MemberReadOnlyView mrov = findMemberReadOnlyView(fragment);
            TextView tvExceptionalUsage = (TextView) mrov.findViewById(TurnDetailsFragment.EXCEPTIONAL_USAGE_ID);
            if (tvExceptionalUsage == null) {

                return usage == null;
            }

            int textId;
            switch (usage) {
                case HAS_TO_DRIVE:
                    textId = R.string.has_to_drive_usage;
                    break;
                case DOES_NOT_USE:
                    textId = R.string.does_not_use_usage;
                    break;
                case CAN_NOT_DRIVE:
                    textId = R.string.can_not_drive_usage;
                    break;
                default:
                    textId = R.string.normal_usage;
                    break;
            }
            return hasText(textId).matches(tvExceptionalUsage);
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("should display ");
            if (usage != null) {
                description.appendText(" usage = ")
                        .appendValue(usage);
            } else {
                description.appendText(" no usage");
            }
            description.appendText(" for ")
                    .appendValue(member);
        }

        @Override
        protected void describeMismatchSafely(TurnDetailsFragment fragment, Description mismatchDescription) {
            MemberReadOnlyView mrov = findMemberReadOnlyView(fragment);
            TextView tvExceptionalUsage = (TextView) mrov.findViewById(TurnDetailsFragment.EXCEPTIONAL_USAGE_ID);
            if (tvExceptionalUsage == null) {
                mismatchDescription.appendText("text view for usage is missing");
            } else {
                mismatchDescription
                        .appendText("text view for usage contains: ")
                        .appendValue(tvExceptionalUsage.getText());
            }
        }

        private MemberReadOnlyView findMemberReadOnlyView(TurnDetailsFragment fragment) {
            RelativeLayout rl = (RelativeLayout) fragment.getView().findViewById(R.id.rl_turn_details);
            return TurnDetailsFragmentTest.findMemberReadOnlyView(member, rl);
        }
    }

    private static MemberReadOnlyView findMemberReadOnlyView(Member member, ViewGroup vg) {
        for (int i = 0; i < vg.getChildCount(); ++i) {
            View child = vg.getChildAt(i);
            if (child instanceof MemberReadOnlyView) {
                MemberReadOnlyView mrov = (MemberReadOnlyView) child;
                if (member.equals(mrov.getMember())) {
                    return mrov;
                }
            }

            if (child instanceof ViewGroup) {
                MemberReadOnlyView mrov = findMemberReadOnlyView(member, (ViewGroup) child);
                if (mrov != null) {
                    return mrov;
                }
            }
        }
        return null;
    }
}