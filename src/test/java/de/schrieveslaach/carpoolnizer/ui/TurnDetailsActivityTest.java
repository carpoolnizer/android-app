/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowLooper;

import java.net.URI;
import java.util.ArrayList;

import de.schrieveslaach.carpoolnizer.Constants;
import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.model.Carpool;
import de.schrieveslaach.carpoolnizer.model.Member;
import de.schrieveslaach.carpoolnizer.model.Usage;
import de.schrieveslaach.carpoolnizer.repository.CarpoolDAO;
import de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers;
import de.schrieveslaach.carpoolnizer.ui.robolectric.shadows.ShadowRecyclerView;
import de.schrieveslaach.carpoolnizer.ui.views.MemberReadOnlyView;

import static de.schrieveslaach.carpoolnizer.Constants.getFullDateFormatter;
import static de.schrieveslaach.carpoolnizer.ui.fragments.TurnDetailsFragmentTest.hasDriver;
import static de.schrieveslaach.carpoolnizer.ui.fragments.TurnDetailsFragmentTest.hasNoDriver;
import static de.schrieveslaach.carpoolnizer.ui.fragments.TurnDetailsFragmentTest.hasNoPassengers;
import static de.schrieveslaach.carpoolnizer.ui.fragments.TurnDetailsFragmentTest.hasNoRemainingMembers;
import static de.schrieveslaach.carpoolnizer.ui.fragments.TurnDetailsFragmentTest.hasPassengers;
import static de.schrieveslaach.carpoolnizer.ui.fragments.TurnDetailsFragmentTest.hasRemainingMembers;
import static de.schrieveslaach.carpoolnizer.ui.fragments.TurnDetailsFragmentTest.showsExceptionalUsageFor;
import static de.schrieveslaach.carpoolnizer.ui.fragments.TurnDetailsFragmentTest.showsNoExceptionalUsageFor;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ActivityMatchers.hasResult;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ActivityMatchers.hasSubtitleTitle;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ActivityMatchers.hasTitle;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ActivityMatchers.isFinishing;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ActivityMatchers.startedActivity;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.hasText;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.withDriverCount;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.robolectric.RuntimeEnvironment.application;

@Config(
        sdk = Build.VERSION_CODES.LOLLIPOP,
        shadows = ShadowRecyclerView.class
)
@Ignore
public class TurnDetailsActivityTest extends AbstractRobolectricActivityTester<TurnDetailsActivity_> {

    private Member john;
    private Member mike;
    private Member daisy;

    private DateTime today;

    public TurnDetailsActivityTest() {
        super(TurnDetailsActivity_.class);
    }

    @Before
    public void init() throws Exception {
        today = DateTime.now().withTimeAtStartOfDay();
    }

    @Before
    public void initMembers() {
        john = contactsCursor.createMember("John");
        mike = contactsCursor.createMember("Mike");
        daisy = contactsCursor.createMember("Daisy");
    }

    @Test
    public void shouldDisplayTurn_SetTitles() throws Exception {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        new ActivityBuilder()
                .withStartDate(today)
                .withCalculationDate(today)
                .withTurnDate(today)
                .withPassenger("Mike")
                .withPassenger("John")
                .withPassenger("Daisy")
                .start();

        assertThat(activity, hasTitle("Name"));
        assertThat(activity, hasSubtitleTitle(R.string.app_name));
    }

    @Test
    public void shouldDisplayArchivedTurn_IgnoreClickEvent() throws Exception {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime startDate = today.withDayOfMonth(1).minusMonths(6);
        DateTime turnDate = startDate.plusMonths(1);

        new ActivityBuilder()
                .withStartDate(startDate)
                .withCalculationDate(today)
                .withTurnDate(turnDate)
                .withPassenger("Mike")
                .withPassenger("John")
                .withPassenger("Daisy")
                .withExceptionalRule("Mike", turnDate, Usage.HAS_TO_DRIVE)
                .start()
                .getViewOfMember("Mike")
                .performClick();

        DialogFragment fragment = (DialogFragment) activity.getSupportFragmentManager().findFragmentByTag("pick.usage");
        assertThat(fragment, is(nullValue()));
    }

    @Test
    public void shouldDisplayArchivedTurn_DriverWithPassengersAndNoRemainingMembers() throws Exception {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime startDate = today.withDayOfMonth(1).minusMonths(6);
        DateTime turnDate = startDate.plusMonths(1);

        new ActivityBuilder()
                .withStartDate(startDate)
                .withCalculationDate(today)
                .withTurnDate(turnDate)
                .withPassenger("Mike")
                .withPassenger("John")
                .withPassenger("Daisy")
                .withExceptionalRule("Mike", turnDate, Usage.HAS_TO_DRIVE)
                .start();

        assertThat(activity.getCurrentFragment(), hasDriver(mike));
        assertThat(activity.getCurrentFragment(), hasPassengers(john, daisy));
        assertThat(activity.getCurrentFragment(), hasNoRemainingMembers());
    }

    @Test
    public void shouldDisplayArchivedTurn_UserCanNotDefineExceptionalUsage() throws Exception {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime startDate = today.withDayOfMonth(1).minusMonths(6);
        DateTime turnDate = startDate.plusMonths(1);

        new ActivityBuilder()
                .withStartDate(startDate)
                .withCalculationDate(today)
                .withTurnDate(turnDate)
                .withPassenger("Mike")
                .withPassenger("John")
                .withRemainingMember("Daisy")
                .withExceptionalRule("Mike", turnDate, Usage.HAS_TO_DRIVE)
                .start();

        assertThat(activity.getCurrentFragment(), hasDriver(not(ViewMatchers.<MemberReadOnlyView>clickable())));
        assertThat(activity.getCurrentFragment(), hasPassengers(not(ViewMatchers.<MemberReadOnlyView>clickable())));
        assertThat(activity.getCurrentFragment(), hasRemainingMembers(not(ViewMatchers.<MemberReadOnlyView>clickable())));
    }

    @Test
    public void shouldDisplayTurnInTheFuture_DriverWithPassengersAndNoRemainingMembers() throws Exception {
        DateTime yesterday = today.minusDays(1);
        DateTime tomorrow = today.plusDays(1);

        new ActivityBuilder()
                .withStartDate(yesterday)
                .withCalculationDate(yesterday)
                .withTurnDate(tomorrow)
                .withPassenger("Mike")
                .withPassenger("John")
                .withPassenger("Daisy")
                .start();

        assertThat(activity.getCurrentFragment(), hasDriver(mike));
        assertThat(activity.getCurrentFragment(), hasPassengers(john, daisy));
        assertThat(activity.getCurrentFragment(), hasNoRemainingMembers());
    }

    @Test
    public void shouldDisplayTurnWithDriverCounts() throws Exception {
        DateTime eightDaysAgo = today.minusDays(8);

        new ActivityBuilder()
                .withStartDate(eightDaysAgo)
                .withCalculationDate(today)
                .withTurnDate(today)
                .withPassenger("Mike")
                .withPassenger("John")
                .withPassenger("Daisy")
                .start();

        assertThat(activity.getCurrentFragment(), hasDriver(withDriverCount(daisy, 3)));
        assertThat(activity.getCurrentFragment(), hasPassengers(withDriverCount(john, 3), withDriverCount(mike, 3)));
        assertThat(activity.getCurrentFragment(), hasNoRemainingMembers());
    }

    @Test
    public void shouldDisplayTurnOfToday_DriverWithPassengersAndNoRemainingMembers() throws Exception {
        DateTime yesterday = today.minusDays(1);

        new ActivityBuilder()
                .withStartDate(yesterday)
                .withCalculationDate(today)
                .withTurnDate(today)
                .withPassenger("Mike")
                .withPassenger("John")
                .withPassenger("Daisy")
                .start();

        assertThat(activity.getCurrentFragment(), hasDriver(mike));
        assertThat(activity.getCurrentFragment(), hasPassengers(john, daisy));
        assertThat(activity.getCurrentFragment(), hasNoRemainingMembers());
    }

    @Test
    public void shouldDisplayTurnInThePast_DriverWithPassengersAndNoRemainingMembers() throws Exception {
        DateTime yesterday = today.minusDays(1);

        new ActivityBuilder()
                .withStartDate(yesterday)
                .withCalculationDate(today)
                .withTurnDate(yesterday)
                .withPassenger("Mike")
                .withPassenger("John")
                .withPassenger("Daisy")
                .start();

        assertThat(activity.getCurrentFragment(), hasDriver(john));
        assertThat(activity.getCurrentFragment(), hasPassengers(mike, daisy));
        assertThat(activity.getCurrentFragment(), hasNoRemainingMembers());
    }

    @Test
    public void shouldDisplayTurn_DriverWithoutPassengersAndRemainingMembers() throws Exception {
        new ActivityBuilder()
                .withStartDate(today)
                .withCalculationDate(today)
                .withTurnDate(today)
                .withDriver("John")
                .withRemainingMember("Mike")
                .withRemainingMember("Daisy")
                .start();

        assertThat(activity.getCurrentFragment(), hasDriver(john));
        assertThat(activity.getCurrentFragment(), hasNoPassengers());
        assertThat(activity.getCurrentFragment(), hasRemainingMembers(mike, daisy));
    }

    @Test
    public void shouldDisplayTurn_UserCanDefineExceptionalUsage() throws Exception {
        new ActivityBuilder()
                .withStartDate(today)
                .withCalculationDate(today)
                .withTurnDate(today)
                .withDriver("John")
                .withPassenger("Mike")
                .withRemainingMember("Daisy")
                .start();

        assertThat(activity.getCurrentFragment(), hasDriver(ViewMatchers.<MemberReadOnlyView>clickable()));
        assertThat(activity.getCurrentFragment(), hasPassengers(ViewMatchers.<MemberReadOnlyView>clickable()));
        assertThat(activity.getCurrentFragment(), hasRemainingMembers(ViewMatchers.<MemberReadOnlyView>clickable()));
    }

    @Test
    public void shouldDisplayTurn_WithoutDriverAndRemainingMembers() throws Exception {
        new ActivityBuilder()
                .withStartDate(today)
                .withCalculationDate(today)
                .withTurnDate(today)
                .withRemainingMember("John")
                .withRemainingMember("Mike")
                .withRemainingMember("Daisy")
                .start();

        assertThat(activity.getCurrentFragment(), hasNoDriver());
        assertThat(activity.getCurrentFragment(), hasNoPassengers());
        assertThat(activity.getCurrentFragment(), hasRemainingMembers(john, mike, daisy));
    }

    private void selectExceptionalUsage(Usage usage) {
        DialogFragment fragment = (DialogFragment) activity.getSupportFragmentManager().findFragmentByTag("pick.usage");
        Dialog dlg = fragment.onCreateDialog(new Bundle());
        // TODO View itemView = dlg.getRecyclerView().findViewHolderForAdapterPosition(usage.ordinal()).itemView;
        // TODO itemView.findViewById(R.id.tv_usage).performClick();
    }

    @Test
    public void shouldDefineExceptionalRule_Normal() throws Exception {
        new ActivityBuilder()
                .withStartDate(today)
                .withCalculationDate(today)
                .withTurnDate(today)
                .withDriver("John")
                .withRemainingMember("Mike")
                .withRemainingMember("Daisy")
                .start()
                .getViewOfMember("Mike")
                .performClick();

        selectExceptionalUsage(Usage.NORMAL);

        assertThat(activity.getCurrentFragment(), hasDriver(john));
        assertThat(activity.getCurrentFragment(), hasPassengers(mike));
        assertThat(activity.getCurrentFragment(), hasRemainingMembers(daisy));
    }

    @Test
    public void shouldDefineExceptionalRule_CanNotDrive() throws Exception {
        new ActivityBuilder()
                .withStartDate(today)
                .withCalculationDate(today)
                .withTurnDate(today)
                .withDriver("John")
                .withPassenger("Mike")
                .start()
                .getViewOfMember("John")
                .performClick();

        selectExceptionalUsage(Usage.CAN_NOT_DRIVE);

        assertThat(activity.getCurrentFragment(), hasDriver(mike));
        assertThat(activity.getCurrentFragment(), hasPassengers(john));
        assertThat(activity.getCurrentFragment(), hasNoRemainingMembers());
    }

    @Test
    public void shouldDefineExceptionalRule_HasToDrive() throws Exception {
        new ActivityBuilder()
                .withStartDate(today)
                .withCalculationDate(today)
                .withTurnDate(today)
                .withDriver("John")
                .withPassenger("Mike")
                .start()
                .getViewOfMember("Mike")
                .performClick();

        selectExceptionalUsage(Usage.HAS_TO_DRIVE);

        assertThat(activity.getCurrentFragment(), hasDriver(mike));
        assertThat(activity.getCurrentFragment(), hasPassengers(john));
        assertThat(activity.getCurrentFragment(), hasNoRemainingMembers());
    }

    @Test
    public void shouldStartActivity_DefineReturnData() throws Exception {
        new ActivityBuilder()
                .withStartDate(today)
                .withCalculationDate(today)
                .withTurnDate(today)
                .withPassenger("John")
                .withPassenger("Mike")
                .start();

        Intent data = new Intent();
        data.putExtra(Constants.CARPOOL_NAME, "Name");
        data.putExtra(Constants.TURN_DATE, getFullDateFormatter(application).print(today));
        assertThat(activity, hasResult(Activity.RESULT_OK, data));
    }

    @Test
    public void shouldFinishActivity_UpButton() throws Exception {
        new ActivityBuilder()
                .withStartDate(today)
                .withCalculationDate(today)
                .withTurnDate(today)
                .withPassenger("John")
                .withPassenger("Mike")
                .start();

        pressMenuItem(android.R.id.home);
        assertThat(activity, isFinishing());
    }

    @Test
    public void shouldDefineExceptionalRule_DoesNotUse() throws Exception {
        new ActivityBuilder()
                .withStartDate(today)
                .withCalculationDate(today)
                .withTurnDate(today)
                .withDriver("John")
                .withPassenger("Mike")
                .start()
                .getViewOfMember("Mike")
                .performClick();

        selectExceptionalUsage(Usage.DOES_NOT_USE);

        assertThat(activity.getCurrentFragment(), hasDriver(john));
        assertThat(activity.getCurrentFragment(), hasNoPassengers());
        assertThat(activity.getCurrentFragment(), hasRemainingMembers(mike));
    }

    @Test
    public void shouldShowExceptionalUsage_OpenedTurnWithExceptionalUsage_Normal() throws Exception {
        new ActivityBuilder()
                .withStartDate(today)
                .withCalculationDate(today)
                .withTurnDate(today)
                .withPassenger("John")
                .withPassenger("Mike")
                .withExceptionalRule("Mike", today, Usage.NORMAL)
                .start();

        assertThat(activity.getCurrentFragment(), showsExceptionalUsageFor(mike, Usage.NORMAL));
        assertThat(activity.getCurrentFragment(), showsNoExceptionalUsageFor(john));
    }

    @Test
    public void shouldShowExceptionalUsage_OpenedTurnWithExceptionalUsage_CanNotDriver() throws Exception {
        new ActivityBuilder()
                .withStartDate(today)
                .withCalculationDate(today)
                .withTurnDate(today)
                .withPassenger("John")
                .withPassenger("Mike")
                .withExceptionalRule("Mike", today, Usage.CAN_NOT_DRIVE)
                .start();

        assertThat(activity.getCurrentFragment(), showsExceptionalUsageFor(mike, Usage.CAN_NOT_DRIVE));
        assertThat(activity.getCurrentFragment(), showsNoExceptionalUsageFor(john));
    }

    @Test
    public void shouldShowExceptionalUsage_OpenedTurnWithExceptionalUsage_DoesNotUse() throws Exception {
        new ActivityBuilder()
                .withStartDate(today)
                .withCalculationDate(today)
                .withTurnDate(today)
                .withPassenger("John")
                .withPassenger("Mike")
                .withExceptionalRule("Mike", today, Usage.DOES_NOT_USE)
                .start();

        assertThat(activity.getCurrentFragment(), showsExceptionalUsageFor(mike, Usage.DOES_NOT_USE));
        assertThat(activity.getCurrentFragment(), showsNoExceptionalUsageFor(john));
    }

    @Test
    public void shouldShowExceptionalUsage_OpenedTurnWithExceptionalUsage_HasToDriver() throws Exception {
        new ActivityBuilder()
                .withStartDate(today)
                .withCalculationDate(today)
                .withTurnDate(today)
                .withPassenger("John")
                .withPassenger("Mike")
                .withExceptionalRule("Mike", today, Usage.HAS_TO_DRIVE)
                .start();

        assertThat(activity.getCurrentFragment(), showsExceptionalUsageFor(mike, Usage.HAS_TO_DRIVE));
        assertThat(activity.getCurrentFragment(), showsNoExceptionalUsageFor(john));
    }

    @Test
    public void shouldShowExceptionalUsage_DefinedExceptionalUsage() throws Exception {
        new ActivityBuilder()
                .withStartDate(today)
                .withCalculationDate(today)
                .withTurnDate(today)
                .withDriver("John")
                .withPassenger("Mike")
                .start()
                .getViewOfMember("Mike")
                .performClick();

        selectExceptionalUsage(Usage.DOES_NOT_USE);

        assertThat(activity.getCurrentFragment(), showsExceptionalUsageFor(mike, Usage.DOES_NOT_USE));
        assertThat(activity.getCurrentFragment(), showsNoExceptionalUsageFor(john));
    }

    @Test
    public void shouldOpenContactInformation_Driver() throws Exception {
        new ActivityBuilder()
                .withStartDate(today)
                .withCalculationDate(today)
                .withTurnDate(today)
                .withDriver("John")
                .start();

        activity.findViewById(R.id.mv_driver).findViewById(R.id.iv_contact_image).performClick();

        Intent expectedIntent = new Intent(Intent.ACTION_VIEW);
        expectedIntent.setData(contactsCursor.createContactUri("John"));

        assertThat(activity, startedActivity(expectedIntent));
    }

    @Test
    public void shouldOpenContactInformation_Passenger() throws Exception {
        new ActivityBuilder()
                .withStartDate(today)
                .withCalculationDate(today)
                .withTurnDate(today)
                .withDriver("Mike")
                .withPassenger("John")
                .start();

        activity.findViewById(R.id.ll_passengers).findViewById(R.id.iv_contact_image).performClick();

        Intent expectedIntent = new Intent(Intent.ACTION_VIEW);
        expectedIntent.setData(contactsCursor.createContactUri("John"));

        assertThat(activity, startedActivity(expectedIntent));
    }

    @Test
    public void shouldOpenContactInformation_RemainingMembers() throws Exception {
        new ActivityBuilder()
                .withStartDate(today)
                .withCalculationDate(today)
                .withTurnDate(today)
                .withRemainingMember("John")
                .start();

        activity.findViewById(R.id.ll_remaining_members).findViewById(R.id.iv_contact_image).performClick();

        Intent expectedIntent = new Intent(Intent.ACTION_VIEW);
        expectedIntent.setData(contactsCursor.createContactUri("John"));

        assertThat(activity, startedActivity(expectedIntent));
    }

    @Test
    public void shouldSlideToNextTurn() throws Exception {
        DateTime tenDaysAgo = today.minusDays(9);

        new ActivityBuilder()
                .withStartDate(tenDaysAgo)
                .withCalculationDate(today)
                .withTurnDate(today)
                .withPassenger("Mike")
                .withPassenger("John")
                .start();

        assertThat(activity.getCurrentFragment(), hasDriver(mike));

        ViewPager vpTurns = (ViewPager) activity.findViewById(R.id.vp_turns);
        // Workaround: https://github.com/robolectric/robolectric/issues/1326
        ShadowLooper.pauseMainLooper();
        vpTurns.setCurrentItem(vpTurns.getCurrentItem() + 1);
        ShadowLooper.unPauseMainLooper();

        assertThat(activity.getCurrentFragment(), hasDriver(john));
    }

    @Test
    public void shouldChangeExceptionalUsageAndSlideToNextTurnWithChangedDriver() throws Exception {
        ActivityBuilder builder = new ActivityBuilder()
                .withStartDate(today)
                .withCalculationDate(today.minusDays(1))
                .withTurnDate(today)
                .withPassenger("Mike")
                .withPassenger("John")
                .start();

        assertThat(activity.getCurrentFragment(), hasDriver(john));

        // change ussage -> next day john should be driver
        builder.getViewOfMember("John").performClick();
        selectExceptionalUsage(Usage.CAN_NOT_DRIVE);

        ViewPager vpTurns = (ViewPager) activity.findViewById(R.id.vp_turns);
        // Workaround: https://github.com/robolectric/robolectric/issues/1326
        ShadowLooper.pauseMainLooper();
        vpTurns.setCurrentItem(1);
        ShadowLooper.unPauseMainLooper();

        assertThat(vpTurns.getCurrentItem(), is(1));
        assertThat(activity.getCurrentFragment(), hasDriver(john));
    }

    @Test
    public void shouldShowTurnDetailsExplanation_FirstStartUp() throws Exception {
        new ActivityBuilder()
                .withStartDate(today)
                .withCalculationDate(today.minusDays(1))
                .withTurnDate(today)
                .withPassenger("Mike")
                .withPassenger("John")
                .start();

        TextView tvExplanation = (TextView) activity.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_content);
        assertThat(tvExplanation, is(notNullValue()));
        assertThat(tvExplanation, hasText(R.string.explanation_turn_details));
    }

    @Test
    public void shouldNotShowTurnDetailsExplanation_SecondStartUp() throws Exception {
        ActivityBuilder builder = new ActivityBuilder()
                .withStartDate(today)
                .withCalculationDate(today.minusDays(1))
                .withTurnDate(today)
                .withPassenger("Mike")
                .withPassenger("John")
                .start();

        activity.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_dismiss).performClick();

        builder.start();
        TextView tvExplanation = (TextView) activity.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_content);
        assertThat(tvExplanation, is(nullValue()));
    }

    private class ActivityBuilder {

        private final ArrayList<Member> passengers = new ArrayList<Member>();
        private final ArrayList<Member> remainingMembers = new ArrayList<Member>();
        private final Table<URI, DateTime, Usage> exceptionalUsage = HashBasedTable.create();
        private Member driver;
        private DateTime startDate;
        private DateTime calculationDate;
        private DateTime turnDate;

        private ActivityBuilder start() throws Exception {
            Carpool carpool = new Carpool("Name");

            for (Member member : passengers) {
                member.addUsageAt(startDate, new boolean[]{true, true, true, true, true, true, true});
                carpool.add(member);
            }

            for (Member member : remainingMembers) {
                member.addUsageAt(startDate, new boolean[]{false, false, false, false, false, false, false});
                carpool.add(member);
            }

            if (driver != null) {
                driver.addUsageAt(startDate, new boolean[]{true, true, true, true, true, true, true});
                carpool.add(driver);
            }

            for (Table.Cell<URI, DateTime, Usage> cell : exceptionalUsage.cellSet()) {
                carpool.defineExceptionalUsage(
                        new Member(cell.getRowKey()),
                        cell.getColumnKey(),
                        cell.getValue()
                );
            }

            carpool.calculateTurnsUntil(calculationDate);

            CarpoolDAO dao = new CarpoolDAO(application);
            dao.save(carpool);

            Intent intent = new Intent();
            intent.putExtra(Constants.TURN_DATE, getFullDateFormatter(application).print(turnDate));
            intent.putExtra(Constants.CARPOOL_NAME, "Name");
            activity.setIntent(intent);

            // Workaround: https://github.com/robolectric/robolectric/issues/1326
            ShadowLooper.pauseMainLooper();
            activity.onStart();
            ShadowLooper.unPauseMainLooper();

            return this;
        }

        private View getViewOfMember(String contactName) {
            View view = activity.getCurrentFragment().getView();
            return getViewOfMember((ViewGroup) view.findViewById(R.id.rl_turn_details), URI.create(contactsCursor.createContactUri(contactName).toString()));
        }

        private View getViewOfMember(ViewGroup view, URI uri) {
            for (int i = 0; i < view.getChildCount(); ++i) {
                View child = view.getChildAt(i);
                if (child instanceof MemberReadOnlyView) {
                    MemberReadOnlyView mrov = (MemberReadOnlyView) child;
                    if (uri.equals(mrov.getMember().getUri())) {
                        return mrov;
                    }
                } else if (child instanceof ViewGroup) {
                    View v = getViewOfMember((ViewGroup) child, uri);
                    if (v != null) {
                        return v;
                    }
                }
            }
            return null;
        }

        private ActivityBuilder withStartDate(DateTime startDate) {
            this.startDate = startDate;
            return this;
        }

        private ActivityBuilder withCalculationDate(DateTime calculationDate) {
            this.calculationDate = calculationDate;
            return this;
        }

        private ActivityBuilder withTurnDate(DateTime turnDate) {
            this.turnDate = turnDate;
            return this;
        }

        private ActivityBuilder withRemainingMember(String contactName) {
            remainingMembers.add(contactsCursor.createMember(contactName));
            return this;
        }

        private ActivityBuilder withDriver(String contactName) {
            driver = contactsCursor.createMember(contactName);
            return this;
        }

        private ActivityBuilder withExceptionalRule(String contactName, DateTime date, Usage usage) {
            exceptionalUsage.put(URI.create(contactsCursor.createContactUri(contactName).toString()), date, usage);
            return this;
        }

        private ActivityBuilder withPassenger(String contactName) {
            passengers.add(contactsCursor.createMember(contactName));
            return this;
        }

    }
}
