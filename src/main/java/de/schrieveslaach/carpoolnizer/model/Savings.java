/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.model;

import java.math.BigDecimal;

/**
 * Shows how many money a {@link Member} saved by using a {@link Carpool}.
 */
public class Savings {

    /**
     * How often the {@link Member} was driver of the {@link Carpool}.
     */
    private final long droveCarpool;

    /**
     * How often the {@link Member} used the {@link Carpool}.
     */
    private final long usedCarpool;

    /**
     * Distance in kilometer
     */
    private BigDecimal distance = BigDecimal.valueOf(30);

    /**
     * Price of one liter of gasoline
     */
    private BigDecimal gasolinePrice = BigDecimal.valueOf(1.50);

    /**
     * How many liter of gasoline does the car consume on 100km.
     */
    private BigDecimal gasolineConsumption = BigDecimal.valueOf(8.0);

    /**
     * Creates a new instance {@code Savings}.
     *
     * @param usedCarpool
     * @param droveCarpool
     */
    public Savings(long usedCarpool, long droveCarpool) {
        if (usedCarpool < 0L || droveCarpool < 0L) {
            throw new IllegalArgumentException("Usage and driver count must be greater or equal zero.");
        }

        if (droveCarpool > usedCarpool) {
            throw new IllegalArgumentException("Driver count must not be usage count.");
        }

        this.droveCarpool = droveCarpool;
        this.usedCarpool = usedCarpool;
    }

    /**
     *
     * @return How often the member was driver of the carpool
     */
    public long getDroveCarpool() {
        return droveCarpool;
    }

    /**
     *
     * @return How often the member used the carpool
     */
    public long getUsedCarpool() {
        return usedCarpool;
    }

    /**
     * Gets distance in kilometer.
     *
     * @return
     */
    public BigDecimal getDistance() {
        return distance;
    }

    /**
     * Sets distance in kilometer
     *
     * @param distance
     */
    public void setDistance(BigDecimal distance) {
        checkValue(distance, "Distance must be greater zero.");
        this.distance = distance;
    }

    /**
     * Gets price of one liter of gasoline
     *
     * @return
     */
    public BigDecimal getGasolinePrice() {
        return gasolinePrice;
    }

    /**
     * Sets price of one liter of gasoline
     *
     * @param gasolinePrice
     */
    public void setGasolinePrice(BigDecimal gasolinePrice) {
        checkValue(gasolinePrice, "Gasoline price must be greater zero.");
        this.gasolinePrice = gasolinePrice;
    }

    /**
     * Gets how many liter of gasoline does the car consume on 100km.
     *
     * @return
     */
    public BigDecimal getGasolineConsumption() {
        return gasolineConsumption;
    }

    /**
     * Sets how many liter of gasoline does the car consume on 100km.
     *
     * @param gasolineConsumption
     */
    public void setGasolineConsumption(BigDecimal gasolineConsumption) {
        checkValue(gasolineConsumption, "Gasoline consumption must be greater zero.");
        this.gasolineConsumption = gasolineConsumption;
    }

    private void checkValue(BigDecimal value, String exceptionMessage) {
        if (value == null || new BigDecimal(0).compareTo(value) >= 0) {
            throw new IllegalArgumentException(exceptionMessage);
        }
    }

    /**
     * Converts the current {@code Savings} to {@link BigDecimal}.
     *
     * @return
     */
    public BigDecimal toBigDecimal() {
        BigDecimal costsPerTurn = distance.multiply(new BigDecimal(2))
                .multiply(gasolineConsumption)
                .divide(new BigDecimal(100))
                .multiply(gasolinePrice);

        return costsPerTurn.multiply(new BigDecimal(usedCarpool))
                .subtract(costsPerTurn.multiply(new BigDecimal(droveCarpool)));
    }

}
