/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import de.schrieveslaach.carpoolnizer.model.Member;
import de.schrieveslaach.carpoolnizer.ui.views.MemberReadOnlyView;

/**
 * {@link MemberReadOnlyView.OnContactInformationListener} for showing contact information.
 */
public class ShowContactInformationListener implements MemberReadOnlyView.OnContactInformationListener {

    private final Activity activity;

    /**
     * Creates a new instance of {@code ShowContactInformationListener}
     *
     * @param activity
     */
    public ShowContactInformationListener(Activity activity) {
        if (activity == null) {
            throw new IllegalArgumentException("activity must not be null");
        }
        this.activity = activity;
    }

    @Override
    public void onContactInformation(Member member) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(member.getUri().toString()));
        activity.startActivity(intent);
    }
}
