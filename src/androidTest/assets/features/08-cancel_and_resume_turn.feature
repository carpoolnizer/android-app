Feature: Cancel and resume turn
  This feature allows to cancel turns for specific days. It also
  allows to resume canceled turns.
  
  Scenario Outline: Cancel a turn
    Given I started the app
    And I opened the carpool <carpool>
    When I clicked long on turn of today
    And I clicked on the pillow
    Then the turn of today is canceled

  Examples:
    | carpool   |
    | Work Mike |

  Scenario Outline: View canceled a turn
    Given I started the app
    And I opened the carpool <carpool>
    When I clicked on turn of today
    Then I should see a big pillow in the turn details
    And I should see no driver, no passengers and no remaining members

  Examples:
    | carpool   |
    | Work Mike |

  Scenario Outline: Resume the canceled turn
    Given I started the app
    And I opened the carpool <carpool>
    When I clicked long on turn of today
    And I clicked on the pillow
    Then the turn of today is not canceled
    
  Examples:
    | carpool   |
    | Work Mike |
