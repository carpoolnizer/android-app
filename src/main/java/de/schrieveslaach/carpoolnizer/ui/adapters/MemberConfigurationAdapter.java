/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.ViewGroup;

import com.google.common.collect.ImmutableList;

import java.util.LinkedHashMap;
import java.util.Map;

import de.schrieveslaach.carpoolnizer.model.Member;
import de.schrieveslaach.carpoolnizer.model.Usage;
import de.schrieveslaach.carpoolnizer.ui.views.MemberConfigurationView;
import lombok.Setter;

/**
 * {@link RecyclerView.Adapter adapter} for displaying {@link Member} and their usage.
 */
public class MemberConfigurationAdapter extends RecyclerView.Adapter<MemberConfigurationViewHolder> {

    private final Map<Member, boolean[]> configurations = new LinkedHashMap<>();

    @Setter
    private ConfigurationChangedListener configurationChangedListener;

    /**
     * Clears all {@link Member members} in the {@code MemberConfigurationAdapter}
     */
    public void clear() {
        synchronized (configurations) {
            configurations.clear();
        }
        notifyDataSetChanged();
    }

    /**
     * Returns all {@link Member members}.
     *
     * @return
     */
    public ImmutableList<Member> getMembers() {
        ImmutableList.Builder<Member> builder = ImmutableList.<Member>builder();
        synchronized (configurations) {
            builder.addAll(configurations.keySet());
        }
        return builder.build();
    }

    /**
     * Returns the {@link Usage weekday usage} of the given {@link Member}.
     *
     * @param member
     * @return
     */
    public boolean[] getWeekdayUsageOf(Member member) {
        synchronized (configurations) {
            return configurations.get(member);
        }
    }

    /**
     * Adds a {@link Member} with the given {@code usage} to the {@code MemberConfigurationAdapter}.
     *
     * @param member
     * @param usage
     * @return
     */
    public boolean add(Member member, boolean[] usage) {
        synchronized (configurations) {
            if (configurations.containsKey(member)) {
                return false;
            }

            configurations.put(member, usage);
        }
        notifyDataSetChanged();
        return true;
    }

    /**
     * Adds a {@link Member} to the {@code MemberConfigurationAdapter}.
     *
     * @param member
     */
    public boolean add(Member member) {
        boolean[] usage = new boolean[]{
                true,
                true,
                true,
                true,
                true,
                false,
                false
        };

        return add(member, usage);
    }

    @Override
    public MemberConfigurationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MemberConfigurationView mcv = new MemberConfigurationView(parent.getContext());
        mcv.setOnConfigurationChangedListener(new OnConfigurationChangedAdapter());
        return new MemberConfigurationViewHolder(mcv);
    }

    @Override
    public void onBindViewHolder(MemberConfigurationViewHolder holder, int position) {
        Member member = getMembers().get(position);

        boolean[] weekdayUsage;
        synchronized (configurations) {
            weekdayUsage = configurations.get(member);
        }

        holder.display(member, weekdayUsage);
    }

    @Override
    public int getItemCount() {
        synchronized (configurations) {
            return configurations.size();
        }
    }

    /**
     * Event-handler when the user changed the configuration of the members.
     */
    public interface ConfigurationChangedListener {

        /**
         * Will be invoked when the user changed the usage of a {@link Member}.
         *
         * @param m
         */
        void changedUsage(Member m);

    }

    private class OnConfigurationChangedAdapter implements MemberConfigurationView.OnConfigurationChangedListener {

        @Override
        public void onConfigurationChanged(MemberConfigurationView mcv) {
            Member member = mcv.getMember();
            boolean[] weekdayUsage = mcv.getWeekdayUsage();

            synchronized (configurations) {
                configurations.put(member, weekdayUsage);
            }

            if (configurationChangedListener != null) {
                configurationChangedListener.changedUsage(member);
            }
        }
    }
}
