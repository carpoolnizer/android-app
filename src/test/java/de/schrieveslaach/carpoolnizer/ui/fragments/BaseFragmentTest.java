/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.fragments;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import org.junit.After;

import de.schrieveslaach.carpoolnizer.ui.RobolectricTest;
import lombok.SneakyThrows;

public abstract class BaseFragmentTest<F extends Fragment> extends RobolectricTest {

    private SupportFragmentController<F> controller;

    private final Class<F> fragmentClass;

    protected BaseFragmentTest(Class<F> fragmentClass) {
        this.fragmentClass = fragmentClass;
    }

    public F openFragment() throws Exception {
        return openFragment(null);
    }

    public F openFragment(FragmentInitializer<F> fragmentInitializer) throws Exception {
        F fragment = fragmentClass.newInstance();

        if (fragmentInitializer != null) {
            fragmentInitializer.init(fragment);
        }

        controller = SupportFragmentController.of(fragment)
                .create();

        if (fragmentInitializer != null) {
            fragmentInitializer.beforeStart(fragment);
        }

        return controller.start()
                .get();
    }

    @After
    @SneakyThrows
    public void cleanUp() {
        F fragment = controller.get();
        FragmentActivity activity = fragment.getActivity();
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        fragmentManager.beginTransaction().remove(fragment).commit();
        fragmentManager.executePendingTransactions();
        activity.finish();

        controller.stop().destroy();
    }

    public interface FragmentInitializer<F extends Fragment> {

        void init(F fragment);

        void beforeStart(F fragment);

    }
}
