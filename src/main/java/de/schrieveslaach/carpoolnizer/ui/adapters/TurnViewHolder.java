/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.adapters;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;
import de.schrieveslaach.carpoolnizer.Constants;
import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.model.Turn;
import de.schrieveslaach.carpoolnizer.ui.views.MemberReadOnlyView;
import de.schrieveslaach.carpoolnizer.ui.views.resolvers.CoffeeDrawableResolver;
import de.schrieveslaach.carpoolnizer.ui.views.resolvers.PillowDrawableResolver;

/**
 * {@link RecyclerView.ViewHolder} for a {@link de.schrieveslaach.carpoolnizer.model.Turn turn}.
 */
public class TurnViewHolder extends RecyclerView.ViewHolder {

    private final DateTimeFormatter formatter;

    private final AppCompatButton buLoadArchivedTurns;

    private final MemberReadOnlyView memberReadOnlyView;

    private final TextView tvTurnDate;

    /**
     * Displays the number of passengers
     */
    private final TextView tvPassengers, tvNumberOfPassengers;

    /**
     * Displays {@code <}year{@code >}/ CW {@code <}week{@code }
     */
    private final TextView tvYear, tvCalendarWeek, tvWeek;

    /**
     * Displays {@code <}year{@code >}/ CW {@code <}week{@code }
     */
    private final RelativeLayout rlCalendarWeek;

    private final View firstDivider;

    private final MemberReadOnlyView.OnContactInformationListener onContactInformationListener;

    private Turn turn;

    /**
     * Creates a new instance of {@code TurnViewHolder}.
     *
     * @param turnView
     */
    public TurnViewHolder(ViewGroup turnView, final TurnAdapter.OnTurnClickListener onTurnClickListener, final MemberReadOnlyView.OnContactInformationListener onContactInformationListener) {
        super(turnView);

        this.buLoadArchivedTurns = turnView.findViewById(R.id.bu_load_archived_turns);
        this.buLoadArchivedTurns.setVisibility(View.GONE);

        this.memberReadOnlyView = turnView.findViewById(R.id.member_read_only_view);
        this.memberReadOnlyView.setBackground(turnView.getContext().getResources().getDrawable(R.drawable.clickable_background));

        this.tvPassengers = turnView.findViewById(R.id.tv_passengers);
        this.tvNumberOfPassengers = turnView.findViewById(R.id.tv_number_of_passengers);

        this.tvTurnDate = turnView.findViewById(R.id.tv_turn_date);
        this.formatter = Constants.getFullDateFormatter(turnView.getContext());

        this.rlCalendarWeek = turnView.findViewById(R.id.rl_calendar_week);
        this.tvYear = turnView.findViewById(R.id.tv_year);
        this.tvCalendarWeek = turnView.findViewById(R.id.tv_calendar_week);
        this.tvWeek = turnView.findViewById(R.id.tv_week);
        firstDivider = turnView.findViewById(R.id.first_divider);

        turnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTurnClickListener.onClick(turn);
            }
        });
        turnView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onTurnClickListener.onLongClick(turn);
                return true;
            }
        });
        this.onContactInformationListener = onContactInformationListener;
    }

    /**
     * Displays the given {@link Turn} in the {@code TurnViewHolder}.
     *
     * @param turn
     */
    public void display(Turn turn) {
        this.turn = turn;

        if (turn.isCanceled()) {
            memberReadOnlyView.setNoMemberDrawableResolver(new PillowDrawableResolver());
        } else {
            memberReadOnlyView.setNoMemberDrawableResolver(new CoffeeDrawableResolver());
        }
        memberReadOnlyView.display(turn.getDriver());

        if (turn.getDriver() != null && !turn.getDriver().isPhoneUser()) {
            memberReadOnlyView.setOnContactInformationListener(onContactInformationListener);
        } else {
            memberReadOnlyView.setOnContactInformationListener(null);
        }
        tvTurnDate.setText(formatter.print(turn.getDate()));

        changeBackground();
        displayCalendarWeek();
        displayNumberOfPassengers();
    }

    /**
     * Set the selection state of this view to selected.
     *
     * @param selected
     */
    public void setSelected(boolean selected) {
        memberReadOnlyView.setSelected(selected);
    }

    /**
     * Indicates the selection state of this view.
     *
     * @return
     */
    public boolean isSelected() {
        return memberReadOnlyView.isSelected();
    }

    /**
     * Returns the displayed {@link Turn}.
     *
     * @return
     * @see TurnViewHolder#display(Turn)
     */
    public Turn getTurn() {
        return turn;
    }

    /**
     * Displays the button for loading the archived turns.
     *
     * @param onClickListener
     */
    public void displayLoadArchivedTurnsButton(View.OnClickListener onClickListener) {
        buLoadArchivedTurns.setVisibility(View.VISIBLE);
        buLoadArchivedTurns.setOnClickListener(onClickListener);
    }

    /**
     * Hides the button for loading the archived turns.
     */
    public void hideLoadArchivedTurnsButton() {
        buLoadArchivedTurns.setVisibility(View.GONE);
        buLoadArchivedTurns.setOnClickListener(null);
    }

    /**
     * Changes the {@link MemberReadOnlyView#setBackground(android.graphics.drawable.Drawable) MROV background}
     * according to {@link Turn#getDate() date}.
     */
    private void changeBackground() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        DateTime date = turn.getDate();
        Drawable background;
        if (date.isBefore(today)) {
            background = itemView.getResources().getDrawable(R.drawable.clickable_turn_in_the_past_background);
        } else if (date.isAfter(today)) {
            background = itemView.getResources().getDrawable(R.drawable.clickable_background);
        } else {
            background = itemView.getResources().getDrawable(R.drawable.clickable_turn_today_background);
        }
        memberReadOnlyView.setBackground(background);
    }

    private void displayCalendarWeek() {
        DateTime date = turn.getDate();
        int visibility;

        // 7 == Monday
        if (date.getDayOfWeek() == 1) {
            visibility = View.VISIBLE;

            tvYear.setText(String.valueOf(date.getYear()));
            tvWeek.setText(String.valueOf(date.getWeekOfWeekyear()));
        } else {
            visibility = View.GONE;
        }

        rlCalendarWeek.setVisibility(visibility);
        firstDivider.setVisibility(visibility);
    }

    private void displayNumberOfPassengers() {
        if (turn.getDriver() == null) {
            tvPassengers.setVisibility(View.GONE);
            tvNumberOfPassengers.setVisibility(View.GONE);
        } else {
            tvPassengers.setVisibility(View.VISIBLE);
            tvNumberOfPassengers.setVisibility(View.VISIBLE);
            tvNumberOfPassengers.setText(String.valueOf(turn.getPassengers().size()));
        }
    }
}

