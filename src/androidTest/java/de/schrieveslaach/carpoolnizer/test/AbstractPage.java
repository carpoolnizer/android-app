/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.test;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.test.internal.runner.lifecycle.ActivityLifecycleMonitorRegistry;
import androidx.test.runner.lifecycle.Stage;
import android.test.ActivityInstrumentationTestCase2;

import java.util.Collection;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public abstract class AbstractPage<T extends Activity> extends ActivityInstrumentationTestCase2<T> {

    private static android.app.Instrumentation.ActivityMonitor contactInformationMonitor;

    private final Class<T> activityClass;

    public AbstractPage(Class<T> activityClass) {
        super(activityClass);
        this.activityClass = activityClass;
    }

    /**
     * Returns the current {@link Activity}.
     *
     * @return
     */
    public T getCurrentActivity() {
        final ActivityHolder<T> holder = new ActivityHolder();
        getInstrumentation().runOnMainSync(new Runnable() {
            public void run() {
                Collection<Activity> resumedActivities = ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(Stage.RESUMED);
                for (Activity act : resumedActivities) {
                    if (!act.getClass().equals(activityClass)) {
                        continue;
                    }
                    holder.activity = (T) act;
                    break;
                }
            }
        });
        return holder.activity;
    }

    public void startContactInformationMonitor() throws IntentFilter.MalformedMimeTypeException {
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_VIEW);
        intentFilter.addDataScheme("content");
        intentFilter.addDataType("vnd.android.cursor.item/contact");
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        contactInformationMonitor = getInstrumentation().addMonitor(intentFilter, null, true);
    }

    public void assertThatContactInformationMonitorHasBeenHit() {
        contactInformationMonitor.waitForActivityWithTimeout(1000);
        assertThat("Should have hit contact information monitor", contactInformationMonitor.getHits(), is(greaterThan(0)));
        getInstrumentation().removeMonitor(contactInformationMonitor);
        contactInformationMonitor = null;
    }

    private static class ActivityHolder<T extends Activity> {
        private T activity;
    }
}
