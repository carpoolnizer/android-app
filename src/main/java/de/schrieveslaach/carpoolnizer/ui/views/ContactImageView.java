/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import de.schrieveslaach.carpoolnizer.R;

/**
 * {@link ImageView Image view} for displaying contact images.
 */
public class ContactImageView extends ImageView {

    private Bitmap original;

    /**
     * Creates a new instance of {@code ContactImageView}.
     *
     * @param context
     */
    public ContactImageView(Context context) {
        super(context);
        initialize();
    }

    /**
     * Creates a new instance of {@code ContactImageView}.
     *
     * @param context
     * @param attrs
     */
    public ContactImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    /**
     * Creates a new instance of {@code ContactImageView}.
     *
     * @param context
     * @param attrs
     * @param defStyleAttr
     */
    public ContactImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    /**
     * Sets the default values for the {@code ContactImageView}
     */
    private void initialize() {
        int size = getContactPictureSize();
        setMinimumHeight(size);
        setMinimumWidth(size);
        setMaxHeight(size);
        setMaxWidth(size);

        BitmapDrawable drawable = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_contact_picture);
        setImageBitmap(drawable.getBitmap());
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        Paint antiPaint = new Paint();
        antiPaint.setAntiAlias(true);

        this.original = bm;

        int pixel = getContactPictureSize();
        int radius = pixel >> 1;

        Bitmap tmp = Bitmap.createScaledBitmap(bm, pixel, pixel, true);

        Bitmap roundContactPicture = Bitmap.createBitmap(pixel, pixel, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(roundContactPicture);
        canvas.save();

        Path circle = new Path();
        circle.addCircle(radius, radius, radius - 4, Path.Direction.CW);
        canvas.clipPath(circle);

        canvas.drawBitmap(tmp, 0, 0, antiPaint);

        if (isClickable()) {
            canvas.restore();

            BitmapDrawable triangleDrawable = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_action_triangle);
            tmp = Bitmap.createScaledBitmap(triangleDrawable.getBitmap(), radius, radius, false);
            canvas.drawBitmap(tmp, radius, radius, antiPaint);
        }

        super.setImageBitmap(roundContactPicture);
    }

    @Override
    public void setClickable(boolean clickable) {
        super.setClickable(clickable);
        setImageBitmap(original);
        if (clickable) {
            setBackground(getResources().getDrawable(R.drawable.clickable_background));
        } else {
            setBackground(null);
        }
    }

    /**
     * Converts dots per inch to pixel.
     *
     * @return
     */
    private int getContactPictureSize() {
        return (int) getResources().getDimension(R.dimen.contact_picture_size);
    }
}
