/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.robolectric.shadows;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;

import org.robolectric.annotation.Implementation;
import org.robolectric.annotation.Implements;
import org.robolectric.annotation.RealObject;
import org.robolectric.shadows.ShadowBaseAdapter;

@Implements(CursorAdapter.class)
public class ShadowCursorAdapter extends ShadowBaseAdapter {

    private Context context;

    private Cursor cursor;

    @RealObject
    CursorAdapter adapter;

    public void __constructor__(Context context, Cursor c, boolean autoRequery) {
        this.context = context;
        this.cursor = c;
    }

    @Implementation
    public View getView(int position, View convertView, ViewGroup parent) {
        cursor.moveToPosition(position);

        View view = adapter.newView(context, cursor, parent);
        adapter.bindView(view, context, cursor);
        return view;
    }
}
