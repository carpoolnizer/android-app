/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.PowerManager;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import android.util.Log;

import org.joda.time.DateTime;

import java.util.List;
import java.util.Objects;

import de.schrieveslaach.carpoolnizer.model.Carpool;
import de.schrieveslaach.carpoolnizer.model.Member;
import de.schrieveslaach.carpoolnizer.model.Turn;
import de.schrieveslaach.carpoolnizer.model.settings.UsageNotification;
import de.schrieveslaach.carpoolnizer.repository.CarpoolDAO;
import de.schrieveslaach.carpoolnizer.repository.CarpoolDAOException;
import de.schrieveslaach.carpoolnizer.repository.ContactDAO_;
import de.schrieveslaach.carpoolnizer.ui.MainActivity_;

public class Alarms extends BroadcastReceiver {

    /**
     * Sets an alarm for the given {@link Carpool} with given {@link UsageNotification}
     *
     * @param context
     * @param carpool
     * @param usageNotification
     */
    public void setAlarm(Context context, Carpool carpool, UsageNotification usageNotification) throws CarpoolDAOException {
        CarpoolDAO dao = new CarpoolDAO(context);
        dao.saveUsageNotifcation(carpool, usageNotification);

        Intent intent = new Intent(context, Alarms.class);
        intent.setAction(Constants.CARPOOL_USAGE_NOTIFICATION);
        intent.putExtra(Constants.CARPOOL_NAME, carpool.getName());
        PendingIntent broadcast = PendingIntent.getBroadcast(context, carpool.getName().hashCode(), intent, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        // Delete previous notification
        alarmManager.cancel(broadcast);

        alarmManager.setRepeating(
                AlarmManager.RTC_WAKEUP,
                usageNotification.toMillis(),
                AlarmManager.INTERVAL_DAY,
                broadcast);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        PowerManager.WakeLock wakeLock = acquire(context);

        if (Objects.equals(intent.getAction(), Constants.CARPOOL_USAGE_NOTIFICATION) && intent.hasExtra(Constants.CARPOOL_NAME)) {
            notifyAboutCarpool(context, intent);
        } else if (Objects.equals(intent.getAction(), "android.intent.action.BOOT_COMPLETED")) {
            loadAllNotifications(context);
        }

        wakeLock.release();
    }

    private PowerManager.WakeLock acquire(Context context) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, Alarms.class.getCanonicalName());
        wakeLock.acquire();
        return wakeLock;
    }

    private void notifyAboutCarpool(Context context, Intent intent) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        CarpoolDAO dao = new CarpoolDAO(context);
        try {
            Carpool carpool = dao.loadCarpool(intent.getStringExtra(Constants.CARPOOL_NAME));
            List<Turn> turns = carpool.calculateTurnsUntil(DateTime.now());

            Turn turn = turns.get(turns.size() - 1);
            if (!turn.contains(new Member(null))) {
                return;
            }

            notificationManager.notify(0, createNotification(context, carpool, turn));
        } catch (CarpoolDAOException ex) {
            Log.e("Alamrs", "Could not load carpool", ex);
        }
    }

    private void loadAllNotifications(Context context) {
        CarpoolDAO dao = new CarpoolDAO(context);
        for (String carpoolName : dao.listCarpoolNames()) {
            try {
                Carpool carpool = dao.loadCarpool(carpoolName);
                UsageNotification notification = dao.loadUsageNotification(carpool);

                if (notification != null) {
                    setAlarm(context, carpool, notification);
                }
            } catch (CarpoolDAOException ex) {
                Log.e("Alamrs", "Could not load carpool", ex);
            }
        }
    }

    private Notification createNotification(Context context, Carpool carpool, Turn turn) {
        Intent resultIntent = new Intent(context, MainActivity_.class);
        resultIntent.putExtra(Constants.CARPOOL_NAME, carpool.getName());
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity_.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        return new NotificationCompat.Builder(context)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.logo_notification)
                .setContentTitle(context.getResources().getString(R.string.notification_you_are_using_the_carpool_title))
                .setContentText(context.getResources().getString(R.string.notification_you_are_using_the_carpool))
                .setContentIntent(resultPendingIntent)
                .setVibrate(new long[]{1000, 1000})
                .setLights(Color.BLUE, 3000, 3000)
                .setStyle(createExpandedLayoutStyle(context, turn))
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .build();
    }

    private NotificationCompat.InboxStyle createExpandedLayoutStyle(Context context, Turn turn) {
        ContactDAO_ contactDAO = ContactDAO_.getInstance_(context);

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle(context.getResources().getString(R.string.notification_you_are_using_the_carpool_title));

        inboxStyle.addLine(context.getResources().getString(R.string.notification_you_are_using_the_carpool_driver, contactDAO.getDisplayName(turn.getDriver())));
        List<Member> passengers = turn.getPassengers();
        if (passengers.isEmpty()) {
            inboxStyle.addLine(context.getResources().getString(R.string.notification_you_are_using_the_carpool_no_passengers));
        } else {
            StringBuilder passengersString = new StringBuilder();
            for (int i = 0; i < passengers.size(); i++) {
                Member passenger = passengers.get(i);
                passengersString.append(contactDAO.getDisplayName(passenger));
                if (i + 1 < passengers.size()) {
                    passengersString.append(", ");
                }
            }
            inboxStyle.addLine(context.getResources().getString(R.string.notification_you_are_using_the_carpool_passengers, passengersString));
        }

        return inboxStyle;
    }
}
