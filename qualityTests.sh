#!/bin/bash

./gradlew sonarqube

# Wait for quality gate
sleep 6

STATUS=$(curl -k -s https://sonarqube.schrieveslaach.no-ip.org/api/qualitygates/project_status?projectKey=de.schrieveslaach.carpoolnizer | python3 -c "import sys, json; print(json.load(sys.stdin)['projectStatus']['status'])")
echo "Quality gate status is $STATUS"
if [ "$STATUS" != "OK" ]
then
    exit 1
fi