/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.model;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.Interval;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.net.URI;

import static org.hamcrest.Matchers.hasToString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class MemberTest {

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldDetermineEquality_ObjectParameter() {
        Member member = new Member(null);
        assertThat(member, is(not(new Object())));
    }

    @Test
    public void shouldDetermineEquality_DifferentURIParameters() {
        Member m1 = new Member(null);
        Member m2 = new Member(null);

        assertThat(m1, is(m2));

        m1 = new Member(URI.create("http://tempuri.org"));
        assertThat(m1, is(not(m2)));

        m2 = new Member(URI.create("http://tempuri.org"));
        assertThat(m1, is(m2));
    }

    @Test
    public void shouldNotUseCarpool_NothingConfigured() {
        Member m = new Member(null);

        assertThat(m, hasUsage(DateTime.now(), Usage.DOES_NOT_USE));
    }

    @Test
    public void shouldFailToDefineUsage_DateNull() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("date must not be null");

        Member m = new Member(null);
        m.addUsageAt(null, new boolean[7]);
    }

    @Test
    public void shouldFailToDefineUsage_UsageNull() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Provide an array of length 7!");

        Member m = new Member(null);
        m.addUsageAt(DateTime.now(), null);
    }

    @Test
    public void shouldFailToDefineUsage_InvalidArrayLength() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Provide an array of length 7!");

        Member m = new Member(null);
        m.addUsageAt(DateTime.now(), new boolean[]{});
    }

    @Test
    public void shouldNotUseCarpool_Yesterday() {
        Member m = new Member(null);
        m.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, true});

        DateTime yesterday = DateTime.now().minusDays(1);
        assertThat(m, hasUsage(yesterday, Usage.DOES_NOT_USE));
    }

    @Test
    public void shouldUseCarpool_Tomorrow() {
        Member m = new Member(null);
        m.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, true});

        assertThat(m, hasUsage(DateTime.now(), Usage.NORMAL));
    }

    @Test
    public void shouldNotUseCarpool_InOneWeek() {
        Member m = new Member(null);
        m.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, true});
        m.addUsageAt(DateTime.now().plusDays(4), new boolean[7]);

        assertThat(m, hasUsage(DateTime.now().plusWeeks(1), Usage.DOES_NOT_USE));
    }

    @Test
    public void shouldNotUseCarpool_OnSunday() {
        Member m = new Member(null);
        m.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, false});

        DateTime dateTime = DateTime.now().plusWeeks(1).withDayOfWeek(DateTimeConstants.SUNDAY);
        assertThat(m, hasUsage(dateTime, Usage.DOES_NOT_USE));
    }

    @Test
    public void shouldNotUseCarpool_OnMonday() {
        Member m = new Member(null);
        m.addUsageAt(DateTime.now(), new boolean[]{false, true, true, true, true, true, true});

        DateTime dateTime = DateTime.now().plusWeeks(1).withDayOfWeek(DateTimeConstants.MONDAY);
        assertThat(m, hasUsage(dateTime, Usage.DOES_NOT_USE));
    }

    @Test
    public void shouldBePhoneUser() {
        Member member = new Member(null);
        assertThat(member.isPhoneUser(), is(true));
    }

    @Test
    public void shouldNotBePhoneUser() {
        Member member = new Member(URI.create("http://tempuri.org#john"));
        assertThat(member.isPhoneUser(), is(false));
    }

    @Test
    public void shouldDefineExceptionalUsage_SingleDay() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        Member member = new Member(URI.create("http://tempuri.org#john"));
        member.addUsageAt(today, new boolean[]{true, true, true, true, true, true, true});

        member.addExceptionalUsage(today.plusDays(1), Usage.DOES_NOT_USE);

        assertThat("before exceptional usage should be normal usage", member, hasUsage(today, Usage.NORMAL));
        assertThat(member, hasUsage(today.plusDays(1), Usage.DOES_NOT_USE));
        assertThat("after exceptional usage should be normal usage", member, hasUsage(today.plusDays(2), Usage.NORMAL));
    }

    @Test
    public void shouldDefineExceptionalUsage_OneWeek() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        Member member = new Member(URI.create("http://tempuri.org#john"));
        member.addUsageAt(today, new boolean[]{true, true, true, true, true, true, true});

        Interval nextWeek = new Interval(today.plusDays(7), today.plusDays(13));
        member.addExceptionalUsage(nextWeek, Usage.DOES_NOT_USE);

        assertThat("before exceptional usage should be normal usage", member, hasUsage(today.plusDays(6), Usage.NORMAL));
        for (DateTime d = today.plusDays(7); d.isBefore(today.plusDays(13)); d = d.plusDays(1)) {
            assertThat(member, hasUsage(d, Usage.DOES_NOT_USE));
        }
        assertThat("after exceptional usage should be normal usage", member, hasUsage(today.plusDays(14), Usage.NORMAL));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotDefineExceptionalUsage_NotHaveExactMatchingExceptionalUsage_AndOverlapsExceptionalUsage() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        Member member = new Member(URI.create("http://tempuri.org#john"));
        member.addUsageAt(today, new boolean[]{true, true, true, true, true, true, true});

        member.addExceptionalUsage(today, Usage.DOES_NOT_USE);
        member.addExceptionalUsage(new Interval(today, today.plusDays(2)), Usage.DOES_NOT_USE);
    }

    @Test
    public void shouldReplaceExceptionalRule() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Member member = new Member(URI.create("http://tempuri.org#john"));
        member.addUsageAt(today, new boolean[]{true, true, true, true, true, true, true});

        member.addExceptionalUsage(today, Usage.CAN_NOT_DRIVE);
        assertThat(member, hasUsage(today, Usage.CAN_NOT_DRIVE));
        member.addExceptionalUsage(today, Usage.HAS_TO_DRIVE);
        assertThat(member, hasUsage(today, Usage.HAS_TO_DRIVE));
    }

    @Test
    public void shouldHaveExceptionalUsage() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Member member = new Member(URI.create("http://tempuri.org#john"));

        member.addExceptionalUsage(today, Usage.CAN_NOT_DRIVE);
        assertThat(member.hasExceptionalUsage(today), is(true));
    }

    @Test
    public void shouldNotHaveExceptionalUsage() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Member member = new Member(URI.create("http://tempuri.org#john"));
        assertThat(member.hasExceptionalUsage(today), is(false));
    }

    private static Matcher<Member> hasUsage(final DateTime dateTime, final Usage usage) {
        return new TypeSafeMatcher<Member>() {
            @Override
            protected boolean matchesSafely(Member member) {
                return member.getUsageAt(dateTime).equals(usage);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(" usage at ")
                        .appendValue(dateTime.withTimeAtStartOfDay())
                        .appendText(" should be ")
                        .appendValue(usage);
            }

            @Override
            protected void describeMismatchSafely(Member member, Description mismatchDescription) {
                mismatchDescription.appendText(" usage is ").appendValue(member.getUsageAt(dateTime));
            }
        };
    }

    @Test
    public void shouldArchiveMember_Name_PhoneUser() {
        Member member = new Member(null)
                .archiveDataOlderThan(DateTime.now());
        assertThat(member, hasToString("phone user"));
    }

    @Test
    public void shouldArchiveMember_Name_Contact() {
        Member member = new Member(URI.create("http://tempuri.org#mike"))
                .archiveDataOlderThan(DateTime.now());
        assertThat(member, hasToString("http://tempuri.org#mike"));
    }

    @Test
    public void shouldArchiveMember_KeepNoneExpiredExceptionalUsage() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime yesterday = today.minusDays(1);

        Member member = new Member(null);
        member.addExceptionalUsage(today, Usage.HAS_TO_DRIVE);
        Member archivedMember = member.archiveDataOlderThan(yesterday);

        assertThat(member, hasUsage(today, Usage.HAS_TO_DRIVE));
        assertThat(archivedMember, hasUsage(yesterday, Usage.DOES_NOT_USE));
        assertThat(archivedMember, hasUsage(today, Usage.DOES_NOT_USE));
    }

    @Test
    public void shouldArchiveMember_ExpiredExceptionalUsage() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime yesterday = today.minusDays(1);

        Member member = new Member(null);
        member.addExceptionalUsage(yesterday, Usage.HAS_TO_DRIVE);
        Member archivedMember = member.archiveDataOlderThan(today);

        assertThat(archivedMember, hasUsage(yesterday, Usage.HAS_TO_DRIVE));
        assertThat(member, hasUsage(yesterday, Usage.DOES_NOT_USE));
    }

    @Test
    public void shouldArchiveMember_OngoingExceptionalUsage() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime yesterday = today.minusDays(1);
        DateTime tomorrow = today.plusDays(1);

        Member member = new Member(null);
        member.addExceptionalUsage(new Interval(yesterday, tomorrow), Usage.HAS_TO_DRIVE);
        Member archivedMember = member.archiveDataOlderThan(today);

        assertThat(archivedMember, hasUsage(yesterday.minusDays(1), Usage.DOES_NOT_USE));
        assertThat(archivedMember, hasUsage(yesterday, Usage.HAS_TO_DRIVE));
        assertThat(archivedMember, hasUsage(today, Usage.DOES_NOT_USE));

        assertThat(member, hasUsage(yesterday, Usage.DOES_NOT_USE));
        assertThat(member, hasUsage(today, Usage.HAS_TO_DRIVE));
        assertThat(member, hasUsage(tomorrow, Usage.HAS_TO_DRIVE));
        assertThat(member, hasUsage(tomorrow.plusDays(1), Usage.DOES_NOT_USE));
    }

    @Test
    public void shouldArchiveMember_UsageShouldBeMoved() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        Member member = new Member(null);
        member.addUsageAt(today.minusDays(10), new boolean[]{true, true, true, true, true, true, true});
        member.addUsageAt(today.plusDays(10), new boolean[]{false, false, false, false, false, false, false});

        Member archivedMember = member.archiveDataOlderThan(today);
        assertThat(archivedMember, hasUsage(today.minusDays(10), Usage.NORMAL));
        assertThat(archivedMember, hasUsage(today, Usage.DOES_NOT_USE));

        assertThat(member, hasUsage(today.minusDays(1), Usage.DOES_NOT_USE));
        assertThat(member, hasUsage(today, Usage.NORMAL));
        assertThat(member, hasUsage(today.plusDays(10), Usage.DOES_NOT_USE));
    }

    @Test
    public void shouldReturnWeekdayUsage_DefinedUsage() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        Member member = new Member(null);
        member.addUsageAt(today.minusDays(10), new boolean[]{true, true, true, true, true, true, true});

        boolean[] weekdayUsage = member.getWeekdayUsage(today);
        assertThat(weekdayUsage, is(new boolean[]{true, true, true, true, true, true, true}));
    }

    @Test
    public void shouldReturnWeekdayUsage_DefinedUsage_DateBeforeDefinition() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        Member member = new Member(null);
        member.addUsageAt(today.plusDays(10), new boolean[]{true, true, true, true, true, true, true});

        boolean[] weekdayUsage = member.getWeekdayUsage(today);
        assertThat(weekdayUsage, is(new boolean[]{false, false, false, false, false, false, false}));
    }

    @Test
    public void shouldReturnWeekdayUsage_DidNotDefineUsage() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        Member member = new Member(null);

        boolean[] weekdayUsage = member.getWeekdayUsage(today);
        assertThat(weekdayUsage, is(new boolean[]{false, false, false, false, false, false, false}));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotReturnWeekdayUsage_NullValue() {
        Member member = new Member(null);
        member.getWeekdayUsage(null);
    }

}