Feature: Example the usage of CarpoolNizer to first time users
  This feature ensures that first time users get an explanation of how to use CarpoolNizer

  Scenario: Hint to create a new carpool.
    Given I started the app
    When I a have no carpools available
    Then I should see a hint to create a carpool

  Scenario Outline: Hint to configure days and hint to add members
    Given I started the app
    When I want to create my first new carpool
    Then I should see a hint to select the days I use the carpool
    And I should see a hint to add members
    When I add <contacts> as a members
    Then I shoud see a hints to select the days for <contacts>

  Examples:
    | contacts |
    | Mike     |

  Scenario Outline: Explaination after creating carpool
    Given I started the app
    And I want to create a new carpool
    When I enter the carpool name <carpool>
    And I add <contacts> as a members
    And I am done with defining the carpool configuration
    Then I should see an explanation regarding overview

  Examples:
    | carpool          | contacts   |
    | My first carpool | Mike, John |

  Scenario Outline: Explanation of turn details
    Given I started the app
    And I opened the carpool <carpool>
    When I clicked on first turn with contact <current_driver> as driver
    Then I should see an explanation about turn details

  Examples:
    | carpool          | current_driver |
    | My first carpool | Mike           |

  Scenario Outline: Explanation of exceptional usage
    Given I started the app
    And I opened the carpool <carpool>
    When I clicked on first turn with contact <current_driver> as driver
    And I clicked on contact <current_driver>
    Then I should see an explanation about 'Uses carpool normally'
    And I should see an explanation about 'Has to drive'
    And I should see an explanation about 'Does not use'
    And I should see an explanation about 'Can not drive'

  Examples:
    | carpool          | current_driver |
    | My first carpool | Mike           |

  Scenario Outline: Explanation to switch between carpools
    Given I started the app
    And I want to create a new carpool
    When I enter the carpool name <carpool>
    And I add <contacts> as a members
    And I am done with defining the carpool configuration
    Then I should see an explanation to switch among carpools

  Examples:
    | carpool           | contacts   |
    | My second carpool | John       |