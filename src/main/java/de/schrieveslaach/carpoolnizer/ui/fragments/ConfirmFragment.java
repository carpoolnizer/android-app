/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

/**
 * {@link android.app.DialogFragment} for confirming actions.
 */
public class ConfirmFragment extends DialogFragment {

    private OnConfirmListener onConfirmListener;

    @StringRes
    private int confirmationMessage;

    @StringRes
    private int confirmationTitle;

    /**
     * Sets the {@link OnConfirmListener listener} which will be invoked when the user confirmed the
     * message.
     *
     * @param onConfirmListener
     */
    public void setOnConfirmListener(OnConfirmListener onConfirmListener) {
        this.onConfirmListener = onConfirmListener;
    }

    /**
     * Sets the string resource id which will be displayed as message.
     *
     * @param confirmationMessage
     */
    public void setConfirmationMessage(@StringRes int confirmationMessage) {
        this.confirmationMessage = confirmationMessage;
    }

    /**
     * Sets the string resource id which will be displayed as title.
     *
     * @param confirmationTitle
     */
    public void setConfirmationTitle(int confirmationTitle) {
        this.confirmationTitle = confirmationTitle;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle(confirmationTitle)
                .setMessage(confirmationMessage)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onConfirmListener.onFinish();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
    }

    /**
     * Listener which will be invoked when the user confirmed the message.
     */
    public interface OnConfirmListener {

        /**
         * Will be invoked when the user confirmed the message.
         */
        void onFinish();

    }

}
