/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.matchers;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import androidx.annotation.DrawableRes;
import android.widget.ImageView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.robolectric.RuntimeEnvironment;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;

public class ImageViewMatchers {

    private ImageViewMatchers() {

    }

    public static Matcher<ImageView> showsDrawable(@DrawableRes final int drawableId) {
        return new TypeSafeMatcher<ImageView>() {

            private byte[] getBytes(BitmapDrawable drawable) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                drawable.getBitmap().compress(Bitmap.CompressFormat.PNG, 100, stream);
                return stream.toByteArray();
            }

            @Override
            protected boolean matchesSafely(ImageView imageView) {
                BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
                byte[] bytesA = getBytes(drawable);
                byte[] bytesB = getBytes((BitmapDrawable) RuntimeEnvironment.application.getApplicationContext().getResources().getDrawable(drawableId));
                return Arrays.equals(bytesA, bytesB);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("drawable should be ").appendValue(drawableId);
            }
        };
    }
}
