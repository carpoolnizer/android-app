/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.model.settings;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.joda.time.DateTime;
import org.joda.time.Period;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Configuration of how to notify the user about his carpool usage.
 */
public class UsageNotification {

    @JsonSerialize(using = Serializer.class)
    @JsonProperty("time")
    private final Period period;

    /**
     * Creates a new instance of {@code UsageNotification}
     *
     * @param hour
     * @param minutes
     */
    public UsageNotification(int hour, int minutes) {
        this(new Period(hour, minutes, 0, 0));
    }

    /**
     * Creates a new instance of {@code UsageNotification}
     *
     * @param period
     */
    @JsonCreator
    public UsageNotification(@JsonProperty("time") @JsonDeserialize(using = Deserializer.class) Period period) {
        this.period = period;

        if (period.toStandardDays().getDays() > 0 || period.toStandardHours().getHours() > period.getHours()) {
            throw new IllegalArgumentException("provide a time between 24 hours");
        }
    }

    /**
     * @return return the time when the notification will be raised
     */
    public Period getPeriod() {
        return period;
    }

    /**
     * Converts this {@code UsageNotification} into millis according to
     * <a href="https://developer.android.com/training/scheduling/alarms.html">RTC examples</a>.
     *
     * @return
     */
    public long toMillis() {
        DateTime dateTime = DateTime.now().withTimeAtStartOfDay();
        return dateTime.withPeriodAdded(period, 1).getMillis();
    }

    private static class Serializer extends JsonSerializer<Period> {

        @Override
        public void serialize(Period value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            gen.writeString(String.format("%02d:%02d", value.getHours(), value.getMinutes()));
        }
    }

    private static class Deserializer extends JsonDeserializer<Period> {

        private static final Pattern P = Pattern.compile("(\\d{2,2}):(\\d{2,2})");

        @Override
        public Period deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
            String value = p.readValueAsTree().toString();

            Matcher matcher = P.matcher(value);
            matcher.find();
            int hours = Integer.parseInt(matcher.group(1));
            int minutes = Integer.parseInt(matcher.group(2));

            return new Period(hours, minutes, 0, 0);
        }
    }
}
