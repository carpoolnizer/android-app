/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;

import java.lang.reflect.Field;

import lombok.SneakyThrows;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Base class for testing {@link DialogFragment fragments}
 */
public abstract class BaseDialogFragmentTest<DF extends DialogFragment, D extends Dialog> extends BaseFragmentTest<DF> {

    protected BaseDialogFragmentTest(Class<DF> fragmentClass) {
        super(fragmentClass);
    }

    @SneakyThrows
    public D openDialog(DF fragment) {
        Dialog dlg = fragment.onCreateDialog(new Bundle());
        assertThat(dlg, is(notNullValue()));
        dlg.show();
        return (D) dlg;
    }
}
