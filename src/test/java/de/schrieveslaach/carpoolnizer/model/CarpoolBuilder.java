/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.model;

import android.net.Uri;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import org.joda.time.DateTime;

import java.net.URI;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import de.schrieveslaach.carpoolnizer.ui.robolectric.fakes.ContactsCursor;

/**
 * <a href="https://en.wikipedia.org/wiki/Builder_pattern">Builder pattern</a> to create
 * {@link Carpool carpools}.
 */
public class CarpoolBuilder {

    private String name;

    private final Set<String> memberNames = new HashSet<String>();

    private final Table<String, DateTime, boolean[]> members = HashBasedTable.create();

    private final Table<String, DateTime, Usage> exceptionalUsage = HashBasedTable.create();

    private DateTime calculationDate;

    public CarpoolBuilder withName(String name) {
        this.name = name;// TODO
        return this;
    }

    public CarpoolBuilder withMemberAndEveryDayUsage(String name) {
        return withMemberAndEveryDayUsage(name, DateTime.now().withTimeAtStartOfDay());
    }

    public CarpoolBuilder withMemberAndEveryDayUsage(String name, DateTime date) {
        return withMemberAndWeekdayUsage(name, date, new boolean[]{true, true, true, true, true, true, true});
    }

    public CarpoolBuilder withMemberAndWeekdayUsage(String name, DateTime date) {
        return withMemberAndWeekdayUsage(name, date, new boolean[]{true, true, true, true, true, false, false});
    }

    private CarpoolBuilder withMemberAndWeekdayUsage(String name, DateTime date, boolean[] usage) {
        if (name == null) {
            name = "<<<<<<<<<<<";
        }
        memberNames.add(name);
        members.put(name, date, usage);
        return this;
    }

    public CarpoolBuilder withMember(String name) {
        if (name == null) {
            name = "<<<<<<<<<<<";
        }
        memberNames.add(name);
        return this;
    }

    public CarpoolBuilder withCalculationDate(DateTime calculationDate) {
        this.calculationDate = calculationDate;
        return this;
    }

    public CarpoolBuilder withExceptionalUsage(String name, DateTime date, Usage usage) {
        if (name == null) {
            name = "<<<<<<<<<<<";
        }
        exceptionalUsage.put(name, date, usage);
        return this;
    }

    public Carpool create() {
        return create(null);
    }

    /**
     * @param contactsCursor Helper to create the {@link Uri uris} for the members.
     * @return
     */
    public Carpool create(ContactsCursor contactsCursor) {
        Carpool carpool = new Carpool(name);

        for (String name : memberNames) {
            Member m = getMember(name, contactsCursor);

            for (Map.Entry<DateTime, boolean[]> entry : members.row(name).entrySet()) {
                m.addUsageAt(entry.getKey(), entry.getValue());
            }

            carpool.add(m);
        }

        for (Table.Cell<String, DateTime, Usage> usage : exceptionalUsage.cellSet()) {
            Member m = getMember(usage.getRowKey(), contactsCursor);
            carpool.defineExceptionalUsage(m, usage.getColumnKey(), usage.getValue());
        }

        if (calculationDate != null) {
            carpool.calculateTurnsUntil(calculationDate);
        }

        return carpool;
    }

    public Member getMember(String name) {
        return getMember(name, null);
    }

    public Member getMember(String name, ContactsCursor contactsCursor) {
        if (!memberNames.contains(name)) {
            throw new IllegalArgumentException("Member is not part of the carpool builder");
        }

        if (contactsCursor == null) {
            return new Member(name == "<<<<<<<<<<<" ? null : URI.create("http://tempuri.org#" + name));
        }

        return contactsCursor.createMember(name);
    }

}
