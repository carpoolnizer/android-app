Feature: Edit carpool
  This feature allows to edit carpool, e.g. add new members or change carpool usage
  
  Scenario Outline: Add new member to carpool
    Given I started the app
    And I opened the carpool <carpool>
    And I want to edit the current carpool
    When I add <contacts> as a members
    And I am done with defining the carpool configuration
    Then I should see turns for the next <days> days with <contacts>

  Examples:
    | carpool    | contacts | all_contacts | days |
    | Work Daisy | John     | Daisy, John  | 7    |
