Feature: Define exceptional rules
  This feature allows to define exceptional rules for the usage of the carpool.
  For example, a member might not be able to drive because the car is in the shop.
  
  Scenario Outline: Define exceptional usage for carpool members
    Given I started the app
    And I opened the carpool <carpool>
    And I clicked on first turn with contact <current_driver> as driver and <current_passengers> passengers
    When I change usage of <member> to <exceptional_usage>
    Then I should see contact <new_driver> as driver
    And I should see passengers <passengers>
    And I should see remaining members <remaining_members>
    When I return to the main page
    Then I should see contact <new_driver> as driver for the turn previously clicked

  Examples:
    | carpool                 | current_driver | current_passengers | member | exceptional_usage | new_driver          | passengers        | remaining_members |
    | Daisy and Mike and John | Daisy          | 3                  | Mike   | does not use      | John or Daisy or Me | 2                 | Mike              |
    | Daisy and Mike and John | Mike           | 3                  | Mike   | cannot drive      | John or Daisy or Me | 3                 |                   |
    | Daisy and Mike and John | John           | 3                  | Me     | normal            | John                | Mike, Me, Daisy   |                   |
    | Daisy and Mike and John | Me             | 3                  | John   | has to drive      | John                | Mike, Me, Daisy   |                   |
  
  Scenario Outline: Override exceptional usage for carpool members
    Given I started the app
    And I opened the carpool <carpool>
    And I clicked on first turn with contact <current_driver> as driver
    When I change usage of <member> to <exceptional_usage1>
    Then I should see contact <new_driver1> as driver
    And I should see passengers <passengers1>
    And I should see remaining members <remaining_members1>
    When I return to the main page
    Then I should see contact <new_driver1> as driver for the turn previously clicked
    When I click on the last turn again
    And I change usage of <member> to <exceptional_usage2>
    Then I should see contact <new_driver2> as driver
    And I should see passengers <passengers2>
    And I should see remaining members <remaining_members2>
    When I return to the main page
    Then I should see contact <new_driver2> as driver for the turn previously clicked

  Examples:
    | carpool                 | current_driver | member | exceptional_usage1 | new_driver1    | passengers1       | remaining_members1 | exceptional_usage2 | new_driver2         | passengers2 | remaining_members2 | 
    | Daisy and John          | John           | Me     | does not use       | John or Daisy  | 1                 | Me                 | normal             | Me or John or Daisy | 2           |                    | 
    | Daisy and John          | Me             | Daisy  | has to drive       | Daisy          | John, Me          |                    | normal             | Me or John or Daisy | 2           |                    | 
    | Daisy and John          | Daisy          | Daisy  | cannot drive       | John or Me     | John or Me, Daisy |                    | normal             | Me or John or Daisy | 2           |                    | 

  Scenario Outline: Can not pick exceptional usage
    Given I started the app
    And I opened the carpool <carpool>
    And I clicked on first turn with contact <current_driver> as driver
    When I change usage of <current_driver> to <exceptional_usage>
    Then I should not be able to change usage of <member> to <exceptional_usage>

  Examples:
    | carpool   | current_driver | exceptional_usage | member |
    | Work John | Me             | has to drive      | John   |
    | Work John | John           | cannot drive      | Me     |