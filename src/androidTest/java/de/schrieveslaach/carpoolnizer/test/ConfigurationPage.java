/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.test;

import android.database.Cursor;
import android.os.Build;
import android.provider.ContactsContract;
import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiObjectNotFoundException;
import androidx.test.uiautomator.UiSelector;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.afollestad.materialdialogs.internal.MDButton;
import com.google.common.base.Objects;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import org.hamcrest.Matcher;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import cucumber.api.CucumberOptions;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.ui.CarpoolConfigurationActivity_;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.scrollToPosition;
import static androidx.test.espresso.matcher.RootMatchers.withDecorView;
import static androidx.test.espresso.matcher.ViewMatchers.Visibility.VISIBLE;
import static androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static de.schrieveslaach.carpoolnizer.test.ViewMatchers.withError;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@CucumberOptions(features = "features")
public class ConfigurationPage extends AbstractPage<CarpoolConfigurationActivity_> {

    public ConfigurationPage() {
        super(CarpoolConfigurationActivity_.class);
    }

    private void allowAllPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            UiDevice device = UiDevice.getInstance(getInstrumentation());
            allow(device, "Allow");
            allow(device, "ALLOW");
            allow(device, "Zulassen");
            allow(device, "ZULASSEN");
        }
    }

    private void allow(UiDevice device, String allowText) {
        UiObject allowPermissions = device.findObject(new UiSelector().text(allowText));
        if (allowPermissions.exists()) {
            try {
                allowPermissions.click();
            } catch (UiObjectNotFoundException e) {
                Log.e(getClass().getName(), "There is no permissions dialog to interact with ", e);
            }
        }
    }

    private DateTimeFormatter getShortFormatter() {
        return DateTimeFormat.shortDate().withLocale(
                getInstrumentation()
                        .getContext()
                        .getResources()
                        .getConfiguration()
                        .locale
        );
    }

    @When("^I enter the carpool name (.*)$")
    public void I_want_to_create_a_carpool(final String name) {
        allowAllPermissions();

        onView(
                withId(R.id.et_name)
        ).perform(typeText(name));
    }

    @When("^I add (.*) as a members$")
    public void I_add_a_contacts_as_a_members(final String contacts) throws Exception {
        allowAllPermissions();

        String[] contactNames = contacts.split(",\\s*");
        for (String contact : contactNames) {
            onView(
                    withId(R.id.ab_add_member)
            ).perform(click());

            onView(
                    withClassName(is(RecyclerView.class.getName()))
            ).perform(scrollToPosition(indexOfContact(contact)));

            onView(allOf(
                    withId(R.id.tv_member_name),
                    withText(contact)
            )).perform(click());
        }
    }

    private int indexOfContact(String contactName) {
        Cursor cursor = getCurrentActivity().getContentResolver().query(
                ContactsContract.Contacts.CONTENT_URI,
                new String[]{
                        ContactsContract.Contacts._ID,
                        ContactsContract.Contacts.DISPLAY_NAME
                },
                null,
                null,
                ContactsContract.Contacts.DISPLAY_NAME
        );

        int columnIndex = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);

        int i = 0;
        while (cursor.moveToNext()) {
            String string = cursor.getString(columnIndex);
            if (Objects.equal(string, contactName)) {
                break;
            }

            ++i;
        }
        return i;
    }


    @When("^I change the start date to (\\d*) ago$")
    public void I_change_the_start_date_to_months_ago(final int months) throws Exception {
        allowAllPermissions();

        onView(withId(R.id.et_select_start_date)).perform(click());

        DateTime date = DateTime.now().withTimeAtStartOfDay().minusMonths(months);

        onView(isAssignableFrom(MaterialCalendarView.class))
                .perform(setCalendarDate(date));

        // press done button of dialog
        onView(allOf(
                isAssignableFrom(MDButton.class),
                withEffectiveVisibility(VISIBLE),
                withText("OK")
        )).perform(click());

        onView(withId(R.id.et_select_start_date)).check(
                matches(anyOf(
                        withText("Starts at " + getShortFormatter().print(date)),
                        withText("Beginnt am " + getShortFormatter().print(date))
                ))
        );
    }

    @When("^I am done with defining the carpool configuration$")
    public void I_am_done() throws Throwable {
        allowAllPermissions();

        onView(
                withId(R.id.done)
        ).perform(click());
    }

    @Then("^I should see \"(.*?)\" as notification$")
    public void I_should_see_message_as_notification(final String message) {
        allowAllPermissions();

        onView(withText(message))
                .inRoot(withDecorView(not(getCurrentActivity().getWindow().getDecorView())))
                .check(matches(isDisplayed()));

        pressBack();
    }

    @Then("^I should see \"(.*?)\" as error in the carpool name field$")
    public void I_should_see_message_as_error_in_the_carpool_name_field(final String message) {
        allowAllPermissions();

        onView(withId(R.id.et_name)).check(matches(withError(message)));

        pressBack();
    }

    @Then("^I should see a hint to select the days I use the carpool$")
    public void I_should_see_a_hint_to_select_the_days_i_use_the_carpool() throws Exception {
        allowAllPermissions();

        Thread.sleep(1000);

        onView(
                withId(uk.co.deanwild.materialshowcaseview.R.id.tv_content)
        ).check(matches(withText(R.string.you_should_select_days_you_use_carpool)));
        onView(
                withId(uk.co.deanwild.materialshowcaseview.R.id.tv_dismiss)
        ).perform(click());
    }

    @Then("^I should see a hint to add members$")
    public void I_should_see_a_hint_to_add_members() throws Exception {
        allowAllPermissions();

        onView(withId(R.id.cb_monday)).perform(click());
        Thread.sleep(4000);

        onView(
                withId(uk.co.deanwild.materialshowcaseview.R.id.tv_content)
        ).check(matches(withText(R.string.you_should_add_contacts)));
        onView(
                withId(uk.co.deanwild.materialshowcaseview.R.id.tv_dismiss)
        ).perform(click());
    }

    @Then("^I shoud see a hints to select the days for (.+)$")
    public void I_shoud_see_a_hints_to_select_the_days_for_contacts(String contacts) {
        allowAllPermissions();

        onView(
                withId(uk.co.deanwild.materialshowcaseview.R.id.tv_content)
        ).check(matches(withText(R.string.you_should_select_days_your_contacts_use_carpool)));
        onView(
                withId(uk.co.deanwild.materialshowcaseview.R.id.tv_dismiss)
        ).perform(click());

    }

    private ViewAction setCalendarDate(final DateTime dateTime) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isAssignableFrom(MaterialCalendarView.class);
            }

            @Override
            public String getDescription() {
                return "set selected date of calendar view to " + dateTime;
            }

            @Override
            public void perform(UiController uiController, View view) {
                MaterialCalendarView mcv = (MaterialCalendarView) view;
                mcv.setSelectedDate(dateTime.toCalendar(getInstrumentation()
                        .getContext()
                        .getResources()
                        .getConfiguration()
                        .locale));
            }
        };
    }
}
