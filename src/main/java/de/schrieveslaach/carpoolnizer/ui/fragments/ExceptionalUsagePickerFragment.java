/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.HashSet;
import java.util.Set;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.schrieveslaach.carpoolnizer.Constants;
import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.model.Usage;
import uk.co.deanwild.materialshowcaseview.IShowcaseListener;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.shape.RectangleShape;
import uk.co.deanwild.materialshowcaseview.shape.Shape;
import uk.co.deanwild.materialshowcaseview.target.Target;

/**
 * {@link DialogFragment} to pick {@link Usage usage}.
 */
public class ExceptionalUsagePickerFragment extends DialogFragment {

    private final Set<Usage> disabledUsages = new HashSet<>();

    private OnPickUsageListener onPickUsageListener;

    private AlertDialog dialog;

    private RecyclerView recyclerView;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        recyclerView = new RecyclerView(getContext());
        recyclerView.setAdapter(new ItemsAdapter());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        dialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.define_usage)
                .setView(recyclerView)
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.dismiss())
                .create();

        // TODO dialog.setOnShowListener(new ShowExplanationListener());

        return dialog;
    }

    /**
     * Sets the listener which will be invoked when the user picked some {@link Usage usage}.
     *
     * @param onPickUsageListener
     */
    public void setOnPickUsageListener(OnPickUsageListener onPickUsageListener) {
        this.onPickUsageListener = onPickUsageListener;
    }

    /**
     * Disables the given usage in the list view.
     *
     * @param usage
     */
    public void disable(Usage usage) {
        disabledUsages.add(usage);
    }

    /**
     * Listener which will be called, when the user pick a {@link Usage usage}.
     */
    public interface OnPickUsageListener {

        /**
         * Will be raised when the user picked a {@link Usage usage}.
         *
         * @param usage
         */
        void onFinish(Usage usage);

    }

    /**
     * Adapter which helps to disable/enable usage items.
     */
    private class ItemsAdapter extends RecyclerView.Adapter<ExceptionalUsageViewHolder> {

        @Override
        public int getItemCount() {
            return Usage.values().length;
        }

        @Override
        public ExceptionalUsageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LinearLayout ll = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.view_usage, parent, false);
            return new ExceptionalUsageViewHolder(ll);
        }

        @Override
        public void onBindViewHolder(ExceptionalUsageViewHolder holder, int position) {
            holder.show(Usage.values()[position]);
        }
    }

    class ExceptionalUsageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final TextView tvUsage;

        private final View dividerTop;

        private Usage usage;

        private ExceptionalUsageViewHolder(LinearLayout ll) {
            super(ll);
            this.tvUsage = ll.findViewById(R.id.tv_usage);
            this.tvUsage.setOnClickListener(this);
            this.dividerTop = ll.findViewById(R.id.divider_top);
        }

        private void show(Usage usage) {
            this.usage = usage;
            switch (usage) {
                case DOES_NOT_USE:
                    tvUsage.setText(R.string.does_not_use_usage);
                    break;
                case CAN_NOT_DRIVE:
                    tvUsage.setText(R.string.can_not_drive_usage);
                    break;
                case HAS_TO_DRIVE:
                    tvUsage.setText(R.string.has_to_drive_usage);
                    break;
                default:
                    tvUsage.setText(R.string.normal_usage);
                    break;
            }
            tvUsage.setEnabled(isEnabled());
            dividerTop.setVisibility(usage.ordinal() == 0 ? View.VISIBLE : View.GONE);
        }

        boolean isEnabled() {
            return !disabledUsages.contains(usage);
        }

        @Override
        public void onClick(View view) {
            dialog.dismiss();

            if (!isEnabled()) {
                return;
            }

            onPickUsageListener.onFinish(usage);
        }

        private MaterialShowcaseView createShowcaseView(ShowExplanationListener listener) {
            int explanationTextId;
            switch (usage) {
                case NORMAL:
                    explanationTextId = R.string.explanation_normal_usage;
                    break;
                case HAS_TO_DRIVE:
                    explanationTextId = R.string.explanation_has_to_drive;
                    break;
                case DOES_NOT_USE:
                    explanationTextId = R.string.explanation_does_not_use;
                    break;
                default:
                    explanationTextId = R.string.explanation_can_not_drive;
                    break;
            }

            Shape shape = new RectangleShape(itemView.getWidth() * 2, itemView.getHeight());

            final MaterialShowcaseView showcaseView = new MaterialShowcaseView.Builder(getActivity())
                    .setDismissText(R.string.got_it)
                    .setContentText(explanationTextId)
                    //.setShape(shape)
                    .setListener(listener)
                    .show();

            showcaseView.setTarget(new Target() {
                @Override
                public Point getPoint() {
                    final int[] pos = new int[2];
                    itemView.getLocationOnScreen(pos);
                    return new Point(pos[0] + (itemView.getWidth() >> 2), pos[1] + (itemView.getHeight() >> 1));
                }

                @Override
                public Rect getBounds() {
                    return new Rect(0, 0, itemView.getWidth() * 2, itemView.getHeight());
                }
            });

            return showcaseView;
        }
    }

    /**
     * This {@link DialogInterface.OnShowListener} is a helper to display the {@link MaterialShowcaseView}
     * in full size. It starts a bottom sheet dialog to host the {@link MaterialShowcaseView}.
     */
    private class ShowExplanationListener implements DialogInterface.OnShowListener, IShowcaseListener {

        private final BottomSheetDialog dlg;

        private int usageIndex = 0;

        private ShowExplanationListener() {
            this.dlg = new BottomSheetDialog(ExceptionalUsagePickerFragment.this.getActivity());
        }

        @Override
        public void onShow(final DialogInterface dialogInterface) {
            new Handler().post(new ShowCaseViewRunnable());
        }

        @Override
        public void onShowcaseDisplayed(final MaterialShowcaseView materialShowcaseView) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    ViewGroup viewGroupActivity = (ViewGroup) getActivity().getWindow().getDecorView();
                    ViewGroup viewGroupDlg = (ViewGroup) dlg.getWindow().getDecorView();

                    viewGroupActivity.removeView(materialShowcaseView);
                    viewGroupDlg.addView(materialShowcaseView);
                }
            });
        }

        @Override
        public void onShowcaseDismissed(MaterialShowcaseView materialShowcaseView) {
            // not interested in this event
        }

        private class ShowCaseViewRunnable implements Runnable {

            @Override
            public void run() {
                final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                if (preferences.getBoolean(Constants.Notifications.EXPLAIN_EXCEPTIONAL_USAGE.getId(), false)) {
                    return;
                }

                dlg.show();

                ExceptionalUsageViewHolder viewHolder = (ExceptionalUsageViewHolder) recyclerView.findViewHolderForAdapterPosition(usageIndex);
                final MaterialShowcaseView showcaseView = viewHolder.createShowcaseView(ShowExplanationListener.this);

                showcaseView.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_dismiss)
                        .setOnClickListener(v -> {
                            ViewGroup decorViewDlg = (ViewGroup) dlg.getWindow().getDecorView();
                            decorViewDlg.removeView(showcaseView);

                            if (usageIndex >= 3) {
                                preferences.edit().putBoolean(Constants.Notifications.EXPLAIN_EXCEPTIONAL_USAGE.getId(), true).commit();
                                dlg.hide();
                                return;
                            }
                            ++usageIndex;
                            new Handler().post(new ShowCaseViewRunnable());
                        });
            }
        }
    }

}

