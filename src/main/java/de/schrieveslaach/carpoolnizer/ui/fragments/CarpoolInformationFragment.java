/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Currency;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.model.Carpool;
import de.schrieveslaach.carpoolnizer.model.Member;
import de.schrieveslaach.carpoolnizer.model.Savings;

/**
 * Displays information of a {@link Carpool} like saved money
 */
public class CarpoolInformationFragment extends DialogFragment {

    private DateTime currentDateTime;

    private Carpool carpool;

    private TextView tvSavedMoney;

    private TextView tvUsageCount;

    private EditText etDistance;

    private EditText etGasolineConsumption;

    private EditText etGasolinePrice;

    /**
     * Returns the {@link Carpool} which is displayed by {@code CarpoolInformationFragment}.
     *
     * @return
     */
    public Carpool getCarpool() {
        return carpool;
    }

    /**
     * Sets the {@link Carpool} which is displayed by {@code CarpoolInformationFragment}.
     *
     * @param carpool
     */
    public void setCarpool(Carpool carpool) {
        this.carpool = carpool;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_carpool_information, null);

        MaterialCalendarView mcv = view.findViewById(R.id.cv_month_of_information);
        mcv.removeView(mcv.findViewById(com.prolificinteractive.materialcalendarview.R.id.mcv_pager));
        mcv.setMinimumDate(carpool.getFirstUsedDate().toGregorianCalendar());
        mcv.setMaximumDate(carpool.getLastUsedDate().toGregorianCalendar());
        mcv.setOnMonthChangedListener(new OnMonthChangeAdapter());

        tvSavedMoney = view.findViewById(R.id.tv_money_put_aside_value);
        tvUsageCount = view.findViewById(R.id.tv_usage_count);

        etDistance = view.findViewById(R.id.et_distance);
        etDistance.addTextChangedListener(new SavingsTextChangeListener());
        etGasolineConsumption = view.findViewById(R.id.et_gasoline_consumption);
        etGasolineConsumption.addTextChangedListener(new SavingsTextChangeListener());
        etGasolinePrice = view.findViewById(R.id.et_gasoline_price);
        etGasolinePrice.addTextChangedListener(new SavingsTextChangeListener());
        etGasolinePrice.setHint(Currency.getInstance(getResources().getConfiguration().locale).getSymbol());

        currentDateTime = DateTime.now().withTimeAtStartOfDay().withDayOfMonth(1);
        restorePreviousValues();
        determineSavedMoney();

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setPositiveButton(android.R.string.ok, new StoreValuesButtonCallback())
                .create();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_carpool_information, container, false);

        return view;
    }

    private void restorePreviousValues() {
        SharedPreferences preferences = getActivity().getSharedPreferences("Carpool-Information", Activity.MODE_PRIVATE);

        etDistance.setText(preferences.getString("distance", ""));
        etGasolineConsumption.setText(preferences.getString("consumption", ""));
        etGasolinePrice.setText(preferences.getString("price", ""));
    }

    private void determineSavedMoney() {
        DateTime lastDayOfMonth = currentDateTime.plusMonths(1).minusDays(1);

        DecimalFormat numberFormat = (DecimalFormat) NumberFormat.getNumberInstance(getResources().getConfiguration().locale);
        numberFormat.setParseBigDecimal(true);

        Savings savings = carpool.determineSavedMoney(new Member(null), new Interval(currentDateTime, lastDayOfMonth));
        tvUsageCount.setText(getActivity().getString(R.string.usage_count, savings.getUsedCarpool(), savings.getDroveCarpool()));

        try {
            savings.setDistance((BigDecimal) numberFormat.parse(etDistance.getText().toString()));
            savings.setGasolineConsumption((BigDecimal) numberFormat.parse(etGasolineConsumption.getText().toString()));
            savings.setGasolinePrice((BigDecimal) numberFormat.parse(etGasolinePrice.getText().toString()));
        } catch (ParseException e) {
            Log.v(getTag(), e.getMessage());
            tvSavedMoney.setText("?");
            return;
        }

        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(getResources().getConfiguration().locale);
        tvSavedMoney.setText(currencyFormat.format(savings.toBigDecimal()));
    }

    /**
     * Invokes {@link #determineSavedMoney()} when the selected month has been changed.
     */
    private class OnMonthChangeAdapter implements OnMonthChangedListener {

        @Override
        public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
            currentDateTime = new DateTime(date.getYear(), date.getMonth() + 1, date.getDay(), 0, 0, 0);
            determineSavedMoney();
        }
    }

    /**
     * Invokes {@link #determineSavedMoney()} when data have been entered.
     */
    private class SavingsTextChangeListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // not interested in this event
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            determineSavedMoney();
        }

        @Override
        public void afterTextChanged(Editable s) {
            // not interested in this event
        }
    }

    private class StoreValuesButtonCallback implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            getActivity().getSharedPreferences("Carpool-Information", Activity.MODE_PRIVATE)
                    .edit()
                    .putString("distance", etDistance.getText().toString())
                    .putString("consumption", etGasolineConsumption.getText().toString())
                    .putString("price", etGasolinePrice.getText().toString())
                    .commit();
        }
    }
}
