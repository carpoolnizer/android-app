/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.repository;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import de.schrieveslaach.carpoolnizer.model.Carpool;

public class RepositoryMatchers {

    public static Matcher<CarpoolDAO> hasArchiveFor(final Carpool carpool) {
        return new TypeSafeMatcher<CarpoolDAO>() {
            @Override
            protected boolean matchesSafely(CarpoolDAO dao) {
                return dao.hasArchiveFor(carpool);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(" carpool dao ")
                        .appendValue(carpool.getName())
                        .appendText(" should have archive");
            }

            @Override
            protected void describeMismatchSafely(CarpoolDAO dao, Description mismatchDescription) {
                mismatchDescription.appendText(" carpool dao has ");
                if (!dao.hasArchiveFor(carpool)) {
                    mismatchDescription.appendText("no ");
                }
                mismatchDescription.appendText("archive");
            }
        };
    }
}
