/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;

import org.joda.time.Interval;

import java.util.SortedSet;

/**
 * Contains archived data of a {@link Carpool}.
 */
public class CarpoolArchive {

    /**
     * The combinations of the members.<br/>
     * <ul>
     * <li>First dimension: The size of the combinations</li>
     * <li>Second dimension: The combination of some members</li>
     * </ul>
     */
    @JsonProperty("combinations")
    private final Multimap<Integer, MemberCombination> combinations;

    /**
     * The {@link Turn turns}..
     */
    @JsonProperty("turns")
    private final SortedSet<Turn> turns;

    @JsonCreator
    CarpoolArchive(
            @JsonProperty("combinations") Multimap<Integer, MemberCombination> combinations,
            @JsonProperty("turns") SortedSet<Turn> turns
    ) {
        if (combinations == null) {
            throw new IllegalArgumentException("combinations must not be null!");
        }

        if (turns == null) {
            throw new IllegalArgumentException("turns must not be null!");
        }

        this.combinations = combinations;
        this.turns = turns;
    }

    /**
     * Returns the {@link Turn turns} contained in the {@code CarpoolArchive}
     *
     * @return
     */
    @JsonIgnore
    public ImmutableList<Turn> getTurns() {
        ImmutableList.Builder<Turn> builder = ImmutableList.<Turn>builder();
        builder.addAll(turns);
        return builder.build();
    }

    /**
     * Returns the {@link MemberCombination combinations} contained in the {@code CarpoolArchive}.
     *
     * @return
     */
    @JsonIgnore
    ImmutableMultimap<Integer, MemberCombination> getCombinations() {
        ImmutableMultimap.Builder<Integer, MemberCombination> builder = ImmutableMultimap.<Integer, MemberCombination>builder();
        builder.putAll(combinations);
        return builder.build();
    }

    /**
     * Gets the {@link Interval interval} which describes for which days the {@code CarpoolArchive}
     * contains {@link Turn turns}.
     *
     * @return {@code [firstTurnDate, lastTurnDate[}
     */
    @JsonIgnore
    public Interval getInterval() {
        return new Interval(
                turns.first().getDate().withTimeAtStartOfDay(),
                turns.last().getDate().withTimeAtStartOfDay().plusDays(1)
        );
    }
}
