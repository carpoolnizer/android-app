/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.repository;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.ContactsContract;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import de.schrieveslaach.carpoolnizer.model.Member;
import de.schrieveslaach.carpoolnizer.repository.resolver.ContactPictureResolver;
import de.schrieveslaach.carpoolnizer.repository.resolver.DisplayNameResolver;

/**
 * Helper class to retrieve data for contacts.
 */
@EBean(scope = EBean.Scope.Singleton)
public class ContactDAO {

    private CursorDataCache<String> displayNameCache;

    private CursorDataCache<Bitmap> contactPictureCache;

    @RootContext
    Context context;

    /**
     * Returns the display of the given {@link Member member}.
     *
     * @param member
     * @return
     */
    public String getDisplayName(Member member) {
        Uri uri;
        if (member.isPhoneUser()) {
            uri = ContactsContract.Profile.CONTENT_URI;
        } else {
            uri = parse(member);
        }

        return displayNameCache.getData(uri, ContactsContract.Contacts.DISPLAY_NAME);
    }

    private Uri parse(Member member) {
        return Uri.parse(member.getUri().toString());
    }

    /**
     * Returns the contact picture of the given {@link Member member}.
     *
     * @return
     */
    public Bitmap getContactPicture(Member member) {

        Uri uri;
        if (member.isPhoneUser()) {
            uri = ContactsContract.Profile.CONTENT_URI;
        } else {
            uri = parse(member);
        }
        uri = Uri.withAppendedPath(uri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);

        return contactPictureCache.getData(uri, ContactsContract.Contacts.Photo.PHOTO);
    }


    @AfterInject
    protected void afterInject() {
        displayNameCache = new CursorDataCache<>(context, new DisplayNameResolver());
        contactPictureCache = new CursorDataCache<>(context, new ContactPictureResolver());
    }


}
