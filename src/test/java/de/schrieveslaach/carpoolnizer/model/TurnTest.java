/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.model;

import com.google.common.collect.ImmutableSet;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TurnTest {

    private static final URI MIKE = URI.create("http://tempuri.org#Mike");

    private static final URI JOHN = URI.create("http://tempuri.org#John");

    private static final URI DAISY = URI.create("http://tempuri.org#Daisy");

    private DateTime today;

    @Before
    public void init() {
        today = DateTime.now().withTimeAtStartOfDay();
    }

    @Test
    public void shouldBeEquals_SameInstance() {
        Turn t = new Turn(today);
        assertThat(t, equalTo(t));
    }

    @Test
    public void shouldBeEquals_DifferentInstance() {
        Member m1 = new Member(null);
        Member m2 = new Member(MIKE);
        MemberCombination combination = new MemberCombination(m1, Arrays.asList(m2));

        Turn t1 = new Turn(today, m1, combination);
        Turn t2 = new Turn(today, m1, combination);
        assertThat(t1, equalTo(t2));
    }

    @Test
    public void shouldNotBeEquals_Null() {
        Turn t = new Turn(today);
        assertThat(t, not(equalTo(null)));
    }

    @Test
    public void shouldNotBeEquals_WrongType() {
        Turn t = new Turn(today);
        assertThat(t, not(equalTo(new Object())));
    }

    @Test
    public void shouldNotBeEquals_DifferentDate() {
        Member m1 = new Member(null);
        Member m2 = new Member(MIKE);
        MemberCombination combination = new MemberCombination(m1, Arrays.asList(m2));

        Turn t1 = new Turn(today, m1, combination);
        Turn t2 = new Turn(today.plusDays(1), m1, combination);

        assertThat(t1, not(equalTo(t2)));
    }

    @Test
    public void shouldNotBeEquals_DifferentDriver() {
        Member m1 = new Member(null);
        Member m2 = new Member(MIKE);
        MemberCombination combination = new MemberCombination(m1, Arrays.asList(m2));

        Turn t1 = new Turn(today, m1, combination);
        Turn t2 = new Turn(today, m2, combination);

        assertThat(t1, not(equalTo(t2)));
    }

    @Test
    public void shouldNotBeEquals_DifferentCombination() {
        Member m1 = new Member(null);
        Member m2 = new Member(MIKE);
        Member m3 = new Member(JOHN);
        MemberCombination combination1 = new MemberCombination(m1, Arrays.asList(m2));
        MemberCombination combination2 = new MemberCombination(m1, Arrays.asList(m3));

        Turn t1 = new Turn(today, m1, combination1);
        Turn t2 = new Turn(today, m1, combination2);

        assertThat(t1, not(equalTo(t2)));
    }

    @Test
    public void shouldReturnPassengers_Regular() {
        Member m1 = new Member(null);
        Member m2 = new Member(MIKE);
        Member m3 = new Member(JOHN);
        MemberCombination combination1 = new MemberCombination(m1, Arrays.asList(m2, m3));

        Turn t = new Turn(today, m1, combination1);

        List<Member> passengers = t.getPassengers();
        assertThat(passengers, not(contains(m1)));
        assertThat(passengers, containsInAnyOrder(m2, m3));
    }

    @Test
    public void shouldReturnPassengers_NoUsage() {
        Turn t = new Turn(today);

        List<Member> passengers = t.getPassengers();
        assertThat(passengers, is(empty()));
    }

    @Test
    public void shouldReturnPassengers_NoPassengers() {
        Turn t = new Turn(today, new Member(MIKE));

        List<Member> passengers = t.getPassengers();
        assertThat(passengers, is(empty()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateTurn_InvalidDriverAndMemberCombination() {
        new Turn(today, null, new MemberCombination(Arrays.asList(new Member(JOHN), new Member(MIKE))));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateTurn_DateNull() {
        new Turn(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateTurn_DriverNotInCombination() {
        MemberCombination mc = new MemberCombination(Arrays.asList(new Member(JOHN), new Member(MIKE)));
        Member driver = new Member(DAISY);
        new Turn(today, driver, mc);
    }

    @Test
    public void shouldReturnDriverCount() {
        Member mike = new Member(MIKE);
        mike.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, true});
        Member john = new Member(JOHN);
        john.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, true});
        MemberCombination combination = new MemberCombination(mike, Arrays.asList(john));
        Turn turn = combination.calculateTurns(Arrays.asList(DateTime.now().withTimeAtStartOfDay())).iterator().next();

        assertThat(turn.getDriverCount(mike), is(0));
        assertThat(turn.getDriverCount(john), is(1));
    }

    @Test(expected = IllegalStateException.class)
    public void shouldNotGetDriverCount_NoPassengers() {
        new Turn(DateTime.now(), new Member(MIKE)).getDriverCount(new Member(MIKE));
    }

    @Test(expected = IllegalStateException.class)
    public void shouldNotRevert_NoCombination() {
        new Turn(DateTime.now()).revert();
    }

    @Test
    public void shouldContain_DriverOnly() {
        Member mike = new Member(MIKE);
        Turn turn = new Turn(today, mike);
        assertThat(turn.contains(mike), is(true));
    }

    @Test
    public void shouldNotContain_DriverOnly_NullValue() {
        Member mike = new Member(MIKE);
        Turn turn = new Turn(today, mike);
        assertThat(turn.contains(null), is(false));
    }

    @Test
    public void shouldContain_MemberInCombination() {
        Member mike = new Member(MIKE);
        Member john = new Member(JOHN);

        Turn turn = new Turn(today, mike, new MemberCombination(Arrays.asList(john, mike)));
        assertThat(turn.contains(john), is(true));
    }

    @Test
    public void shouldNotContain_Combination_NullValue() {
        Member mike = new Member(MIKE);
        Member john = new Member(JOHN);

        Turn turn = new Turn(today, mike, new MemberCombination(Arrays.asList(john, mike)));
        assertThat(turn.contains(null), is(false));
    }

    @Test
    public void shouldNotContain_MemberNotInCombination() {
        Member mike = new Member(MIKE);
        Member john = new Member(JOHN);
        Member daisy = new Member(DAISY);

        Turn turn = new Turn(today, mike, new MemberCombination(Arrays.asList(john, mike)));
        assertThat(turn.contains(daisy), is(false));
    }

    @Test
    public void shouldNotContain_TurnIsEmpty() {
        Turn turn = new Turn(today);
        Member mike = new Member(MIKE);
        assertThat(turn.contains(mike), is(false));
    }

    @Test
    public void shouldGetMembers_NoOneAvailable() {
        Turn turn = new Turn(today);
        assertThat(turn.getMembers(), is(empty()));
    }

    @Test
    public void shouldGetMembers_DriverAvailable() {
        Turn turn = new Turn(today, new Member(MIKE));
        assertThat(turn.getMembers(), hasSize(1));
        assertThat(turn.getMembers(), contains(new Member(MIKE)));
    }

    @Test
    public void shouldGetMembers_WithPassengers() {
        Member mike = new Member(MIKE);
        Member john = new Member(JOHN);

        Turn turn = new Turn(today, mike, new MemberCombination(Arrays.asList(john, mike)));
        assertThat(turn.getMembers(), hasSize(2));
        assertThat(turn.getMembers(), contains(john, mike));
    }

    @Test
    public void shouldGetRemainingMembers_None() {
        Member mike = new Member(MIKE);
        Member john = new Member(JOHN);

        Turn turn = new Turn(today, mike, new MemberCombination(Arrays.asList(john, mike)));
        Carpool carpool = mock(Carpool.class);
        when(carpool.getMembers()).thenReturn(ImmutableSet.of(john, mike));
        turn.setCarpool(carpool);

        assertThat(turn.getRemainingMembers(), is(empty()));
    }

    @Test
    public void shouldGetRemainingMembers_JohnRemains() {
        Member mike = new Member(MIKE);
        Member john = new Member(JOHN);

        Turn turn = new Turn(today, mike);
        Carpool carpool = mock(Carpool.class);
        when(carpool.getMembers()).thenReturn(ImmutableSet.of(john, mike));
        turn.setCarpool(carpool);

        assertThat(turn.getRemainingMembers(), hasSize(1));
        assertThat(turn.getRemainingMembers(), contains(john));
    }
}


