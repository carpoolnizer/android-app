Feature: Searching for specific turns.
  This feature allows to search for specific turns in the overview of turns. 
  
  Scenario Outline: Scroll to the turn of today by menu button.
    Given I started the app
    And I opened the carpool <carpool>
    And I scrolled to the <first_or_last> turn
    When I click on the today menu button
    Then I should see the turn of today    

  Examples:
    | carpool       | first_or_last | 
    | Mike and John | first         |
    | Mike and John | last          |
    | Work Mike     | last          |
    | Work Mike     | first         |

