/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.robolectric.fakes;

import android.net.Uri;
import android.provider.ContactsContract;

import org.robolectric.RuntimeEnvironment;
import org.robolectric.fakes.RoboCursor;
import org.robolectric.shadows.ShadowContentResolver;

import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import de.schrieveslaach.carpoolnizer.model.Member;

import static org.robolectric.Shadows.shadowOf;

public class ContactsCursor {
    private final Set<String> contactNames = new LinkedHashSet<String>();

    private final Set<URI> uris = new LinkedHashSet<URI>();

    private final ShadowContentResolver scr;

    public ContactsCursor() {
        scr = shadowOf(RuntimeEnvironment.application.getContentResolver());
    }

    public ContactsCursor withContact(String name) {
        if (name == null || name.trim().equals("")) {
            throw new IllegalArgumentException("Provide a name");
        }
        contactNames.add(name);
        return this;
    }

    public ContactsCursor withContact(URI uri) {
        if (uri == null || uri.getFragment() == null || contactNames.contains(uri.getFragment())) {
            throw new IllegalArgumentException("Provide a unused uri");
        }
        uris.add(uri);
        return this;
    }

    public int indexOfContact(String lookUpContactName) {
        int index = 0;
        for (String contactName : contactNames) {
            if (lookUpContactName.equals(contactName)) {
                return index;
            }
            ++index;
        }
        throw new IllegalArgumentException("can not find contact: " + lookUpContactName);
    }

    public Member createMember(String contactName) {
        if (contactName == "<<<<<<<<<<<") {
            return new Member(null);
        }

        return new Member(URI.create(createContactUri(contactName).toString()));
    }

    public Uri createContactUri(String contactName) {
        if (contactName == null) {
            return null;
        }
        int id = indexOfContact(contactName);
        return createContactUri(id);
    }

    private Uri createContactUri(int id) {
        return Uri.withAppendedPath(
                ContactsContract.Contacts.CONTENT_URI,
                String.valueOf(id)
        );
    }

    /**
     * Fakes a {@link RoboCursor} for {@link ContactsContract.Contacts}.
     */
    public ContactsCursor fake() {
        fakeProfile();

        RoboCursor cursor = new RoboCursor();

        List<String> columnNames = new ArrayList<String>();
        columnNames.add(ContactsContract.Contacts._ID);
        columnNames.add(ContactsContract.Contacts.DISPLAY_NAME);
        cursor.setColumnNames(columnNames);

        List<Object[]> results = new ArrayList<Object[]>();
        int id = 0;
        for (String contactName : contactNames) {
            results.add(new Object[]{
                    id,
                    contactName
            });

            fakeDisplayNameCursor(id, contactName);
            fakePhotoCursor(id);

            ++id;
        }

        for (URI uri : uris) {
            results.add(new Object[]{
                    id,
                    uri.getFragment()
            });

            fakeDisplayNameCursor(uri);
            fakePhotoCursor(uri);
        }

        cursor.setResults(results.toArray(new Object[results.size()][]));

        scr.setCursor(ContactsContract.Contacts.CONTENT_URI, cursor);

        return this;
    }

    private void fakeDisplayNameCursor(int id, String contactName) {
        RoboCursor cursor = new RoboCursor();

        List<String> columnNames = new ArrayList<String>();
        columnNames.add(ContactsContract.Contacts.Photo.DISPLAY_NAME);
        cursor.setColumnNames(columnNames);

        cursor.setResults(new Object[][]{{contactName}});

        Uri uri = createContactUri(id);

        scr.setCursor(uri, cursor);
    }

    private void fakeDisplayNameCursor(URI uri) {
        RoboCursor cursor = new RoboCursor();

        List<String> columnNames = new ArrayList<String>();
        columnNames.add(ContactsContract.Contacts.Photo.DISPLAY_NAME);
        cursor.setColumnNames(columnNames);

        cursor.setResults(new Object[][]{{uri.getFragment()}});

        scr.setCursor(Uri.parse(uri.toString()), cursor);
    }

    private void fakePhotoCursor(int id) {
        RoboCursor cursor = new RoboCursor();

        List<String> columnNames = new ArrayList<String>();
        columnNames.add(ContactsContract.Contacts.Photo.PHOTO);
        cursor.setColumnNames(columnNames);

        Uri uri = createContactUri(id);
        uri = Uri.withAppendedPath(uri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);

        scr.setCursor(uri, cursor);
    }

    private void fakePhotoCursor(URI uri) {
        RoboCursor cursor = new RoboCursor();

        List<String> columnNames = new ArrayList<String>();
        columnNames.add(ContactsContract.Contacts.Photo.PHOTO);
        cursor.setColumnNames(columnNames);

        Uri contactUri = Uri.parse(uri.toString());
        contactUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);

        scr.setCursor(contactUri, cursor);
    }

    /**
     * Fakes the profile
     *
     * @see ContactsContract.Profile#CONTENT_URI
     */
    private void fakeProfile() {
        scr.setCursor(
                ContactsContract.Profile.CONTENT_URI,
                new RoboCursor()
        );
        scr.setCursor(
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY),
                new RoboCursor()
        );
    }
}
