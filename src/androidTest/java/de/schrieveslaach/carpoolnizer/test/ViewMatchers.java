/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.test;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.Constants;
import de.schrieveslaach.carpoolnizer.ui.adapters.TurnViewHolder;

import static androidx.test.espresso.matcher.ViewMatchers.withText;

/**
 * Defines {@link Matcher matchers} to check the status of {@link View views} with
 * {@link androidx.test.espresso.Espresso}.
 */
public class ViewMatchers {

    public static Matcher<Activity> hasTitle(final String title) {
        return new TypeSafeMatcher<Activity>() {
            @Override
            public boolean matchesSafely(Activity activity) {
                return activity.getTitle().toString().equals(title);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(" title = ").appendValue(title);
            }
        };
    }

    public static Matcher<View> withError(final String expectedErrorMessage) {
        return new TypeSafeMatcher<View>() {
            @Override
            public boolean matchesSafely(View view) {
                if (!(view instanceof EditText)) {
                    return false;
                }

                CharSequence errorMessage = ((EditText) view).getError();
                return expectedErrorMessage.equals(errorMessage);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("error text: ").appendValue(expectedErrorMessage);
            }
        };
    }

    public static Matcher<View> withContactName(final String contactName) {
        String[] options = contactName.trim().split("\\s+or\\s+");
        List<Matcher<View>> texts = new ArrayList<Matcher<View>>();
        for (String option : options) {
            texts.add(withText(option));
        }
        return exactlyOne(texts);
    }

    public static Matcher<View> exactlyOne(final List<Matcher<View>> matchers) {
        return new TypeSafeMatcher<View>() {
            @Override
            public boolean matchesSafely(View view) {
                int match = 0;
                for (Matcher<View> m : matchers) {
                    if (m.matches(view)) {
                        ++match;
                    }
                }
                return match == 1;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("exactly one should match ");
                for (Matcher<View> vm : matchers) {
                    description.appendDescriptionOf(vm);
                }
            }
        };
    }

    public static Matcher<RecyclerView.ViewHolder> withDriver(final String contactName) {
        return new TypeSafeMatcher<RecyclerView.ViewHolder>() {
            @Override
            public boolean matchesSafely(RecyclerView.ViewHolder viewHolder) {
                TextView textView = (TextView) viewHolder.itemView.findViewById(R.id.tv_member_name);
                return withContactName(contactName).matches(textView);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("driver name should be ").appendValue(contactName);
            }
        };
    }

    public static Matcher<RecyclerView.ViewHolder> withPassengerCount(final int passengerCount) {
        return new TypeSafeMatcher<RecyclerView.ViewHolder>() {
            @Override
            public boolean matchesSafely(RecyclerView.ViewHolder viewHolder) {
                TextView tvPassengerCount = (TextView) viewHolder.itemView.findViewById(R.id.tv_number_of_passengers);
                return Integer.parseInt(tvPassengerCount.getText().toString()) == passengerCount;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("should have passenger count ").appendValue(passengerCount);
            }
        };
    }

    public static Matcher<RecyclerView.ViewHolder> isCanceledTurn() {
        return new TypeSafeMatcher<RecyclerView.ViewHolder>() {
            @Override
            public boolean matchesSafely(RecyclerView.ViewHolder viewHolder) {
                if (!(viewHolder instanceof TurnViewHolder)) {
                    return false;
                }

                TurnViewHolder tvh = (TurnViewHolder) viewHolder;
                return tvh.getTurn().isCanceled();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("turn is canceled");
            }
        };
    }

    public static Matcher<RecyclerView.ViewHolder> isNotCanceledTurn() {
        return new TypeSafeMatcher<RecyclerView.ViewHolder>() {
            @Override
            public boolean matchesSafely(RecyclerView.ViewHolder viewHolder) {
                if (!(viewHolder instanceof TurnViewHolder)) {
                    return false;
                }

                TurnViewHolder tvh = (TurnViewHolder) viewHolder;
                return !tvh.getTurn().isCanceled();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("turn is not canceled");
            }
        };
    }

    public static Matcher<RecyclerView.ViewHolder> withDate(final DateTime date) {
        return withinInterval(new Interval(date.withTimeAtStartOfDay(), date.withTimeAtStartOfDay().plusDays(1)));
    }

    public static Matcher<RecyclerView.ViewHolder> withinInterval(final Interval interval) {
        return new TypeSafeMatcher<RecyclerView.ViewHolder>() {
            @Override
            public boolean matchesSafely(RecyclerView.ViewHolder viewHolder) {
                TextView textView = (TextView) viewHolder.itemView.findViewById(R.id.tv_turn_date);
                DateTimeFormatter formatter = Constants.getFullDateFormatter(viewHolder.itemView.getContext());
                DateTime dateTime = formatter.parseDateTime(textView.getText().toString());
                return interval.contains(dateTime);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("turn date should be in ").appendValue(interval);
            }
        };
    }

    public static <T> Matcher<T> isFirst() {
        return new TypeSafeMatcher<T>() {
            private int called;

            @Override
            public boolean matchesSafely(T elem) {
                return called++ == 0;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("should be first");
            }
        };
    }

    public static <V extends View> Matcher<V> isEmptyRecyclerView() {
        return new TypeSafeMatcher<V>() {
            @Override
            public boolean matchesSafely(V view) {
                if (!(view instanceof  RecyclerView)) {
                    return false;
                }

                RecyclerView recyclerView = (RecyclerView) view;
                return recyclerView.getAdapter().getItemCount() == 0;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("is empty");
            }
        };
    }
}
