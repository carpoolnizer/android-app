/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.repository;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CursorDataCacheTest {

    private Context context;

    private CursorDataCache.DataResolver<String> dataResolver;

    @Before
    public void init() {
        context = mock(Context.class);
        dataResolver = mock(CursorDataCache.DataResolver.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateCache_ContextIsNull() {
        new CursorDataCache<String>(null, dataResolver);
    }


    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateCache_ResolverIsNull() {
        new CursorDataCache<String>(context, null);
    }

    @Test
    public void shouldLoadDataFromCursor() {
        CursorDataCache<String> cache = new CursorDataCache<String>(context, dataResolver);

        String value = "hello world";
        when(dataResolver.loadFrom(any(Cursor.class), anyInt())).thenReturn(value);

        Uri uri = mock(Uri.class);
        String projection = "COLUMN_1";
        Cursor cursor = mock(Cursor.class);
        when(cursor.moveToFirst()).thenReturn(true);

        ContentResolver resolver = mock(ContentResolver.class);
        when(resolver.query(uri, new String[]{projection}, null, null, null)).thenReturn(cursor);
        when(context.getContentResolver()).thenReturn(resolver);

        String data = cache.getData(uri, projection);
        assertThat(data, is(sameInstance(value)));

        verify(cursor).close();
        verify(dataResolver).loadFrom(cursor, 0);
    }

    @Test
    public void shouldLoadDataFromCursor_LoadAlternative() {
        CursorDataCache<String> cache = new CursorDataCache<String>(context, dataResolver);

        String value = "hello world";
        when(dataResolver.getAlternativeData(context)).thenReturn(value);

        Uri uri = mock(Uri.class);
        String projection = "COLUMN_1";
        Cursor cursor = mock(Cursor.class);
        when(cursor.moveToFirst()).thenReturn(false);

        ContentResolver resolver = mock(ContentResolver.class);
        when(resolver.query(uri, new String[]{projection}, null, null, null)).thenReturn(cursor);
        when(context.getContentResolver()).thenReturn(resolver);

        String data = cache.getData(uri, projection);
        assertThat(data, is(sameInstance(value)));

        verify(cursor).close();
        verify(dataResolver).getAlternativeData(context);
    }

    @Test
    public void shouldLoadDataFromCursor_ReturnCachedValue() {
        CursorDataCache<String> cache = new CursorDataCache<String>(context, dataResolver);

        String value = "hello world";
        when(dataResolver.loadFrom(any(Cursor.class), anyInt())).thenReturn(value);

        Uri uri = mock(Uri.class);
        String projection = "COLUMN_1";
        Cursor cursor = mock(Cursor.class);
        when(cursor.moveToFirst()).thenReturn(true);

        ContentResolver resolver = mock(ContentResolver.class);
        when(resolver.query(uri, new String[]{projection}, null, null, null)).thenReturn(cursor);
        when(context.getContentResolver()).thenReturn(resolver);

        cache.getData(uri, projection);

        String data = cache.getData(uri, projection);
        assertThat(data, is(sameInstance(value)));
    }

    @Test
    public void shouldCacheDataFromCursor_DataAvailable() {
        CursorDataCache<String> cache = new CursorDataCache<String>(context, dataResolver);

        Uri uri = mock(Uri.class);
        String projection = "COLUMN_1";
        Cursor cursor = mock(Cursor.class);

        String value = "hello world";
        when(dataResolver.loadFrom(any(Cursor.class), anyInt())).thenReturn(value);

        String data = cache.cacheData(uri, cursor, projection);
        assertThat(data, is(sameInstance(value)));

        verify(dataResolver).loadFrom(cursor, 0);
        verify(dataResolver, never()).getAlternativeData(context);
    }

    @Test
    public void shouldCacheDataFromCursor_LoadAlternative() {
        CursorDataCache<String> cache = new CursorDataCache<String>(context, dataResolver);

        Uri uri = mock(Uri.class);
        String projection = "COLUMN_1";
        Cursor cursor = mock(Cursor.class);

        String value = "hello world";
        when(dataResolver.loadFrom(any(Cursor.class), anyInt())).thenReturn(null);
        when(dataResolver.getAlternativeData(context)).thenReturn(value);

        String data = cache.cacheData(uri, cursor, projection);
        assertThat(data, is(sameInstance(value)));

        verify(dataResolver).loadFrom(cursor, 0);
        verify(dataResolver).getAlternativeData(context);
    }

    @Test
    public void shouldCacheDataFromCursor_DataAlreadyCached() {
        CursorDataCache<String> cache = new CursorDataCache<String>(context, dataResolver);

        Uri uri = mock(Uri.class);
        String projection = "COLUMN_1";
        Cursor cursor = mock(Cursor.class);

        String value = "hello world";
        when(dataResolver.loadFrom(any(Cursor.class), anyInt())).thenReturn(value);

        cache.cacheData(uri, cursor, projection);
        String data = cache.cacheData(uri, cursor, projection);
        assertThat(data, is(sameInstance(value)));

        verify(dataResolver).loadFrom(cursor, 0);
        verify(dataResolver, never()).getAlternativeData(context);
    }
}