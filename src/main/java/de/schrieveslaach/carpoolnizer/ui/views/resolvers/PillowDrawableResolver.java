/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.views.resolvers;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.ui.views.MemberReadOnlyView;

/**
 * Resolves an image displaying a {@link R.drawable#ic_action_pillow2 pillow}.
 */
public class PillowDrawableResolver implements MemberReadOnlyView.NoMemberDrawableResolver {
    private static final Object PILLOW_MUTEX = new Object();

    private static BitmapDrawable pillowDrawable;

    private static Drawable getPillowDrawable(Context context) {
        synchronized (PILLOW_MUTEX) {
            if (pillowDrawable == null) {
                pillowDrawable = (BitmapDrawable) context.getResources().getDrawable(R.drawable.ic_action_pillow2);
            }
            return pillowDrawable;
        }
    }

    @Override
    public Drawable resolveBitmap(Context context) {
        return getPillowDrawable(context);
    }
}
