/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.matchers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import com.google.common.base.Objects;
import com.google.common.collect.Sets;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.robolectric.shadows.ShadowActivity;

import static org.robolectric.RuntimeEnvironment.application;
import static org.robolectric.Shadows.shadowOf;

/**
 * Matchers for robolectric testing
 */
public class ActivityMatchers {

    public static Matcher<Activity> notStartedActivity() {
        return new TypeSafeMatcher<Activity>() {

            private Intent startedIntent;

            @Override
            protected boolean matchesSafely(Activity activity) {
                startedIntent = shadowOf(activity).getNextStartedActivity();
                return startedIntent == null;
            }

            @Override
            public void describeTo(Description description) {
                description.appendValue("did not started activity");
            }

            @Override
            protected void describeMismatchSafely(Activity item, Description mismatchDescription) {
                mismatchDescription.appendText("started activity by: ").appendValue(startedIntent);
            }
        };
    }

    public static Matcher<Activity> startedActivity(final Intent intent) {
        return new TypeSafeMatcher<Activity>() {

            private Intent startedIntent;

            @Override
            protected boolean matchesSafely(Activity activity) {
                startedIntent = shadowOf(activity).getNextStartedActivity();
                return ActivityMatchers.equals(intent, startedIntent);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(" should started activity by ")
                        .appendValue(intent);
            }

            @Override
            protected void describeMismatchSafely(Activity activity, Description mismatchDescription) {
                mismatchDescription.appendText(" was started by ")
                        .appendValue(startedIntent);
            }
        };
    }

    private static boolean equals(Intent a, Intent b) {
        return Objects.equal(a.getAction(), b.getAction())
                && Objects.equal(a.getData(), b.getData())
                && Objects.equal(a.getComponent(), b.getComponent())
                && equals(a.getExtras(), b.getExtras());
    }

    private static boolean equals(Bundle a, Bundle b) {
        if (a == b) {
            return true;
        }

        if (a == null || b == null) {
            return false;
        }

        if (!Sets.difference(a.keySet(), b.keySet()).isEmpty()) {
            return false;
        }

        for (String key : a.keySet()) {
            Object o1 = a.get(key);
            Object o2 = b.get(key);

            if (!Objects.equal(o1, o2)) {
                return false;
            }
        }

        return true;
    }

    public static Matcher<Activity> hasTitle(@StringRes final int title) {
        return hasTitle(application.getString(title));
    }

    public static Matcher<AppCompatActivity> hasNoSubtitleTitle() {
        return new TypeSafeMatcher<AppCompatActivity>() {
            @Override
            protected boolean matchesSafely(AppCompatActivity activity) {
                return activity.getSupportActionBar().getSubtitle() == null;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("has no subtitle");
            }

            @Override
            protected void describeMismatchSafely(AppCompatActivity activity, Description mismatchDescription) {
                mismatchDescription.appendText("subtitle ").appendValue(activity.getSupportActionBar().getSubtitle());
            }
        };
    }

    public static Matcher<AppCompatActivity> hasSubtitleTitle(@StringRes final int resId) {
        return hasSubtitleTitle(application.getString(resId));
    }

    public static Matcher<AppCompatActivity> hasSubtitleTitle(final String subtitle) {
        return new TypeSafeMatcher<AppCompatActivity>() {
            @Override
            public boolean matchesSafely(AppCompatActivity activity) {
                return activity.getSupportActionBar().getSubtitle().toString().equals(subtitle);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(" subtitle = ").appendValue(subtitle);
            }

            @Override
            public void describeMismatchSafely(AppCompatActivity activity, Description mismatchDescription) {
                mismatchDescription.appendText(" subtitle = ").appendValue(activity.getSupportActionBar().getSubtitle());
            }
        };
    }

    public static Matcher<Activity> hasTitle(final String title) {
        return new TypeSafeMatcher<Activity>() {
            @Override
            public boolean matchesSafely(Activity activity) {
                return activity.getTitle().toString().equals(title);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(" title = ").appendValue(title);
            }

            @Override
            public void describeMismatchSafely(Activity activity, Description mismatchDescription) {
                mismatchDescription.appendText(" title = ").appendValue(activity.getTitle());
            }
        };
    }

    public static Matcher<Activity> isFinishing() {
        return new TypeSafeMatcher<Activity>() {
            @Override
            protected boolean matchesSafely(Activity activity) {
                return shadowOf(activity).isFinishing();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(" activity is finishing");
            }

            @Override
            protected void describeMismatchSafely(Activity activity, Description mismatchDescription) {
                mismatchDescription.appendText(" isFinishing() = ").appendValue(shadowOf(activity).isFinishing());
            }
        };
    }

    public static Matcher<Activity> hasResult(final int resultCode, final Intent data) {
        return new TypeSafeMatcher<Activity>() {
            @Override
            protected boolean matchesSafely(Activity activity) {
                ShadowActivity shadowActivity = shadowOf(activity);
                return shadowActivity.getResultCode() == resultCode && ActivityMatchers.equals(shadowActivity.getResultIntent(), data);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("activity should have result code ").appendValue(resultCode)
                        .appendText(" and activity should have result intent ").appendValue(data);
            }

            @Override
            protected void describeMismatchSafely(Activity activity, Description mismatchDescription) {
                ShadowActivity shadowActivity = shadowOf(activity);
                mismatchDescription.appendText("activity has result code ").appendValue(shadowActivity.getResultCode())
                        .appendText(" and activity has result intent ").appendValue(shadowActivity.getResultIntent());
            }
        };
    }
}
