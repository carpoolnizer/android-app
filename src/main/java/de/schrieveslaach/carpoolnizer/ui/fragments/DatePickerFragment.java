/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.joda.time.DateTime;

import java.util.Calendar;
import java.util.Locale;

import androidx.fragment.app.DialogFragment;

/**
 * {@link DialogFragment} to pick dates.
 */
public class DatePickerFragment extends DialogFragment {

    private DateTime date = DateTime.now().withTimeAtStartOfDay();

    private DateTime minimumDate;

    private OnPickDateListener onPickDateListener;


    /**
     * Sets the initial date displayed in the {@code DatePickerFragment}.
     *
     * @param date
     */
    public void setDate(DateTime date) {
        this.date = date;
    }

    /**
     * Sets the minimum date which is selectable.
     *
     * @param date
     */
    public void setMinimumDate(DateTime date) {
        this.minimumDate = date;
    }

    /**
     * Sets the {@link OnPickDateListener listener} which will be invoked when the user picked a
     * {@link DateTime date}.
     *
     * @param onPickDateListener
     */
    public void setOnPickDateListener(OnPickDateListener onPickDateListener) {
        this.onPickDateListener = onPickDateListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Locale locale = getResources().getConfiguration().locale;

        Calendar now = date.toCalendar(locale);
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerFragmentCallback(),
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );

        if (minimumDate != null) {
            dpd.setMinDate(minimumDate.toCalendar(locale));
        }

        dpd.show(getFragmentManager(), "Datepickerdialog");

        return dpd.getDialog();
    }

    /**
     * Listener which will be called when the user picked a {@link DateTime date}.
     */
    public interface OnPickDateListener {

        /**
         * Will be raised when the user picked a {@link DateTime date}.
         *
         * @param date
         */
        void onFinish(DateTime date);

    }

    private class DatePickerFragmentCallback implements DatePickerDialog.OnDateSetListener {

        @Override
        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
            DateTime selectedDate = new DateTime(year, monthOfYear + 1, dayOfMonth, 0, 0);
            onPickDateListener.onFinish(selectedDate);
        }
    }


}
