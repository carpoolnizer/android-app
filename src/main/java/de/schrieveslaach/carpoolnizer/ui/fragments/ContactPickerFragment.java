/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.fragments;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.model.Member;
import de.schrieveslaach.carpoolnizer.ui.views.MemberReadOnlyView;

/**
 * {@link DialogFragment} to pick contacts.
 */
@TargetApi(18)
public class ContactPickerFragment extends DialogFragment {

    private OnPickContactListener contactListener;

    /**
     * Set the {@link OnPickContactListener listener} for picking contacts.
     *
     * @param contactListener
     */
    public void setOnPickContactListener(OnPickContactListener contactListener) {
        this.contactListener = contactListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Cursor cursor = getActivity().getContentResolver().query(
                ContactsContract.Contacts.CONTENT_URI,
                new String[]{
                        ContactsContract.Contacts._ID,
                        ContactsContract.Contacts.DISPLAY_NAME
                },
                null,
                null,
                ContactsContract.Contacts.DISPLAY_NAME
        );

        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.pick_contact)
                .setAdapter(new ContactsAdapter(cursor), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .create();
    }

    /**
     * {@link CursorAdapter Adapter} to load contacts and display them in a {@link ListView}.
     */
    private class ContactsAdapter extends CursorAdapter {

        private ContactsAdapter(Cursor cursor) {
            super(getActivity(), cursor, false);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            MemberReadOnlyView mrov = new MemberReadOnlyView(getActivity());
            mrov.setOnClickListener(v -> {
                Member member = mrov.getMember();
                contactListener.onFinish(Uri.parse(member.getUri().toString()));
                dismiss();
            });
            return mrov;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            MemberReadOnlyView mrov = (MemberReadOnlyView) view;
            mrov.display(cursor);
        }
    }

    /**
     * Listener for picking contacts
     */
    public interface OnPickContactListener {

        /**
         * Will be raised when the user pick a contact.
         *
         * @param uri
         */
        void onFinish(Uri uri);

    }
}
