/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.fragments;

import android.os.Build;
import android.view.View;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowDialog;

import java.lang.reflect.Field;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.model.Usage;
import de.schrieveslaach.carpoolnizer.ui.robolectric.shadows.ShadowRecyclerView;
import lombok.SneakyThrows;

import static de.schrieveslaach.carpoolnizer.ui.fragments.ExceptionalUsagePickerFragment.OnPickUsageListener;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.robolectric.shadows.ShadowLooper.runUiThreadTasksIncludingDelayedTasks;

@Config(
        sdk = Build.VERSION_CODES.LOLLIPOP,
        shadows = ShadowRecyclerView.class
)
public class ExceptionalUsagePickerFragmentTest extends BaseDialogFragmentTest<ExceptionalUsagePickerFragment, AlertDialog> {

    private ExceptionalUsagePickerFragment fragment;

    public ExceptionalUsagePickerFragmentTest() {
        super(ExceptionalUsagePickerFragment.class);
    }

    @Before
    public void createFragment() throws Exception {
        fragment = openFragment();
    }

    private void clickOnDialogItem(Usage usage) {
        RecyclerView rv = getRecyclerView();
        View itemView = rv.findViewHolderForAdapterPosition(usage.ordinal()).itemView;
        itemView.findViewById(R.id.tv_usage).performClick();
    }

    @Test
    public void shouldSelectUsage_Normal() {
        OnPickUsageListener usageListener = mock(OnPickUsageListener.class);
        fragment.setOnPickUsageListener(usageListener);

        clickOnDialogItem(Usage.NORMAL);
        verify(usageListener).onFinish(Usage.NORMAL);
    }

    @Test
    public void shouldSelectUsage_CanNotDrive() {
        OnPickUsageListener usageListener = mock(OnPickUsageListener.class);
        fragment.setOnPickUsageListener(usageListener);

        clickOnDialogItem(Usage.CAN_NOT_DRIVE);
        verify(usageListener).onFinish(Usage.CAN_NOT_DRIVE);
    }

    @Test
    public void shouldSelectUsage_DoesNotUse() {
        OnPickUsageListener usageListener = mock(OnPickUsageListener.class);
        fragment.setOnPickUsageListener(usageListener);

        clickOnDialogItem(Usage.DOES_NOT_USE);
        verify(usageListener).onFinish(Usage.DOES_NOT_USE);
    }

    @Test
    public void shouldSelectUsage_HasToDrive() {
        OnPickUsageListener usageListener = mock(OnPickUsageListener.class);
        fragment.setOnPickUsageListener(usageListener);

        clickOnDialogItem(Usage.HAS_TO_DRIVE);
        verify(usageListener).onFinish(Usage.HAS_TO_DRIVE);
    }

    @Test
    public void shouldDisableUsage_HasToDrive() {
        fragment.disable(Usage.HAS_TO_DRIVE);

        assertThat(getViewHolder(Usage.NORMAL).isEnabled(), is(true));
        assertThat(getViewHolder(Usage.DOES_NOT_USE).isEnabled(), is(true));
        assertThat(getViewHolder(Usage.CAN_NOT_DRIVE).isEnabled(), is(true));
        assertThat(getViewHolder(Usage.HAS_TO_DRIVE).isEnabled(), is(false));
    }

    private ExceptionalUsagePickerFragment.ExceptionalUsageViewHolder getViewHolder(Usage usage) {
        RecyclerView rv = getRecyclerView();
        return (ExceptionalUsagePickerFragment.ExceptionalUsageViewHolder) rv.findViewHolderForAdapterPosition(usage.ordinal());
    }

    @SneakyThrows
    private RecyclerView getRecyclerView() {
        AlertDialog dlg = openDialog(fragment);
        Field mAlertField = dlg.getClass().getDeclaredField("mAlert");
        mAlertField.setAccessible(true);
        Object alertController = mAlertField.get(dlg);
        Field mViewField = alertController.getClass().getDeclaredField("mView");
        mViewField.setAccessible(true);
        return (RecyclerView) mViewField.get(alertController);
    }

    @Test
    public void shouldDisableUsage_DoNotFireEvent() {
        OnPickUsageListener usageListener = mock(OnPickUsageListener.class);
        fragment.setOnPickUsageListener(usageListener);

        fragment.disable(Usage.HAS_TO_DRIVE);
        clickOnDialogItem(Usage.HAS_TO_DRIVE);

        verify(usageListener, never()).onFinish(Usage.HAS_TO_DRIVE);
    }

    @Test
    @Ignore
    public void shouldShowExplanationSequenceOfExceptionalUsage_FirstStart() throws Exception {
        openDialog(openFragment()).show();

        // TODO BottomSheetDialog dialog = (BottomSheetDialog) ShadowDialog.getLatestDialog();

        // TODO TextView tvExplanation = (TextView) dialog.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_content);
        // TODO assertThat(tvExplanation, hasText(R.string.explanation_normal_usage));

        // TODO dialog.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_dismiss).performClick();

        // TODO tvExplanation = (TextView) dialog.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_content);
        // TODO assertThat(tvExplanation, hasText(R.string.explanation_has_to_drive));

        // TODO dialog.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_dismiss).performClick();

        // TODO tvExplanation = (TextView) dialog.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_content);
        // TODO assertThat(tvExplanation, hasText(R.string.explanation_does_not_use));

        // TODO dialog.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_dismiss).performClick();

        // TODO tvExplanation = (TextView) dialog.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_content);
        // TODO assertThat(tvExplanation, hasText(R.string.explanation_can_not_drive));
    }

    @Test
    @Ignore
    public void shouldNotShowExplanationSequenceOfExceptionalUsage_SecondStart() throws Exception {
        openDialog(openFragment()).show();
        runUiThreadTasksIncludingDelayedTasks();

        // TODO BottomSheetDialog dialog = (BottomSheetDialog) ShadowDialog.getLatestDialog();
        // TODO dialog.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_dismiss).performClick();
        // TODO dialog.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_dismiss).performClick();
        // TODO dialog.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_dismiss).performClick();
        // TODO dialog.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_dismiss).performClick();

        ShadowDialog.reset();
        openDialog(openFragment()).show();

        // TODO Dialog latestDialog = ShadowDialog.getLatestDialog();
        // TODO assertThat(latestDialog, is(not(instanceOf(BottomSheetDialog.class))));
    }

}
