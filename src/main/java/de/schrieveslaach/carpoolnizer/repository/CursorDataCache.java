/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.repository;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.util.HashMap;
import java.util.Map;

/**
 * Caches data which is loaded by a {@link android.database.Cursor}
 *
 * @param <V> The type of values which will be cached.
 */
public class CursorDataCache<V> {

    private final Map<Uri, V> data = new HashMap<>();

    private final Context context;

    private final DataResolver<V> dataResolver;

    /**
     * Creates a new {@code CursorDataCache}
     *
     * @param context
     * @param dataResolver
     */
    public CursorDataCache(Context context, DataResolver<V> dataResolver) {
        if (context == null) {
            throw new IllegalArgumentException("context must not be null");
        }

        if (dataResolver == null) {
            throw new IllegalArgumentException("cursorToData must not be null");
        }

        this.context = context;
        this.dataResolver = dataResolver;
    }

    /**
     * Caches the given {@code value}.
     *
     * @param uri
     * @param value
     */
    private void cacheData(Uri uri, V value) {
        synchronized (data) {
            data.put(uri, value);
        }
    }

    /**
     * Caches the data from the given {@link android.database.Cursor}.
     *
     * @param uri
     * @param cursor
     * @param projection
     * @return
     */
    public V cacheData(Uri uri, Cursor cursor, String projection) {
        synchronized (data) {
            if (data.containsKey(uri)) {
                return data.get(uri);
            }
        }

        V value;
        int columnIndex = cursor.getColumnIndex(projection);
        value = dataResolver.loadFrom(cursor, columnIndex);

        if (value == null) {
            value = dataResolver.getAlternativeData(context);
        }

        cacheData(uri, value);
        return value;
    }

    /**
     * Looks up if an value is stored for the given {@link Uri}. If not, the cache object
     * will resolve the data from {@link android.content.Context}.
     *
     * @param uri
     * @param projection
     * @return
     */
    public V getData(Uri uri, String projection) {
        synchronized (data) {
            if (data.containsKey(uri)) {
                return data.get(uri);
            }
        }

        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(
                    uri,
                    new String[]{projection},
                    null,
                    null,
                    null
            );

            V value;
            if (cursor != null && cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndex(projection);
                value = dataResolver.loadFrom(cursor, columnIndex);
            } else {
                value = dataResolver.getAlternativeData(context);
            }

            cacheData(uri, value);
            return value;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    /**
     * Resolves data from a {@link android.database.Cursor} for storing the data in the cache
     */
    public interface DataResolver<V> {

        /**
         * Loads the data from the {@link android.database.Cursor}
         *
         * @param cursor
         * @param columnIndex
         * @return
         */
        V loadFrom(Cursor cursor, int columnIndex);

        /**
         * Returns an alternative value if no value is available for caching.
         *
         * @param context
         * @return
         */
        V getAlternativeData(Context context);

    }
}
