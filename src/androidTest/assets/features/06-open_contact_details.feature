Feature: Open contact details
  This feature allows to open the contact information right from the app. The
  user can quickly access the contact for contacting someone of the carpool.
  
  Scenario Outline: Open contact information from main activity.
    Given I started the app
    And I opened the carpool <carpool>
    When I clicked on the contact image of the first turn with contact <driver> as driver
    Then I should see the contact information of <driver>

  Examples:
    | carpool       | driver |
    | Work Mike     | Mike   |

  Scenario Outline: Open contact information from turn details activity.
    Given I started the app
    And I opened the carpool <carpool>
    And I clicked on first turn with contact <driver> as driver
    When I clicked on the contact image of the contact <member>
    Then I should see the contact information of <member>

  Examples:
    | carpool             | driver | member |
    | A long term carpool | Mike   | Mike   |
    | A long term carpool | Mike   | Daisy  |
    | A long term carpool | Mike   | John   |
