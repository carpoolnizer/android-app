/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.io.IOException;

/**
 * Defines the exceptional {@link Usage} for a certain {@link Interval interval}.
 */
class ExceptionalUsage implements Comparable<ExceptionalUsage> {

    @JsonDeserialize(using = IntervalDeserializer.class)
    @JsonSerialize(using = IntervalSerializer.class)
    @JsonProperty("interval")
    private final Interval interval;

    @JsonProperty("usage")
    private Usage usage;

    /**
     * Creates a new instance of {@code ExceptionalUsage}.
     *
     * @param interval
     * @param usage
     */
    @JsonCreator
    public ExceptionalUsage(@JsonProperty("interval") Interval interval, @JsonProperty("usage") Usage usage) {
        if (interval == null || usage == null) {
            throw new IllegalArgumentException("arguments must not be null");
        }

        if (!isNormalized(interval)) {
            throw new IllegalArgumentException("The interval must not have millis of the day");
        }

        this.interval = interval;
        this.usage = usage;
    }

    private static boolean isNormalized(Interval interval) {
        return interval.getStart().getMillisOfDay() == 0 && interval.getEnd().getMillisOfDay() == 0;
    }

    /**
     * Returns the {@link Interval} of the {@code ExceptionalUsage}.
     *
     * @return
     */
    @JsonIgnore
    Interval getInterval() {
        return interval;
    }

    /**
     * Returns the {@link Usage} of the {@code ExceptionalUsage}.
     *
     * @return
     */
    @JsonIgnore
    public Usage getUsage() {
        return usage;
    }

    /**
     * Sets the {@link Usage} of the {@code ExceptionalUsage}.
     *
     * @param usage
     */
    void setUsage(Usage usage) {
        this.usage = usage;
    }

    /**
     * Checks if the given {@link DateTime date} matches the {@code ExceptionalUsage}.
     *
     * @param date
     * @return
     */
    public boolean matches(DateTime date) {
        return interval.contains(date);
    }

    /**
     * Checks if the given {@link Interval interval} matches the {@code ExceptionalUsage}.
     *
     * @param interval
     * @return
     */
    public boolean matches(Interval interval) {
        return this.interval.equals(interval);
    }

    /**
     * Checks if the given {@link Interval interval} overlaps the {@code ExceptionalUsage}.
     *
     * @param interval
     * @return
     */
    public boolean overlaps(Interval interval) {
        return this.interval.overlaps(interval);
    }

    @Override
    public int compareTo(ExceptionalUsage another) {
        int cmp = interval.getStart().compareTo(another.interval.getStart());
        if (cmp == 0) {
            cmp = interval.getEnd().compareTo(another.interval.getEnd());
        }
        return cmp;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(interval)
                .append(usage)
                .build();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ExceptionalUsage)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        ExceptionalUsage other = (ExceptionalUsage) obj;
        return new EqualsBuilder()
                .append(interval, other.interval)
                .append(usage, other.usage)
                .isEquals();
    }

    /**
     * Deserializes {@link ExceptionalUsage#interval}
     */
    private static class IntervalDeserializer extends JsonDeserializer<Interval> {

        @Override
        public Interval deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
            String[] intervalValues = p.getText().split(" ");
            return new Interval(
                    Constants.DATE_TIME_FORMATTER.parseDateTime(intervalValues[0]),
                    Constants.DATE_TIME_FORMATTER.parseDateTime(intervalValues[1])
            );
        }
    }

    /**
     * Serializes {@link ExceptionalUsage#interval}
     */
    private static class IntervalSerializer extends JsonSerializer<Interval> {

        @Override
        public void serialize(Interval value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            StringBuilder str = new StringBuilder(21);
            str.append(Constants.DATE_TIME_FORMATTER.print(value.getStart()))
                    .append(" ")
                    .append(Constants.DATE_TIME_FORMATTER.print(value.getEnd()));
            gen.writeString(str.toString());
        }
    }
}
