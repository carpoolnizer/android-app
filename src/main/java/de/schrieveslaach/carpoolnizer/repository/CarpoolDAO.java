/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.repository;

import android.content.Context;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.schrieveslaach.carpoolnizer.model.Carpool;
import de.schrieveslaach.carpoolnizer.model.CarpoolArchive;
import de.schrieveslaach.carpoolnizer.model.Constants;
import de.schrieveslaach.carpoolnizer.model.settings.UsageNotification;

import static de.schrieveslaach.carpoolnizer.model.Constants.DATE_TIME_FORMATTER;

/**
 * Data access object for loading {@link de.schrieveslaach.carpoolnizer.model.Carpool carpools} from
 * filesystem.
 */
public class CarpoolDAO {

    private static final String REPOSITORY_SETTINGS_FILENAME = "Repository.settings.json";

    private static final String CARPOOL_FILENAME_SUFFIX = ".carpool.json";

    private static final String USAGE_NOTIFICATION_FILENAME_SUFFIX = ".usage.notification.json";

    private static final Pattern CARPOOL_FILENAME_PATTERN = Pattern.compile("^(.*)" + CARPOOL_FILENAME_SUFFIX.replace(".", "\\.") + "$");

    private static final Pattern CARPOOL_ARCHIVE_FILENAME_PATTERN = Pattern.compile(
            "(\\w[\\w ]*)\\.carpool\\.archive\\.(\\d{4}-\\d{2}-\\d{2})-(\\d{4}-\\d{2}-\\d{2})\\.json$"
    );

    /**
     * Context for saving and loading {@link de.schrieveslaach.carpoolnizer.model.Carpool carpools}.
     */
    private final Context context;

    private final ObjectMapper mapper;

    private RepositorySettings settings;

    /**
     * Creates a new instance of {@code CarpoolDAO}.
     *
     * @param context
     */
    public CarpoolDAO(Context context) {
        this.context = context;
        this.mapper = new ObjectMapper();
        this.mapper.registerModule(new GuavaModule());
        this.mapper.registerModule(new JodaModule());
    }

    /**
     * Saves the given {@link Carpool} into the internal storage.
     *
     * @param carpool
     * @throws CarpoolDAOException
     */
    public void save(Carpool carpool) throws CarpoolDAOException {
        File file = getCarpoolFile(carpool);

        archive(carpool);

        try {
            mapper.writeValue(file, carpool);
        } catch (IOException ex) {
            throw new CarpoolDAOException("Could not save carpool.", ex);
        }
    }

    /**
     * Archives and stores all data which is older than {@link CarpoolDAO#getArchiveDate() the archive date}.
     *
     * @param carpool
     * @throws CarpoolDAOException
     */
    private void archive(Carpool carpool) throws CarpoolDAOException {
        DateTime archiveDate = getArchiveDate();
        DateTime date = archiveDate.plusMonths(getRepositorySettings().getArchiveCarpoolDataAfterMonths());

        // The second part of the if statement ensures that turns will be archived when the user
        // creates a long term carpool at the first day of a month
        if (!carpool.hasTurnsBefore(archiveDate) || (!carpool.hasTurnsUntil(date) && !carpool.hasTurnsUntil(date.minusDays(1)))) {
            return;
        }

        final DateTime startDate = carpool.getFirstUsedDate().plusMonths(1).withDayOfMonth(1);
        for (date = startDate; date.isBefore(archiveDate) || date.isEqual(archiveDate); date = date.plusMonths(1)) {

            CarpoolArchive archive = carpool.archiveDataOlderThan(date);
            File archiveFile = getArchiveFileName(carpool, archive);
            try {
                mapper.writeValue(archiveFile, archive);
            } catch (IOException ex) {
                throw new CarpoolDAOException("Could not save archived data", ex);
            }
        }
    }

    /**
     * Checks if the {@code CarpoolDAO} has a {@link CarpoolArchive} for the given {@link Carpool}.
     *
     * @param carpool
     * @return
     */
    public boolean hasArchiveFor(Carpool carpool) {
        return getArchiveFile(carpool) != null;
    }

    /**
     * Loads the {@link CarpoolArchive} for the given {@link Carpool}.
     *
     * @param carpool
     * @return {@link CarpoolArchive#getInterval() interval} of the restored {@link de.schrieveslaach.carpoolnizer.model.Turn turns}.
     * @throws CarpoolDAOException
     */
    public Interval loadArchive(Carpool carpool) throws CarpoolDAOException {
        File archiveFile = getArchiveFile(carpool);
        try {
            CarpoolArchive archive = mapper.readValue(archiveFile, CarpoolArchive.class);
            carpool.restore(archive);
            return archive.getInterval();
        } catch (IOException e) {
            throw new CarpoolDAOException("Could not load carpool archive.", e);
        }
    }

    /**
     * Returns the {@link File} of the previous and adjacent {@link CarpoolArchive}.
     *
     * @param carpool
     * @return
     */
    private File getArchiveFile(Carpool carpool) {
        Map<Interval, File> files = getArchiveFilesFor(carpool);
        for (Interval interval : files.keySet()) {
            if (carpool.getFirstUsedDate().isEqual(interval.getEnd())) {
                return files.get(interval);
            }
        }
        return null;
    }

    /**
     * Lists all archive files for the given {@link Carpool}.
     *
     * @param carpool
     * @return
     */
    Map<Interval, File> getArchiveFilesFor(Carpool carpool) {
        Map<Interval, File> files = new HashMap<>();

        if (carpool != null) {
            for (String file : context.fileList()) {
                Matcher matcher = CARPOOL_ARCHIVE_FILENAME_PATTERN.matcher(file);
                if (!matcher.find()) {
                    continue;
                }

                String carpoolName = matcher.group(1);
                if (!carpool.getName().equals(carpoolName)) {
                    continue;
                }

                DateTime startDate = Constants.DATE_TIME_FORMATTER.parseDateTime(matcher.group(2));
                DateTime endDate = Constants.DATE_TIME_FORMATTER.parseDateTime(matcher.group(3));

                files.put(
                        new Interval(startDate, endDate),
                        new File(context.getFilesDir(), matcher.group(0))
                );
            }
        }

        return files;
    }

    /**
     * Determines the date (always beginning of a month) which determines the date for archiving
     * ({@link Carpool#archiveDataOlderThan(org.joda.time.DateTime)} see archive method). The
     * date can be configured by the {@link RepositorySettings#getArchiveCarpoolDataAfterMonths() repository settings}.
     *
     * @return
     * @throws CarpoolDAOException
     */
    private DateTime getArchiveDate() throws CarpoolDAOException {
        DateTime firstDayOfMonth = DateTime.now().withTimeAtStartOfDay()
                .withDayOfMonth(1);
        return firstDayOfMonth.minusMonths(getRepositorySettings().getArchiveCarpoolDataAfterMonths());
    }

    private File getArchiveFileName(Carpool carpool, CarpoolArchive archive) {
        Interval interval = archive.getInterval();
        String filename = String.format("%s.carpool.archive.%s-%s.json",
                carpool.getName(),
                DATE_TIME_FORMATTER.print(interval.getStart()),
                DATE_TIME_FORMATTER.print(interval.getEnd()));

        return new File(context.getFilesDir(), filename);
    }


    /**
     * Lists all available carpools.
     *
     * @return
     */
    public List<String> listCarpoolNames() {
        String[] files = context.fileList();

        List<String> names = new ArrayList<>();
        for (String filename : files) {
            File file = new File(filename);

            Matcher matcher = CARPOOL_FILENAME_PATTERN.matcher(file.getName());
            if (matcher.matches()) {
                names.add(matcher.group(1));
            }
        }

        Collections.sort(names);

        return names;
    }

    /**
     * Loads the carpool with the given name.
     *
     * @param name
     * @return
     */
    public Carpool loadCarpool(String name) throws CarpoolDAOException {
        if (name == null) {
            return null;
        }

        File carpoolFile = getCarpoolFile(name);
        if (!carpoolFile.isFile()) {
            return null;
        }

        try {
            Carpool carpool = mapper.readValue(carpoolFile, Carpool.class);
            getRepositorySettings().setLastUsedCarpool(carpool.getName());
            return carpool;
        } catch (IOException ex) {
            throw new CarpoolDAOException("Could not load carpool", ex);
        }
    }

    File getUsageNotificationFile(Carpool carpool) {
        return context.getFileStreamPath(carpool.getName() + USAGE_NOTIFICATION_FILENAME_SUFFIX);
    }

    File getCarpoolFile(Carpool carpool) {
        return getCarpoolFile(carpool.getName());
    }

    private File getCarpoolFile(String name) {
        return context.getFileStreamPath(name + CARPOOL_FILENAME_SUFFIX);
    }

    /**
     * Loads the {@link Carpool carpool} which has been {@link CarpoolDAO#loadCarpool(String) loaded}
     * the last time,
     *
     * @return
     */
    public Carpool loadLastUsedCarpool() throws CarpoolDAOException {
        return loadCarpool(getRepositorySettings().getLastUsedCarpool());
    }

    /**
     * Loads the carpool name of the {@link Carpool} loaded the last time.
     *
     * @return
     * @throws CarpoolDAOException
     */
    String getLastUsedCarpoolName() throws CarpoolDAOException {
        return getRepositorySettings().getLastUsedCarpool();
    }

    private RepositorySettings getRepositorySettings() throws CarpoolDAOException {
        if (settings == null) {
            File settingsFile = new File(context.getFilesDir(), REPOSITORY_SETTINGS_FILENAME);
            if (!settingsFile.isFile()) {
                settings = new RepositorySettings();
            } else {
                try {
                    settings = mapper.readValue(settingsFile, RepositorySettings.class);
                } catch (IOException e) {
                    throw new CarpoolDAOException("Could not load repository settings", e);
                }
            }

            settings.addPropertyChangeListener(new SettingsPropertiesChangeListener());
        }
        return settings;
    }

    /**
     * Deletes all data of the given {@link Carpool}.
     *
     * @param carpool
     */
    public void delete(Carpool carpool) {
        Map<Interval, File> archiveFilesFor = getArchiveFilesFor(carpool);
        for (File file : archiveFilesFor.values()) {
            file.delete();
        }
        File file = getCarpoolFile(carpool);
        file.delete();
    }

    /**
     * Stores the given {@link UsageNotification notification option} for the given {@link Carpool}
     *
     * @param carpool
     * @param notification
     */
    public void saveUsageNotifcation(Carpool carpool, UsageNotification notification) throws CarpoolDAOException {
        File usageNotificationFile = getUsageNotificationFile(carpool);

        try {
            mapper.writeValue(usageNotificationFile, notification);
        } catch (IOException ex) {
            throw new CarpoolDAOException("Could not save usage notification settings", ex);
        }
    }

    /**
     * Loads the {@link UsageNotification} for the given {@link Carpool}
     *
     * @param carpool
     * @return
     */
    public UsageNotification loadUsageNotification(Carpool carpool) throws CarpoolDAOException {
        File usageNotificationFile = getUsageNotificationFile(carpool);
        if (!usageNotificationFile.isFile()) {
            return null;
        }


        try {
            return mapper.readValue(usageNotificationFile, UsageNotification.class);
        } catch (IOException ex) {
            throw new CarpoolDAOException("Could not load usage notification settings", ex);
        }
    }

    private class SettingsPropertiesChangeListener implements PropertyChangeListener {

        @Override
        public void propertyChange(PropertyChangeEvent event) {
            File settingsFile = new File(context.getFilesDir(), REPOSITORY_SETTINGS_FILENAME);
            try {
                mapper.writeValue(settingsFile, settings);
            } catch (IOException e) {
                Log.e(CarpoolDAO.class.getSimpleName(), "Could not write repository settings", e);
            }
        }
    }
}
