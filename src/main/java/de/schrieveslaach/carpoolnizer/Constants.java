/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer;

import android.content.Context;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Define constants for UI components.
 */
public final class Constants {

    /**
     * Request id for creating or editing a {@link de.schrieveslaach.carpoolnizer.model.Carpool}.
     *
     * @see {@link android.app.Activity#startActivityForResult(android.content.Intent, int)}.
     */
    public static final int CONFIGURE_CARPOOL_REQUEST = 1;

    /**
     * Request id for showing details of a {@link de.schrieveslaach.carpoolnizer.model.Turn}.
     *
     * @see {@link android.app.Activity#startActivityForResult(android.content.Intent, int)}.
     */
    public static final int DISPLAY_TURN_DETAILS = 2;

    /**
     * {@link android.content.Intent} key for {@link de.schrieveslaach.carpoolnizer.model.Carpool#getName() carpool name}.
     */
    public static final String CARPOOL_NAME = "carpool.name";

    /**
     * {@link android.content.Intent} key for {@link de.schrieveslaach.carpoolnizer.model.Turn#getDate() turn date}.
     */
    public static final String TURN_DATE = "turn.date";

    /**
     * {@link android.content.Intent} key for {@link Alarms}.
     */
    public static final String CARPOOL_USAGE_NOTIFICATION = "carpool.usage.notification";

    private Constants() {
    }

    /**
     * Returns a {@link DateTimeFormatter} to print full date.
     *
     * @param context
     * @return
     */
    public static DateTimeFormatter getFullDateFormatter(Context context) {
        return DateTimeFormat.forStyle("F-").withLocale(context.getResources().getConfiguration().locale);
    }

    /**
     * Returns a {@link DateTimeFormatter} to print short date.
     *
     * @param context
     * @return
     */
    public static DateTimeFormatter getShortDateFormatter(Context context) {
        return DateTimeFormat.shortDate().withLocale(context.getResources().getConfiguration().locale);
    }

    /**
     * Returns a {@link DateTimeFormatter} to print short time
     *
     * @param context
     * @return
     */
    public static DateTimeFormatter getShortTimeDateFormatter(Context context) {
        return DateTimeFormat.shortTime().withLocale(context.getResources().getConfiguration().locale);
    }

    /**
     * Returns the {@link DateTime date} which is used to predict the {@link de.schrieveslaach.carpoolnizer.model.Turn turns}
     * until this {@link DateTime date}.
     *
     * @return
     */
    public static DateTime getPredictionDate() {
        return DateTime.now().withTimeAtStartOfDay().plusDays(6);
    }

    /**
     * Contains constant values for defining the notification shown by
     * {@link uk.co.deanwild.materialshowcaseview.MaterialShowcaseView}
     */
    public enum Notifications {

        /**
         * Notification ID for creating a carpool.
         */
        CREATE_CARPOOL("create-carpool"),

        /**
         * Notification ID for selecting phone user days
         */
        SELECT_DAYS_OF_PHONE_USER("select-phone-user-usage-days"),

        /**
         * Notification ID for adding contacts
         */
        ADD_CONTACTS("add-contacts"),

        /**
         * Notification ID for selecting contact days
         */
        SELECT_DAYS_OF_CONTACT("select-contact-usage-days"),

        /**
         * Notification ID for explaining turn overview
         */
        EXPLAIN_TURN_OVERVIEW("explain-turn-overview"),

        /**
         * Notification ID for explaining turn details
         */
        EXPLAIN_TURN_DETAILS("explain-turn-details"),

        /**
         * Notification ID for explaining exceptional usage
         */
        EXPLAIN_EXCEPTIONAL_USAGE("exceptional-usage"),

        /**
         * Notification ID for explaining to switch among carpools.
         */
        EXPLAIN_SWITCH_AMONG_CARPOOLS("switch-among-carpools");

        private final String id;

        Notifications(String code) {
            this.id = "notification-" + code;
        }

        /**
         * Required for {@link uk.co.deanwild.materialshowcaseview.MaterialShowcaseView.Builder#singleUse(String)}.
         *
         * @return
         */
        public String getId() {
            return id;
        }
    }
}
