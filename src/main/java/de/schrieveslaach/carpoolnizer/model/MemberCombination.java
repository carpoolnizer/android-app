/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.SimpleObjectIdResolver;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.core.io.IOContext;
import com.fasterxml.jackson.core.json.ReaderBasedJsonParser;
import com.fasterxml.jackson.core.sym.CharsToNameCanonicalizer;
import com.fasterxml.jackson.core.util.BufferRecycler;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.type.SimpleType;
import com.google.common.collect.Lists;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.joda.time.DateTime;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/**
 * Contains information how often members were driver of a {@link Carpool}.
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@comb-id")
public class MemberCombination implements Iterable<Member>, Serializable {

    /**
     * The {@link Member members} of the {@link Carpool}. The {@link Member members} are the keys
     * and the values count how often the {@link Member} drove.
     */
    @JsonDeserialize(using = MemberDriverCountDeserializer.class)
    @JsonSerialize(using = MemberDriverCountSerializer.class)
    @JsonProperty("driverCounts")
    private final Map<Member, Integer> members;

    /**
     * The order in which the {@link Member members} have used the carpool.
     */
    @JsonProperty("history")
    private final Queue<Member> history = new LinkedList<>();

    /**
     * Creates a new instance of {@code MemberCombination}.
     *
     * @param members
     */
    public MemberCombination(List<Member> members) {
        if (members.size() < 2) {
            throw new IllegalArgumentException("Need two members at least!");
        }

        this.members = new LinkedHashMap<>(members.size());
        for (Member m : members) {
            this.members.put(m, 0);
        }
    }

    private MemberCombination() {
        members = new HashMap<>();
    }

    /**
     * Creates a new instance of {@code MemberCombination}.
     *
     * @param m
     * @param members
     */
    public MemberCombination(Member m, Iterable<Member> members) {
        this(createMemberList(m, members));
    }

    private static List<Member> createMemberList(Member firstMember, Iterable<Member> members) {
        List<Member> memberList = new ArrayList<>();
        memberList.add(firstMember);
        for (Member m : members) {
            memberList.add(m);
        }
        return memberList;
    }

    /**
     * Returns how often the given {@link Member member} was driver in this combination.
     *
     * @param member
     * @return
     */
    public int getDriverCount(Member member) {
        if (!members.containsKey(member)) {
            throw new IllegalArgumentException("This combination does not contain the given member.");
        }

        return members.get(member);
    }

    /**
     * Increments the driver count for the given {@link Member member}.
     *
     * @param member
     */
    void incrementDriverCount(Member member) {
        if (!members.containsKey(member)) {
            throw new IllegalArgumentException("This combination does not contain the given member.");
        }

        members.put(member, getDriverCount(member) + 1);
    }


    /**
     * Returns the number of {@link Member members} in the combination.
     *
     * @return
     */
    public int size() {
        return members.size();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(members)
                .hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }

        MemberCombination rhs = (MemberCombination) obj;
        return new EqualsBuilder()
                .append(members.keySet(), rhs.members.keySet())
                .isEquals();
    }

    /**
     * Checks if the {@link Member members} of the {@code MemberCombination} are equal to the given
     * {@link Member members}.
     *
     * @param members
     * @return
     */
    public boolean containsAll(List<Member> members) {
        if (size() != members.size()) {
            return false;
        }

        for (Member m : members) {
            if (!this.members.containsKey(m)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks if the {@link Member members} of the {@code MemberCombination} are equal to the given
     * {@link Member members}.
     *
     * @param members
     * @return
     */
    public boolean containsAll(Member... members) {
        return containsAll(Lists.newArrayList(members));
    }

    /**
     * Calculates the {@link Turn turns} for the {@link DateTime dates}.
     *
     * @param dates
     * @return
     */
    public List<Turn> calculateTurns(Collection<DateTime> dates) {
        List<Turn> turns = new ArrayList<>();

        for (DateTime date : dates) {
            turns.add(predictTurn(date.withTimeAtStartOfDay(), members, history));
        }

        return turns;
    }

    /**
     * Predicts the {@link Turn turns} for the {@link DateTime dates}.
     *
     * @param dates
     * @return
     */
    public List<Turn> predictTurns(Collection<DateTime> dates) {
        Map<Member, Integer> temporaryDriverCounts = new LinkedHashMap<>(this.members);
        Queue<Member> temporaryHistory = new LinkedList<>(this.history);

        List<Turn> turns = new ArrayList<>();

        for (DateTime date : dates) {
            turns.add(predictTurn(date.withTimeAtStartOfDay(), temporaryDriverCounts, temporaryHistory));
        }

        return turns;
    }

    private Turn predictTurn(DateTime date, Map<Member, Integer> members, Queue<Member> history) {
        int minimumCount = Integer.MAX_VALUE;

        if (history.size() >= members.size()) {
            history.poll();
        }

        Member driver = null;

        for (Member m : members.keySet()) {
            int count = members.get(m);

            switch (m.getUsageAt(date)) {
                case DOES_NOT_USE:
                    throw new IllegalStateException("Not all members (" + m.toString() + ") use the carpool on the given date (" + date.toString() + ").");

                case HAS_TO_DRIVE:
                    members.put(m, ++count);
                    return new Turn(date, m, this);

                case CAN_NOT_DRIVE:
                    continue;

                default:
                    if (count < minimumCount) {
                        driver = m;
                        minimumCount = count;
                    } else if (count == minimumCount && !history.contains(m)) {
                        driver = m;
                    }
            }
        }

        if (driver == null) {
            throw new IllegalStateException("All members of the combination can't use the carpool.");
        }

        members.put(driver, ++minimumCount);
        history.offer(driver);

        return new Turn(date, driver, this);
    }

    @Override
    public Iterator<Member> iterator() {
        return new MemberIterator(members.keySet().iterator());
    }

    /**
     * Should the driver count for the given {@link Turn}
     *
     * @param turn
     */
    void revert(Turn turn) {
        if (!equals(turn.getCombination())) {
            throw new IllegalArgumentException("The turn does not match to the current combination.");
        }

        int driverCount = members.get(turn.getDriver());
        if (driverCount == 0) {
            throw new IllegalStateException("Can not revert turn because the driver did not drive in the past.");
        }

        members.put(turn.getDriver(), --driverCount);
    }

    /**
     * Checks if the {@code MemberCombination} contains the given {@link Member}.
     *
     * @param member
     * @return
     */
    public boolean contains(Member member) {
        return members.containsKey(member);
    }

    /**
     * Calculates the intersection between the given {@link Member members} and the current {@code MemberCombination}.
     *
     * @param members
     * @return
     */
    List<Member> intersect(List<Member> members) {
        List<Member> intersection = new ArrayList<>();
        for (Member m : members) {
            if (contains(m)) {
                intersection.add(m);
            }
        }
        return intersection;
    }

    /**
     * Helper class to iterate over all {@link MemberCombination#members members}.
     */
    private static class MemberIterator implements Iterator<Member> {

        private final Iterator<Member> it;

        private MemberIterator(Iterator<Member> it) {
            this.it = it;
        }

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public Member next() {
            return it.next();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("The iterator does not support removing.");
        }
    }

    /**
     * Helper class to deserialize {@link MemberCombination#members}
     */
    private static class MemberDriverCountDeserializer extends JsonDeserializer<Map<Member, Integer>> {

        @Override
        public Map<Member, Integer> deserialize(JsonParser p, DeserializationContext context) throws IOException {
            Map<Member, Integer> driverCounts = new HashMap<>();

            final TreeNode treeNode = p.readValueAsTree();
            for (int i = 0; i < treeNode.size(); ++i) {
                TreeNode child = treeNode.get(i);

                Member member = getMember(child.path("member"), p, context);
                IntNode driverCount = (IntNode) child.path("driverCount");

                driverCounts.put(member, driverCount.intValue());
            }

            return driverCounts;
        }

        private Member getMember(TreeNode node, JsonParser p, DeserializationContext context) throws IOException {
            if (node instanceof IntNode) {
                IntNode memberId = (IntNode) node;
                return (Member) context.findObjectId(
                        memberId.intValue(),
                        new ObjectIdGenerators.IntSequenceGenerator(),
                        new SimpleObjectIdResolver()
                ).resolve();
            }

            StringReader reader = new StringReader(node.toString());
            IOContext ioContext = new IOContext(new BufferRecycler(), null, false);
            ReaderBasedJsonParser parser = new ReaderBasedJsonParser(ioContext, 0, reader, p.getCodec(), CharsToNameCanonicalizer.createRoot());
            parser.nextToken();

            SimpleType memberType = SimpleType.construct(Member.class);
            JsonDeserializer<Object> deserializer = context.findRootValueDeserializer(memberType);
            return (Member) deserializer.deserialize(parser, context);
        }
    }

    /**
     * Helper class to serialize {@link MemberCombination#members}
     */
    private static class MemberDriverCountSerializer extends JsonSerializer<Map<Member, Integer>> {

        @Override
        public void serialize(Map<Member, Integer> driverCounts, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            gen.writeStartArray();

            for (Member m : driverCounts.keySet()) {
                gen.writeStartObject();

                gen.writeFieldName("member");
                serializers.defaultSerializeValue(m, gen);

                gen.writeFieldName("driverCount");
                gen.writeNumber(driverCounts.get(m));

                gen.writeEndObject();
            }

            gen.writeEndArray();
        }
    }
}
