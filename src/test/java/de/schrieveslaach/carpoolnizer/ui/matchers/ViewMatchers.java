/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.matchers;

import androidx.annotation.PluralsRes;
import androidx.annotation.StringRes;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatCheckBox;
import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.model.Member;
import de.schrieveslaach.carpoolnizer.ui.fragments.TurnDetailsFragment;
import de.schrieveslaach.carpoolnizer.ui.views.MemberReadOnlyView;

import static org.hamcrest.Matchers.is;
import static org.robolectric.RuntimeEnvironment.application;

public class ViewMatchers {

    public static Matcher<View> visible() {
        return new VisibilityMatcher(View.VISIBLE);
    }

    public static Matcher<View> gone() {
        return new VisibilityMatcher(View.GONE);
    }

    private static class VisibilityMatcher extends TypeSafeMatcher<View> {

        private final int state;

        private VisibilityMatcher(int state) {
            this.state = state;
        }

        @Override
        protected boolean matchesSafely(View view) {
            return view.getVisibility() == state;
        }

        @Override
        public void describeTo(Description description) {
            description.appendText(getState(state));
        }

        @Override
        protected void describeMismatchSafely(View item, Description mismatchDescription) {
            mismatchDescription.appendValue(getState(item.getVisibility()));
        }

        private String getState(int state) {
            switch (state) {
                case View.VISIBLE:
                    return "visible";
                case View.GONE:
                    return "gone";
                case View.INVISIBLE:
                    return "invisible";
                default:
                    return "unknown";
            }
        }

    }

    public static Matcher<View> checkBoxIsChecked(final int checkBoxId) {
        return new TypeSafeMatcher<View>() {
            @Override
            public boolean matchesSafely(View v) {
                return isChecked(v);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("checkbox is checked");
            }

            @Override
            public void describeMismatchSafely(View v, Description mismatchDescription) {
                mismatchDescription.appendText(" checked: ")
                        .appendValue(isChecked(v));
            }

            private boolean isChecked(View v) {
                AppCompatCheckBox cb = v.findViewById(checkBoxId);
                return cb.isChecked();
            }
        };
    }

    public static Matcher<TextView> hasQuantityText(@PluralsRes final int textId, final int count) {
        String text = application.getResources().getQuantityString(textId, count, count);
        return hasText(text);
    }

    public static Matcher<TextView> hasText(@StringRes final int textId) {
        String text = application.getString(textId);
        return hasText(text);
    }

    public static Matcher<TextView> hasText(final String text) {
        return hasText(is(text));
    }

    public static Matcher<TextView> hasText(final Matcher<String> textMatcher) {
        return new TypeSafeMatcher<TextView>() {

            @Override
            protected boolean matchesSafely(TextView textView) {
                return textMatcher.matches(textView.getText().toString());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("text ")
                        .appendDescriptionOf(textMatcher);
            }

            @Override
            protected void describeMismatchSafely(TextView textView, Description mismatchDescription) {
                mismatchDescription.appendText("text ");
                textMatcher.describeMismatch(textView.getText().toString(), mismatchDescription);
            }
        };
    }

    public static Matcher<View> selected() {
        return new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View view) {
                return view.isSelected();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("selected");
            }

            @Override
            protected void describeMismatchSafely(View view, Description mismatchDescription) {
                if (view.isSelected()) {
                    mismatchDescription.appendText("selected");
                } else {
                    mismatchDescription.appendText("not selected");
                }
            }
        };
    }

    public static <V extends View> Matcher<V> enabled() {
        return new TypeSafeMatcher<V>() {
            @Override
            protected boolean matchesSafely(V view) {
                return view.isEnabled();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("enabled");
            }

            @Override
            protected void describeMismatchSafely(V view, Description mismatchDescription) {
                if (!view.isEnabled()) {
                    mismatchDescription.appendText("not ");
                }
                mismatchDescription.appendText("enabled.");
            }
        };
    }

    public static Matcher<EditText> hasErrorText(final int textId) {
        return new TypeSafeMatcher<EditText>() {
            @Override
            public boolean matchesSafely(EditText editText) {
                String expectedErrorText = editText.getContext().getResources().getString(textId);
                String errorText = null;
                if (editText.getError() != null) {
                    errorText = editText.getError().toString();
                }

                return expectedErrorText.equals(errorText);
            }

            @Override
            public void describeTo(Description description) {

            }
        };
    }

    public static Matcher<MemberReadOnlyView> displaysMember(final Member member) {
        return new TypeSafeMatcher<MemberReadOnlyView>() {
            @Override
            public boolean matchesSafely(MemberReadOnlyView memberReadOnlyView) {
                return new EqualsBuilder()
                        .append(member, memberReadOnlyView.getMember())
                        .isEquals();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("member view should display").appendValue(member);
            }

            @Override
            protected void describeMismatchSafely(MemberReadOnlyView item, Description mismatchDescription) {
                mismatchDescription.appendText("member view displays ").appendValue(item.getMember());
            }
        };
    }

    public static <V extends View> Matcher<ViewGroup> exactlyOneChild(final Matcher<V> matcher) {
        return new TypeSafeMatcher<ViewGroup>() {
            @Override
            public boolean matchesSafely(ViewGroup viewGroup) {
                int match = 0;
                for (int i = 0; i < viewGroup.getChildCount(); ++i) {
                    if (matcher.matches(viewGroup.getChildAt(i))) {
                        if (++match > 1) {
                            return false;
                        }
                    }
                }
                return match == 1;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("excatly one child").appendDescriptionOf(matcher);
            }

            @Override
            protected void describeMismatchSafely(ViewGroup viewGroup, Description mismatchDescription) {
                for (int i = 0; i < viewGroup.getChildCount(); ++i) {
                    View childAt = viewGroup.getChildAt(i);
                    if (!matcher.matches(childAt)) {
                        matcher.describeMismatch(childAt, mismatchDescription);
                        break;
                    }
                }
            }
        };
    }

    public static Matcher<View> hasOnClickListeners() {
        return new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                return item.hasOnClickListeners();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("should have on click listeners");
            }

            @Override
            protected void describeMismatchSafely(View item, Description mismatchDescription) {
                if (item.hasOnClickListeners()) {
                    mismatchDescription.appendText("view has on click listeners");
                } else {
                    mismatchDescription.appendText("view does not have on click listeners");
                }
            }
        };
    }

    public static <V extends View> Matcher<V> clickable() {
        return new TypeSafeMatcher<V>() {
            @Override
            protected boolean matchesSafely(V item) {
                return item.isClickable();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(" view should be clickable");
            }

            @Override
            protected void describeMismatchSafely(V item, Description mismatchDescription) {
                if (item.isClickable()) {
                    mismatchDescription.appendText("is clickable");
                } else {
                    mismatchDescription.appendText("is not clickable");
                }
            }
        };
    }

    public static Matcher<MemberReadOnlyView> withDriverCount(final Member driver, final Integer count) {
        return new TypeSafeMatcher<MemberReadOnlyView>() {
            @Override
            public boolean matchesSafely(MemberReadOnlyView memberReadOnlyView) {
                if (!visible().matches(memberReadOnlyView) || !displaysMember(driver).matches(memberReadOnlyView)) {
                    return false;
                }

                if (count != null) {
                    TextView tvDriverCount = (TextView) memberReadOnlyView.findViewById(TurnDetailsFragment.DRIVER_COUNT_ID);
                    return hasQuantityText(R.plurals.times_driven, count).matches(tvDriverCount);
                }

                return true;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(" member ")
                        .appendValue(driver);
                if (count != null) {
                    description.appendText(" and with driver count ")
                            .appendValue(count);
                }
            }

            @Override
            protected void describeMismatchSafely(MemberReadOnlyView mrov, Description mismatchDescription) {
                mismatchDescription.appendText("member ").appendValue(mrov.getMember());
                if (count != null) {
                    TextView tvDriverCount = (TextView) mrov.findViewById(TurnDetailsFragment.DRIVER_COUNT_ID);
                    mismatchDescription.appendText(" and driver count is: ").appendValue(tvDriverCount.getText());
                }
            }
        };
    }
}
