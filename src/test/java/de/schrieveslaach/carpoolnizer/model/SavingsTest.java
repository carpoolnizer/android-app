/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.model;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.closeTo;
import static org.junit.Assert.assertThat;

public class SavingsTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateSavings_MoreDrivenThanUsed() {
        new Savings(10L, 100L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateSavings_DriverCountBelowZero() {
        new Savings(10L, -1l);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateSavings_UsingCountBelowZero() {
        new Savings(-1l, 10L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotChangeDistance_BelowZero() {
        Savings savings = new Savings(10, 10);
        savings.setDistance(new BigDecimal(-10));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotChangeDistance_Zero() {
        Savings savings = new Savings(10, 10);
        savings.setDistance(new BigDecimal(0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotChangeDistance_NullValue() {
        Savings savings = new Savings(10, 10);
        savings.setDistance(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotChangeGasolineConsumption_BelowZero() {
        Savings savings = new Savings(10, 10);
        savings.setGasolineConsumption(new BigDecimal(-10));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotChangeGasolineConsumption_Zero() {
        Savings savings = new Savings(10, 10);
        savings.setGasolineConsumption(new BigDecimal(0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotChangeGasolineConsumption_NullValue() {
        Savings savings = new Savings(10, 10);
        savings.setGasolineConsumption(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotChangeGasolinePrice_BelowZero() {
        Savings savings = new Savings(10, 10);
        savings.setGasolineConsumption(new BigDecimal(-10));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotChangeGasolinePrice_Zero() {
        Savings savings = new Savings(10, 10);
        savings.setGasolineConsumption(new BigDecimal(0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotChangeGasolinePrice_NullValue() {
        Savings savings = new Savings(10, 10);
        savings.setGasolineConsumption(null);
    }

    @Test
    public void shouldConvertToBigDecimal_NotDroveAndNotUsedCarpool() {
        Savings savings = new Savings(0, 0);
        assertThat(savings.toBigDecimal(), closeTo(new BigDecimal(0), new BigDecimal(0)));
    }

    @Test
    public void shouldConvertToBigDecimal_DroveAndUsesCarpoolAlone() {
        Savings savings = new Savings(10, 10);
        assertThat(savings.toBigDecimal(), closeTo(new BigDecimal(0), new BigDecimal(0)));
    }

    @Test
    public void shouldConvertToBigDecimal_UsedCarpool_HalfAmount() {
        Savings savings = new Savings(10, 5);
        assertThat(savings.toBigDecimal(), closeTo(new BigDecimal(36), new BigDecimal(0)));
    }

    @Test
    public void shouldConvertToBigDecimal_UsedCarpool_OneFifth() {
        Savings savings = new Savings(10, 2);
        assertThat(savings.toBigDecimal(), closeTo(new BigDecimal(57.6), new BigDecimal(0.001)));
    }
}