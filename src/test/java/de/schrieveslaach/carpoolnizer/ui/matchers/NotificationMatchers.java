/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.matchers;

import android.app.Notification;
import android.content.Context;
import androidx.annotation.StringRes;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.shadows.ShadowNotification;

import java.util.Objects;

import static org.robolectric.Shadows.shadowOf;

public class NotificationMatchers {

    private NotificationMatchers() {

    }

    public static Matcher<Notification> hasContentTitle(@StringRes final int title) {
        final Context context = RuntimeEnvironment.application.getApplicationContext();

        return new TypeSafeMatcher<Notification>() {
            @Override
            protected boolean matchesSafely(Notification item) {
                ShadowNotification shadowNotification = shadowOf(item);
                String contentTitle = context.getResources().getString(title);
                return Objects.equals(contentTitle, shadowNotification.getContentTitle());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("has content title ").appendValue(context.getResources().getString(title));
            }

            @Override
            protected void describeMismatchSafely(Notification item, Description mismatchDescription) {
                ShadowNotification shadowNotification = shadowOf(item);
                mismatchDescription.appendText("has content title ").appendValue(shadowNotification.getContentTitle());
            }
        };
    }
}
