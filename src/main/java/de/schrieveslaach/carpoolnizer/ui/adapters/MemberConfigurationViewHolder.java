/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.adapters;

import androidx.recyclerview.widget.RecyclerView;

import de.schrieveslaach.carpoolnizer.model.Member;
import de.schrieveslaach.carpoolnizer.ui.views.MemberConfigurationView;

/**
 * {@link RecyclerView.ViewHolder View holder} for {@link MemberConfigurationView}.
 */
public class MemberConfigurationViewHolder extends RecyclerView.ViewHolder {

    private final MemberConfigurationView mvc;

    /**
     * Creates a new instance of {@code MemberConfigurationView}.
     *
     * @param mvc
     */
    public MemberConfigurationViewHolder(MemberConfigurationView mvc) {
        super(mvc);
        this.mvc = mvc;
    }

    /**
     * Displays the weekday usage for the given {@link Member member}.
     *
     * @param member
     * @param weekdayUsage
     */
    public void display(Member member, boolean[] weekdayUsage) {
        mvc.display(member);
        mvc.display(weekdayUsage);
    }

    /**
     * Returns the configured weekday usage.
     *
     * @return
     */
    public boolean[] getWeekdayUsage() {
        return mvc.getWeekdayUsage();
    }
}
