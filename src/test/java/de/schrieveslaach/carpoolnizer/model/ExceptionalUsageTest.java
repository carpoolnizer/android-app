/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.model;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.Test;

import static org.hamcrest.Matchers.comparesEqualTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class ExceptionalUsageTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateExceptionalUsage_InvalidInterval_Null() {
        new ExceptionalUsage(null, Usage.CAN_NOT_DRIVE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateExceptionalUsage_InvalidInterval_StartHasMillis() {
        DateTime now = DateTime.now();
        if (now.getMillisOfDay() == 0) {
            now = now.plusMillis(10);
        }
        new ExceptionalUsage(new Interval(now, now.plusDays(1).withTimeAtStartOfDay()), Usage.CAN_NOT_DRIVE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateExceptionalUsage_InvalidInterval_EndHasMillis() {
        DateTime now = DateTime.now();
        if (now.getMillisOfDay() == 0) {
            now = now.plusMillis(10);
        }
        new ExceptionalUsage(new Interval(now.withTimeAtStartOfDay(), now.plusDays(1)), Usage.CAN_NOT_DRIVE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateExceptionalUsage_InvalidUsage_Null() {
        DateTime now = DateTime.now().withTimeAtStartOfDay();
        new ExceptionalUsage(new Interval(now, now.plusDays(1)), null);
    }

    @Test
    public void shouldMatchDate_InTheMiddleOfTheInterval() {
        Interval nextWeek = new Interval(DateTime.now().withTimeAtStartOfDay(), DateTime.now().plusDays(7).withTimeAtStartOfDay());
        ExceptionalUsage eu = new ExceptionalUsage(nextWeek, Usage.NORMAL);

        assertThat(eu, matchesDate(nextWeek.getStart().plusDays(2)));
    }

    @Test
    public void shouldMatchDate_StartOfTheInterval() {
        Interval nextWeek = new Interval(DateTime.now().withTimeAtStartOfDay(), DateTime.now().plusDays(7).withTimeAtStartOfDay());
        ExceptionalUsage eu = new ExceptionalUsage(nextWeek, Usage.NORMAL);

        assertThat(eu, matchesDate(nextWeek.getStart()));
    }

    @Test
    public void shouldNotMatchDate_BeforeTheIntervalStart() {
        Interval nextWeek = new Interval(DateTime.now().withTimeAtStartOfDay(), DateTime.now().plusDays(7).withTimeAtStartOfDay());
        ExceptionalUsage eu = new ExceptionalUsage(nextWeek, Usage.NORMAL);

        assertThat(eu, not(matchesDate(nextWeek.getStart().minusDays(1))));
    }

    @Test
    public void shouldNotMatchDate_EndOfTheIntervalStart() {
        Interval nextWeek = new Interval(DateTime.now().withTimeAtStartOfDay(), DateTime.now().plusDays(7).withTimeAtStartOfDay());
        ExceptionalUsage eu = new ExceptionalUsage(nextWeek, Usage.NORMAL);

        assertThat(eu, not(matchesDate(nextWeek.getEnd())));
    }

    @Test
    public void shouldMatchInterval() {
        Interval nextWeek = new Interval(DateTime.now().withTimeAtStartOfDay(), DateTime.now().plusDays(7).withTimeAtStartOfDay());
        ExceptionalUsage eu = new ExceptionalUsage(nextWeek, Usage.NORMAL);

        assertThat(eu, matchesInterval(nextWeek));
    }

    @Test
    public void shouldNotMatchInterval() {
        Interval nextWeek = new Interval(DateTime.now().withTimeAtStartOfDay(), DateTime.now().plusDays(7).withTimeAtStartOfDay());
        ExceptionalUsage eu = new ExceptionalUsage(nextWeek, Usage.NORMAL);

        Interval overlappingNextWeek = new Interval(nextWeek.getStart(), nextWeek.getEnd().minusDays(1));
        assertThat(eu, not(matchesInterval(overlappingNextWeek)));
    }

    @Test
    public void shouldOverlapInterval() {
        Interval nextWeek = new Interval(DateTime.now().withTimeAtStartOfDay(), DateTime.now().plusDays(7).withTimeAtStartOfDay());
        ExceptionalUsage eu = new ExceptionalUsage(nextWeek, Usage.NORMAL);

        Interval overlappingNextWeek = new Interval(nextWeek.getStart(), nextWeek.getEnd().minusDays(1));
        assertThat(eu, overlapsInterval(overlappingNextWeek));
    }

    @Test
    public void shouldNotOverlapInterval() {
        Interval nextWeek = new Interval(DateTime.now().withTimeAtStartOfDay(), DateTime.now().plusDays(7).withTimeAtStartOfDay());
        ExceptionalUsage eu = new ExceptionalUsage(nextWeek, Usage.NORMAL);

        Interval beforeNextWeek = new Interval(nextWeek.getStart().minusDays(10), nextWeek.getStart());
        assertThat(eu, not(overlapsInterval(beforeNextWeek)));
    }

    @Test
    public void shouldCompareEU_StartAndEndBefore() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime yesterday = today.minusDays(1);
        DateTime tomorrow = today.plusDays(1);

        ExceptionalUsage eu1 = new ExceptionalUsage(new Interval(yesterday, today), Usage.NORMAL);
        ExceptionalUsage eu2 = new ExceptionalUsage(new Interval(today, tomorrow), Usage.NORMAL);

        assertThat(eu1, is(lessThan(eu2)));
    }

    @Test
    public void shouldCompareEU_StartBefore() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime yesterday = today.minusDays(1);
        DateTime tomorrow = today.plusDays(1);

        ExceptionalUsage eu1 = new ExceptionalUsage(new Interval(yesterday, tomorrow), Usage.NORMAL);
        ExceptionalUsage eu2 = new ExceptionalUsage(new Interval(today, tomorrow), Usage.NORMAL);

        assertThat(eu1, is(lessThan(eu2)));
    }

    @Test
    public void shouldCompareEU_StartAfter() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime yesterday = today.minusDays(1);
        DateTime tomorrow = today.plusDays(1);

        ExceptionalUsage eu1 = new ExceptionalUsage(new Interval(today, tomorrow), Usage.NORMAL);
        ExceptionalUsage eu2 = new ExceptionalUsage(new Interval(yesterday, today), Usage.NORMAL);

        assertThat(eu1, is(greaterThan(eu2)));
    }

    @Test
    public void shouldCompareEU_Equal() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime yesterday = today.minusDays(1);

        ExceptionalUsage eu1 = new ExceptionalUsage(new Interval(yesterday, today), Usage.NORMAL);
        ExceptionalUsage eu2 = new ExceptionalUsage(new Interval(yesterday, today), Usage.NORMAL);

        assertThat(eu1, is(comparesEqualTo(eu2)));
    }

    @Test
    public void shouldBeEquals_SameInstance() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime yesterday = today.minusDays(1);

        ExceptionalUsage eu = new ExceptionalUsage(new Interval(yesterday, today), Usage.NORMAL);
        assertThat(eu, is(equalTo(eu)));
    }

    @Test
    public void shouldBeEquals_DifferentInstance() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime yesterday = today.minusDays(1);

        ExceptionalUsage eu1 = new ExceptionalUsage(new Interval(yesterday, today), Usage.NORMAL);
        ExceptionalUsage eu2 = new ExceptionalUsage(new Interval(yesterday, today), Usage.NORMAL);
        assertThat(eu1, is(equalTo(eu2)));
    }

    @Test
    public void shouldNotBeEquals_NullValue() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime yesterday = today.minusDays(1);

        ExceptionalUsage eu = new ExceptionalUsage(new Interval(yesterday, today), Usage.NORMAL);
        assertThat(eu, is(not(equalTo(null))));
    }

    @Test
    public void shouldNotBeEquals_DifferentInterval() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime yesterday = today.minusDays(1);

        ExceptionalUsage eu1 = new ExceptionalUsage(new Interval(yesterday, today), Usage.NORMAL);
        ExceptionalUsage eu2 = new ExceptionalUsage(new Interval(yesterday, today.plusDays(1)), Usage.NORMAL);
        assertThat(eu1, is(not(equalTo(eu2))));
    }

    @Test
    public void shouldNotBeEquals_DifferentUsage() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime yesterday = today.minusDays(1);

        ExceptionalUsage eu1 = new ExceptionalUsage(new Interval(yesterday, today), Usage.NORMAL);
        ExceptionalUsage eu2 = new ExceptionalUsage(new Interval(yesterday, today), Usage.DOES_NOT_USE);
        assertThat(eu1, is(not(equalTo(eu2))));
    }

    public static Matcher<ExceptionalUsage> matchesDate(final DateTime dateTime) {
        return new TypeSafeMatcher<ExceptionalUsage>() {
            @Override
            protected boolean matchesSafely(ExceptionalUsage eu) {
                return eu.matches(dateTime);
            }

            @Override
            public void describeTo(Description description) {
                description.appendValue(dateTime)
                        .appendText(" is be contained in the exceptional usage");
            }

            @Override
            protected void describeMismatchSafely(ExceptionalUsage eu, Description mismatchDescription) {
                mismatchDescription.appendText(" interval: ").appendValue(eu.getInterval());
            }
        };
    }

    public static Matcher<ExceptionalUsage> matchesInterval(final Interval interval) {
        return new TypeSafeMatcher<ExceptionalUsage>() {
            @Override
            protected boolean matchesSafely(ExceptionalUsage eu) {
                return eu.matches(interval);
            }

            @Override
            public void describeTo(Description description) {
                description.appendValue(interval)
                        .appendText(" matches exactly exceptional usage");
            }

            @Override
            protected void describeMismatchSafely(ExceptionalUsage eu, Description mismatchDescription) {
                mismatchDescription.appendText(" interval: ").appendValue(eu.getInterval());
            }
        };
    }

    public static Matcher<ExceptionalUsage> overlapsInterval(final Interval interval) {
        return new TypeSafeMatcher<ExceptionalUsage>() {
            @Override
            protected boolean matchesSafely(ExceptionalUsage eu) {
                return eu.overlaps(interval);
            }

            @Override
            public void describeTo(Description description) {
                description.appendValue(interval)
                        .appendText(" overlaps exceptional usage");
            }

            @Override
            protected void describeMismatchSafely(ExceptionalUsage eu, Description mismatchDescription) {
                mismatchDescription.appendText(" interval: ").appendValue(eu.getInterval());
            }
        };
    }
}