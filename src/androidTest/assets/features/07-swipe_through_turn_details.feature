Feature: Swipe through turn details
  This feature allows swipe through the turn details from day to day. 
  
  Scenario Outline: Slide from today to the right or left turn.
    Given I started the app
    And I opened the carpool <carpool>
    And I clicked on turn of today
    When I swipe to the <direction> <swipe_times> times
    Then I should see turn in <days> days

  Examples:
    | carpool   | direction | swipe_times | days |
    | Work Mike | left      | 6           | 6    |
    | Work Mike | right     | 6           | -6   |
    
  Scenario Outline: Slide from today to the right and change exceptional usage
    Given I started the app
    And I opened the carpool <carpool>
    And I clicked on first turn with contact <current_driver> as driver and <current_passengers> passengers
    When I change exceptional usage of the current driver to cannot drive
    And I swipe to the left to next turn with driver
    Then I should see contact as driver which was the current driver
    
  Examples:
    | carpool   | current_driver | current_passengers |
    | Work John | John           | 1                  |
