/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.google.common.io.Files;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlarmManager;
import org.robolectric.shadows.ShadowApplication;
import org.robolectric.shadows.ShadowNotificationManager;
import org.robolectric.shadows.ShadowPendingIntent;

import java.io.File;
import java.net.URI;
import java.nio.charset.Charset;

import de.schrieveslaach.carpoolnizer.model.Carpool;
import de.schrieveslaach.carpoolnizer.model.CarpoolBuilder;
import de.schrieveslaach.carpoolnizer.model.settings.UsageNotification;
import de.schrieveslaach.carpoolnizer.repository.CarpoolDAO;
import de.schrieveslaach.carpoolnizer.repository.CarpoolDAOException;
import de.schrieveslaach.carpoolnizer.ui.RobolectricTest;
import de.schrieveslaach.carpoolnizer.ui.robolectric.fakes.ContactsCursor;
import lombok.SneakyThrows;

import static de.schrieveslaach.carpoolnizer.ui.matchers.IntentMatchers.hasAction;
import static de.schrieveslaach.carpoolnizer.ui.matchers.IntentMatchers.hasStringExtra;
import static de.schrieveslaach.carpoolnizer.ui.matchers.NotificationMatchers.hasContentTitle;
import static net.javacrumbs.jsonunit.JsonMatchers.jsonEquals;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.robolectric.Shadows.shadowOf;

@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
public class AlarmsTest extends RobolectricTest {

    private Context context;

    private ShadowAlarmManager shadowAlarmManager;

    private ShadowNotificationManager shadowNotificationManager;

    private ContactsCursor contactsCursor;

    @Before
    public void addMembersToContentResolver() {
        contactsCursor = new ContactsCursor()
                .withContact("John")
                .withContact("Dave")
                .withContact("Mike")
                .withContact("Daisy")
                .withContact(URI.create("http://tempuri.org#john"))
                .withContact(URI.create("http://tempuri.org#tom"))
                .fake();
    }

    @Before
    public void initAlarmManager() {
        context = RuntimeEnvironment.application.getApplicationContext();
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        shadowAlarmManager = shadowOf(alarmManager);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        shadowNotificationManager = shadowOf(notificationManager);
    }

    @Test
    public void shouldSetUpAlarmManager_HasDailyInterval() {
        setUpAlarm("Work", 7, 0);

        ShadowAlarmManager.ScheduledAlarm scheduledAlarm = shadowAlarmManager.getNextScheduledAlarm();
        assertThat(scheduledAlarm.interval, is(60 * 60 * 24 * 1000L));
    }

    @SneakyThrows(CarpoolDAOException.class)
    private void setUpAlarm(String carpoolName, int hours, int minutes) {
        UsageNotification notification = new UsageNotification(hours, minutes);

        Carpool carpool = mock(Carpool.class);
        when(carpool.getName()).thenReturn(carpoolName);

        new Alarms().setAlarm(context, carpool, notification);
    }

    @Test
    public void shouldSetUpAlarmManager_HasNotificationTriggerTime() {
        setUpAlarm("Work", 7, 0);

        DateTime today = DateTime.now().withTimeAtStartOfDay();

        ShadowAlarmManager.ScheduledAlarm scheduledAlarm = shadowAlarmManager.getNextScheduledAlarm();
        assertThat(scheduledAlarm.triggerAtTime, is(today.getMillis() + 60 * 60 * 7 * 1000L));
    }

    @Test
    public void shouldSetUpAlarmManager_HasCarpoolNameIntent() {
        setUpAlarm("Work", 7, 0);

        ShadowAlarmManager.ScheduledAlarm scheduledAlarm = shadowAlarmManager.getNextScheduledAlarm();
        ShadowPendingIntent pendingIntent = shadowOf(scheduledAlarm.operation);
        assertThat(pendingIntent.getSavedIntent(), hasStringExtra(Constants.CARPOOL_NAME, "Work"));
    }

    @Test
    public void shouldSetUpAlarmManager_HasNotifyCarpoolActionIntent() {
        setUpAlarm("Work", 7, 0);

        ShadowAlarmManager.ScheduledAlarm scheduledAlarm = shadowAlarmManager.getNextScheduledAlarm();
        ShadowPendingIntent pendingIntent = shadowOf(scheduledAlarm.operation);
        assertThat(pendingIntent.getSavedIntent(), hasAction(Constants.CARPOOL_USAGE_NOTIFICATION));
    }

    @Test
    public void shouldSetUpAlarmManager_StoreNotification() throws Exception {
        setUpAlarm("Work", 12, 0);

        File notificationFile = context.getFileStreamPath("Work.usage.notification.json");
        String json = Files.toString(notificationFile, Charset.defaultCharset());

        assertThat(json, jsonEquals("{ \"time\": \"12:00\" }"));
    }

    @Test
    public void shouldNotCreateNotification_OnReceiveBroadcast_WrongAction() {
        Intent intent = new Intent(context, Alarms.class);
        intent.putExtra(Constants.CARPOOL_NAME, "Work");

        new Alarms().onReceive(context, intent);

        assertThat(shadowNotificationManager.getAllNotifications(), is(empty()));
    }

    @Test
    public void shouldNotCreateNotification_OnReceiveBroadcast_NoCarpoolName() {
        Intent intent = new Intent(context, Alarms.class);
        intent.setAction(Constants.CARPOOL_USAGE_NOTIFICATION);

        new Alarms().onReceive(context, intent);

        assertThat(shadowNotificationManager.getAllNotifications(), is(empty()));
    }

    @Test
    public void shouldNotCreateNotification_OnReceiveBroadcast_PhoneUserDoesNotUseCarpool() throws Exception {
        CarpoolDAO dao = new CarpoolDAO(context);
        dao.save(new CarpoolBuilder()
                .withName("Work")
                .withMember("Mike")
                .withMember(null)
                .create(contactsCursor)
        );

        Intent intent = new Intent(context, Alarms.class);
        intent.setAction(Constants.CARPOOL_USAGE_NOTIFICATION);
        intent.putExtra(Constants.CARPOOL_NAME, "Work");

        new Alarms().onReceive(context, intent);

        assertThat(shadowNotificationManager.getAllNotifications(), is(empty()));
    }

    @Test
    public void shouldCreateNotification_OnReceiveBroadcast_PhoneUserUsesCarpool() throws Exception {
        CarpoolDAO dao = new CarpoolDAO(context);
        dao.save(new CarpoolBuilder()
                .withName("Work")
                .withMember("Mike")
                .withMemberAndEveryDayUsage(null)
                .create(contactsCursor)
        );

        Intent intent = new Intent(context, Alarms.class);
        intent.setAction(Constants.CARPOOL_USAGE_NOTIFICATION);
        intent.putExtra(Constants.CARPOOL_NAME, "Work");

        new Alarms().onReceive(context, intent);

        Notification notification = shadowNotificationManager.getAllNotifications().get(0);
        assertThat(notification, hasContentTitle(R.string.notification_you_are_using_the_carpool_title));
    }

    @Test
    public void shouldCreateNotification_OnReceiveBroadcast_BootComplete() throws Exception {
        Carpool carpool = new CarpoolBuilder()
                .withName("Work")
                .withMember("Mike")
                .withMemberAndEveryDayUsage(null)
                .create(contactsCursor);

        CarpoolDAO dao = new CarpoolDAO(context);
        dao.save(carpool);

        UsageNotification notification = new UsageNotification(7, 00);
        dao.saveUsageNotifcation(carpool, notification);

        new Alarms().onReceive(context, new Intent("android.intent.action.BOOT_COMPLETED"));

        ShadowAlarmManager.ScheduledAlarm scheduledAlarm = shadowAlarmManager.getNextScheduledAlarm();
        ShadowPendingIntent pendingIntent = shadowOf(scheduledAlarm.operation);
        assertThat(pendingIntent.getSavedIntent(), hasAction(Constants.CARPOOL_USAGE_NOTIFICATION));
    }

    @Test
    public void shouldNotCreateNotification_OnReceiveBroadcast_BootComplete_NoNotification() throws Exception {
        Carpool carpool = new CarpoolBuilder()
                .withName("Work")
                .withMember("Mike")
                .withMemberAndEveryDayUsage(null)
                .create(contactsCursor);

        CarpoolDAO dao = new CarpoolDAO(context);
        dao.save(carpool);

        new Alarms().onReceive(context, new Intent("android.intent.action.BOOT_COMPLETED"));

        ShadowAlarmManager.ScheduledAlarm scheduledAlarm = shadowAlarmManager.getNextScheduledAlarm();
        assertThat(scheduledAlarm, is(nullValue()));
    }
}
