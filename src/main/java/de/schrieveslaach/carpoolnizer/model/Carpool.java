/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Ordering;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Pattern;

/**
 * Organizes a carpool and their {@link Member members}.
 */
@JsonPropertyOrder({"members", "combinations", "turnsInThePast"})
public class Carpool implements Serializable {

    private static final Pattern CARPOOL_NAME_PATTER = Pattern.compile("^[\\wäöüßÄÖÜ]+[\\w äöüßÄÖÜ]*[\\wäöüßÄÖÜ]+$");

    /**
     * All {@link Member members} of the carpool.
     */
    @JsonProperty("members")
    private final Set<Member> members;

    /**
     * The combinations of the members.<br/>
     * <ul>
     * <li>First dimension: The size of the combinations</li>
     * <li>Second dimension: The combination of some members</li>
     * </ul>
     */
    @JsonProperty("combinations")
    private final Multimap<Integer, MemberCombination> combinations;

    /**
     * The {@link Turn turns} which are in the past.
     */
    private final SortedSet<Turn> turnsInThePast;


    @JsonProperty("canceled")
    private final SortedSet<DateTime> canceled;

    /**
     * Display name of the carpool
     */
    private String name;

    /**
     * Creates a new carpool.
     *
     * @param name The name of the carpool.
     */
    @JsonCreator
    public Carpool(@JsonProperty("name") String name) {
        setName(name);
        members = new HashSet<>();
        combinations = ArrayListMultimap.<Integer, MemberCombination>create();
        turnsInThePast = new TurnTreeSet();
        canceled = new TreeSet<>();
    }

    /**
     * Checks if the given {@code name} is a valid name for a carpool.
     *
     * @param name
     * @return
     */
    public static boolean isValidCarpoolName(String name) {
        return name != null && CARPOOL_NAME_PATTER.matcher(name).matches();
    }

    /**
     * Returns the name of the {@code Carpool}.
     *
     * @return
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the current {@code Carpool}.
     *
     * @param name
     */
    @JsonProperty("name")
    public final void setName(String name) {
        if (!isValidCarpoolName(name)) {
            throw new IllegalArgumentException("The given name is invalid.");
        }

        this.name = name;
    }


    /**
     * Returns all {@link Member members} of the {@link Carpool}.
     *
     * @return
     */
    @JsonIgnore
    public ImmutableSet<Member> getMembers() {
        ImmutableSet.Builder<Member> builder = ImmutableSet.<Member>builder();
        builder.addAll(members);
        return builder.build();
    }

    /**
     * Returns all {@link Turn turns} in the past in the given {@link Interval}.
     *
     * @param interval {@code [start, end[}
     * @return
     */
    @JsonIgnore
    public ImmutableList<Turn> getTurnsInThePast(Interval interval) {
        ImmutableList.Builder<Turn> builder = ImmutableList.<Turn>builder();

        for (Turn turn : turnsInThePast) {
            if (interval.contains(turn.getDate())) {
                builder.add(turn);
            }
        }

        return builder.build();
    }

    @JsonProperty("turnsInThePast")
    private void setTurnsInThePast(SortedSet<Turn> turnsInThePast) {
        this.turnsInThePast.clear();
        this.turnsInThePast.addAll(turnsInThePast);
    }

    /**
     * Returns all {@link Turn turns} in the past.
     *
     * @return
     */
    @JsonProperty("turnsInThePast")
    public ImmutableList<Turn> getTurnsInThePast() {
        ImmutableList.Builder<Turn> builder = ImmutableList.<Turn>builder();
        builder.addAll(turnsInThePast);
        return builder.build();
    }

    /**
     * Returns the date when the {@code Carpool} was used last time.
     *
     * @return
     */
    @JsonIgnore
    public DateTime getLastUsedDate() {
        if (!turnsInThePast.isEmpty()) {
            return turnsInThePast.last().getDate();
        }

        Iterable<DateTime> dateTimes = Iterables.transform(Iterables.filter(members, new Predicate<Member>() {
            @Override
            public boolean apply(Member input) {
                return input.getFirstUsageDateTime() != null;
            }
        }), new Function<Member, DateTime>() {
            @Override
            public DateTime apply(Member input) {
                return input.getFirstUsageDateTime();
            }
        });

        if (dateTimes.iterator().hasNext()) {
            return Ordering.natural().min(dateTimes).minusDays(1);
        }

        return new DateTime().withTimeAtStartOfDay().minusDays(1);
    }

    /**
     * Returns the date when the {@code Carpool} was used the first time.
     *
     * @return
     */
    @JsonIgnore
    public DateTime getFirstUsedDate() {
        if (!turnsInThePast.isEmpty()) {
            return turnsInThePast.first().getDate();
        }

        return getLastUsedDate();
    }

    /**
     * Returns the number of {@link Member members} in the current {@link Carpool}.
     *
     * @return
     */
    public int size() {
        return members.size();
    }

    /**
     * Adds a new {@link Member member} to the carpool.
     *
     * @param member
     * @return
     */
    public boolean add(Member member) {
        if (members.contains(member)) {
            return false;
        }

        if (!members.isEmpty()) {

            // Build bigger combinations
            for (int i = members.size(); i >= 2; --i) {
                buildCombinations(i, member);
            }

            // Add combination of two.
            for (Member m : members) {
                combinations.put(2, new MemberCombination(Arrays.asList(m, member)));
            }
        }

        members.add(member);

        return true;
    }

    /**
     * Builds all new {@link MemberCombination combinations} from the base
     * {@link MemberCombination combination}.
     *
     * @param size   The size of the base combinations.
     * @param member The new member of the new combinations.
     */
    private void buildCombinations(int size, Member member) {
        // Get a base combination and build a new one
        for (MemberCombination c : combinations.get(size)) {
            // Add new combination
            combinations.put(c.size() + 1, new MemberCombination(member, c));
        }
    }

    /**
     * Returns the
     *
     * @param members
     * @return
     */
    public MemberCombination getMemberCombination(Member... members) {
        if (members.length < 2 || size() < members.length) {
            return null;
        }

        for (MemberCombination mc : combinations.get(members.length)) {
            if (mc.containsAll(members)) {
                return mc;
            }
        }

        return null;
    }

    /**
     * Calculates and stores the {@link Turn turns} until the given {@link DateTime date}. The turns
     * will be stored into {@link Carpool#getTurnsInThePast()}.
     *
     * @param date The {@link DateTime date} inclusive until the {@link Turn turns} will be
     *             predicted.
     * @return A {@link List list} of calculated {@link Turn turns}.
     */
    public List<Turn> calculateTurnsUntil(DateTime date) {
        List<Turn> turns = calculateOrPredictTurns(date, true);
        turnsInThePast.addAll(turns);
        return turns;
    }

    /**
     * Predict the turns until the given date.
     *
     * @param date The {@link DateTime date} inclusive until the {@link Turn turns} will be
     *             predicted.
     * @return A {@link List list} of predicted {@link Turn turns}.
     */
    public List<Turn> predictTurnsUntil(DateTime date) {
        return calculateOrPredictTurns(date, false);
    }

    private List<Turn> calculateOrPredictTurns(DateTime date, boolean calculate) {
        if (date.isBefore(getLastUsedDate()) || date.isEqual(getLastUsedDate())) {
            return Arrays.<Turn>asList();
        }

        CalculateTurns ct = new CalculateTurns(
                getLastUsedDate().plusDays(1),
                date
        );

        if (calculate) {
            ct.doCalculation();
        } else {
            ct.doPrediction();
        }

        return ct.getTurns();
    }

    /**
     * Defines {@link Usage exceptional usage} for the given {@link Member member} for the
     * given {@link DateTime date}.
     *
     * @param member
     * @param date
     * @param usage
     */
    public void defineExceptionalUsage(Member member, DateTime date, Usage usage) {
        DateTime start = date.withTimeAtStartOfDay();
        defineExceptionalUsage(member, new Interval(start, start.plusMinutes(1)), usage);
    }

    /**
     * Defines {@link Usage exceptional usage} for the given {@link Member member} for the
     * given {@link Interval interval}.
     *
     * @param member
     * @param interval
     * @param usage
     */
    public void defineExceptionalUsage(Member member, Interval interval, Usage usage) {
        // Find the right member reference
        Member actualMember = getActualMember(member);

        if (actualMember == null) {
            throw new IllegalArgumentException("The given member '" + member + "' is not part of the carpool.");
        }

        for (DateTime d = interval.getStart(); d.isBefore(interval.getEnd()); d = d.plusDays(1)) {
            if (getInvalidUsage(d).contains(usage)) {
                throw new IllegalArgumentException("The given usage (" + usage + ") is invalid for " + d);
            }
        }

        actualMember.addExceptionalUsage(interval, usage);

        if (!interval.getStart().isAfter(getLastUsedDate())) {
            recalculateTurns(interval);
        }
    }

    /**
     * Recalculates the {@link Carpool#turnsInThePast} during the given {@link Interval interval}.
     *
     * @param interval
     */
    private void recalculateTurns(Interval interval) {
        DateTime start = max(interval.getStart(), getFirstUsedDate());
        DateTime end = min(interval.getEnd(), getLastUsedDate());
        CalculateTurns ct = new CalculateTurns(start, end);

        // revert turns which are in this period
        for (Turn turn : Lists.newArrayList(turnsInThePast)) {
            if (ct.interval.contains(turn.getDate())) {
                turnsInThePast.remove(turn);

                if (turn.getCombination() != null) {
                    turn.revert();
                }
            }
        }

        ct.doCalculation();

        turnsInThePast.addAll(ct.getTurns());
    }

    private DateTime min(DateTime a, DateTime b) {
        if (a.compareTo(b) < 0) {
            return a;
        }
        return b;
    }

    private DateTime max(DateTime a, DateTime b) {
        if (a.compareTo(b) > 0) {
            return a;
        }
        return b;
    }

    /**
     * Checks if the {@code Carpool} is canceled at the given {@link Turn#getDate() turn date}.
     *
     * @param turn
     * @return
     */
    public boolean isCanceled(Turn turn) {
        return isCanceled(turn.getDate());
    }

    /**
     * Checks if the {@code Carpool} is canceled at the given {@link DateTime date}.
     *
     * @param date
     * @return
     */
    public boolean isCanceled(DateTime date) {
        return canceled.contains(date);
    }

    /**
     * Cancels the {@code Carpool} at the given {@link DateTime date}.
     *
     * @param date
     */
    public void cancel(DateTime date) {
        DateTime dateTime = date.withTimeAtStartOfDay();
        if (canceled.contains(dateTime)) {
            return;
        }

        canceled.add(dateTime);

        if (!date.isAfter(getLastUsedDate())) {
            recalculateTurns(new Interval(dateTime, dateTime.plusDays(1)));
        }
    }

    /**
     * Resumes the {@code Carpool} at the given {@link DateTime date}.
     *
     * @param date
     */
    public void resume(DateTime date) {
        DateTime dateTime = date.withTimeAtStartOfDay();
        if (!canceled.contains(dateTime)) {
            return;
        }

        canceled.remove(dateTime);
        if (!date.isAfter(getLastUsedDate())) {
            recalculateTurns(new Interval(dateTime, dateTime.plusDays(1)));
        }
    }

    /**
     * Archives all data which is older than the given {@link DateTime date}.
     *
     * @param dateTime
     * @return
     */
    public CarpoolArchive archiveDataOlderThan(DateTime dateTime) {
        List<Member> archivedMembers = new ArrayList<>();
        for (Member m : members) {
            archivedMembers.add(m.archiveDataOlderThan(dateTime));
        }

        SortedSet<Turn> archivedTurns = new TreeSet<>();
        Multimap<Integer, MemberCombination> archivedCombinations = ArrayListMultimap.create();

        for (Turn turn : Lists.newArrayList(turnsInThePast)) {
            if (!turn.getDate().isBefore(dateTime)) {
                break;
            }

            MemberCombination combination = null;
            if (turn.getCombination() != null) {
                List<Member> intersection = turn.getCombination().intersect(archivedMembers);
                combination = getCombination(archivedCombinations, intersection);
                if (combination == null) {
                    combination = new MemberCombination(intersection);
                    archivedCombinations.put(combination.size(), combination);
                }
                combination.incrementDriverCount(turn.getDriver());
            }

            turnsInThePast.remove(turn);
            archivedTurns.add(new Turn(
                    turn.getDate(),
                    getMember(turn.getDriver(), archivedMembers),
                    combination
            ));
        }

        if (archivedTurns.isEmpty()) {
            return null;
        }
        return new CarpoolArchive(archivedCombinations, archivedTurns);
    }

    /**
     * Returns the actual {@link Member member instance}.
     *
     * @param member
     * @return
     */
    public Member getActualMember(Member member) {
        return getMember(member, members);
    }

    private static Member getMember(Member member, Iterable<Member> members) {
        for (Member m : members) {
            if (m.equals(member)) {
                return m;
            }
        }
        return null;
    }

    private MemberCombination getActualCombination(MemberCombination combination) {
        if (combination == null) {
            return null;
        }

        for (MemberCombination actualCombination : combinations.get(combination.size())) {
            if (actualCombination.equals(combination)) {
                return actualCombination;
            }
        }
        return null;
    }

    private static MemberCombination getCombination(Multimap<Integer, MemberCombination> combinations, List<Member> members) {
        for (MemberCombination combination : combinations.get(members.size())) {
            if (combination.containsAll(members)) {
                return combination;
            }
        }
        return null;
    }

    /**
     * Checks if the {@code Carpool} has {@link Carpool#getTurnsInThePast() turns} which happened before
     * the given date.
     *
     * @param dateTime
     * @return
     */
    public boolean hasTurnsBefore(DateTime dateTime) {
        if (turnsInThePast.isEmpty()) {
            return false;
        }
        return turnsInThePast.first().getDate().isBefore(dateTime);
    }

    /**
     * Checks if the {@code Carpool} has {@link Carpool#getTurnsInThePast() turns} until the given date.
     *
     * @param dateTime
     * @return
     */
    public boolean hasTurnsUntil(DateTime dateTime) {
        if (turnsInThePast.isEmpty()) {
            return false;
        }

        if (dateTime.isBefore(turnsInThePast.first().getDate())) {
            return false;
        }

        DateTime lastUsedDate = getLastUsedDate();
        return dateTime.isBefore(lastUsedDate) || dateTime.isEqual(lastUsedDate);
    }

    /**
     * Restores the given {@link CarpoolArchive archive}. The {@link CarpoolArchive#getInterval()} archive end}
     * must be adjacent to {@link Carpool#getFirstUsedDate() first used date}.
     *
     * @param archive
     */
    public void restore(CarpoolArchive archive) {
        if (!archive.getInterval().getEnd().isEqual(getFirstUsedDate())) {
            throw new IllegalArgumentException("The archive must be adjacent to the first used date");
        }

        if (!containsCombinationsOf(archive)) {
            throw new IllegalArgumentException("Combinations do not match to the carpool");
        }

        for (Turn turn : archive.getTurns()) {
            Turn restoredTurn = new Turn(
                    turn.getDate(),
                    getActualMember(turn.getDriver()),
                    getActualCombination(turn.getCombination())
            );
            turnsInThePast.add(restoredTurn);
        }
    }

    /**
     * Checks if the {@code Carpool} contains the {@link MemberCombination combinations} given by the
     * {@link CarpoolArchive archive}.
     *
     * @param archive
     * @return
     */
    private boolean containsCombinationsOf(CarpoolArchive archive) {
        for (MemberCombination combination : archive.getCombinations().values()) {
            MemberCombination found = null;
            for (MemberCombination actualCombination : combinations.get(combination.size())) {
                if (actualCombination.equals(combination)) {
                    found = actualCombination;
                    break;
                }
            }
            if (found == null) {
                return false;
            }
        }
        return true;
    }

    /**
     * Determines the invalid {@link Usage} for the given {@link Member member} at a specific
     * {@link DateTime date}.
     *
     * @param dateTime
     * @return
     */
    public Set<Usage> getInvalidUsage(DateTime dateTime) {
        DateTime normalizedDate = dateTime.withTimeAtStartOfDay();

        Turn turn = getTurnAt(normalizedDate);
        Set<Usage> invalidUsage = new HashSet<>();

        if (turn != null) {
            int membersWhoCanNotDrive = 0;

            for (Member m : turn.getMembers()) {
                if (Usage.HAS_TO_DRIVE.equals(m.getUsageAt(normalizedDate))) {
                    invalidUsage.add(Usage.HAS_TO_DRIVE);
                }

                if (Usage.CAN_NOT_DRIVE.equals(m.getUsageAt(normalizedDate))) {
                    ++membersWhoCanNotDrive;
                }
            }

            if (turn.getMembers().isEmpty() || membersWhoCanNotDrive + 1 == turn.getMembers().size()) {
                invalidUsage.add(Usage.CAN_NOT_DRIVE);
            }
        }

        return invalidUsage;
    }

    /**
     * @param dateTime
     * @return
     */
    public Turn getTurnAt(DateTime dateTime) {
        DateTime normalizedDate = dateTime.withTimeAtStartOfDay();

        if (normalizedDate.isAfter(getLastUsedDate())) {
            List<Turn> turns = predictTurnsUntil(normalizedDate);
            int days = (int) new Duration(getLastUsedDate(), normalizedDate).getStandardDays();
            return turns.get(days - 1);
        }

        for (Turn turn : turnsInThePast) {
            if (turn.getDate().equals(normalizedDate)) {
                return turn;
            }
        }

        return null;
    }

    /**
     * Determines how many {@link Savings money} the {@link Member} saved by using the {@code Carpool}.
     *
     * @param member
     * @param interval
     * @return
     */
    public Savings determineSavedMoney(Member member, Interval interval) {
        Member actualMember = getActualMember(member);
        if (actualMember == null) {
            throw new IllegalArgumentException("The given member is not part of the carpool");
        }

        if (interval == null) {
            throw new IllegalArgumentException("Provide a valid interval.");
        }

        long drove = 0;
        long used = 0;
        for (Turn turn : getTurnsInThePast(interval)) {
            if (!turn.contains(actualMember)) {
                continue;
            }

            ++used;

            if (turn.getDriver().equals(actualMember)) {
                ++drove;
            }
        }

        return new Savings(used, drove);
    }

    /**
     * Reverts the {@link #getTurnsInThePast()} from the {@link DateTime given date}.
     *
     * @param dateTime
     */
    public void revertTurnsFrom(final DateTime dateTime) {
        for (DateTime d = getLastUsedDate(); d.isAfter(dateTime) || d.isEqual(dateTime); d = d.minusDays(1)) {
            Turn turn = getTurnAt(d);
            if (turn == null) {
                break;
            }

            turnsInThePast.remove(turn);
            if (turn.getCombination() != null) {
                turn.revert();
            }
        }
    }

    /**
     * Helper class to predict/calculate {@link Turn turns}.
     */
    private class CalculateTurns {

        private final List<Turn> turns = new TurnList();

        private final Multimap<MemberCombination, DateTime> combinationsAndDateTimes = ArrayListMultimap.create();

        private final Interval interval;

        private CalculateTurns(DateTime start, DateTime end) {
            this.interval = new Interval(
                    start.withTimeAtStartOfDay(),
                    end.withTimeAtStartOfDay().plusDays(1)
            );
        }

        /**
         * Executes a sorting for doing a {@link CalculateTurns#doPrediction() prediction} or
         * {@link CalculateTurns#doCalculation() calculation}
         */
        private void sortCombinationsForCalculation() {
            for (DateTime d = interval.getStart(); interval.contains(d); d = d.plusDays(1)) {

                if (isCanceled(d)) {
                    // carpool is canceled
                    // no one uses the carpool
                    turns.add(new Turn(d));
                    continue;
                }

                List<Member> participatingMembers = getParticipatingMembers(d);

                if (participatingMembers.isEmpty()) {
                    // no one uses the carpool
                    turns.add(new Turn(d));
                } else if (participatingMembers.size() == 1) {
                    // only one driver
                    turns.add(new Turn(d, participatingMembers.get(0)));
                } else {
                    // multiple drivers find the right combination
                    MemberCombination combination = getCombination(combinations, participatingMembers);
                    combinationsAndDateTimes.put(combination, d);
                }
            }
        }

        /**
         * Calculates the {@link CalculateTurns#getTurns() turns}.
         */
        public void doCalculation() {
            sortCombinationsForCalculation();

            for (MemberCombination mc : combinationsAndDateTimes.keySet()) {
                turns.addAll(mc.calculateTurns(combinationsAndDateTimes.get(mc)));
            }
        }

        /**
         * Predicts the {@link CalculateTurns#getTurns() turns}.
         */
        public void doPrediction() {
            sortCombinationsForCalculation();

            for (MemberCombination mc : combinationsAndDateTimes.keySet()) {
                turns.addAll(mc.predictTurns(combinationsAndDateTimes.get(mc)));
            }
        }

        /**
         * Returns the {@link Turn turns}.
         *
         * @return
         */
        public List<Turn> getTurns() {
            Collections.sort(turns);
            return turns;
        }

        /**
         * Returns the {@link Member members} which will use the {@code Carpool} on the given
         * {@link DateTime date}.
         *
         * @param date
         * @return
         */
        private List<Member> getParticipatingMembers(DateTime date) {
            List<Member> participatingMembers = new ArrayList<>();

            for (Member m : members) {
                Usage usage = m.getUsageAt(date);
                if (usage.equals(Usage.NORMAL)
                        || usage.equals(Usage.CAN_NOT_DRIVE)
                        || usage.equals(Usage.HAS_TO_DRIVE)) {
                    participatingMembers.add(m);
                }
            }

            return participatingMembers;
        }

    }

    private class TurnTreeSet extends TreeSet<Turn> {

        @Override
        public boolean add(Turn turn) {
            turn.setCarpool(Carpool.this);
            return super.add(turn);
        }

        @Override
        public boolean addAll(Collection<? extends Turn> turns) {
            for (Turn turn : turns) {
                if (!add(turn)) {
                    return false;
                }
            }
            return true;
        }
    }

    private class TurnList extends ArrayList<Turn> {

        @Override
        public boolean add(Turn turn) {
            turn.setCarpool(Carpool.this);
            return super.add(turn);
        }

        @Override
        public boolean addAll(Collection<? extends Turn> turns) {
            for (Turn turn : turns) {
                if (!add(turn)) {
                    return false;
                }
            }
            return true;
        }
    }
}
