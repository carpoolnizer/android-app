/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.model;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.Test;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static de.schrieveslaach.carpoolnizer.model.ModelMatchers.hasDriverCount;
import static de.schrieveslaach.carpoolnizer.model.ModelMatchers.hasDrivers;
import static de.schrieveslaach.carpoolnizer.model.ModelMatchers.hasMembers;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class MemberCombinationTest {

    private static final URI JOHN = URI.create("http://tempuri.org#john");
    private Member john = new Member(JOHN);
    private static final URI DAISY = URI.create("http://tempuri.org#daisy");
    private Member daisy = new Member(DAISY);
    private static final URI MIKE = URI.create("http://tempuri.org#mike");
    private Member mike = new Member(MIKE);

    @Test
    public void shouldFullFileHashCodeContract() {
        Member m1 = new Member(JOHN);
        Member m2 = new Member(DAISY);

        MemberCombination mc = new MemberCombination(Arrays.asList(m1, m2));

        Map<MemberCombination, String> map = new HashMap<MemberCombination, String>();
        map.put(mc, "Hello");

        m1 = new Member(JOHN);
        m2 = new Member(DAISY);
        MemberCombination key = new MemberCombination(Arrays.asList(m1, m2));
        assertThat(map, hasEntry(key, "Hello"));
    }

    @Test
    public void shouldBeEquals_SameInstance() {
        MemberCombination mc = new MemberCombination(Arrays.asList(john, daisy));
        assertThat(mc, equalTo(mc));
    }

    @Test
    public void shouldBeEquals_DifferentInstance() {
        MemberCombination mc1 = new MemberCombination(Arrays.asList(john, daisy));
        MemberCombination mc2 = new MemberCombination(Arrays.asList(john, daisy));
        assertThat(mc1, equalTo(mc2));
    }

    @Test
    public void shouldNotBeEquals_WrongType() {
        MemberCombination mc = new MemberCombination(Arrays.asList(john, daisy));
        assertThat(mc, not(equalTo(new Object())));
    }

    @Test
    public void shouldNotBeEquals_Null() {
        MemberCombination mc = new MemberCombination(Arrays.asList(john, daisy));
        assertThat(mc, not(equalTo(null)));
    }

    @Test
    public void shouldNotBeEquals_DifferentMembers() {
        MemberCombination mc1 = new MemberCombination(Arrays.asList(john, daisy));
        MemberCombination mc2 = new MemberCombination(Arrays.asList(john, mike));

        assertThat(mc1, not(equalTo(mc2)));
    }

    @Test
    public void shouldBeEquals_DifferentMemberOrder() {
        MemberCombination mc1 = new MemberCombination(Arrays.asList(john, daisy, mike));
        MemberCombination mc2 = new MemberCombination(Arrays.asList(daisy, mike, john));

        assertThat(mc1, equalTo(mc2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailToCreateMemberCombination_NotEnoughMembers() {
        new MemberCombination(new ArrayList<Member>());
    }

    @Test
    public void shouldCreateMemberCombination() {
        MemberCombination mc = new MemberCombination(Arrays.asList(john, daisy));
        assertThat(mc, hasMembers(john, daisy));

        assertThat(mc, hasDriverCount(john, 0));
        assertThat(mc, hasDriverCount(daisy, 0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotGetDriverCount_MemberIsNotPartOfCombination() {
        MemberCombination mc = new MemberCombination(Arrays.asList(john, daisy));

        Member m3 = new Member(null);
        mc.getDriverCount(m3);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldFailToRemoveMember() {
        MemberCombination mc = new MemberCombination(Arrays.asList(john, daisy));
        mc.iterator().remove();
    }

    @Test
    public void shouldNotContainMembers() {
        MemberCombination mc = new MemberCombination(Arrays.asList(john, daisy));
        assertThat(mc, not(hasMembers(mike)));
    }

    @Test
    public void shouldPredictDrivers_RegularUsage() {
        john.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, true});
        daisy.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, true});
        mike.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, true});

        MemberCombination mc = new MemberCombination(Arrays.asList(john, daisy, mike));
        List<Turn> turns = mc.predictTurns(Arrays.asList(
                DateTime.now().plusDays(1),
                DateTime.now().plusDays(2),
                DateTime.now().plusDays(3),
                DateTime.now().plusDays(4)
        ));

        assertThat(turns, hasDrivers(mike, daisy, john, mike));
        assertThat(mc, hasDriverCount(john, 0));
        assertThat(mc, hasDriverCount(daisy, 0));
        assertThat(mc, hasDriverCount(mike, 0));
    }

    @Test
    public void shouldCalculateDrivers_RegularUsage() {
        john.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, true});
        daisy.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, true});
        mike.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, true});

        MemberCombination mc = new MemberCombination(Arrays.asList(john, daisy, mike));
        List<Turn> turns = mc.calculateTurns(Arrays.asList(
                DateTime.now().plusDays(1),
                DateTime.now().plusDays(2),
                DateTime.now().plusDays(3),
                DateTime.now().plusDays(4)
        ));

        assertThat(turns, hasDrivers(mike, daisy, john, mike));
        assertThat(mc, hasDriverCount(john, 1));
        assertThat(mc, hasDriverCount(daisy, 1));
        assertThat(mc, hasDriverCount(mike, 2));
    }

    @Test
    public void shouldCalculateTurns_OneHasToDrive() {
        john.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, true});
        daisy.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, true});
        mike.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, true});

        mike.addExceptionalUsage(new Interval(DateTime.now(), DateTime.now().plusDays(7)), Usage.HAS_TO_DRIVE);

        MemberCombination mc = new MemberCombination(Arrays.asList(john, daisy, mike));
        List<Turn> turns = mc.calculateTurns(Arrays.asList(
                DateTime.now().plusDays(1),
                DateTime.now().plusDays(2),
                DateTime.now().plusDays(3),
                DateTime.now().plusDays(4)
        ));

        assertThat(turns, hasDrivers(mike, mike, mike, mike));
        assertThat(mc, hasDriverCount(john, 0));
        assertThat(mc, hasDriverCount(daisy, 0));
        assertThat(mc, hasDriverCount(mike, 4));
    }

    @Test
    public void shouldCalculateTurns_TwoCannotDrive() {
        john.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, true});
        daisy.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, true});
        mike.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, true});

        daisy.addExceptionalUsage(new Interval(DateTime.now(), DateTime.now().plusDays(7)), Usage.CAN_NOT_DRIVE);
        mike.addExceptionalUsage(new Interval(DateTime.now(), DateTime.now().plusDays(7)), Usage.CAN_NOT_DRIVE);

        MemberCombination mc = new MemberCombination(Arrays.asList(john, daisy, mike));
        List<Turn> turns = mc.calculateTurns(Arrays.asList(
                DateTime.now().plusDays(1),
                DateTime.now().plusDays(2),
                DateTime.now().plusDays(3),
                DateTime.now().plusDays(4)
        ));

        assertThat(turns, hasDrivers(john, john, john, john));
        assertThat(mc, hasDriverCount(john, 4));
        assertThat(mc, hasDriverCount(daisy, 0));
        assertThat(mc, hasDriverCount(mike, 0));
    }

    @Test(expected = IllegalStateException.class)
    public void shouldNotPredictTurns_NoOneCanDriver() {
        john.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, true});
        daisy.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, true});

        john.addExceptionalUsage(new Interval(DateTime.now(), DateTime.now().plusDays(7)), Usage.CAN_NOT_DRIVE);
        daisy.addExceptionalUsage(new Interval(DateTime.now(), DateTime.now().plusDays(7)), Usage.CAN_NOT_DRIVE);

        MemberCombination mc = new MemberCombination(Arrays.asList(john, daisy));
        mc.predictTurns(Arrays.asList(
                DateTime.now().plusDays(1)
        ));
    }

    @Test(expected = IllegalStateException.class)
    public void shouldNotPredictTurns_SomeOneDoesNotUseCarpool() {
        MemberCombination mc = new MemberCombination(Arrays.asList(john, daisy, mike));
        mc.predictTurns(Arrays.asList(
                DateTime.now().plusDays(1)
        ));
    }

    @Test
    public void shouldRevertTurn() {
        john.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, true});
        daisy.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, true});
        mike.addUsageAt(DateTime.now(), new boolean[]{true, true, true, true, true, true, true});

        MemberCombination mc = new MemberCombination(Arrays.asList(john, daisy, mike));
        List<Turn> turns = mc.calculateTurns(Arrays.asList(
                DateTime.now().plusDays(1),
                DateTime.now().plusDays(2),
                DateTime.now().plusDays(3)
        ));

        assertThat(turns, hasDrivers(mike, daisy, john));
        assertThat(mc, hasDriverCount(john, 1));
        assertThat(mc, hasDriverCount(daisy, 1));
        assertThat(mc, hasDriverCount(mike, 1));

        mc.revert(turns.get(0));

        assertThat(mc, hasDriverCount(john, 1));
        assertThat(mc, hasDriverCount(daisy, 1));
        assertThat(mc, hasDriverCount(mike, 0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotRevertTurn_TurnDoesNotMatchToCombination() {
        MemberCombination mc = new MemberCombination(Arrays.asList(john, daisy, mike));
        mc.revert(new Turn(DateTime.now().withTimeAtStartOfDay()));
    }

    @Test(expected = IllegalStateException.class)
    public void shouldNotRevertTurn_ZeroDriverCount() {
        MemberCombination mc = new MemberCombination(Arrays.asList(john, daisy, mike));
        Turn turn = new Turn(DateTime.now().withTimeAtStartOfDay(), john, mc);
        mc.revert(turn);
    }

    @Test
    public void shouldContainMember() {
        MemberCombination mc = new MemberCombination(Arrays.asList(john, daisy, mike));
        assertThat(mc.contains(john), is(true));
    }

    @Test
    public void shouldNotContainMember_MemberNotPartOfCombination() {
        MemberCombination mc = new MemberCombination(Arrays.asList(daisy, mike));
        assertThat(mc.contains(john), is(false));
    }

    @Test
    public void shouldNotContainMember_NullValue() {
        MemberCombination mc = new MemberCombination(Arrays.asList(daisy, mike));
        assertThat(mc.contains(null), is(false));
    }

    @Test
    public void shouldIntersect() {
        List<Member> members = Arrays.asList(john, daisy, mike);
        MemberCombination mc = new MemberCombination(Arrays.asList(john, daisy));

        List<Member> intersection = mc.intersect(members);
        assertThat(intersection, containsInAnyOrder(john, daisy));
    }

    @Test
    public void shouldIncrementDriverCount() {
        MemberCombination mc = new MemberCombination(Arrays.asList(john, daisy));
        assertThat(mc, hasDriverCount(john, 0));
        mc.incrementDriverCount(john);
        assertThat(mc, hasDriverCount(john, 1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotIncrementDriverCount_MemberNotPartOfCombination() {
        MemberCombination mc = new MemberCombination(Arrays.asList(john, daisy));
        mc.incrementDriverCount(mike);
    }
}