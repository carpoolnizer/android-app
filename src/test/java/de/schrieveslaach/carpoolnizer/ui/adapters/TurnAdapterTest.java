/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.adapters;

import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.Button;
import android.widget.LinearLayout;

import org.hamcrest.Matchers;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.model.Member;
import de.schrieveslaach.carpoolnizer.model.Turn;
import de.schrieveslaach.carpoolnizer.ui.robolectric.fakes.ContactsCursor;
import de.schrieveslaach.carpoolnizer.ui.views.MemberReadOnlyView;

import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.gone;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.visible;
import static org.hamcrest.Matchers.arrayWithSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.robolectric.RuntimeEnvironment.application;

@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
@RunWith(RobolectricTestRunner.class)
public class TurnAdapterTest {

    private ContactsCursor contactsCursor;

    @Before
    public void fakeContactCursor() {
        contactsCursor = new ContactsCursor()
                .withContact("Mike")
                .fake();
    }

    private Turn mockTurn(DateTime date) {
        return mockTurn(date, null);
    }

    private Turn mockTurn(DateTime date, Member driver, Member... passengers) {
        Turn turn = mock(Turn.class);
        when(turn.getDate()).thenReturn(date);
        when(turn.getDriver()).thenReturn(driver);
        when(turn.getPassengers()).thenReturn(Arrays.asList(passengers));
        return turn;
    }

    @Test
    public void shouldSetTurns() {
        DateTime date = DateTime.now().withTimeAtStartOfDay();
        Turn ty = mockTurn(date.minusDays(1));
        Turn t1 = mockTurn(date);
        Turn t2 = mockTurn(date.plusDays(1));

        RecyclerView.AdapterDataObserver dataObserver = mock(RecyclerView.AdapterDataObserver.class);
        TurnAdapter adapter = new TurnAdapterBuilder()
                .withDataObserver(dataObserver)
                .withTurnsInThePast(ty)
                .withTurns(t1, t2)
                .create();

        verify(dataObserver).onChanged();
        assertThat(adapter.getItemCount(), is(3));
    }

    @Test
    public void shouldAddArchivedTurns() {
        DateTime date = DateTime.now().withTimeAtStartOfDay();
        Turn ta = mockTurn(date.minusDays(2));
        Turn ty = mockTurn(date.minusDays(1));
        Turn t1 = mockTurn(date);
        Turn t2 = mockTurn(date.plusDays(1));

        RecyclerView.AdapterDataObserver dataObserver = mock(RecyclerView.AdapterDataObserver.class);
        TurnAdapter adapter = new TurnAdapterBuilder()
                .withArchivedTurns(ta)
                .withDataObserver(dataObserver)
                .withTurnsInThePast(ty)
                .withTurns(t1, t2)
                .create();

        verify(dataObserver, times(2)).onChanged();
        assertThat(adapter.getItemCount(), is(4));
    }

    @Test
    public void shouldBindViewHolder_PredictedTurn() {
        DateTime date = DateTime.now().withTimeAtStartOfDay();
        Turn ta = mockTurn(date.minusDays(2));
        Turn ty = mockTurn(date.minusDays(1));
        Turn t1 = mockTurn(date);
        Turn t2 = mockTurn(date.plusDays(1));

        TurnAdapter adapter = new TurnAdapterBuilder()
                .withArchivedTurns(ta)
                .withTurnsInThePast(ty)
                .withTurns(t1, t2)
                .create();

        TurnViewHolder viewHolder = adapter.createViewHolder(new LinearLayout(application), 2);
        adapter.onBindViewHolder(viewHolder, 2);
        assertThat(viewHolder.getTurn(), is(t1));
    }

    @Test
    public void shouldBindViewHolder_TurnInThePast() {
        DateTime date = DateTime.now().withTimeAtStartOfDay();
        Turn ta = mockTurn(date.minusDays(2));
        Turn ty = mockTurn(date.minusDays(1));
        Turn t1 = mockTurn(date);
        Turn t2 = mockTurn(date.plusDays(1));

        TurnAdapter adapter = new TurnAdapterBuilder()
                .withArchivedTurns(ta)
                .withTurnsInThePast(ty)
                .withTurns(t1, t2)
                .create();

        TurnViewHolder viewHolder = adapter.createViewHolder(new LinearLayout(application), 1);
        adapter.onBindViewHolder(viewHolder, 1);
        assertThat(viewHolder.getTurn(), is(ty));
    }

    @Test
    public void shouldBindViewHolder_ArchivedTurn() {
        DateTime date = DateTime.now().withTimeAtStartOfDay();
        Turn ta = mockTurn(date.minusDays(2));
        Turn ty = mockTurn(date.minusDays(1));
        Turn t1 = mockTurn(date);
        Turn t2 = mockTurn(date.plusDays(1));

        TurnAdapter adapter = new TurnAdapterBuilder()
                .withArchivedTurns(ta)
                .withTurnsInThePast(ty)
                .withTurns(t1, t2)
                .create();

        TurnViewHolder viewHolder = adapter.createViewHolder(new LinearLayout(application), 0);
        adapter.onBindViewHolder(viewHolder, 0);
        assertThat(viewHolder.getTurn(), is(ta));
    }

    @Test
    public void shouldGetFirstPredictedTurnIndex_WithoutTurnsInThePastAndArchivedTurns() {
        DateTime date = DateTime.now().withTimeAtStartOfDay();
        Turn t1 = mockTurn(date);
        Turn t2 = mockTurn(date.plusDays(1));

        TurnAdapter adapter = new TurnAdapterBuilder()
                .withTurns(t1, t2)
                .create();

        assertThat(adapter.getFirstPredictedTurnIndex(), is(0));
    }

    @Test
    public void shouldGetFirstPredictedTurnIndex_WithoutArchivedTurns() {
        DateTime date = DateTime.now().withTimeAtStartOfDay();
        Turn ty = mockTurn(date.minusDays(1));
        Turn t1 = mockTurn(date);
        Turn t2 = mockTurn(date.plusDays(1));

        TurnAdapter adapter = new TurnAdapterBuilder()
                .withTurnsInThePast(ty)
                .withTurns(t1, t2)
                .create();

        assertThat(adapter.getFirstPredictedTurnIndex(), is(1));
    }

    @Test
    public void shouldGetFirstPredictedTurnIndex() {
        DateTime date = DateTime.now().withTimeAtStartOfDay();
        Turn ta = mockTurn(date.minusDays(2));
        Turn ty = mockTurn(date.minusDays(1));
        Turn t1 = mockTurn(date);
        Turn t2 = mockTurn(date.plusDays(1));

        TurnAdapter adapter = new TurnAdapterBuilder()
                .withArchivedTurns(ta)
                .withTurnsInThePast(ty)
                .withTurns(t1, t2)
                .create();

        assertThat(adapter.getFirstPredictedTurnIndex(), is(2));
    }

    @Test
    public void shouldClickOnTurn() {
        DateTime date = DateTime.now().withTimeAtStartOfDay();
        Turn t1 = mockTurn(date);
        Turn t2 = mockTurn(date.plusDays(1));

        TurnAdapter.OnTurnClickListener listener = mock(TurnAdapter.OnTurnClickListener.class);
        TurnAdapter adapter = new TurnAdapterBuilder()
                .withOnTurnClickListener(listener)
                .withTurns(t1, t2)
                .create();

        TurnViewHolder viewHolder = adapter.createViewHolder(new LinearLayout(application), 0);
        adapter.onBindViewHolder(viewHolder, 0);

        viewHolder.itemView.performClick();

        verify(listener).onClick(t1);
    }

    @Test
    public void shouldLongClickOnTurn() {
        DateTime date = DateTime.now().withTimeAtStartOfDay();
        Turn t1 = mockTurn(date);
        Turn t2 = mockTurn(date.plusDays(1));

        TurnAdapter.OnTurnClickListener listener = mock(TurnAdapter.OnTurnClickListener.class);
        TurnAdapter adapter = new TurnAdapterBuilder()
                .withOnTurnClickListener(listener)
                .withTurns(t1, t2)
                .create();

        TurnViewHolder viewHolder = adapter.createViewHolder(new LinearLayout(application), 0);
        adapter.onBindViewHolder(viewHolder, 0);

        viewHolder.itemView.performLongClick();

        verify(listener).onLongClick(t1);
    }

    @Test
    public void shouldGetIndexOfDate_DateInArchivedTurns() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Turn ta = mockTurn(today.minusDays(1));
        Turn ty = mockTurn(today);
        Turn t1 = mockTurn(today.plusDays(1));
        Turn t2 = mockTurn(today.plusDays(2));

        TurnAdapter adapter = new TurnAdapterBuilder()
                .withArchivedTurns(ta)
                .withTurnsInThePast(ty)
                .withTurns(t1, t2)
                .create();

        int index = adapter.getIndexOfTurn(today);
        assertThat(index, is(1));
    }

    @Test
    public void shouldGetIndexOfDate_DateInThePastTurns() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Turn t1 = mockTurn(today.minusDays(1));
        Turn t2 = mockTurn(today);
        Turn t3 = mockTurn(today.plusDays(1));
        Turn t4 = mockTurn(today.plusDays(2));

        TurnAdapter adapter = new TurnAdapterBuilder()
                .withTurnsInThePast(t1, t2)
                .withTurns(t3, t4)
                .create();

        int index = adapter.getIndexOfTurn(today);
        assertThat(index, is(1));
    }

    @Test
    public void shouldGetIndexOfDate_DateInFutureTurns() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Turn t1 = mockTurn(today.minusDays(1));
        Turn t2 = mockTurn(today);
        Turn t3 = mockTurn(today.plusDays(1));
        Turn t4 = mockTurn(today.plusDays(2));

        TurnAdapter adapter = new TurnAdapterBuilder()
                .withTurnsInThePast(t1, t2)
                .withTurns(t3, t4)
                .create();

        int index = adapter.getIndexOfTurn(today.plusDays(1));
        assertThat(index, is(2));
    }

    @Test
    public void shouldNotGetIndexOfDate_DateNotInTheTurns() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Turn ta = mockTurn(today.minusDays(1));
        Turn ty = mockTurn(today);
        Turn t1 = mockTurn(today.plusDays(1));
        Turn t2 = mockTurn(today.plusDays(2));

        TurnAdapter adapter = new TurnAdapterBuilder()
                .withArchivedTurns(ta)
                .withTurnsInThePast(ty)
                .withTurns(t1, t2)
                .create();

        int index = adapter.getIndexOfTurn(today.plusMonths(1));
        assertThat(index, is(-1));
    }

    @Test(expected = IllegalStateException.class)
    public void shouldNotShowLoadArchivedTurnsButton_NoTurnsInThePast() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Turn t1 = mockTurn(today);
        Turn t2 = mockTurn(today.plusDays(1));

        TurnAdapter adapter = new TurnAdapterBuilder()
                .withTurns(t1, t2)
                .create();

        adapter.setShowLoadArchivedTurnsButton(true);
    }

    @Test
    public void shouldShowLoadArchivedTurnsButton_NotifyDataChanged() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Turn t1 = mockTurn(today.minusDays(2));
        Turn t2 = mockTurn(today.minusDays(1));
        Turn t3 = mockTurn(today);
        Turn t4 = mockTurn(today.plusDays(1));

        RecyclerView.AdapterDataObserver dataObserver = mock(RecyclerView.AdapterDataObserver.class);
        TurnAdapter adapter = new TurnAdapterBuilder()
                .withDataObserver(dataObserver)
                .withTurnsInThePast(t1, t2)
                .withTurns(t3, t4)
                .create();

        adapter.setShowLoadArchivedTurnsButton(true);
        verify(dataObserver, times(2)).onChanged();
    }

    @Test
    public void shouldShowLoadArchivedTurnsButton_ButtonVisible_WithArchivedTurns() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Turn ta = mockTurn(today.minusDays(2));
        Turn ty = mockTurn(today.minusDays(1));
        Turn t1 = mockTurn(today);
        Turn t2 = mockTurn(today.plusDays(1));

        TurnAdapter adapter = new TurnAdapterBuilder()
                .withArchivedTurns(ta)
                .withTurnsInThePast(ty)
                .withTurns(t1, t2)
                .create();

        adapter.setShowLoadArchivedTurnsButton(true);

        TurnViewHolder viewHolder = adapter.createViewHolder(new LinearLayout(application), 0);
        adapter.onBindViewHolder(viewHolder, 0);

        Button buLoadArchivedTurns = (Button) viewHolder.itemView.findViewById(R.id.bu_load_archived_turns);
        assertThat(buLoadArchivedTurns, is(visible()));
    }

    @Test
    public void shouldNotShowLoadArchivedTurnsButton_HideButton() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Turn t1 = mockTurn(today.minusDays(2));
        Turn t2 = mockTurn(today.minusDays(1));
        Turn t3 = mockTurn(today);
        Turn t4 = mockTurn(today.plusDays(1));

        TurnAdapter adapter = new TurnAdapterBuilder()
                .withTurnsInThePast(t1, t2)
                .withTurns(t3, t4)
                .withLoadArchivedTurnsButton()
                .create();

        TurnViewHolder viewHolder = adapter.createViewHolder(new LinearLayout(application), 0);
        adapter.onBindViewHolder(viewHolder, 0);

        Button buLoadArchivedTurns = (Button) viewHolder.itemView.findViewById(R.id.bu_load_archived_turns);
        assertThat(buLoadArchivedTurns, is(visible()));

        adapter.setShowLoadArchivedTurnsButton(false);
        adapter.onBindViewHolder(viewHolder, 0);
        assertThat(buLoadArchivedTurns, is(gone()));
    }

    @Test
    public void shouldNotifyToLoadArchivedTurns() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Turn t1 = mockTurn(today.minusDays(2));
        Turn t2 = mockTurn(today.minusDays(1));
        Turn t3 = mockTurn(today);
        Turn t4 = mockTurn(today.plusDays(1));

        TurnAdapter.OnLoadArchivedTurnsListener archivedTurnsListener = mock(TurnAdapter.OnLoadArchivedTurnsListener.class);
        TurnAdapter adapter = new TurnAdapterBuilder()
                .withOnLoadArchivedTurnsListener(archivedTurnsListener)
                .withTurnsInThePast(t1, t2)
                .withTurns(t3, t4)
                .withLoadArchivedTurnsButton()
                .create();

        TurnViewHolder viewHolder = adapter.createViewHolder(new LinearLayout(application), 0);
        adapter.onBindViewHolder(viewHolder, 0);

        Button buLoadArchivedTurns = (Button) viewHolder.itemView.findViewById(R.id.bu_load_archived_turns);
        buLoadArchivedTurns.performClick();

        verify(archivedTurnsListener).onLoadArchivedTurns();
    }

    @Test
    public void shouldNotifyToViewContactInformation() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Member member = contactsCursor.createMember("Mike");
        Turn t1 = mockTurn(today, member);

        MemberReadOnlyView.OnContactInformationListener listener = mock(MemberReadOnlyView.OnContactInformationListener.class);
        TurnAdapter adapter = new TurnAdapterBuilder()
                .withOnContactInformationListener(listener)
                .withTurns(t1)
                .create();

        TurnViewHolder viewHolder = adapter.createViewHolder(new LinearLayout(application), 0);
        adapter.onBindViewHolder(viewHolder, 0);
        viewHolder.itemView.findViewById(R.id.iv_contact_image).performClick();

        verify(listener).onContactInformation(member);
    }

    @Test
    public void shouldProvideEmptySections_NoWeek() {
        TurnAdapter turnAdapter = new TurnAdapterBuilder().create();
        Object[] sections = turnAdapter.getSections();
        assertThat(sections, arrayWithSize(0));
    }

    @Test
    public void shouldProvideSections_OneWeek() {
        TurnAdapterBuilder builder = new TurnAdapterBuilder();

        for (int dayOfWeek = 1; dayOfWeek <= 7; ++dayOfWeek) {
            DateTime dt = new DateTime()
                    .withYear(2016)
                    .withWeekOfWeekyear(3)
                    .withDayOfWeek(dayOfWeek);

            Turn turn = mockTurn(dt);
            builder.withTurns(turn);
        }

        TurnAdapter turnAdapter = builder.create();
        Object[] sections = turnAdapter.getSections();

        assertThat(sections, arrayWithSize(1));
        assertThat(sections[0], Matchers.<Object>is("CW 3"));
    }

    @Test
    public void shouldProvideSections_SevenTurns_OverlappingCalenderWeek() {
        TurnAdapterBuilder builder = new TurnAdapterBuilder();

        DateTime dt = new DateTime()
                .withYear(2016)
                .withWeekOfWeekyear(3)
                .withDayOfWeek(3);
        for (int dayOfWeek = 1; dayOfWeek <= 7; ++dayOfWeek) {
            Turn turn = mockTurn(dt.plusDays(dayOfWeek));
            builder.withTurns(turn);
        }

        TurnAdapter turnAdapter = builder.create();
        Object[] sections = turnAdapter.getSections();

        assertThat(sections, arrayWithSize(2));
        assertThat(sections[0], Matchers.<Object>is("CW 3"));
        assertThat(sections[1], Matchers.<Object>is("CW 4"));
    }

    @Test
    public void shouldProvideSections_ThreeWeeks_OverlappingCalenderWeek() {
        TurnAdapterBuilder builder = new TurnAdapterBuilder();

        DateTime dt = new DateTime()
                .withYear(2016)
                .withWeekOfWeekyear(3)
                .withDayOfWeek(3);
        for (int dayOfWeek = 1; dayOfWeek <= 21; ++dayOfWeek) {
            Turn turn = mockTurn(dt.plusDays(dayOfWeek));
            builder.withTurns(turn);
        }

        TurnAdapter turnAdapter = builder.create();
        Object[] sections = turnAdapter.getSections();

        assertThat(sections, arrayWithSize(4));
        assertThat(sections[0], Matchers.<Object>is("CW 3"));
        assertThat(sections[1], Matchers.<Object>is("CW 4"));
        assertThat(sections[2], Matchers.<Object>is("CW 5"));
        assertThat(sections[3], Matchers.<Object>is("CW 6"));
    }

    @Test
    public void shouldProvideSection_SevenTurns_OverlappingYear() {
        TurnAdapterBuilder builder = new TurnAdapterBuilder();

        DateTime dt = new DateTime()
                .withYear(2015)
                .withMonthOfYear(12)
                .withDayOfMonth(30);
        for (int dayOfWeek = 1; dayOfWeek <= 7; ++dayOfWeek) {
            Turn turn = mockTurn(dt.plusDays(dayOfWeek));
            builder.withTurns(turn);
        }

        TurnAdapter turnAdapter = builder.create();
        Object[] sections = turnAdapter.getSections();

        assertThat(sections, arrayWithSize(2));
        assertThat(sections[0], Matchers.<Object>is("CW 53"));
        assertThat(sections[1], Matchers.<Object>is("CW 1"));
    }

    @Test
    public void shouldReturnPosition_BySectionIndex_SevenTurns() {
        TurnAdapterBuilder builder = new TurnAdapterBuilder();

        for (int dayOfWeek = 1; dayOfWeek <= 7; ++dayOfWeek) {
            DateTime dt = new DateTime()
                    .withYear(2016)
                    .withWeekOfWeekyear(3)
                    .withDayOfWeek(dayOfWeek);

            Turn turn = mockTurn(dt);
            builder.withTurns(turn);
        }

        TurnAdapter turnAdapter = builder.create();

        int positionForSection = turnAdapter.getPositionForSection(0);
        assertThat(positionForSection, is(0));
    }

    @Test
    public void shouldReturnPosition_BySectionIndex_SevenTurns_OverlappingCalenderWeek() {
        TurnAdapterBuilder builder = new TurnAdapterBuilder();

        DateTime dt = new DateTime()
                .withYear(2016)
                .withWeekOfWeekyear(3)
                .withDayOfWeek(3);
        for (int dayOfWeek = 1; dayOfWeek <= 7; ++dayOfWeek) {
            Turn turn = mockTurn(dt.plusDays(dayOfWeek));
            builder.withTurns(turn);
        }

        TurnAdapter turnAdapter = builder.create();

        int positionForSection = turnAdapter.getPositionForSection(1);
        assertThat(positionForSection, is(4));
    }

    @Test
    public void shouldReturnSection_ByPosition_SevenTurns() {
        TurnAdapterBuilder builder = new TurnAdapterBuilder();

        for (int dayOfWeek = 1; dayOfWeek <= 7; ++dayOfWeek) {
            DateTime dt = new DateTime()
                    .withYear(2016)
                    .withWeekOfWeekyear(3)
                    .withDayOfWeek(dayOfWeek);

            Turn turn = mockTurn(dt);
            builder.withTurns(turn);
        }

        TurnAdapter turnAdapter = builder.create();
        int section = turnAdapter.getSectionForPosition(0);
        assertThat(section, is(0));
    }

    @Test
    public void shouldReturnSection_ByPosition_SevenTurns_OverlappingCalenderWeek() {
        TurnAdapterBuilder builder = new TurnAdapterBuilder();

        DateTime dt = new DateTime()
                .withYear(2016)
                .withWeekOfWeekyear(3)
                .withDayOfWeek(3);
        for (int dayOfWeek = 1; dayOfWeek <= 7; ++dayOfWeek) {
            Turn turn = mockTurn(dt.plusDays(dayOfWeek));
            builder.withTurns(turn);
        }

        TurnAdapter turnAdapter = builder.create();
        int section = turnAdapter.getSectionForPosition(6);
        assertThat(section, is(1));
    }

    @Test
    public void shouldReturnSection_ByPostion_PositionAfterLastIndex() {
        TurnAdapterBuilder builder = new TurnAdapterBuilder();

        DateTime dt = new DateTime()
                .withYear(2016)
                .withWeekOfWeekyear(3)
                .withDayOfWeek(3);

        for (int dayOfWeek = 1; dayOfWeek <= 7; ++dayOfWeek) {
            Turn turn = mockTurn(dt.minusWeeks(1).plusDays(dayOfWeek));
            builder.withTurnsInThePast(turn);
        }
        for (int dayOfWeek = 1; dayOfWeek <= 7; ++dayOfWeek) {
            Turn turn = mockTurn(dt.plusDays(dayOfWeek));
            builder.withTurns(turn);
        }

        TurnAdapter turnAdapter = builder.create();
        int section = turnAdapter.getSectionForPosition(turnAdapter.getItemCount());
        assertThat(section, is(2));
    }

    private static class TurnAdapterBuilder {

        private RecyclerView.AdapterDataObserver dataObserver;

        private TurnAdapter.OnTurnClickListener onTurnClickListener;

        private TurnAdapter.OnLoadArchivedTurnsListener onLoadArchivedTurnsListener;

        private MemberReadOnlyView.OnContactInformationListener onContactInformationListener;

        private Boolean showLoadArchivedTurnsButton;

        private final List<Turn> archivedTurns = new ArrayList<Turn>();
        private final List<Turn> turnsInThePast = new ArrayList<Turn>();
        private final List<Turn> turns = new ArrayList<Turn>();

        public TurnAdapterBuilder withDataObserver(RecyclerView.AdapterDataObserver dataObserver) {
            this.dataObserver = dataObserver;
            return this;
        }

        public TurnAdapterBuilder withOnTurnClickListener(TurnAdapter.OnTurnClickListener onTurnClickListener) {
            this.onTurnClickListener = onTurnClickListener;
            return this;
        }

        public TurnAdapterBuilder withOnLoadArchivedTurnsListener(TurnAdapter.OnLoadArchivedTurnsListener onLoadArchivedTurnsListener) {
            this.onLoadArchivedTurnsListener = onLoadArchivedTurnsListener;
            return this;
        }

        public TurnAdapterBuilder withOnContactInformationListener(MemberReadOnlyView.OnContactInformationListener onContactInformationListener) {
            this.onContactInformationListener = onContactInformationListener;
            return this;
        }

        public TurnAdapterBuilder withArchivedTurns(Turn... turns) {
            this.archivedTurns.addAll(Arrays.asList(turns));
            return this;
        }

        public TurnAdapterBuilder withTurnsInThePast(Turn... turns) {
            this.turnsInThePast.addAll(Arrays.asList(turns));
            return this;
        }

        public TurnAdapterBuilder withTurns(Turn... turns) {
            this.turns.addAll(Arrays.asList(turns));
            return this;
        }

        public TurnAdapterBuilder withLoadArchivedTurnsButton() {
            this.showLoadArchivedTurnsButton = true;
            return this;
        }

        public TurnAdapter create() {
            TurnAdapter adapter = new TurnAdapter(RuntimeEnvironment.application.getApplicationContext());

            if (dataObserver != null) {
                adapter.registerAdapterDataObserver(dataObserver);
            }

            if (onTurnClickListener != null) {
                adapter.setOnTurnClickListener(onTurnClickListener);
            }

            if (onLoadArchivedTurnsListener != null) {
                adapter.setOnLoadArchivedTurnsListener(onLoadArchivedTurnsListener);
            }

            if (onContactInformationListener != null) {
                adapter.setOnContactInformationListener(onContactInformationListener);
            }

            if (!turns.isEmpty() || !turnsInThePast.isEmpty()) {
                adapter.setTurns(turnsInThePast, turns);
            }

            if (!archivedTurns.isEmpty()) {
                adapter.addArchivedTurns(archivedTurns);
            }

            if (showLoadArchivedTurnsButton != null) {
                adapter.setShowLoadArchivedTurnsButton(showLoadArchivedTurnsButton);
            }

            return adapter;
        }
    }
} 

