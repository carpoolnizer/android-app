/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui.views;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.junit.Before;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.model.Member;
import de.schrieveslaach.carpoolnizer.ui.RobolectricTest;
import de.schrieveslaach.carpoolnizer.ui.robolectric.fakes.ContactsCursor;

import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.clickable;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.hasText;
import static de.schrieveslaach.carpoolnizer.ui.views.MemberReadOnlyView.OnContactInformationListener;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.robolectric.RuntimeEnvironment.application;

@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
public class MemberReadOnlyViewTest extends RobolectricTest {

    private MemberReadOnlyView mrov;
    private MemberReadOnlyView.NoMemberDrawableResolver resolver;

    @Before
    public void createMemberReadOnlyView() {
        mrov = new MemberReadOnlyView(application);

        resolver = mock(MemberReadOnlyView.NoMemberDrawableResolver.class);
        BitmapDrawable drawable = (BitmapDrawable) RuntimeEnvironment.application
                .getApplicationContext()
                .getResources()
                .getDrawable(R.drawable.ic_action_coffee);
        when(resolver.resolveBitmap(any(Context.class))).thenReturn(drawable);

        mrov.setNoMemberDrawableResolver(resolver);
    }

    @Before
    public void initContactCursor() {
        new ContactsCursor()
                .fake();
    }

    @Test
    public void shouldDisplayMember_PhoneUser() {
        Member member = new Member(null);
        mrov.display(member);

        TextView tvMemberName = (TextView) mrov.findViewById(R.id.tv_member_name);
        assertThat(tvMemberName, hasText("Me"));
    }

    @Test
    public void shouldNotDisplayMember_NullMember() {
        mrov.display((Member) null);

        TextView tvMemberName = (TextView) mrov.findViewById(R.id.tv_member_name);
        assertThat(tvMemberName, hasText("-"));

        ImageView ivContactImage = (ImageView) mrov.findViewById(R.id.iv_contact_image);
        assertThat(ivContactImage, is(not(clickable())));
    }

    @Test
    public void shouldHaveContactImageListener_OnContactInformationListenerIsSet_BeforeDisplayingMember() {
        mrov.setOnContactInformationListener(mock(OnContactInformationListener.class));
        mrov.display(new Member(null));

        View ivContactImage = mrov.findViewById(R.id.iv_contact_image);
        assertThat(ivContactImage, is(clickable()));
    }

    @Test
    public void shouldHaveContactImageListener_OnContactInformationListenerIsSet_AfterDisplayingMember() {
        mrov.display(new Member(null));
        mrov.setOnContactInformationListener(mock(OnContactInformationListener.class));

        View ivContactImage = mrov.findViewById(R.id.iv_contact_image);
        assertThat(ivContactImage, is(clickable()));
    }

    @Test
    public void shouldNotHaveContactImageListener_OnContactInformationListenerIsNotSet() {
        mrov.display(new Member(null));
        View ivContactImage = mrov.findViewById(R.id.iv_contact_image);
        assertThat(ivContactImage, is(not(clickable())));
    }

    @Test
    public void shouldInvokeContactInformationEvent_ClickOnContactImage_WithMember() {
        OnContactInformationListener listener = mock(OnContactInformationListener.class);
        mrov.setOnContactInformationListener(listener);

        Member m = new Member(null);
        mrov.display(m);

        mrov.findViewById(R.id.iv_contact_image).performClick();
        verify(listener).onContactInformation(m);
    }

    @Test
    public void shouldNotInvokeContactInformationEvent_ClickOnContactImage_NoMember() {
        OnContactInformationListener listener = mock(OnContactInformationListener.class);
        mrov.setOnContactInformationListener(listener);

        mrov.findViewById(R.id.iv_contact_image).performClick();
        verify(listener, never()).onContactInformation(any(Member.class));
    }

    @Test
    public void shouldNotFailToClickOnContactImage_OnContactInformationListenerIsMissing() {
        Member m = new Member(null);
        mrov.display(m);
        mrov.findViewById(R.id.iv_contact_image).performClick();
    }

    @Test
    public void shouldResolveNoMemberDrawable_NoMember() {
        mrov.display((Member) null);

        verify(resolver).resolveBitmap(any(Context.class));
    }

    @Test
    public void shouldNotResolveNoMemberDrawable_HasMember() {
        Member m = new Member(null);
        mrov.display(m);

        verify(resolver, never()).resolveBitmap(any(Context.class));
    }
}
