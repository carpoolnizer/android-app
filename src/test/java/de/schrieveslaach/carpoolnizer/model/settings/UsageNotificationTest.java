/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.model.settings;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.joda.time.DateTime;
import org.junit.Test;

import static net.javacrumbs.jsonunit.JsonMatchers.jsonEquals;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class UsageNotificationTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateUsageNotification_WrongHour() {
        new UsageNotification(25, 33);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateUsageNotification_WrongMinutes() {
        new UsageNotification(12, 99);
    }

    @Test
    public void shouldSerializeAsJson() throws Exception {
        String expectedJson = "{ \"time\": \"07:00\" }";

        UsageNotification notification = new UsageNotification(7, 00);
        String json = new ObjectMapper().writeValueAsString(notification);

        assertThat(json, jsonEquals(expectedJson));
    }

    @Test
    public void shouldDeserializeFromJson() throws Exception {
        String json = "{ \"time\": \"07:00\" }";

        UsageNotification notification = new ObjectMapper().readValue(json, UsageNotification.class);
        assertThat(notification.getPeriod().getHours(), is(7));
        assertThat(notification.getPeriod().getMinutes(), is(0));
    }

    @Test
    public void shouldConvertToMillis() {
        UsageNotification notification = new UsageNotification(7, 0);
        long millis = notification.toMillis();

        DateTime dateTime = DateTime.now().withTimeAtStartOfDay().withHourOfDay(7);
        assertThat(millis, is(dateTime.getMillis()));
    }
}