/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.model;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMultimap;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static de.schrieveslaach.carpoolnizer.model.ModelMatchers.containsCombination;
import static de.schrieveslaach.carpoolnizer.model.ModelMatchers.containsMembers;
import static de.schrieveslaach.carpoolnizer.model.ModelMatchers.hasCarpoolReference;
import static de.schrieveslaach.carpoolnizer.model.ModelMatchers.hasDriverCount;
import static de.schrieveslaach.carpoolnizer.model.ModelMatchers.hasDrivers;
import static de.schrieveslaach.carpoolnizer.model.ModelMatchers.hasTurnsInThePastFor;
import static de.schrieveslaach.carpoolnizer.model.ModelMatchers.withDriverCount;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class CarpoolTest {

    @Before
    public void init() {
        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .create();
        assertThat(carpool.getName(), is(notNullValue()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotChangeName_NullValue() {
        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .create();
        carpool.setName(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotChangeName_InvalidString() {
        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .create();
        carpool.setName("\n");
    }

    @Test
    public void shouldChangeName() {
        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .create();
        carpool.setName("Another Name");
        assertThat(carpool.getName(), is("Another Name"));
    }

    @Test
    public void shouldAddMembers() {
        Carpool carpool = new Carpool("Name");

        Member m1 = new Member(URI.create("http://tempuri.org#johndoe"));
        assertTrue(carpool.add(m1));
        assertThat(carpool.size(), is(1));

        Member m2 = new Member(URI.create("http://tempuri.org#johndoe"));
        assertFalse(carpool.add(m2));
        assertThat(carpool.size(), is(1));
    }

    @Test
    public void shouldCreateAndGetCombinations() {
        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMember("john")
                .withMember("daisy")
                .withMember("bill");
        Carpool carpool = builder.create();

        Member m1 = builder.getMember("john");
        Member m2 = builder.getMember("daisy");
        Member m3 = builder.getMember("bill");

        assertThat(carpool, containsCombination(m1, m2));
        assertThat(carpool, containsCombination(m1, m3));
        assertThat(carpool, containsCombination(m1, m2, m3));
    }

    @Test
    public void shouldNotContainCombinations_TooManyOrNotEnoughParameters() {
        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMember("john")
                .withMember("daisy");
        Carpool carpool = builder.create();

        Member m1 = builder.getMember("john");
        Member m2 = builder.getMember("daisy");
        Member m3 = new Member(URI.create("http://tempuri.org#bill"));

        assertThat(carpool, not(containsCombination(m1)));
        assertThat(carpool, not(containsCombination(m1, m2, m3)));
    }

    @Test
    public void shouldNotContainCombinations_MemberNotInCarpool() {
        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMember("john")
                .withMember("daisy");
        Carpool carpool = builder.create();

        Member m1 = builder.getMember("john");
        Member m3 = new Member(URI.create("http://tempuri.org#bill"));

        assertThat(carpool, not(containsCombination(m1, m3)));
    }

    @Test
    public void shouldPredictTurns_TurnsHaveCarpoolReference() {
        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy")
                .create();

        List<Turn> turns = carpool.predictTurnsUntil(new DateTime().plusDays(3));
        assertThat(turns, everyItem(hasCarpoolReference(carpool)));
    }

    @Test
    public void shouldPredictTurns_RegularUsageOfWholeCarpool() {
        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy");
        Carpool carpool = builder.create();

        Member m1 = builder.getMember("john");
        Member m2 = builder.getMember("daisy");

        List<Turn> turns = carpool.predictTurnsUntil(new DateTime().plusDays(3));
        assertThat(turns, hasDrivers(m2, m1, m2, m1));
    }

    @Test
    public void shouldPredictTurns_NoUsageOfWholeCarpool() {
        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMember("john")
                .withMember("daisy")
                .create();

        List<Turn> turns = carpool.predictTurnsUntil(new DateTime().plusDays(2));
        assertThat(turns, hasDrivers(null, null, null));
    }

    @Test
    public void shouldPredictTurns_RegularUsageOfOneMember() {
        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMember("daisy");
        Carpool carpool = builder.create();

        Member m1 = builder.getMember("john");

        List<Turn> turns = carpool.predictTurnsUntil(new DateTime().plusDays(3));
        assertThat(turns, hasDrivers(m1, m1, m1, m1));
    }

    @Test
    public void shouldNotPredictTurns_DateInThePast() {
        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy")
                .create();

        List<Turn> turns = carpool.predictTurnsUntil(DateTime.now().minusDays(1));
        assertThat(turns, is(empty()));
    }

    @Test
    public void shouldNotCalculateTurns_CalculationDateBeforeLastUsedDate() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy")
                .withCalculationDate(today)
                .create();

        List<Turn> turns = carpool.calculateTurnsUntil(today.minusDays(10));
        assertThat(turns, is(empty()));
    }

    @Test
    public void shouldNotCalculateTurns_CalculationDateEqualsLastUsedDate() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy")
                .withCalculationDate(today)
                .create();

        List<Turn> turns = carpool.calculateTurnsUntil(today);
        assertThat(turns, is(empty()));
    }

    @Test
    public void shouldCalculateTurns_TurnsHaveCarpoolReference() {
        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy")
                .create();

        List<Turn> turns = carpool.calculateTurnsUntil(DateTime.now().plusDays(3));
        assertThat(turns, everyItem(hasCarpoolReference(carpool)));
    }

    @Test
    public void shouldCalculateTurns_RegularUsageOfWholeCarpool() {
        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy");
        Carpool carpool = builder.create();

        Member m1 = builder.getMember("john");
        Member m2 = builder.getMember("daisy");

        List<Turn> turns = carpool.calculateTurnsUntil(DateTime.now().plusDays(3));
        assertThat(turns, hasDrivers(m2, m1, m2, m1));

        assertThat(carpool.getLastUsedDate(), is(DateTime.now().withTimeAtStartOfDay().plusDays(3)));

        turns = carpool.getTurnsInThePast();
        assertThat(turns, hasDrivers(m2, m1, m2, m1));
    }

    @Test
    public void shouldCalculateTurns_MultipleCalculations() {
        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy");
        Carpool carpool = builder.create();

        Member m1 = builder.getMember("john");
        Member m2 = builder.getMember("daisy");

        carpool.calculateTurnsUntil(DateTime.now());
        carpool.calculateTurnsUntil(DateTime.now().plusDays(1));
        carpool.calculateTurnsUntil(DateTime.now().plusDays(2));
        carpool.calculateTurnsUntil(DateTime.now().plusDays(3));

        List<Turn> turns = carpool.getTurnsInThePast();
        assertThat(turns, hasDrivers(m2, m1, m2, m1));
    }

    @Test
    public void shouldNotCalculateTurns_DateInThePast() {
        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy");
        Carpool carpool = builder.create();

        List<Turn> turns = carpool.calculateTurnsUntil(DateTime.now().minusDays(1));
        assertThat(turns, is(empty()));

        turns = carpool.getTurnsInThePast();
        assertThat(turns, is(empty()));
    }

    @Test
    public void shouldRecalculateMultipleTimes_DefineExceptionalRulesInThePast() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime aMonthAgo = today.minusDays(29);

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john", aMonthAgo)
                .withMemberAndEveryDayUsage("daisy", aMonthAgo)
                .withMemberAndEveryDayUsage("mike", aMonthAgo);
        Carpool carpool = builder.create();

        Member m1 = builder.getMember("john");
        Member m2 = builder.getMember("daisy");
        Member m3 = builder.getMember("mike");

        List<Turn> turns = carpool.calculateTurnsUntil(today);
        assertThat(turns.get(0), withDriverCount(m1, 10));
        assertThat(turns.get(0), withDriverCount(m2, 10));
        assertThat(turns.get(0), withDriverCount(m3, 10));

        carpool.defineExceptionalUsage(m1, new Interval(aMonthAgo, aMonthAgo.plusDays(9)), Usage.HAS_TO_DRIVE);
        turns = carpool.getTurnsInThePast();
        assertThat(turns.get(0), withDriverCount(m1, 16));
        assertThat(turns.get(0), withDriverCount(m2, 7));
        assertThat(turns.get(0), withDriverCount(m3, 7));

        carpool.defineExceptionalUsage(m2, new Interval(aMonthAgo.plusDays(10), aMonthAgo.plusDays(19)), Usage.HAS_TO_DRIVE);
        turns = carpool.getTurnsInThePast();
        assertThat(turns.get(0), withDriverCount(m1, 13));
        assertThat(turns.get(0), withDriverCount(m2, 14));
        assertThat(turns.get(0), withDriverCount(m3, 3));

        carpool.defineExceptionalUsage(m3, new Interval(aMonthAgo.plusDays(20), today), Usage.HAS_TO_DRIVE);
        turns = carpool.getTurnsInThePast();
        assertThat(turns.get(0), withDriverCount(m1, 10));
        assertThat(turns.get(0), withDriverCount(m2, 10));
        assertThat(turns.get(0), withDriverCount(m3, 10));
    }

    @Test
    public void shouldRecalculateMultipleTimes_RevertToNormalUsage() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime aMonthAgo = today.minusDays(29);

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john", aMonthAgo)
                .withMemberAndEveryDayUsage("daisy", aMonthAgo)
                .withMemberAndEveryDayUsage("mike", aMonthAgo);
        Carpool carpool = builder.create();

        Member m1 = builder.getMember("john");
        Member m2 = builder.getMember("daisy");
        Member m3 = builder.getMember("mike");

        List<Turn> turns = carpool.calculateTurnsUntil(today);
        assertThat(turns.get(0), withDriverCount(m1, 10));
        assertThat(turns.get(0), withDriverCount(m2, 10));
        assertThat(turns.get(0), withDriverCount(m3, 10));

        carpool.defineExceptionalUsage(m1, new Interval(aMonthAgo, aMonthAgo.plusDays(9)), Usage.HAS_TO_DRIVE);
        turns = carpool.getTurnsInThePast();
        assertThat(turns.get(0), withDriverCount(m1, 16));
        assertThat(turns.get(0), withDriverCount(m2, 7));
        assertThat(turns.get(0), withDriverCount(m3, 7));

        carpool.defineExceptionalUsage(m1, new Interval(aMonthAgo, aMonthAgo.plusDays(9)), Usage.NORMAL);
        turns = carpool.getTurnsInThePast();
        assertThat(turns.get(0), withDriverCount(m1, 10));
        assertThat(turns.get(0), withDriverCount(m2, 10));
        assertThat(turns.get(0), withDriverCount(m3, 10));
    }

    @Test
    public void shouldReturnLastUsedDate_MemberFirstUsageInThePast() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john", today.minusDays(10))
                .withMemberAndWeekdayUsage("john", today.minusDays(5))
                .withMemberAndEveryDayUsage("daisy")
                .create();

        assertThat(carpool.getLastUsedDate(), is(today.minusDays(11)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotDefineExceptionalRule_MemberNotPartOfCarpool() {
        Member m = new Member(null);

        Carpool carpool = new Carpool("My Carpool");
        carpool.defineExceptionalUsage(m, DateTime.now(), Usage.HAS_TO_DRIVE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotDefineExceptionalRule_MemberHasOverlappingExceptionalRule() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        Member m = new Member(null);

        Carpool carpool = new Carpool("My Carpool");
        carpool.add(m);
        carpool.defineExceptionalUsage(m, today, Usage.CAN_NOT_DRIVE);
        carpool.defineExceptionalUsage(m, new Interval(today, today.plusDays(2)), Usage.HAS_TO_DRIVE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotDefineExceptionalUsage_InvalidUsage() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("Carpool")
                .withMember("john")
                .withMember("mike");

        Carpool carpool = builder.create();
        Member john = builder.getMember("john");

        carpool.defineExceptionalUsage(john, today, Usage.CAN_NOT_DRIVE);
    }

    @Test
    public void shouldDefineExceptionalRule_InTheFuture_SingleDate() {
        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMember("john")
                .withMember("daisy");
        Carpool carpool = builder.create();

        Member m1 = builder.getMember("john");
        carpool.defineExceptionalUsage(m1, DateTime.now(), Usage.HAS_TO_DRIVE);

        List<Turn> turns = carpool.predictTurnsUntil(DateTime.now().plusDays(1));
        assertThat(turns, hasDrivers(m1, (Member) null));
    }

    @Test
    public void shouldDefineExceptionalRule_InTheFuture_Interval() {
        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMember("john")
                .withMember("daisy");
        Carpool carpool = builder.create();

        Member m1 = builder.getMember("john");
        Interval interval = new Interval(DateTime.now(), DateTime.now().plusDays(6));
        carpool.defineExceptionalUsage(m1, interval, Usage.HAS_TO_DRIVE);

        List<Turn> turns = carpool.predictTurnsUntil(DateTime.now().plusDays(7));
        assertThat(turns, hasDrivers(m1, m1, m1, m1, m1, m1, m1, null));
    }

    @Test
    public void shouldDefineExceptionalRule_InThePast_SingleDate() {
        DateTime yesterday = DateTime.now().minusDays(1);

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john", yesterday.minusDays(1))
                .withMemberAndEveryDayUsage("daisy", yesterday.minusDays(1));
        Carpool carpool = builder.create();

        Member m1 = builder.getMember("john");
        Member m2 = builder.getMember("daisy");

        carpool.calculateTurnsUntil(yesterday);

        List<Turn> turns = carpool.getTurnsInThePast();
        assertThat(turns, hasDrivers(m2, m1));

        carpool.defineExceptionalUsage(m2, yesterday, Usage.HAS_TO_DRIVE);

        turns = carpool.getTurnsInThePast();
        assertThat(turns, hasDrivers(m2, m2));
    }

    @Test
    public void shouldDefineExceptionalRule_InThePast_Interval() {
        DateTime yesterday = DateTime.now().minusDays(1);
        DateTime aWeekAgo = DateTime.now().minusDays(7);

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john", aWeekAgo)
                .withMemberAndEveryDayUsage("daisy", aWeekAgo);
        Carpool carpool = builder.create();

        Member m1 = builder.getMember("john");
        Member m2 = builder.getMember("daisy");

        carpool.calculateTurnsUntil(yesterday);

        List<Turn> turns = carpool.getTurnsInThePast();
        assertThat(turns, hasDrivers(m2, m1, m2, m1, m2, m1, m2));

        Interval interval = new Interval(aWeekAgo, yesterday);
        carpool.defineExceptionalUsage(m1, interval, Usage.CAN_NOT_DRIVE);

        turns = carpool.getTurnsInThePast();
        assertThat(turns, hasDrivers(m2, m2, m2, m2, m2, m2, m2));
    }

    @Test
    public void shouldDefineExceptionalRule_InThePastAndInTheFuture_Interval_StartAfterFirstTurnDate() {
        DateTime yesterday = DateTime.now().minusDays(1);
        DateTime aWeekAgo = DateTime.now().minusDays(7);
        DateTime inAWeek = DateTime.now().plusDays(7);

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john", aWeekAgo)
                .withMemberAndEveryDayUsage("daisy", aWeekAgo);
        Carpool carpool = builder.create();

        Member m1 = builder.getMember("john");
        Member m2 = builder.getMember("daisy");

        carpool.calculateTurnsUntil(yesterday);

        List<Turn> turns = carpool.getTurnsInThePast();
        assertThat(turns, hasDrivers(m2, m1, m2, m1, m2, m1, m2));
        turns = carpool.predictTurnsUntil(inAWeek);
        assertThat(turns, hasDrivers(m1, m2, m1, m2, m1, m2, m1, m2));

        Interval interval = new Interval(aWeekAgo, inAWeek);
        carpool.defineExceptionalUsage(m1, interval, Usage.CAN_NOT_DRIVE);

        turns = carpool.getTurnsInThePast();
        assertThat(turns, hasDrivers(m2, m2, m2, m2, m2, m2, m2));
        turns = carpool.predictTurnsUntil(inAWeek);
        assertThat(turns, hasDrivers(m2, m2, m2, m2, m2, m2, m2, m2));
    }

    @Test
    public void shouldDefineExceptionalRule_InThePastAndInTheFuture_Interval_StartBeforeFirstTurnDate() {
        DateTime twoDaysAgo = DateTime.now().minusDays(2);
        DateTime yesterday = DateTime.now().minusDays(1);
        DateTime aWeekAgo = DateTime.now().minusDays(7);
        DateTime inAWeek = DateTime.now().plusDays(7);

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john", twoDaysAgo)
                .withMemberAndEveryDayUsage("daisy", twoDaysAgo);
        Carpool carpool = builder.create();

        Member m1 = builder.getMember("john");
        Member m2 = builder.getMember("daisy");

        carpool.calculateTurnsUntil(yesterday);

        List<Turn> turns = carpool.getTurnsInThePast();
        assertThat(turns, hasDrivers(m2, m1));
        turns = carpool.predictTurnsUntil(inAWeek);
        assertThat(turns, hasDrivers(m2, m1, m2, m1, m2, m1, m2, m1));

        Interval interval = new Interval(aWeekAgo, inAWeek);
        carpool.defineExceptionalUsage(m1, interval, Usage.CAN_NOT_DRIVE);

        turns = carpool.getTurnsInThePast();
        assertThat(turns, hasDrivers(m2, m2));
        turns = carpool.predictTurnsUntil(inAWeek);
        assertThat(turns, hasDrivers(m2, m2, m2, m2, m2, m2, m2, m2));
    }

    @Test
    public void shouldDefineExceptionalRule_InThePast_RevertDriverCounts() {
        DateTime yesterday = DateTime.now().minusDays(1);
        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john", yesterday.minusDays(1))
                .withMemberAndEveryDayUsage("daisy", yesterday.minusDays(1));
        Carpool carpool = builder.create();

        Member m1 = builder.getMember("john");
        Member m2 = builder.getMember("daisy");

        carpool.calculateTurnsUntil(yesterday);

        List<Turn> turns = carpool.getTurnsInThePast();
        assertThat(turns, hasDrivers(m2, m1));

        carpool.defineExceptionalUsage(m2, yesterday, Usage.HAS_TO_DRIVE);

        turns = carpool.getTurnsInThePast();
        assertThat(turns, hasDrivers(m2, m2));

        assertThat(turns.get(0), withDriverCount(m1, 0));
        assertThat(turns.get(0), withDriverCount(m2, 2));
    }

    @Test
    public void shouldOverrideExceptionalRule_InTheFuture_SingleDate() {
        DateTime tomorrow = DateTime.now().plusDays(1);

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy");
        Carpool carpool = builder.create();

        Member m1 = builder.getMember("john");
        Member m2 = builder.getMember("daisy");

        carpool.defineExceptionalUsage(m1, tomorrow, Usage.CAN_NOT_DRIVE);

        List<Turn> turns = carpool.predictTurnsUntil(tomorrow);
        assertThat(turns, hasDrivers(m2, m2));

        carpool.defineExceptionalUsage(m1, tomorrow, Usage.HAS_TO_DRIVE);
        turns = carpool.predictTurnsUntil(tomorrow);
        assertThat(turns, hasDrivers(m2, m1));
    }

    @Test
    public void shouldOverrideExceptionalRule_InThePast_SingleDate() {
        DateTime yesterday = DateTime.now().minusDays(1);
        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john", yesterday.minusDays(1))
                .withMemberAndEveryDayUsage("daisy", yesterday.minusDays(1));
        Carpool carpool = builder.create();

        Member m1 = builder.getMember("john");
        Member m2 = builder.getMember("daisy");

        carpool.defineExceptionalUsage(m1, yesterday, Usage.CAN_NOT_DRIVE);

        List<Turn> turns = carpool.calculateTurnsUntil(yesterday);
        assertThat(turns, hasDrivers(m2, m2));

        carpool.defineExceptionalUsage(m1, yesterday, Usage.HAS_TO_DRIVE);
        turns = carpool.getTurnsInThePast();
        assertThat(turns, hasDrivers(m2, m1));

        assertThat(turns.get(0), withDriverCount(m1, 1));
        assertThat(turns.get(0), withDriverCount(m2, 1));
    }

    @Test
    public void shouldOverrideExceptionalRule_InTheFuture_Interval() {
        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy");
        Carpool carpool = builder.create();

        Member m1 = builder.getMember("john");
        Member m2 = builder.getMember("daisy");

        Interval nextWeek = new Interval(DateTime.now(), DateTime.now().plusDays(6));
        carpool.defineExceptionalUsage(m1, nextWeek, Usage.CAN_NOT_DRIVE);

        List<Turn> turns = carpool.predictTurnsUntil(nextWeek.getEnd());
        assertThat(turns, hasDrivers(m2, m2, m2, m2, m2, m2, m2));

        carpool.defineExceptionalUsage(m1, nextWeek, Usage.HAS_TO_DRIVE);

        turns = carpool.predictTurnsUntil(nextWeek.getEnd());
        assertThat(turns, hasDrivers(m1, m1, m1, m1, m1, m1, m1));
    }

    @Test
    public void shouldOverrideExceptionalRule_InThePast_Interval() {
        Interval aWeekAgo = new Interval(DateTime.now().minusDays(6), DateTime.now());
        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john", aWeekAgo.getStart())
                .withMemberAndEveryDayUsage("daisy", aWeekAgo.getStart());
        Carpool carpool = builder.create();

        Member m1 = builder.getMember("john");
        Member m2 = builder.getMember("daisy");

        carpool.defineExceptionalUsage(m1, aWeekAgo, Usage.CAN_NOT_DRIVE);

        List<Turn> turns = carpool.calculateTurnsUntil(aWeekAgo.getEnd());
        assertThat(turns, hasDrivers(m2, m2, m2, m2, m2, m2, m2));
        assertThat(turns.get(0), withDriverCount(m1, 0));
        assertThat(turns.get(0), withDriverCount(m2, 7));

        carpool.defineExceptionalUsage(m1, aWeekAgo, Usage.HAS_TO_DRIVE);

        turns = carpool.getTurnsInThePast();
        assertThat(turns, hasDrivers(m1, m1, m1, m1, m1, m1, m1));

        assertThat(turns.get(0), withDriverCount(m1, 7));
        assertThat(turns.get(0), withDriverCount(m2, 0));
    }

    @Test
    public void shouldNotArchiveTurns_NoTurnsOlderThanTwoMonths() {
        DateTime monday = DateTime.now().withTimeAtStartOfDay().withDayOfWeek(1);
        DateTime sixWeeksAgo = monday.minusWeeks(6);

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndWeekdayUsage("john", sixWeeksAgo)
                .withMemberAndWeekdayUsage("daisy", sixWeeksAgo)
                .withMemberAndWeekdayUsage("mike", sixWeeksAgo)
                .withCalculationDate(monday.minusDays(1));
        Carpool carpool = builder.create();

        CarpoolArchive archive = carpool.archiveDataOlderThan(monday.minusMonths(2));
        assertThat(archive, is(nullValue()));
    }

    @Test
    public void shouldArchiveTurnsOlderThanTwoMonths_EveryDayUsage() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime fiveMonthsAgo = today.minusDays(150);
        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john", fiveMonthsAgo)
                .withMemberAndEveryDayUsage("daisy", fiveMonthsAgo)
                .withMemberAndEveryDayUsage("mike", fiveMonthsAgo)
                .withCalculationDate(today.minusDays(1));
        Carpool carpool = builder.create();

        Member m1 = builder.getMember("john");
        Member m2 = builder.getMember("daisy");
        Member m3 = builder.getMember("mike");

        DateTime twoMonthsAgo = today.minusDays(60);
        CarpoolArchive archive = carpool.archiveDataOlderThan(twoMonthsAgo);

        Duration duration = new Duration(fiveMonthsAgo, twoMonthsAgo.plusDays(1));
        ImmutableList<Turn> turns = archive.getTurns();
        assertThat(turns, hasSize(90));
        assertThat(turns.get(0), withDriverCount(m1, 30));
        assertThat(turns.get(0), withDriverCount(m2, 30));
        assertThat(turns.get(0), withDriverCount(m3, 30));
        assertThat(archive, containsMembers(m1, m2, m3));

        duration = new Duration(twoMonthsAgo, today);
        turns = carpool.getTurnsInThePast();
        assertThat(turns, hasSize((int) duration.getStandardDays()));
        assertThat(turns.get(0), withDriverCount(m1, 50));
        assertThat(turns.get(0), withDriverCount(m2, 50));
        assertThat(turns.get(0), withDriverCount(m3, 50));
    }

    @Test
    public void shouldArchiveTurnsOlderThanThreeWeeks_WeekDayUsage() {
        DateTime monday = DateTime.now().withTimeAtStartOfDay().withDayOfWeek(1);
        DateTime sixWeeksAgo = monday.minusWeeks(6);

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndWeekdayUsage("john", sixWeeksAgo)
                .withMemberAndWeekdayUsage("daisy", sixWeeksAgo)
                .withMemberAndWeekdayUsage("mike", sixWeeksAgo)
                .withCalculationDate(monday.minusDays(1));
        Carpool carpool = builder.create();

        Member m1 = builder.getMember("john");
        Member m2 = builder.getMember("daisy");
        Member m3 = builder.getMember("mike");

        DateTime threeWeeksAgo = monday.minusWeeks(3);
        CarpoolArchive archive = carpool.archiveDataOlderThan(threeWeeksAgo);

        ImmutableList<Turn> turns = archive.getTurns();
        assertThat("Turns for three weeks must be archived", turns, hasSize(7 * 3));
        assertThat(turns.get(0), withDriverCount(m1, 5));
        assertThat(turns.get(0), withDriverCount(m2, 5));
        assertThat(turns.get(0), withDriverCount(m3, 5));
        assertThat(archive, containsMembers(m1, m2, m3));

        turns = carpool.getTurnsInThePast();
        assertThat("Turns for three weeks must be left over as turns in the past", turns, hasSize(7 * 3));
        assertThat(turns.get(0), withDriverCount(m1, 10));
        assertThat(turns.get(0), withDriverCount(m2, 10));
        assertThat(turns.get(0), withDriverCount(m3, 10));
    }

    @Test
    public void shouldHaveTurnsBeforeDate() {
        DateTime monday = DateTime.now().withTimeAtStartOfDay().withDayOfWeek(1);
        DateTime aWeekAgo = monday.minusWeeks(1);

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndWeekdayUsage("john", aWeekAgo)
                .withMemberAndWeekdayUsage("daisy", aWeekAgo)
                .withMemberAndWeekdayUsage("mike", aWeekAgo)
                .withCalculationDate(monday.minusDays(1))
                .create();

        assertThat(carpool.hasTurnsBefore(monday), is(true));
    }

    @Test
    public void shouldNotHaveTurnsBeforeDate_DateIsBeforeFirstTurn() {
        DateTime monday = DateTime.now().withTimeAtStartOfDay().withDayOfWeek(1);
        DateTime aWeekAgo = monday.minusWeeks(1);

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndWeekdayUsage("john", aWeekAgo)
                .withMemberAndWeekdayUsage("daisy", aWeekAgo)
                .withMemberAndWeekdayUsage("mike", aWeekAgo)
                .withCalculationDate(monday.minusDays(1))
                .create();

        assertThat(carpool.hasTurnsBefore(aWeekAgo), is(false));
    }


    @Test
    public void shouldNotHaveTurnsBeforeDate_NoTurnsInThePast() {
        DateTime monday = DateTime.now().withTimeAtStartOfDay().withDayOfWeek(1);

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMember("john")
                .withMember("daisy")
                .withMember("mike")
                .create();

        assertThat(carpool.hasTurnsBefore(monday), is(false));
    }

    @Test
    public void shouldHaveTurnUntil_DateMatchesLastTurnDate() {
        DateTime monday = DateTime.now().withTimeAtStartOfDay().withDayOfWeek(1);
        DateTime aWeekAgo = monday.minusWeeks(1);

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndWeekdayUsage("john", aWeekAgo)
                .withMemberAndWeekdayUsage("daisy", aWeekAgo)
                .withMemberAndWeekdayUsage("mike", aWeekAgo)
                .withCalculationDate(monday)
                .create();

        assertThat(carpool.hasTurnsUntil(monday), is(true));
    }

    @Test
    public void shouldHaveTurnUntil_DateBeforeLastTurnDate() {
        DateTime monday = DateTime.now().withTimeAtStartOfDay().withDayOfWeek(1);
        DateTime aWeekAgo = monday.minusWeeks(1);

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndWeekdayUsage("john", aWeekAgo)
                .withMemberAndWeekdayUsage("daisy", aWeekAgo)
                .withMemberAndWeekdayUsage("mike", aWeekAgo)
                .withCalculationDate(monday)
                .create();

        assertThat(carpool.hasTurnsUntil(monday.minusDays(1)), is(true));
    }

    @Test
    public void shouldNotHaveTurnUntil_DateBeforeFirstTurn() {
        DateTime monday = DateTime.now().withTimeAtStartOfDay().withDayOfWeek(1);
        DateTime aWeekAgo = monday.minusWeeks(1);

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndWeekdayUsage("john", aWeekAgo)
                .withMemberAndWeekdayUsage("daisy", aWeekAgo)
                .withMemberAndWeekdayUsage("mike", aWeekAgo)
                .withCalculationDate(monday)
                .create();

        assertThat(carpool.hasTurnsUntil(aWeekAgo.minusDays(1)), is(false));
    }

    @Test
    public void shouldNotHaveTurnUntil_DateAfterLastTurn() {
        DateTime monday = DateTime.now().withTimeAtStartOfDay().withDayOfWeek(1);
        DateTime aWeekAgo = monday.minusWeeks(1);

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndWeekdayUsage("john", aWeekAgo)
                .withMemberAndWeekdayUsage("daisy", aWeekAgo)
                .withMemberAndWeekdayUsage("mike", aWeekAgo)
                .withCalculationDate(monday)
                .create();

        assertThat(carpool.hasTurnsUntil(monday.plusDays(1)), is(false));
    }

    @Test
    public void shouldNotHaveTurnUntil_NoTurns() {
        DateTime monday = DateTime.now().withTimeAtStartOfDay().withDayOfWeek(1);

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMember("john")
                .withMember("daisy")
                .withMember("mike")
                .create();

        assertThat(carpool.hasTurnsUntil(monday), is(false));
    }

    @Test
    public void shouldRestoreArchive() {
        DateTime monday = DateTime.now().withTimeAtStartOfDay().withDayOfWeek(1);
        DateTime aWeekAgo = monday.minusWeeks(1);

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john", aWeekAgo)
                .withMemberAndEveryDayUsage("daisy", aWeekAgo)
                .withMemberAndEveryDayUsage("mike", aWeekAgo)
                .withCalculationDate(monday)
                .create();

        CarpoolArchive archive = carpool.archiveDataOlderThan(monday);
        assertThat(carpool, hasTurnsInThePastFor(monday, monday));

        carpool.restore(archive);
        assertThat(carpool, hasTurnsInThePastFor(aWeekAgo, monday));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotRestoreArchive_CombinationIsNotContainedByCarpool() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndWeekdayUsage("john", today)
                .withMemberAndWeekdayUsage("daisy", today)
                .withCalculationDate(today)
                .create();

        MemberCombination combination = new MemberCombination(
                Arrays.asList(new Member(null), new Member(URI.create("http://tempuri.org#Mike")))
        );
        Turn turn = new Turn(today.minusDays(1));
        CarpoolArchive archive = new CarpoolArchive(
                ImmutableMultimap.<Integer, MemberCombination>builder().put(combination.size(), combination).build(),
                new TreeSet<Turn>(Arrays.asList(turn))
        );

        carpool.restore(archive);
    }


    @Test(expected = IllegalArgumentException.class)
    public void shouldNotRestoreArchive_TurnsAreNotAdjacent() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndWeekdayUsage("john", today)
                .withMemberAndWeekdayUsage("daisy", today)
                .withCalculationDate(today)
                .create();

        Turn turn = new Turn(today.minusWeeks(10));
        CarpoolArchive archive = new CarpoolArchive(
                ImmutableMultimap.<Integer, MemberCombination>builder().build(),
                new TreeSet<Turn>(Arrays.asList(turn))
        );

        carpool.restore(archive);
    }

    @Test
    public void shouldReturnTurnsOfInterval() {
        DateTime monday = DateTime.now().withTimeAtStartOfDay().withDayOfWeek(1);
        DateTime sixWeeksAgo = monday.minusWeeks(6);

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndWeekdayUsage("john", sixWeeksAgo)
                .withMemberAndWeekdayUsage("daisy", sixWeeksAgo)
                .withMemberAndWeekdayUsage("mike", sixWeeksAgo)
                .withCalculationDate(monday)
                .create();

        DateTime mondayAWeekAgo = monday.minusWeeks(1);
        Interval interval = new Interval(mondayAWeekAgo, monday);
        List<Turn> turns = carpool.getTurnsInThePast(interval);
        assertThat(turns, hasSize(7));
        assertThat(turns.get(0).getDate(), is(mondayAWeekAgo));
        assertThat(turns.get(6).getDate(), is(monday.minusDays(1)));
    }

    @Test
    public void shouldReturnRemainingMembers_TurnWithPassengers() {
        DateTime monday = DateTime.now().withTimeAtStartOfDay().withDayOfWeek(1);

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndWeekdayUsage("john", monday)
                .withMemberAndWeekdayUsage("daisy", monday)
                .withMember("mike")
                .withCalculationDate(monday);
        Carpool carpool = builder.create();

        Turn turn = carpool.getTurnsInThePast().get(0);
        List<Member> remainingMembers = turn.getRemainingMembers();

        Member mike = builder.getMember("mike");
        assertThat(remainingMembers, hasSize(1));
        assertThat(remainingMembers, contains(mike));
    }

    @Test
    public void shouldReturnRemainingMembers_EveryOneParticipate() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy")
                .withMemberAndEveryDayUsage("mike")
                .withCalculationDate(today)
                .create();

        Turn turn = carpool.getTurnsInThePast().get(0);
        List<Member> remainingMembers = turn.getRemainingMembers();
        assertThat(remainingMembers, is(empty()));
    }

    @Test
    public void shouldReturnRemainingMembers_NoOneUsesCarpool() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMember("john")
                .withMember("daisy")
                .withMember("mike")
                .withCalculationDate(today)
                .create();

        Turn turn = carpool.getTurnsInThePast().get(0);
        List<Member> remainingMembers = turn.getRemainingMembers();
        assertThat(remainingMembers, hasSize(3));
    }

    @Test
    public void shouldCancelCarpool_DateInTheFuture() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime tomorrow = today.plusDays(1);

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy")
                .withMemberAndEveryDayUsage("mike")
                .withCalculationDate(today)
                .create();

        carpool.cancel(tomorrow);
        List<Turn> turns = carpool.predictTurnsUntil(tomorrow);
        assertThat(carpool.isCanceled(turns.get(0)), is(true));
        assertThat(turns, hasDrivers(null));
    }

    @Test
    public void shouldResumeCarpool_DateInTheFuture() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime tomorrow = today.plusDays(1);

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy")
                .withMemberAndEveryDayUsage("mike")
                .withCalculationDate(today)
                .create();

        carpool.cancel(tomorrow);
        assertThat(carpool.isCanceled(tomorrow), is(true));

        carpool.resume(tomorrow);
        List<Turn> turns = carpool.predictTurnsUntil(tomorrow);
        assertThat(carpool.isCanceled(turns.get(0)), is(false));
    }

    @Test
    public void shouldCancelCarpool_DateInThePast() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime yesterday = today.minusDays(1);
        DateTime twoDaysAgo = yesterday.minusDays(1);

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john", twoDaysAgo)
                .withMemberAndEveryDayUsage("daisy", twoDaysAgo)
                .withMemberAndEveryDayUsage("mike", twoDaysAgo)
                .withCalculationDate(today);

        Carpool carpool = builder.create();
        Member m1 = builder.getMember("john");
        Member m2 = builder.getMember("daisy");
        Member m3 = builder.getMember("mike");

        carpool.cancel(yesterday);
        assertThat(carpool.isCanceled(yesterday), is(true));

        List<Turn> turns = carpool.getTurnsInThePast();
        assertThat(turns.get(0), withDriverCount(m1, 1));
        assertThat(turns.get(0), withDriverCount(m2, 1));
        assertThat(turns.get(0), withDriverCount(m3, 0));
        assertThat(turns, hasDrivers(m1, null, m2));
    }

    @Test
    public void shouldResumeCarpool_DateInThePast() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime yesterday = today.minusDays(1);
        DateTime twoDaysAgo = yesterday.minusDays(1);

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john", twoDaysAgo)
                .withMemberAndEveryDayUsage("daisy", twoDaysAgo)
                .withMemberAndEveryDayUsage("mike", twoDaysAgo)
                .withCalculationDate(today);

        Carpool carpool = builder.create();
        Member m1 = builder.getMember("john");
        Member m2 = builder.getMember("daisy");
        Member m3 = builder.getMember("mike");

        carpool.cancel(yesterday);
        assertThat(carpool.isCanceled(yesterday), is(true));

        carpool.resume(yesterday);
        assertThat(carpool.isCanceled(yesterday), is(false));

        List<Turn> turns = carpool.getTurnsInThePast();
        assertThat(turns.get(0), withDriverCount(m1, 1));
        assertThat(turns.get(0), withDriverCount(m2, 1));
        assertThat(turns.get(0), withDriverCount(m3, 1));
    }

    @Test
    public void shouldNotResumeCarpool_CarpoolIsNotCanceled() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy")
                .withMemberAndEveryDayUsage("mike")
                .withCalculationDate(today)
                .create();

        Turn turnBeforeResume = carpool.getTurnsInThePast().iterator().next();

        carpool.resume(today);
        Turn turnAfterResume = carpool.getTurnsInThePast().iterator().next();
        assertThat(turnAfterResume, is(sameInstance(turnBeforeResume)));
    }

    @Test
    public void shouldNotCancelCarpool_CarpoolIsAlreadyCanceled() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy")
                .withMemberAndEveryDayUsage("mike")
                .withCalculationDate(today)
                .create();

        carpool.cancel(today);
        Turn turnBeforeResume = carpool.getTurnsInThePast().iterator().next();

        carpool.cancel(today);
        Turn turnAfterResume = carpool.getTurnsInThePast().iterator().next();
        assertThat(turnAfterResume, is(sameInstance(turnBeforeResume)));
    }

    @Test
    public void shouldReturnInvalidUsage_NoInvalidUsage() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy")
                .withMemberAndEveryDayUsage("mike")
                .withCalculationDate(today);

        Carpool carpool = builder.create();
        Member john = builder.getMember("john");

        Set<Usage> invalidUsage = carpool.getInvalidUsage(today);
        assertThat(invalidUsage, is(empty()));
    }

    @Test
    public void shouldReturnInvalidUsage_CanNotDrive_NoOneUsesCarpool() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMember("john")
                .withMember("daisy")
                .withMember("mike")
                .withCalculationDate(today);

        Carpool carpool = builder.create();
        Member john = builder.getMember("john");

        Set<Usage> invalidUsage = carpool.getInvalidUsage(today);
        assertThat(invalidUsage, contains(Usage.CAN_NOT_DRIVE));
    }

    @Test
    public void shouldReturnInvalidUsage_CanNotDriver_ForAllMembers() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy")
                .withMemberAndEveryDayUsage("mike")
                .withCalculationDate(today);

        Carpool carpool = builder.create();
        Member john = builder.getMember("john");
        Member daisy = builder.getMember("daisy");
        Member mike = builder.getMember("mike");

        carpool.defineExceptionalUsage(john, today, Usage.CAN_NOT_DRIVE);
        carpool.defineExceptionalUsage(daisy, today, Usage.CAN_NOT_DRIVE);

        Set<Usage> invalidUsage = carpool.getInvalidUsage(today);
        assertThat(invalidUsage, contains(Usage.CAN_NOT_DRIVE));
    }

    @Test
    public void shouldReturnInvalidUsage_HasToDrive_AlreadySomeOneHasToDrive() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy")
                .withMemberAndEveryDayUsage("mike")
                .withCalculationDate(today);

        Carpool carpool = builder.create();
        Member john = builder.getMember("john");
        Member daisy = builder.getMember("daisy");

        carpool.defineExceptionalUsage(john, today, Usage.HAS_TO_DRIVE);

        Set<Usage> invalidUsage = carpool.getInvalidUsage(today);
        assertThat(invalidUsage, contains(Usage.HAS_TO_DRIVE));
    }

    @Test
    public void shouldGetTurnAt_Today() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy")
                .withMemberAndEveryDayUsage("mike")
                .withCalculationDate(today);

        Carpool carpool = builder.create();
        Member john = builder.getMember("john");
        Member daisy = builder.getMember("daisy");
        Member mike = builder.getMember("mike");

        Turn turn = carpool.getTurnAt(today);
        assertThat(turn.getMembers(), contains(daisy, mike, john));
        assertThat(turn.getDate(), is(today));
    }

    @Test
    public void shouldGetTurnAt_Tomorrow() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy")
                .withMemberAndEveryDayUsage("mike")
                .withCalculationDate(today);

        Carpool carpool = builder.create();
        Member john = builder.getMember("john");
        Member daisy = builder.getMember("daisy");
        Member mike = builder.getMember("mike");

        DateTime tomorrow = today.plusDays(1);
        Turn turn = carpool.getTurnAt(tomorrow);

        assertThat(turn.getMembers(), contains(daisy, mike, john));
        assertThat(turn.getDate(), is(tomorrow));
    }

    @Test
    public void shouldGetTurnAt_Yesterday() {
        // move the today into the future
        DateTime today = DateTime.now().withTimeAtStartOfDay().plusDays(10);

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy")
                .withMemberAndEveryDayUsage("mike")
                .withCalculationDate(today);

        Carpool carpool = builder.create();
        Member john = builder.getMember("john");
        Member daisy = builder.getMember("daisy");
        Member mike = builder.getMember("mike");

        DateTime yesterday = today.minusDays(1);
        Turn turn = carpool.getTurnAt(yesterday);
        assertThat(turn.getMembers(), contains(daisy, mike, john));
        assertThat(turn.getDate(), is(yesterday));
    }

    @Test
    public void shouldGetTurnAt_NoTurnAvailable() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy")
                .withMemberAndEveryDayUsage("mike")
                .withCalculationDate(today)
                .create();

        DateTime inThePast = today.minusDays(10);
        Turn turn = carpool.getTurnAt(inThePast);
        assertThat(turn, is(nullValue()));
    }

    @Test
    public void shoudGetTurnAt_Tomorrow_NoPreviousTurns() {
        Carpool carpool = new CarpoolBuilder()
                .withName("My Carpool")
                .withMemberAndEveryDayUsage("john")
                .withMemberAndEveryDayUsage("daisy")
                .withMemberAndEveryDayUsage("mike")
                .create();

        DateTime tomorrow = DateTime.now().withTimeAtStartOfDay().plusDays(1);
        Turn turn = carpool.getTurnAt(tomorrow);
        assertThat(turn.getDate(), is(tomorrow));
    }

    @Test
    public void shouldDetermineSavedMoney_NoTurnsAvailable() {
        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("Carpool")
                .withMember("Mike")
                .withMember("John");

        Member mike = builder.getMember("Mike");
        Carpool carpool = builder.create();

        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Interval interval = new Interval(today.minusYears(1), today);

        Savings savings = carpool.determineSavedMoney(mike, interval);
        assertThat(savings.toBigDecimal(), is(closeTo(new BigDecimal(0), new BigDecimal(0))));
    }

    @Test
    public void shouldDetermineSavedMoney_TurnsOfPreviousMonthAvailable() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime halfAYearAgo = today.minusMonths(6);

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("Carpool")
                .withMemberAndWeekdayUsage("Mike", halfAYearAgo)
                .withMemberAndWeekdayUsage("John", halfAYearAgo)
                .withCalculationDate(today);

        Member mike = builder.getMember("Mike");
        Carpool carpool = builder.create();

        DateTime firstDayOfMonth = today.withDayOfMonth(1);
        Interval interval = new Interval(firstDayOfMonth.minusMonths(1), firstDayOfMonth);

        Savings savings = carpool.determineSavedMoney(mike, interval);
        savings.setDistance(new BigDecimal(50));
        savings.setGasolineConsumption(new BigDecimal(7.5));
        savings.setGasolinePrice(new BigDecimal(1.5));
        assertThat(savings.toBigDecimal(), is(closeTo(new BigDecimal(112.5), new BigDecimal(25))));
    }

    @Test
    public void shouldDetermineSavedMoney_TenTurnsAvailable() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime tenDaysAgo = today.minusDays(9);

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("Carpool")
                .withMemberAndEveryDayUsage("Mike", tenDaysAgo)
                .withMemberAndEveryDayUsage("John", tenDaysAgo)
                .withCalculationDate(today);

        Member mike = builder.getMember("Mike");
        Carpool carpool = builder.create();

        Interval interval = new Interval(tenDaysAgo, today.plusDays(1));

        Savings savings = carpool.determineSavedMoney(mike, interval);
        savings.setDistance(new BigDecimal(50));
        savings.setGasolineConsumption(new BigDecimal(7.5));
        savings.setGasolinePrice(new BigDecimal(1.5));
        assertThat(savings.toBigDecimal(), is(closeTo(new BigDecimal(56.25), new BigDecimal(0))));
    }


    @Test(expected = IllegalArgumentException.class)
    public void shouldNotDetermineSavedMoney_MemberIsNull() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Carpool carpool = new CarpoolBuilder()
                .withName("Carpool")
                .withMemberAndEveryDayUsage("Mike")
                .withMemberAndEveryDayUsage("John")
                .withCalculationDate(today)
                .create();

        Interval interval = new Interval(today.minusDays(20), today);
        carpool.determineSavedMoney(
                null,
                interval
        );
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotDetermineSavedMoney_MemberIsNotPartOfCarpool() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Carpool carpool = new CarpoolBuilder()
                .withName("Carpool")
                .withMemberAndEveryDayUsage("Mike")
                .withMemberAndEveryDayUsage("John")
                .withCalculationDate(today)
                .create();

        Interval interval = new Interval(today.minusDays(20), today);
        carpool.determineSavedMoney(
                new Member(URI.create("http://tempuri.org")),
                interval
        );
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotDetermineSavedMoney_IntervalIsNull() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        Carpool carpool = new CarpoolBuilder()
                .withName("Carpool")
                .withMemberAndEveryDayUsage("Mike")
                .withMemberAndEveryDayUsage("John")
                .withCalculationDate(today)
                .create();

        Member member = carpool.getMembers().iterator().next();

        carpool.determineSavedMoney(
                member,
                null
        );
    }

    @Test
    public void shouldRevertTurns_WithDriverAndPassenger() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime aWeekAgo = today.minusWeeks(1);

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("Carpool")
                .withMemberAndEveryDayUsage("Mike", aWeekAgo)
                .withMemberAndEveryDayUsage("John", aWeekAgo)
                .withCalculationDate(today);
        Carpool carpool = builder.create();
        carpool.calculateTurnsUntil(today);

        carpool.revertTurnsFrom(aWeekAgo);

        assertThat(carpool.getTurnsInThePast(), is(empty()));
        Member mike = builder.getMember("Mike");
        Member john = builder.getMember("John");
        MemberCombination combination = carpool.getMemberCombination(mike, john);
        assertThat(combination, hasDriverCount(mike, 0));
        assertThat(combination, hasDriverCount(john, 0));
    }

    @Test
    public void shouldRevertTurns_WithDriverAndPassenger_TwoWeeks() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime twoWeeksAgo = today.minusWeeks(2);

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("Carpool")
                .withMemberAndEveryDayUsage("Mike", twoWeeksAgo)
                .withMemberAndEveryDayUsage("John", twoWeeksAgo)
                .withCalculationDate(today);
        Carpool carpool = builder.create();
        carpool.calculateTurnsUntil(today);

        carpool.revertTurnsFrom(today.minusWeeks(1));

        assertThat(carpool.getTurnsInThePast(), hasSize(7));
        Member mike = builder.getMember("Mike");
        Member john = builder.getMember("John");
        MemberCombination combination = carpool.getMemberCombination(mike, john);
        assertThat(combination, hasDriverCount(mike, 3));
        assertThat(combination, hasDriverCount(john, 4));
    }

    @Test
    public void shouldRevertTurns_WithoutDriverAndPassenger() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime aWeekAgo = today.minusWeeks(1);

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("Carpool")
                .withMember("Mike")
                .withMember("John")
                .withCalculationDate(today);
        Carpool carpool = builder.create();
        carpool.calculateTurnsUntil(today);

        carpool.revertTurnsFrom(aWeekAgo);

        assertThat(carpool.getTurnsInThePast(), is(empty()));
    }

    @Test
    public void shouldRevertTurns_WithDriverAndWithoutPassenger() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime aWeekAgo = today.minusWeeks(1);

        CarpoolBuilder builder = new CarpoolBuilder()
                .withName("Carpool")
                .withMember("Mike")
                .withMemberAndEveryDayUsage("John", aWeekAgo)
                .withCalculationDate(today);
        Carpool carpool = builder.create();
        carpool.calculateTurnsUntil(today);

        carpool.revertTurnsFrom(aWeekAgo);

        assertThat(carpool.getTurnsInThePast(), is(empty()));
    }
}