/*
 * CarpoolNizer Android App
 * Copyright (C) 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.schrieveslaach.carpoolnizer.ui;

import android.app.Activity;
import android.app.AlarmManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.google.android.material.navigation.NavigationView;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Ignore;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.fakes.RoboMenu;
import org.robolectric.fakes.RoboMenuItem;
import org.robolectric.shadow.api.Shadow;
import org.robolectric.shadows.ShadowAlarmManager;
import org.robolectric.shadows.ShadowPendingIntent;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.text.NumberFormat;
import java.util.Objects;

import androidx.annotation.IdRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ActionMode;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;
import de.schrieveslaach.carpoolnizer.Constants;
import de.schrieveslaach.carpoolnizer.R;
import de.schrieveslaach.carpoolnizer.model.Carpool;
import de.schrieveslaach.carpoolnizer.model.CarpoolBuilder;
import de.schrieveslaach.carpoolnizer.model.Member;
import de.schrieveslaach.carpoolnizer.model.Usage;
import de.schrieveslaach.carpoolnizer.model.settings.UsageNotification;
import de.schrieveslaach.carpoolnizer.repository.CarpoolDAO;
import de.schrieveslaach.carpoolnizer.ui.adapters.TurnViewHolder;
import de.schrieveslaach.carpoolnizer.ui.fragments.CarpoolInformationFragment;
import de.schrieveslaach.carpoolnizer.ui.robolectric.shadows.ShadowNavigationView;
import de.schrieveslaach.carpoolnizer.ui.robolectric.shadows.ShadowRecyclerView;
import lombok.SneakyThrows;

import static de.schrieveslaach.carpoolnizer.Constants.getShortTimeDateFormatter;
import static de.schrieveslaach.carpoolnizer.repository.CarpoolDAOTest.getCarpoolExampleFile;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ActivityMatchers.hasNoSubtitleTitle;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ActivityMatchers.hasTitle;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ActivityMatchers.notStartedActivity;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ActivityMatchers.startedActivity;
import static de.schrieveslaach.carpoolnizer.ui.matchers.IntentMatchers.hasAction;
import static de.schrieveslaach.carpoolnizer.ui.matchers.MenuMatchers.hasMenuTitle;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.gone;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.hasText;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.selected;
import static de.schrieveslaach.carpoolnizer.ui.matchers.ViewMatchers.visible;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.robolectric.RuntimeEnvironment.application;
import static org.robolectric.Shadows.shadowOf;
import static org.robolectric.shadows.ShadowDialog.getLatestDialog;
import static org.robolectric.shadows.ShadowLooper.runUiThreadTasksIncludingDelayedTasks;

@Config(
        sdk = Build.VERSION_CODES.LOLLIPOP,
        shadows = {ShadowNavigationView.class, ShadowRecyclerView.class}
)
public class MainActivityTest extends AbstractRobolectricActivityTester<MainActivity_> {

    public MainActivityTest() {
        super(MainActivity_.class);
    }

    @Test
    public void shouldStartToCreateCarpoolActivity_OnOptionsMenu() {
        pressMenuItem(R.id.mi_create_carpool);

        Intent expectedIntent = new Intent(activity, CarpoolConfigurationActivity_.class);
        assertThat(activity, startedActivity(expectedIntent));
    }

    @Test
    public void shouldNotifyToCreateCarpool_NoCarpoolDefinedOnStart() {
        activity.onStart();
        activity.onCreateOptionsMenu(new RoboMenu(application));

        TextView explanationContent = activity.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_content);
        assertThat(explanationContent, is(notNullValue()));
        assertThat(explanationContent, hasText(R.string.you_should_create_a_carpool));
    }

    @Test
    public void shouldNotNotifyToCreateCarpool_ACarpoolExists() throws Exception {
        CarpoolDAO dao = new CarpoolDAO(application);
        dao.save(new CarpoolBuilder()
                .withName("Name")
                .create(contactsCursor)
        );

        activity.onStart();
        activity.onCreateOptionsMenu(new RoboMenu(application));

        TextView explanationContent = activity.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_content);
        assertThat(explanationContent, is(nullValue()));
    }

    @Test
    public void shouldNotNotifyToCreateCarpool_LastUsedCarpoolDoesNotExistAnymore() throws Exception {
        activity.onStart();
        activity.onCreateOptionsMenu(new RoboMenu(application));
        activity.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_dismiss).performClick();

        // create and save carpool and delete file to simulate the case that the carpool does not
        // exist any more
        CarpoolDAO dao = new CarpoolDAO(application);
        dao.save(new CarpoolBuilder()
                .withName("Name")
                .create(contactsCursor)
        );
        dao.loadCarpool("Name");
        File carpool = new File(application.getFilesDir(), "Name.carpool.json");
        assertThat(carpool.delete(), is(true));

        activity.onStart();
        activity.onCreateOptionsMenu(new RoboMenu(application));

        TextView explanationContent = activity.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_content);
        assertThat(explanationContent, is(nullValue()));
    }

    @Test
    public void shouldNotifyToSwitchAmongCarpools_SecondCarpoolCreated() throws Exception {
        CarpoolDAO dao = new CarpoolDAO(application);
        dao.save(new CarpoolBuilder()
                .withName("First Carpool")
                .create(contactsCursor)
        );
        dao.save(new CarpoolBuilder()
                .withName("Second Carpool")
                .create(contactsCursor)
        );

        Intent data = new Intent();
        data.putExtra(Constants.CARPOOL_NAME, "Second Carpool");
        activity.onActivityResult(Constants.CONFIGURE_CARPOOL_REQUEST, Activity.RESULT_OK, data);

        TextView explanationContent = activity.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_content);
        assertThat(explanationContent, hasText(R.string.explanation_to_switch_among_carpools));
    }

    @Test
    public void shouldNotNotifyToSwitchAmongCarpools_ThirdCarpoolCreated() throws Exception {
        CarpoolDAO dao = new CarpoolDAO(application);
        dao.save(new CarpoolBuilder()
                .withName("First Carpool")
                .create(contactsCursor)
        );
        dao.save(new CarpoolBuilder()
                .withName("Second Carpool")
                .create(contactsCursor)
        );

        Intent data = new Intent();
        data.putExtra(Constants.CARPOOL_NAME, "Second Carpool");
        activity.onActivityResult(Constants.CONFIGURE_CARPOOL_REQUEST, Activity.RESULT_OK, data);
        activity.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_dismiss).performClick();

        dao.save(new CarpoolBuilder()
                .withName("Third Carpool")
                .create(contactsCursor)
        );
        data = new Intent();
        data.putExtra(Constants.CARPOOL_NAME, "Third Carpool");
        activity.onActivityResult(Constants.CONFIGURE_CARPOOL_REQUEST, Activity.RESULT_OK, data);

        TextView explanationContent = activity.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_content);
        assertThat(explanationContent, is(nullValue()));
    }

    @Test
    public void shouldDisplayLastUsedCarpool_LastUsedCarpoolExists() throws Exception {
        CarpoolDAO dao = new CarpoolDAO(application);
        dao.save(new CarpoolBuilder()
                .withName("Name")
                .create(contactsCursor)
        );
        dao.loadCarpool("Name");

        activity.onStart();

        assertThat(activity, hasTitle("Name"));

        RecyclerView rvTurns = activity.findViewById(R.id.rv_turns);
        assertThat(rvTurns.getAdapter().getItemCount(), is(greaterThan(0)));
    }

    @Test
    public void shouldNotDisplayFirstCarpool_LastUsedCarpoolDoesNotExistAnymore() throws Exception {
        CarpoolDAO dao = new CarpoolDAO(application);
        for (int i = 0; i < 3; ++i) {
            dao.save(new CarpoolBuilder()
                    .withName("Name " + i)
                    .create(contactsCursor)
            );
            dao.loadCarpool("Name " + i);
        }
        File carpool = new File(application.getFilesDir(), "Name 2.carpool.json");
        assertThat(carpool.delete(), is(true));

        activity.onStart();

        assertThat(activity, hasTitle("Name 0"));

        RecyclerView rvTurns = activity.findViewById(R.id.rv_turns);
        assertThat(rvTurns.getAdapter().getItemCount(), is(greaterThan(0)));
    }

    @Test
    public void shouldHandleOnActivityResult_CreatedCarpool() throws Exception {
        CarpoolDAO dao = new CarpoolDAO(application);
        dao.save(new CarpoolBuilder()
                .withName("Name")
                .create(contactsCursor)
        );

        Intent data = new Intent();
        data.putExtra(Constants.CARPOOL_NAME, "Name");
        activity.onActivityResult(Constants.CONFIGURE_CARPOOL_REQUEST, Activity.RESULT_OK, data);

        // Carpools (1 element) plus notification settings menu
        assertThat(activity.navigationView.getMenu().size(), is(2));

        assertThat(activity, hasTitle("Name"));

        RecyclerView rvTurns = activity.findViewById(R.id.rv_turns);
        assertThat(rvTurns.getAdapter().getItemCount(), is(greaterThan(0)));
    }

    @Test
    public void shouldShowTurnOverviewExplanation_AfterCreatedCarpool() throws Exception {
        CarpoolDAO dao = new CarpoolDAO(application);
        dao.save(new CarpoolBuilder()
                .withName("Name")
                .create(contactsCursor)
        );

        Intent data = new Intent();
        data.putExtra(Constants.CARPOOL_NAME, "Name");
        activity.onActivityResult(Constants.CONFIGURE_CARPOOL_REQUEST, Activity.RESULT_OK, data);
        runUiThreadTasksIncludingDelayedTasks();

        TextView tvExplanation = activity.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_content);
        assertThat(tvExplanation, is(notNullValue()));
        assertThat(tvExplanation, hasText(R.string.explanation_turn_overview));
    }

    @Test
    public void shouldNotShowTurnOverviewExplanation_AfterCreatedSecondCarpool() throws Exception {
        CarpoolDAO dao = new CarpoolDAO(application);
        dao.save(new CarpoolBuilder()
                .withName("Name")
                .create(contactsCursor)
        );

        Intent data = new Intent();
        data.putExtra(Constants.CARPOOL_NAME, "Name");
        activity.onActivityResult(Constants.CONFIGURE_CARPOOL_REQUEST, Activity.RESULT_OK, data);
        runUiThreadTasksIncludingDelayedTasks();

        // first time
        activity.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_dismiss).performClick();

        dao.save(new CarpoolBuilder()
                .withName("Name 2")
                .create(contactsCursor)
        );

        data = new Intent();
        data.putExtra(Constants.CARPOOL_NAME, "Name 2");
        activity.onActivityResult(Constants.CONFIGURE_CARPOOL_REQUEST, Activity.RESULT_OK, data);

        TextView tvExplanation = activity.findViewById(uk.co.deanwild.materialshowcaseview.R.id.tv_content);
        assertThat(tvExplanation, hasText(R.string.explanation_to_switch_among_carpools));
    }

    @Test
    public void shouldHandleOnActivityResult_DisplayTurnDetails() throws Exception {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        Carpool c = new Carpool("Name");
        Member mike = contactsCursor.createMember("Mike");
        mike.addUsageAt(today, new boolean[]{false, false, false, false, false, false, false});
        c.add(mike);
        Member john = contactsCursor.createMember("John");
        john.addUsageAt(today, new boolean[]{false, false, false, false, false, false, false});
        c.add(john);

        c.defineExceptionalUsage(mike, today, Usage.NORMAL);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(c);

        String turnDate = Constants.getFullDateFormatter(application).print(today);

        Intent data = new Intent();
        data.putExtra(Constants.CARPOOL_NAME, "Name");
        data.putExtra(Constants.TURN_DATE, turnDate);
        activity.onActivityResult(Constants.DISPLAY_TURN_DETAILS, Activity.RESULT_OK, data);

        RecyclerView rvTurns = activity.findViewById(R.id.rv_turns);
        RecyclerView.ViewHolder viewHolder = rvTurns.findViewHolderForAdapterPosition(0);
        TextView tv = viewHolder.itemView.findViewById(R.id.tv_member_name);
        assertThat(tv, hasText("Mike"));

        tv = viewHolder.itemView.findViewById(R.id.tv_turn_date);
        assertThat(tv, hasText(turnDate));
    }

    @Test
    public void shouldSelectCarpool_UseNavigationBar() throws Exception {
        CarpoolDAO dao = new CarpoolDAO(activity);
        Carpool c = new Carpool("Name 1");
        dao.save(c);
        c = new Carpool("Name 2");
        dao.save(c);
        activity.onStart();

        selectCarpool("Name 2");

        assertThat(activity, hasTitle("Name 2"));
    }

    @Test
    public void shouldNotSelectCarpool_UseNavigationBar_InvalidCarpool() throws Exception {
        CarpoolDAO dao = new CarpoolDAO(activity);
        Carpool c = new Carpool("Name 1");
        dao.save(c);
        c = new Carpool("Name 2");
        dao.save(c);

        activity.onStart();

        BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream(activity.getFileStreamPath("Name 2.carpool.json")));
        fos.write(new byte[10]);

        selectCarpool("Name 2");

        AlertDialog dialog = (AlertDialog) getLatestDialog();
        assertThat(dialog, is(notNullValue()));
        // TODO assertThat(dialog.conten(), hasText(containsString("No content to map due to end-of-input")));
    }

    private void selectCarpool(String name) {
        ShadowNavigationView shadow = Shadow.extract(activity.navigationView);
        shadow.selectMenu(name);
    }

    private void selectNavigationMenu(@IdRes int menuId) {
        ShadowNavigationView shadow = Shadow.extract(activity.navigationView);
        shadow.selectMenu(menuId);
    }

    @Test
    public void shouldShowTurnDetails() throws Exception {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMember("Mike")
                .withMemberAndEveryDayUsage("John")
                .withMemberAndEveryDayUsage("Daisy")
                .create(contactsCursor);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        // start to display the copied carpool
        activity.onStart();

        RecyclerView rvTurns = activity.findViewById(R.id.rv_turns);
        RecyclerView.ViewHolder viewHolder = rvTurns.findViewHolderForAdapterPosition(0);
        viewHolder.itemView.performClick();

        Intent expectedIntent = new Intent(activity, TurnDetailsActivity_.class);
        expectedIntent.putExtra(Constants.CARPOOL_NAME, "Name");
        expectedIntent.putExtra(Constants.TURN_DATE, Constants.getFullDateFormatter(application).print(today));

        assertThat(activity, startedActivity(expectedIntent));
    }

    @Test
    public void shouldSelectTurn_ByLongClick() throws Exception {
        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMember("Mike")
                .withMemberAndEveryDayUsage("John")
                .withMemberAndEveryDayUsage("Daisy")
                .create(contactsCursor);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        // start to display the copied carpool
        activity.onStart();

        RecyclerView rvTurns = activity.findViewById(R.id.rv_turns);
        RecyclerView.ViewHolder viewHolder = rvTurns.findViewHolderForAdapterPosition(0);

        viewHolder.itemView.performLongClick();
        assertThat(viewHolder.itemView.findViewById(R.id.member_read_only_view), is(selected()));
    }

    @Test
    public void shouldSelectedAnotherTurn_ByLongClick() throws Exception {
        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMember("Mike")
                .withMemberAndEveryDayUsage("John")
                .withMemberAndEveryDayUsage("Daisy")
                .create(contactsCursor);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        // start to display the copied carpool
        activity.onStart();

        RecyclerView rvTurns = activity.findViewById(R.id.rv_turns);
        RecyclerView.ViewHolder firstViewHolder = rvTurns.findViewHolderForAdapterPosition(0);
        firstViewHolder.itemView.performLongClick();

        RecyclerView.ViewHolder secondViewHolder = rvTurns.findViewHolderForAdapterPosition(1);
        secondViewHolder.itemView.performLongClick();

        assertThat(firstViewHolder.itemView.findViewById(R.id.member_read_only_view), is(not(selected())));
        assertThat(secondViewHolder.itemView.findViewById(R.id.member_read_only_view), is(selected()));
    }

    @Test
    public void shouldCancelTurn_ByLongClickAndActionModeMenu() throws Exception {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMember("Mike")
                .withMemberAndEveryDayUsage("John")
                .withMemberAndEveryDayUsage("Daisy")
                .create(contactsCursor);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        // start to display the copied carpool
        activity.onStart();

        RecyclerView rvTurns = activity.findViewById(R.id.rv_turns);
        RecyclerView.ViewHolder viewHolder = rvTurns.findViewHolderForAdapterPosition(0);

        viewHolder.itemView.performLongClick();
        performActionMode(R.id.mi_cancel_turn);

        carpool = dao.loadCarpool(carpool.getName());
        assertThat(carpool.isCanceled(today), is(true));
    }

    @Test
    public void shouldUndoCancelTurn_ByLongClickAndActionModeMenu() throws Exception {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMember("Mike")
                .withMemberAndEveryDayUsage("John")
                .withMemberAndEveryDayUsage("Daisy")
                .create(contactsCursor);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        // start to display the copied carpool
        activity.onStart();

        RecyclerView rvTurns = activity.findViewById(R.id.rv_turns);
        RecyclerView.ViewHolder viewHolder = rvTurns.findViewHolderForAdapterPosition(0);

        viewHolder.itemView.performLongClick();
        performActionMode(R.id.mi_cancel_turn);

        viewHolder.itemView.performLongClick();
        performActionMode(R.id.mi_cancel_turn);

        carpool = dao.loadCarpool(carpool.getName());
        assertThat(carpool.isCanceled(today), is(false));
    }

    @SneakyThrows
    private void performActionMode(@IdRes int menuId) {
        View menu = activity.findViewById(menuId);
        if (menu != null) {
            menu.performClick();
        } else {
            // Workaround
            Field turnActionModeField = MainActivity.class.getDeclaredField("turnActionMode");
            turnActionModeField.setAccessible(true);
            ActionMode actionMode = (ActionMode) turnActionModeField.get(activity);

            Field callbackField = actionMode.getClass().getDeclaredField("mCallback");
            callbackField.setAccessible(true);
            ActionMode.Callback callback = (ActionMode.Callback) callbackField.get(actionMode);

            callback.onActionItemClicked(mock(ActionMode.class), new RoboMenuItem(menuId));
        }
    }

    @Test
    public void shouldCloseActionMode_AfterSingleClickOnTurn() throws Exception {
        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMember("Mike")
                .withMemberAndEveryDayUsage("John")
                .withMemberAndEveryDayUsage("Daisy")
                .create(contactsCursor);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        activity.onStart();

        RecyclerView rvTurns = activity.findViewById(R.id.rv_turns);
        RecyclerView.ViewHolder viewHolder = rvTurns.findViewHolderForAdapterPosition(0);
        viewHolder.itemView.performLongClick();
        viewHolder.itemView.performClick();

        assertThat(activity.isActionModeActivated(), is(false));
    }

    @Test
    public void shouldLoadCarpoolWithArchivedData_LoadAllArchivesUntilNoArchivesAvailable() throws Exception {
        DateTime today = DateTime.now().withTimeAtStartOfDay();
        DateTime halfYearAgo = today.withDayOfMonth(1).minusMonths(6);
        DateTime threeMonthsAgo = halfYearAgo.plusMonths(3);

        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage("Mike", halfYearAgo)
                .withMemberAndEveryDayUsage("John", halfYearAgo)
                .withMemberAndEveryDayUsage("Daisy", halfYearAgo)
                .withCalculationDate(today)
                .create(contactsCursor);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        activity.onStart();
        RecyclerView rvTurns = activity.findViewById(R.id.rv_turns);
        TurnViewHolder viewHolder = (TurnViewHolder) rvTurns.findViewHolderForAdapterPosition(0);

        for (int i = 0; i < 4; ++i) {
            Button buLoadArchivedTurns = viewHolder.itemView.findViewById(R.id.bu_load_archived_turns);
            assertThat(buLoadArchivedTurns, is(visible()));
            buLoadArchivedTurns.performClick();

            viewHolder = (TurnViewHolder) rvTurns.findViewHolderForAdapterPosition(0);
            assertThat("Archived turn should be display at the first position", viewHolder.getTurn().getDate(), is(threeMonthsAgo.minusMonths(i)));
        }

        viewHolder = (TurnViewHolder) rvTurns.findViewHolderForAdapterPosition(0);
        Button buLoadArchivedTurns = viewHolder.itemView.findViewById(R.id.bu_load_archived_turns);
        assertThat(buLoadArchivedTurns, is(gone()));
    }

    @Test
    public void shouldNotLoadCarpool_InvalidCarpool() throws Exception {
        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage("Mike")
                .withMemberAndEveryDayUsage("John")
                .withMemberAndEveryDayUsage("Daisy")
                .create(contactsCursor);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);
        dao.loadCarpool("Name");

        BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream(activity.getFileStreamPath("Name.carpool.json")));
        fos.write(new byte[10]);

        activity.onStart();

        AlertDialog dialog = (AlertDialog) getLatestDialog();
        assertThat(dialog, is(notNullValue()));
        dialog.show();
        // TODO assertThat(dialog.getv(), hasText(containsString("No content to map due to end-of-input")));
    }

    @Test
    public void shouldDeleteCarpool_UserConfirmed_ClearMainLayout() throws Exception {
        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMember("Mike")
                .withMemberAndEveryDayUsage("John")
                .withMemberAndEveryDayUsage("Daisy")
                .create(contactsCursor);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        activity.onStart();
        pressMenuItem(R.id.mi_delete_carpool);

        DialogFragment fragment = (DialogFragment) activity.getSupportFragmentManager().findFragmentByTag("deleteConfirmation");
        AlertDialog dialog = (AlertDialog) fragment.onCreateDialog(new Bundle());
        dialog.show();
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).performClick();

        assertThat("Carpool should not exists anymore", dao.listCarpoolNames(), is(empty()));
        RecyclerView rvTurns = activity.findViewById(R.id.rv_turns);
        assertThat("No turns displayed", rvTurns.getAdapter().getItemCount(), is(0));

        assertThat("No carpool names displayed", activity.navigationView.getMenu().size(), is(0));

        assertThat(activity, hasTitle(R.string.app_name));
        assertThat(activity, hasNoSubtitleTitle());
    }

    @Test
    public void shouldDeleteCarpool_UserConfirmed_ClearNavigationViewLayout() throws Exception {
        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMember("Mike")
                .withMemberAndEveryDayUsage("John")
                .withMemberAndEveryDayUsage("Daisy")
                .create(contactsCursor);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        activity.onStart();
        pressMenuItem(R.id.mi_delete_carpool);

        DialogFragment fragment = (DialogFragment) activity.getSupportFragmentManager().findFragmentByTag("deleteConfirmation");
        AlertDialog dialog = (AlertDialog) fragment.onCreateDialog(new Bundle());
        dialog.show();
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).performClick();

        MenuItem menuItem = findCarpoolMenuItem("Name");
        assertThat(menuItem, is(nullValue()));
    }

    private MenuItem findCarpoolMenuItem(String carpoolName) {
        NavigationView nv = activity.findViewById(R.id.nav_view);
        MenuItem carpoolsMenuItem = nv.getMenu().getItem(1);
        SubMenu subMenu = carpoolsMenuItem.getSubMenu();
        for (int i = 0; i < subMenu.size(); ++i) {
            MenuItem subMenuItem = subMenu.getItem(i);
            if (Objects.equals(subMenuItem.getTitle().toString(), carpoolName)) {
                return subMenuItem;
            }
        }

        return null;
    }

    @Test
    public void shouldNotDeleteCarpool_UserDidNotConfirm() throws Exception {
        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMember("Mike")
                .withMemberAndEveryDayUsage("John")
                .withMemberAndEveryDayUsage("Daisy")
                .create(contactsCursor);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        activity.onStart();
        pressMenuItem(R.id.mi_delete_carpool);

        DialogFragment fragment = (DialogFragment) activity.getSupportFragmentManager().findFragmentByTag("deleteConfirmation");
        AlertDialog dialog = (AlertDialog) fragment.onCreateDialog(new Bundle());
        dialog.show();
        dialog.getButton(DialogInterface.BUTTON_NEGATIVE).performClick();

        assertThat("Carpool should still exists", dao.listCarpoolNames(), contains("Name"));
    }

    @Test
    public void shouldNotDeleteCarpool_NoAvailableCarpool() {
        activity.onStart();
        pressMenuItem(R.id.mi_delete_carpool);
        DialogFragment fragment = (DialogFragment) activity.getSupportFragmentManager().findFragmentByTag("deleteConfirmation");
        assertThat(fragment, is(nullValue()));
    }

    @Test
    public void shouldOpenContactInformation() throws Exception {
        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMember("Mike")
                .withMemberAndEveryDayUsage("John")
                .create(contactsCursor);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        activity.onStart();

        RecyclerView rvTurns = activity.findViewById(R.id.rv_turns);
        RecyclerView.ViewHolder viewHolder = rvTurns.findViewHolderForAdapterPosition(0);
        viewHolder.itemView.findViewById(R.id.iv_contact_image).performClick();

        Intent expectedIntent = new Intent(Intent.ACTION_VIEW);
        expectedIntent.setData(contactsCursor.createContactUri("John"));

        assertThat(activity, startedActivity(expectedIntent));
    }

    @Test
    public void shouldNotStartEditingCarpool_NoCarpoolLoaded() {
        pressMenuItem(R.id.mi_edit_carpool);
        assertThat(activity, notStartedActivity());
    }

    @Test
    public void shouldStartEditingCarpool() throws Exception {
        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage("Mike")
                .withMemberAndEveryDayUsage("John")
                .create(contactsCursor);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        activity.onStart();

        pressMenuItem(R.id.mi_edit_carpool);

        Intent expectedIntent = new Intent(activity, CarpoolConfigurationActivity_.class);
        expectedIntent.putExtra(Constants.CARPOOL_NAME, "Name");
        assertThat(activity, startedActivity(expectedIntent));
    }

    @Test
    public void shouldReloadCarpool_LastUsedDateIsYesterday() throws Exception {
        ObjectMapper mapper = new ObjectMapper().registerModule(new GuavaModule());
        Carpool carpool = mapper.readValue(getCarpoolExampleFile(), Carpool.class);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        activity.display(carpool, carpool.getLastUsedDate());
        activity.onStart();

        DateTime yesterday = DateTime.now().withTimeAtStartOfDay().minusDays(1);
        carpool = activity.getDisplayedCarpool();
        assertThat(carpool.getLastUsedDate(), is(equalTo(yesterday)));
    }

    @Test
    public void shouldNotReloadCarpool_RestartBefore15Minutes() {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage("Mike")
                .withMemberAndEveryDayUsage("John")
                .withCalculationDate(today)
                .create(contactsCursor);

        activity.onStart();
        activity.display(carpool, today);
        activity.onStart();

        assertThat(activity.getDisplayedCarpool(), is(sameInstance(carpool)));
    }

    @Test
    public void shouldNotReloadCarpool_RestartAfter15Minutes() throws Exception {
        DateTime today = DateTime.now().withTimeAtStartOfDay();

        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage("Mike")
                .withMemberAndEveryDayUsage("John")
                .withCalculationDate(today)
                .create(contactsCursor);

        activity.onStart();
        activity.display(carpool, today);

        setLastStartUpDate(DateTime.now().minusMinutes(30));

        activity.onStart();

        assertThat(activity.getDisplayedCarpool(), is(not(sameInstance(carpool))));
    }

    @Test
    public void shouldNotOpenCarpoolInformation_NoCarpoolIsLoaded() {
        activity.onStart();
        activity.findViewById(R.id.ab_carpool_information).performClick();

        DialogFragment fragment = (DialogFragment) activity.getSupportFragmentManager().findFragmentByTag("carpoolInformation");
        assertThat(fragment, is(nullValue()));
    }

    @Test
    public void shouldOpenCarpoolInformationFragment_CarpoolIsLoaded() {
        startWithTwoMemberCarpoolStartingToday();

        activity.findViewById(R.id.ab_carpool_information).performClick();

        CarpoolInformationFragment fragment = (CarpoolInformationFragment) activity.getSupportFragmentManager()
                .findFragmentByTag("carpoolInformation");
        assertThat(fragment, is(notNullValue()));
        assertThat(fragment.getCarpool(), is(notNullValue()));
    }

    private void startWithTwoMemberCarpoolStartingToday() {
        startWithTwoMemberCarpoolStartingTodayWithNotification(null);
    }

    @SneakyThrows
    private void startWithTwoMemberCarpoolStartingTodayWithNotification(UsageNotification notification) {
        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage(null)
                .withMemberAndEveryDayUsage("John")
                .create(contactsCursor);

        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);

        if (notification != null) {
            dao.saveUsageNotifcation(carpool, notification);
        }

        activity.onStart();
    }

    @Test
    public void shouldOpenCarpoolInformationFragment_CalcualatedTurnDatesOverCoupleOfDays() throws Exception {
        DateTime aWeekAgo = DateTime.now().withTimeAtStartOfDay().minusWeeks(1);

        Carpool carpool = new CarpoolBuilder()
                .withName("Name")
                .withMemberAndEveryDayUsage(null, aWeekAgo)
                .withMemberAndEveryDayUsage("John", aWeekAgo)
                .create(contactsCursor);

        // Simulate activity suspended for a week
        CarpoolDAO dao = new CarpoolDAO(activity);
        dao.save(carpool);
        activity.display(carpool, aWeekAgo);

        setLastStartUpDate(aWeekAgo);

        activity.onStart();
        activity.findViewById(R.id.ab_carpool_information).performClick();

        CarpoolInformationFragment fragment = (CarpoolInformationFragment) activity.getSupportFragmentManager().findFragmentByTag("carpoolInformation");
        EditText et = fragment.getDialog().findViewById(R.id.et_gasoline_price);
        et.setText("1");
        et = fragment.getDialog().findViewById(R.id.et_gasoline_consumption);
        et.setText("1");
        et = fragment.getDialog().findViewById(R.id.et_distance);
        et.setText("1");


        TextView tvSavedMoney = fragment.getDialog().findViewById(R.id.tv_money_put_aside_value);

        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(RuntimeEnvironment.application.getApplicationContext().getResources().getConfiguration().locale);
        double savedMoney = currencyFormat.parse(tvSavedMoney.getText().toString()).doubleValue();
        assertThat(savedMoney, is(greaterThanOrEqualTo(0.0)));
    }

    @Test
    public void shouldShowThatThereIsNoAlarm() {
        startWithTwoMemberCarpoolStartingToday();

        MenuItem menuItem = activity.navigationView.getMenu().findItem(R.id.nav_mi_notification_settings);
        assertThat(menuItem, hasMenuTitle(R.string.no_notification));
    }

    @Test
    public void shouldShowThatThereIsAnAlarm() {
        startWithTwoMemberCarpoolStartingTodayWithNotification(new UsageNotification(8, 30));

        MenuItem menuItem = activity.navigationView.getMenu().findItem(R.id.nav_mi_notification_settings);
        DateTimeFormatter formatter = getShortTimeDateFormatter(application);
        assertThat(menuItem, hasMenuTitle(R.string.notifaction_at, formatter.print(DateTime.now().withHourOfDay(8).withMinuteOfHour(30))));
    }

    @Test
    @Ignore
    public void shouldSetNotificationForCarpool_SaveNotification_CarpoolDOAhasNotification() throws Exception {
        startWithTwoMemberCarpoolStartingToday();

        selectNavigationMenu(R.id.nav_mi_notification_settings);

// TODO       TimePickerDialog dialog = (TimePickerDialog) getLatestDialog();
//        dialog.hour(19).minute(53);
//        dialog.findViewById(ACTION_POSITIVE).performClick();

        CarpoolDAO dao = new CarpoolDAO(application);
        UsageNotification notification = dao.loadUsageNotification(dao.loadCarpool("Name"));
        assertThat(notification.getPeriod().getHours(), is(19));
        assertThat(notification.getPeriod().getMinutes(), is(53));
    }

    @Test
    @Ignore
    public void shouldSetNotificationForCarpool_SaveNotification_AlarmManagerHasPendingIntent() {
        startWithTwoMemberCarpoolStartingToday();

        selectNavigationMenu(R.id.nav_mi_notification_settings);

// TODO       TimePickerDialog dialog = (TimePickerDialog) getLatestDialog();
//        dialog.hour(19).minute(53);
//        dialog.findViewById(ACTION_POSITIVE).performClick();

        AlarmManager alarmManager = (AlarmManager) activity.getSystemService(Context.ALARM_SERVICE);
        ShadowAlarmManager shadowAlarmManager = shadowOf(alarmManager);
        ShadowAlarmManager.ScheduledAlarm scheduledAlarm = shadowAlarmManager.getNextScheduledAlarm();
        ShadowPendingIntent pendingIntent = shadowOf(scheduledAlarm.operation);
        assertThat(pendingIntent.getSavedIntent(), hasAction(Constants.CARPOOL_USAGE_NOTIFICATION));
    }

    @Test
    @Ignore
    public void shouldSetNotificationForCarpool_ChangeMenuTitle() {
        startWithTwoMemberCarpoolStartingToday();

        selectNavigationMenu(R.id.nav_mi_notification_settings);

//        TimePickerDialog dialog = (TimePickerDialog) getLatestDialog();
//        dialog.hour(19).minute(53);
//        dialog.findViewById(ACTION_POSITIVE).performClick();

        MenuItem menuItem = activity.navigationView.getMenu().findItem(R.id.nav_mi_notification_settings);

        DateTimeFormatter formatter = getShortTimeDateFormatter(application);
        assertThat(menuItem, hasMenuTitle(R.string.notifaction_at, formatter.print(DateTime.now().withHourOfDay(19).withMinuteOfHour(53))));
    }

    @Test
    @Ignore
    public void shouldSetNotificationForCarpool_DismissDialog() {
        startWithTwoMemberCarpoolStartingToday();

        selectNavigationMenu(R.id.nav_mi_notification_settings);

//        TimePickerDialog dialog = (TimePickerDialog) getLatestDialog();
//        dialog.hour(19).minute(53);
//        dialog.findViewById(ACTION_POSITIVE).performClick();

//        assertThat(dialog.isShowing(), is(false));
    }

    @Test
    @Ignore
    public void shouldShowNoficationTimeInDialog_CarpoolHasNotification() {
        startWithTwoMemberCarpoolStartingTodayWithNotification(new UsageNotification(11, 22));

        selectNavigationMenu(R.id.nav_mi_notification_settings);
        // TODO TimePickerDialog dialog = (TimePickerDialog) getLatestDialog();

        // TODO assertThat(dialog.getHour(), is(11));
        // TODO assertThat(dialog.getMinute(), is(22));
    }

    @Test
    @Ignore
    public void shouldShowNoficationTimeInDialog_CarpoolHasNoNotification() {
        startWithTwoMemberCarpoolStartingToday();

        selectNavigationMenu(R.id.nav_mi_notification_settings);
        // TODO TimePickerDialog dialog = (TimePickerDialog) getLatestDialog();

        // TODO assertThat(dialog.getHour(), is(7));
        // TODO assertThat(dialog.getMinute(), is(0));
    }

    @Test
    public void shouldOpenCarpool_ByProvidedIntent() throws Exception {
        CarpoolDAO dao = new CarpoolDAO(application);
        dao.save(new CarpoolBuilder()
                .withName("Name 1")
                .withMemberAndEveryDayUsage("Mike")
                .create(contactsCursor)
        );

        dao.save(new CarpoolBuilder()
                .withName("Name 2")
                .withMemberAndEveryDayUsage("Dave")
                .create(contactsCursor)
        );

        Intent intent = new Intent(activity, MainActivity_.class);
        intent.putExtra(Constants.CARPOOL_NAME, "Name 2");
        activity.setIntent(intent);

        activity.onStart();

        assertThat(activity, hasTitle("Name 2"));
    }

    private void setLastStartUpDate(DateTime aWeekAgo) throws NoSuchFieldException, IllegalAccessException {
        Field lastStartUpField = activity.getClass().getSuperclass().getDeclaredField("lastStartUp");
        lastStartUpField.setAccessible(true);
        lastStartUpField.set(activity, aWeekAgo);
    }
}