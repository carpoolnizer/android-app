Feature: Look up savings
  This feature helps the user to determine how many money he saves by using the carpool.

  Scenario Outline: Look up savings
    Given I started the app
    And I opened the carpool <carpool>
    When I open carpool information details
    And I enter <distance> in km
    And I enter <price> as gasoline price
    And I enter <liter> as gasoline consumption
    Then I should see <money_put_aside> for the last month

  Examples:
    | carpool             | distance | price | liter | money_put_aside |
    | A long term carpool | 50       | 1.50  | 8     | 0-200           |
